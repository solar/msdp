c   Programme dme1.f - UNIX
c**********************************************************************
      subroutine dme1(iqp,nxmtot,npo,dnap,iue,     iuj,jname,jnameref,
c                ----  etiq            in  e(q ou p)out       inutil 
     1       qname,nvel,lineref,sundec, lbda,dlbd,milsec,ttab,tab1,tab2,
c              **      inutil    in            
     2               iitab,jjtab,jje,dxana,dyana,sana,kana,kanam,nqs,
c                                    inutilises            in
     3     ncx,icutcor,kind,kdisc,lmh,jt100)     !   m2403
c                                 nombre arrays,lu head(iud)
c vmoy lu  dans fichier l, m/s
c le jname sert pour
c     lire le fichier j  si lcor.lt.0  (-lcor est le tableau de reference)
c     creer le fichier j si lcor=0 (recouvrements a partir de pixstep)
c                        ou lcor.gt.0 (correl a partir du tableau lcor)
c le dessin (si kana=kanam) continue le q....ps
c            le plot de ond (2 derniers tableaux)
c            est toujours recale sur les "tabmoy" correspondants

c                              iud  iue     iuj
c les fichiers ouverts seront   51   52      53
c                             les d  q ou p   j
      double precision sana(6),vmoypro
      integer*2 tabint(1000,1000)
      dimension corm(150),kedge(150),ond(2000,200,3), !cr(10)
     1          xcor(150),ycor(150),jan(150),jbn(150),jcn(150),jdn(150),
     2          jtan(150)
      dimension poids(2000,200,3),tabmoy(90),
     1       sigma(60),sig(200,60,6),sigmoy(60,6),dxana(6),dyana(6),
     2       ll(60)
      integer*4 head(512),qhead(512),i1(150),jr(150),   !nrej(90),
     1          dlb(60),lold(60),kind(60)
      integer*4 pixstep,pixstepp,corstep,dlbd,priscan,fixscat,
     1          center,sum,ncx
      integer icor(150),jcor(150),burst,sundec
      integer*2 lec(2048),
     1          ttab(iitab,jje),tab1(iitab,jjtab),tab2(iitab,jjtab),
     2          kdisc(iitab,jje),tab(iitab,jje)   ! tab(1500,1500)

      character*22 dname,jname,jnameref,dnanfp(500,2),qname,
     1             dnap(500,2)
      dimension jlimb(2000)
      write(3,*)' msdp4.f 43  jt100=',jt100    !  m2403
c      character*1 let1,let2
c      dimension xdes(2),ydes(2)
c      integer cmoy(10,2,60),fac100,cented,sumd,sumr
c      character*22 ename,pname,name,charact

c      print *,'etape0'!!!!!!!!!

       print *,' etape q/p   nxmtot=',nxmtot
c      print *,'jname :',jname !!!!!!!!!!!
c       print *,'dmedat : ',dmedat
c      jname=dname !!!!!!!!!!
c      write(jname(1:1),'(a)')'j' !!!!!!!!
c                  voir plus loin lineref
c                     pour le cas de plusieurs raies simeltanees
c                     reference correlations
    
c      
c attention 150 cliches maximum (?)
c          170 lignes maximum par tableau
c         1024 pixels maximum par ligne
c         4000 lignes maximum dans l'image finale
      jsmax=jje
 1    format(' Too many pixels in the scans:',
     1       ' increase milsec or reduce ntmax')

c
c        iminus = intervalles en i elimines en debut et fin    inutilise  
c        jminus = intervalles en j elimines en debut et fin    inutilise  
c        jlap2 = demi-intervalle pour reduction du recouvrement
c                     (cas des grands delta lambdas non couverts dans tout le
c                      champ)
c              inutilise
c        lcor = indice du tableau servant de reference pour les correlations
c             si lcor = -1, on lit un nom de fichier "correlations" print  J
c                            de la raie lineref
c             si lcor = 0, on utilise pixstep sans calcul de correlations
c             si lcor.gt.0, on calcule pour chaque ny
c                         a partir des tableaux l=lcor,npo=nporef
c        jrsom # jrmoyen .le. 170 = recouvrement sur (1,jm) # jm-pixstep
c        ijcormax .le. 50
c        milcor = 1000 * correlation minimum admissible
c        corstep=pas pour les differences premieres en i dans calcul correl
c               =pas pour l'integration
c        jdel=   pas pour les differences premieres en j dans calcul correl
c              (pas 1 en j pour l'integration)
c     milsig  moyennes et
c             corrections calculees a partir pts < moy+/-milsig*sigma/1000
c       pas de calcul de corrections si moins de 3 images
c     les corrections sont lissees sur im/10
c     corrections des defauts d'interpolation: icorrec=1
c                                  les moy sont calculees sur les pts dont 
c                                        l'int est > suplec/4
c                                       (suplec=max 1-51/im-50,im)
c         (vitesses:   ll=-1 tab=vit-moy     ??????)
c      icorrec=-1 pas de correction d'interpolation ni de moyennes
c              0   pas de correction des defauts d'interpolation
c                    mais dessin des corrections repliees
c              1   correction avec repliement en fonction de trj
c              2   correction sans repliement
c              3   histogrammes
c              4   correctiopns lineaires recouvrements (voir param.txt)
c     iplot =1  plots de controle pour 2 dernieres cartes
c                   
c---------     
c      write(3,*)' avant pi= , ncx ',ncx
      call par1('  iqcut1',1,iqcut1)   
      call par1('  ifield',1,ifield)
      call par1('  jqcut1',1,jqcut1)
      call par1('  jqcut2',1,jqcut2)

      call par1('  nmlimb',1,nmlimb)
      call par1('   llimb',1,llimb)
      call par1('    lbis',1,lbis)
      call par1('  hchrom',1,khchrom)
      call par1('   jmaxs',1,jmaxs)

      call par1(' iminpro',1,iminpro)
      call par1('  minint',1,minint)
      call par1('  maxint',1,maxint)
      call par1('  minmax',1,minmax)
      call par1(' mcorrec',1,mcorrec)
      call par1('  inverl',1,inverl) ! inversion lambdas
c      call par1('    lint',1,lint) ! indice tableau intensites pour protus
     
      call par1('   xsky1',1,kpiv)  ! intervalle ciel lumière diffusée
                    xsky1=kpiv
      call par1('   xsky2',1,kpiv)
                    xsky2=kpiv
      call par1('  milsec',1,milsec) ! 1/1000 pixel par arcsec
        iqcut1=float(iqcut1)*1000./float(milsec)+0.5
        ifield=float(ifield)*1000./float(milsec)+0.5
        jqcut1=float(jqcut1)*1000./float(milsec)+0.5
        jqcut2=float(jqcut2)*1000./float(milsec)+0.5
        jhchrom=float(khchrom)*1000./float(milsec)+0.5
        jmax=float(jmaxs)*1000./float(milsec)+1.5
          if(jmaxs.eq.0)jmax=0
      pi=3.1415927
      iplot=0
      if(kana.eq.kanam)iplot=1

      call par1(' icentre',1,icentre)
      call par1(' mindisc',1,mindisc)
      call par1(' milsigc',1,milsigc)
      call par1(' minprot',1,minprot)

      call par1('  minima',1,minimages_crec)
c      minimages_crec=5  ! pas de correction petits scans 

6     format(2(5x,a))
9     format(' ltrj=',i6)
c      print *,'dans dme step1' !!!!!!!!!!!!!!!!!
c obs
      call par1(' priscan',1,priscan)
      call par1('   burst',1,burst)
      call par1('    invi',1,invi)
      call par1('    invj',1,invj)
      call par1('   jypas',1,jystep)
                             jystep=iabs(jystep)
      if(burst.eq.0)burst=1
c----- 
      call par1('     jy1',1,jy1)
      call par1('     jy2',1,jy2)
      call par1('    jyq1',1,jyq1)
      call par1('    jyq2',1,jyq2)
c      jyq1=jy1
c      jyq2=jy2
c      call par1('  inverj',1,inverj)
c        if(jyq2.eq.0)then
c            jyq1=0
c            jyq2=jy2-jy1
            
c        else
c          if(inverj.eq.0)then
c    d/c                      0   jy1     jyq1      jyq2      jy2      lj
c    q/d                          0       jyq1-jy1  jyq2-jy1  jy2-jy1
c          jyq1=jyq1-jy1
c          jyq2=jyq2-jy1
c          else
c    d/c                      0   lj-jy2  lj-jyq2   lj-jyq1   lj-jy1   lj
c    q/d                          0       jy2-jyq2  jy2-jyq1  jy2-jy1        
c             kpiv1=jyq1
c             kpiv2=jyq2   
c          jyq1=jy2-kpiv2
c          jyq2=jy2-kpiv1
c          endif
c        endif

c----                                      calcul dans dme1
      j1=1+float(jy1)/float(milsec)+0.5
      j2=1+float(jy2)/float(milsec)+0.5
      jq1=1+float(jyq1)/float(milsec)+0.5
      jq2=1+float(jyq2)/float(milsec)+0.5
c      jqm=jq2-jq1+1      dimension maximale de q*
c                         (dimension ramenee a js suivant correlations) 

      call par1('  inverj',1,inverj)

c             c*   (1)    j1                                j2    (jm)
c                                   jq1          jq2     
c   inverj=0  d*           1                                jm
c                          1   1+jq1-j1     1+jq2-j1   j2-j1+1
c             q*                      1          jqm
c                                    ja           jd
c
c   inverj=1  d*          jm                                 1
c                    j2-j1+1  jm-jq1+j1    jm-jq2+j1         1
c                              j2-jq1+1     j2-jq2+1 
c             q*                    jqm            1
c                                    jd           ja

      if(inverj.eq.0)then
        ja=1+jq1-j1
        jd=1+jq2-j1
      else
        ja=1-jq2+j2
        jd=1-jq1+j2
      endif        

      call par1('  linref',1,linref)
c                            utilise si lcorrel ou lcorp = -1
      if(iqp.eq.0)then       !        files q
c fix
      call par1('  jlap2q',1,jlap2)
                            
      call par1(' lcorrel',1,lcor)
c     if(lcor.gt.100)lcor=lcor-100
            call par1('    lmpd',1,lmpa)
            call par1('  cented',1,center)
            if (lcor.eq.0.and.center.eq.1)lcor=lmpa+center
            write(3,*)' msdp4 232 lcor= ',lcor
      call par1('  icormq',1,ijcormax)
                             ijcormax=ijcormax/milsec
      call par1('  copasq',1,corstep)
                             corstep=corstep/milsec
      call par1('  milcoq',1,milcor)
      call par1('   decmq',1,maxdecal)
                             maxdecal=maxdecal/milsec
c exe
      call par1('   crecq',1,icorrec)
                             icorrec=10
      call par1(' milsigq',1,milsig)
      call par1('  lcrecq',1,ilissond)
                             ilissond=ilissond/milsec

c  pour calcul lv      indice tableau vitesses, pour sortie j de quick
      call par1('  cented',1,center)
      call par1('    sumd',1,sum)
        lsum=0
        if(sum.ne.0)lsum=1
      call par1('    lmpd',1,lmpa)
      call par1('    lmpd',2,lmpb)
      call par1('    lmpd',3,lmpc)
      lint=center+2*(lmpa+lmpb)+1 ! intensity of first I/V couple
      write(3,*)' lint= ',lint
      call par1('   mgrim',1,mgrim)
      else                                  ! iqpfiles p
c fix
c      call par1('  jlap2p',1,jlap2)
                           
c      call par1('   lcorp',1,lcor)
c        if(lcor.gt.100)lcor=lcor-100
      call par1('  icormp',1,ijcormax)
                             ijcormax=ijcormax/milsec                 
      call par1('  copasp',1,corstep)
                             corstep=corstep/milsec
      call par1('  milcop',1,milcor)
      call par1('   decmp',1,maxdecal)
                             maxdecal=maxdecal/milsec
c exe
c      call par1('   crecp',1,icorrec)
c            icorrec=10                       
c      call par1(' milsigp',1,milsig)
c      call par1('  lcrecp',1,ilissond)
                             ilissond=ilissond/milsec

c  pour calcul lv       pour verification centrage (vitesse nulle)
cc      call par1('  center',1,center)
c      call par1('    sumr',1,sum)
c         lsum=0
c         if(sum.ne.0)lsum=1
c      call par1('    lmpr',1,lmpa)
c      call par1('    lmpr',2,lmpb)
c      call par1('    lmpr',3,lmpc)
c      call par1('   mgrim',1,mgrim)
c      call par1('    nquv',1,nquv)
cc      call par1('    ilog',1,ilog)
      endif                             ! iqp
c------------------------
      jlap2=0

c      if(jlap2.eq.0)then
c        jlap2=100000
c      else
c        jlap2=jlap2/milsec
c      endif
      call par1('  promax',1,kpromax) ! max int protus
      call par1(' milwmax',1,milwmax) 

c        if(mgrim.eq.0.and.nqs.le.6)then
c          ----------     --------
           lm=center+lsum+2*lmpa+2*lmpb+2*lmpc
      call par1(' iminpro',1,iminpro)
           if(iminpro.ne.0)lm=lm+1
c      if(kpromax.ne.0.and.milwmax.ne.0)lm=lm+3
           lv=lm
c           lv=center+lsum+2*lmpa+2*lmpb+2      ! pour vitesses moy
           lvp=lv-1                           ! intensite corresp.
c        else
c           lv=2*(center+lsum+2*lmpa+2*lmpb)+3
c           lvp=lv-2
c           lm=2*(center+lsum+2*lmpa+2*lmpb+2*lmpc)
c        endif

      write(3,*)' dme: lv= ',lv

      pixstepp=jystep/float(milsec)+0.5    !96

c   calcul de lold(lnew) pour replacer les profils 
c                        dans l'ordre des lambdas

c      if(ilog.ne.0)lm=2*(center+lsum+2*lmpa+2*lmpb)  !+2*ilog)

      do l=1,lm
      lold(l)=l
      enddo

c      if(ilog.gt.1)goto10  !ne.0)goto 10 

c---------------------------------------------------- lold inutile 200204
      if(center.ne.0.and.lsum.eq.0.and.lmpa.ne.0)then
        if(mgrim.eq.0)then   !.and.nqs.le.6)then           (oui)
c          ----------     --------
c            if(inverl.eq.0)then
c  ex                        lmpa=4         *
c                            l      1 2 3 4 5 6 7 8 9
c                            lold   9 7 5 3 1 2 4 6 8
          do l=1,lmpa
          lold(l)=2*(lmpa-l)+3
          enddo
          lold(lmpa+1)=1
          do l=lmpa+2,2*lmpa+1
          lold(l)=2*(l-lmpa)-2
          enddo
c            else
c                                   1 2 3 4 5 6 7 8 9        (inverl=1)
c                                   8 6 4 2 1 3 5 7 9
c          do l=1,lmpa
c          lold(l)=2*(lmpa-l)+2
c          enddo
c          lold(lmpa+1)=1
c          do l=lmpa+2,2*lmpa+1
c          lold(l)=2*(l-lmpa)-1
c          enddo
c            endif     !   inverl

        else
c  ex          lmpa=4
c                     Stokes I                    Stokes V
c              l      1  2  3  4  5  6  7  8  9   10 11 12 13 14 15 16 17 18
c              lold  17 13  9  5  1  3  7 11 15   18 14 10  6  2  4  8 12 16
          do l=1,lmpa
          lold(l)=4*(lmpa-l)+5
          enddo
          lold(lmpa+1)=1
          do l=lmpa+2,2*lmpa+1
          lold(l)=4*(l-lmpa)-5
          enddo

          do l=2*lmpa+2,3*lmpa+1
          lold(l)=4*(3*lmpa-l)+10
          enddo
          lold(3*lmpa+2)=2
          do l=3*lmpa+3,4*lmpa+2
          lold(l)=4*(l-3*lmpa)-8
          enddo


        endif
      endif      
c------------------------------------------------------------------
c 10   continue
      iminus=0
      jminus=0
      jdel=0
c      print *,'step 2'
c  Si pixstepp=0, on prend pour pixstep la valeur lue dans les
c                 headers des fichiers b
c                                  a programmer ensuite!!!
c      print *,'pixstepp',pixstepp
        if(pixstepp.ne.0)pixstep=pixstepp   ! 96
c      print *,'corstep',corstep
      if(corstep.eq.0)corstep=1
        
      nporef=1
c      iplot=1
      jsor=1
      modeg=icorrec-1
c                      degre du moment utilise pour correction
      iz1=0

        if(iplot.ne.0)call pgadvance

c      print *,'step 3'
c--------------------------------------  boucle burst
c      do200 nbu=1,burst
      call newscan(priscan,burst,nxmtot,dnap,nbu,nxm,dnanfp)
c          -------   in..... 1                 out...
      nm=nxm
c--------------------------------------
c      write(3,*)' avant lecture du header  ncx ',ncx

c                            lecture du header 1er fichier d
        iud=51
c      dname=dnanfp(1,1)
      dname=dnap(1,1)

      call openold22(dname,sundec,51)
c          ---------                 
       print *,' etape q/p   dname(/rname):',dname

      rewind(iud)
      read(iud)(head(n),n=1,40)
                     write(3,*)' dme: head of d ',(head(k),k=1,40)
      im=head(2)
      jm=head(3)    
        write(3,*)'im,jm ',im,jm

                  if(jd-ja+1.gt.jm)then
                  ja=1
                  jd=jm
                  endif
      lm=head(4)   !     h=head(4)
      lmh=lm
c      if(lmh.ne.lm)then
c      print*,' Number of output arrays not consistent with ms.par'
c      stop
c      endif

      nxx=1
      nyy=1
      ltrj=jt100                !head(12)
      
      write(3,*)' 446 msdp4.f  ltrj=',ltrj     !  m2403
c          if(icorrec.eq.2)ltrj=10000
c        print*,' dme: head(12) ',ltrj

c                             ceci remplace les moyennes par periode
c                                       par les moyennes sur tout le champ
c       convervx=-float(lbda)*float(ltrj)/(3000000.*float(dlbd))

       do l=1,lm
       dlb(l)=head(l+4) !     12)
       enddo
c      center=head(lm+10)
c      lsum=head(lm+11)
c      lmp=head(lm+12)....
c      lbdl=head(lm+13)....
c      lbdstep=head(lm+14)....
c                les dimensions du fichier d sont:  IJ l po sto XY
c                        "           "     e seront:UV l po sto  Y
c les boucles qui suivent respectent l'ordre de creation de e:
c                                           Y  sto po  l   X  J   I
c                                          150    140 100 80 50  3interval.
c       print*,' dme: lecture header d ',(head(i),i=1,lm+12) 
     
c--------------------------------------------
c      write(3,*)' avant calcul des correl ncx ',ncx

c                 calcul des correlations si lcor.gt.0    sur npo
      if(lcor.gt.0)then

c       if(i1i2.eq.0)then
      print *,'avant mulcor' !!!!!!!!!!!!!!!

       call mulcor
c           ------
     1  (nxm,npo,dnanfp,sundec,pixstep,lcor,jrsom,ijcormax,ijc,corstep,
     2  im,jm,nm,i1,jr,iminus,jminus,i1i2,icor,jcor,corm,milcor,
     3     kedge,xcor,ycor,maxdecal,jdel,
     4     tab1,tab2,iitab,jjtab,icutcor)
c                        nm est reduit au dernier de correl > milcor/1000
c       endif
c
       print *,'apres mulcor' !!!!!!!!!!!!!!!!!!!!!
      is=i1i2+1

c      if(icutcor.eq.1)then
c      js=jm-2*jminus
c        do n=2,nm
c        js=js+jm-jr(n)
c        enddo
c      jsp=jm-2*jminus+(nm-1)*pixstep
c      endif

c215   format(' js,jsp=',2i6)
c      write(3,215)js,jsp

c      if(js.gt.jsmax)then
c        print 1
c        write(3,1)
c        stop
c      endif

c      if(js.ne.jsp)jr(nm)=jr(nm)+js-jsp
c      js=jsp
c
c      nf2=nf1+nm-1
c        print *,' Fin de mulcor'

      if(iplot.ne.0)
     1         call sdescor(ijc,icor,jcor,corm,nm,xcor,ycor)
c                   -------
      goto21
      endif

c--------------------------------------
c      write(3,*)' avant cas pixstep ncx ',ncx

c pas pixstep si lcor=0          calculer a partir de im,jm,pixstep
      if(lcor.eq.0)then
            is=im-2*iminus
            js=jm-2*jminus+(nm-1)*pixstep

      print *,' im,jm,nm ',im,jm,nm
          do n=1,nm
          corm(n)=0
          enddo
        jr(1)=0
        i1(1)=1
          do n=2,nm
          jr(n)=jm-pixstep  
          i1(n)=1
          enddo
c                   write(3,*)' dme: jr ',(jr(n),n=1,nm)
         goto 21
      endif
c--------------------------------------------
c calcul precedent de correlations lu dans fichier "j" si lcor=-1
      if(lcor.eq.-1)then


      write(3,*)' avant jnameref=jname ncx ',ncx
        do nnn=1,22
        write(jnameref(nnn:nnn),'(a1)')jname(nnn:nnn)
          write(3,*)' nnn ncx ',ncx
        enddo
      write(3,*)' apres jnameref=jname ncx ',ncx


        write(jnameref(1:1),'(a)')'j'

c          if(iqp.eq.0)then
c          write(jnameref(18:22),'(i5)')linref
c            do index=18,22
c            write(charact,'(a1)')jnameref(index:index)
c            if(charact.eq.' ')
c     1         write(jnameref(index:index),'(a1)')'0'
c            enddo
c          endif

          write(7,*)' jnameref = ',jnameref
          write(3,*)' jnameref = ',jnameref

       call lecj(jnameref,sundec,is,js,nm,jminus,jm,vmoy,
c           ----
     1          i1,jr,corm)

      goto 21
      endif
c---------------------------------------------
21    continue

c      write(3,*)' avant calcul de ja,jb.. ncx ',ncx
c calcul de ja,jb,jc,jd,js

        nxm=nm
      do ncli=1,nxm

c dans fichier e
        if(ncli.ne.1)then
           jta=jta+jc-ja
c              ---------
        else
           jta=1
        endif
        jtan(ncli)=jta

c    recouvrement sur (1,jm) # jm-translation

c   ncli-1            ...   jc     jd   jm
c   ncli                1   ja     jb            jc   jd   jm
c   ncli+1                                   1   ja   jb   ...
c                        <---  jr  ------>
c                           |overlap|         <------------->
c                             final
c  ja,jd independants de ncli
        jb0=jb
        jc0=jc

      if(ncli.ne.1)then
        jb=ja+jd-jcn(ncli-1)
      else
        jb=ja
      endif

      if(ncli.ne.nm)then
        jc=jm-jr(ncli+1)+ja
      else
        jc=jd
      endif
      if(ncli .ne. 1 .and. ncli .ne. nm)then   ! m2403
      write(3,*)'line 613 msdp4: ncli,jb0,jc0,jcn,jr,   ja,jb,jc,jd '
      write(3,*) ncli,jb0,jc0,jcn(ncli-1),jr(ncli+1),   ja,jb,jc,jd
      endif
c      if(jc.gt.jd.or.jb.ge.jc)then
       if(jc.gt.jd)then
        print *,' ncli=',ncli
        print *,' Impossible overlaps: modify jyq1,jyq2'
        print *,' or check 2D correlations (icorm...)'
        print *,' ja,jb,jc,jd=', ja,jb,jc,jd
        write(7,*)' ncli=',ncli
        write(7,*)' Impossible overlaps: modify jyq1,jyq2'
        write(7,*)' or check 2D correlations (icorm...)'
        write(7,*)' ja,jb,jc,jd=', ja,jb,jc,jd
        stop
      endif

      if(jb.gt.jc)then
        jb=jc
        write(7,*)' warning dme: jb.gt.jc  for ncli=',ncli
      endif

c      1    jb1    jb0        jc0    jc1    jm     <--- ancienne version
c         ja    jb                jc     jd

c      if(ncli.ne.1)then
c        jb1=1+jr(ncli)/2
c        jb0=1+jr(ncli)
c      else
c        jb1=1+jr(1)/2
c        jb0=jb1
c      endif

c      if(ncli.ne.nm)then
c        jc0=jm-jr(ncli+1)
c        jc1=jm-jr(ncli+1)/2
c      else
c        jc0=jm-jr(ncli)/2
c        jc1=jc0
c      endif

c      lap=jlap2

c      ja=jb1-lap
c        if(ncli.eq.1)ja=jb1
c        if(ja.lt.1)ja=1
c      jb=jb1+lap
c        if(jb.gt.jb0)jb=jb0

c      jc=jc1-lap
c        if(jc.lt.jc0)jc=jc0
c      jd=jc1+lap
c        if(ncli.eq.nm)jd=jc1
c        if(jd.gt.jm)jd=jm

          jan(ncli)=ja
          jbn(ncli)=jb
          jcn(ncli)=jc
          jdn(ncli)=jd
      enddo

      js=jta+jd-ja
c---                   bornes ecriture qp

c      call par1('    j1qp',1,j1qp)
c      call par1('   j2mqp',1,j2mqp)  !  j2 moins pour qp
c        if(j1qp.eq.0)j1qp=1
c        jqp1=j1qp
c     jqp2=js-j2mqp
      jqp1=1
      jqp2=js
c---
      if(js.gt.jsmax)then
        print 1
        write(3,1)
        stop
      endif

      write(3,113)
      if(kana.eq.kanam)write(7,113)
      print 113
       do n=1,nm
c       write(iuj,112)i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
c     1               jtan(n)
       write(3,112)i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
     1               jtan(n)
       if(kana.eq.kanam)
     1 write(7,112)i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
     2               jtan(n)
       print 112, i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
     1               jtan(n)
       enddo

c**********************************************
c  On dispose maintenant de 
c          iminus, i1(...nm), i1i2
c          jminus, jlap2
c  qui decrivent les jonctions du balayage nxx.

      if(icorrec.eq.3)then
      call scorond3(nxm,npo,dnanfp,sundec,jm,im,lm,lec,kpiv,sig,sigmoy,
c          -------
     1   iminus,jminus,milsig,ll,ilissond,iplot,ides,jjtab)

      endif
c-------
          do i=1,is
          do j=1,js

          tab(i,j)=32000
          enddo
          enddo
c
c            cliche 1   JA = JB
c                        .
c                       JC
c                          .                   (commun)  JA du 2
c                          +JRecouvrement      (commun)  JD du 1
c            cliche 2   JB
c                        .
c                       JC
c                          .                   (commun)  JA du nm
c                          +JR                 (commun)  JD du 2
c            cliche nm  JB
c                        .
c                       JC = JD
c
c
22    format(' is=',i5,'  js=',i5)
      write(3,22)is,js
        ldeb=0

c      write(3,*)' avant boucle l ncx ',ncx

c                       suivent boucle tableaux l
c                                 boucle X (nf) - ouvertures d
c                                   boucle ltz (recherche tableau l dans nf)


c      print *,'avant boucle sur les tableaux : is,js',is,js !!!!!!!!!!!!
c==========================================================================
c                                                  grande boucle ltz
c      call par1('    ilog',1,ilog)
c      if(ilog.gt.1)then ! ne.0)then
c          lm=5
c        do l=1,lm
c        lold(l)=l
c        enddo
c      endif

c      write(3,*)'ilog, lold ',ilog,lold
c=================================================
      call par1('  inverl',1,inverl)
      call par1('   nfilt',1,nfilt)
        nf0=nfilt/2
c      call par1('    lsqr',1,lsqr)

      do 100 lnewp=1,lm
c     ---------------                 modifs int  avec ratio
      call par1('   f1rat',lnewp,iratio)
      ratio=1.
      if(iratio.ne.0.)ratio=float(iratio)/1000.
c     ---------------
          lnew=lnewp
c        if(inverl.eq.1)then
c          if(lnewp.lt.lint)then
c             lnew=lnewp
c          else
c             lnew=lint-lnewp
c          endif
c        endif

c                      lambdas ficiers Q = lambdas fichiers D
        ltz=lnewp    !lold(lnew)
c        ltz=lnewp

      write(3,*)'dme1:lnew,ltz  ',lnew,ltz
c      if(ilog.gt.1)ltz=lnew  !ne.0)ltz=lnew

         ides=3
c                if(lnew.ge.lm-1)ides=lnew-lm+2
c                if(ltz.ge.lm-1) ides=ltz-lm+2    

        kpivdes1=lvp
        kpivdes2=lv
        if(iqp.eq.1)then
          kpivdes1=1
          kpivdes2=3
        endif

         if(lnew.eq.kpivdes1)ides=1
         if(lnew.eq.kpivdes2)ides=2

      ldeb=ldeb+1
      if(ldeb.gt.1.and.lnew.gt.lm)goto100
c      if(ldeb.gt.1.and.ltz.gt.lm)goto100
      print *,' etape q/p  tableau lnew=', lnew

225   format(/' ltz,milsig,ides=',3i6)
      write(3,225)ltz,milsig,ides
        lpiv=1
        kpiv=ltrj
c------------------                 calcul    corrections ondulations

c                                 icorrec=-1  pas de correc ni calcul moyennes
c                                          0   pas de correction mais moyennes
c                                          1   moyennes repliees
c                                          2   moyennes non repliees
c                                          3   histogrammes non replies
c                                                 deja fait
c                                          4   correction lineaire recouvremts
c------           cas icorrec=0,1 ou 2
      if(icorrec.ge.0.and.icorrec.le.2)then
c      if(icorrec.eq.0.and.lnew.ne.lv.and.lnew.ne.lvp)goto228

      if(icorrec.eq.2)kpiv=100*(jm+1)
      call scorond(nxm,npo,dnanfp,sundec,jm,im,lec,ltz,kpiv,ond,poids,
c          -------
     1   iminus,jminus,milsig,tabmoy,  sigma,lpiv,ilissond,
c                             in
c                  tabmoy: pour normaliser les dessins d'intensites
     2   iplot,ides,maxy,iitab,jjtab)
        write(3,*)' scorond: ltz(in), maxy(out) ',ltz,maxy
c      print *,' Fin scorond'
c------                                dessin ond et poids
c vmoy pour ltz=lv
c

      if((ides.eq.1).and.(iplot.ne.0))then
                 print *,'js',js


      piv=float(js)
        call pgvport(0.1,0.9,0.1,0.8)
        call pgsch(1.5)
        call pglabel('correl /   x / y      /        poids ', 
     1              ' ',dname)
      call pgvport(0.7,0.9,0.1,0.8)
      call pgwindow(0.,1.,1.,piv)
      call pgbox('bcnts',0.5,5,'bcmts',0.,0)
      call pgsch(1.)
      endif
c
      endif
c-----------------------------------------
      if(icorrec.eq.5)then
c      kpiv=100*(jm+1)
      call scorond5(nxm,npo,dnanfp,sundec,jm,im,lec,ltz,kpiv,ond,poids,
c          -------
     1   iminus,jminus,milsig,tabmoy,  sigma,lpiv,ilissond,
c                  tabmoy: pour normaliser les dessins d'intensites
     2   iplot,ides,maxy,iitab,jjtab,kind,nquv)
        write(3,*)' scorond: ltz(in), maxy(out) ',ltz,maxy
c------                                dessin ond et poids
      if((ides.eq.1).and.(iplot.ne.0))then
      piv=float(js)
        call pgvport(0.1,0.9,0.1,0.8)
        call pgsch(1.5)
        call pglabel('correl /   x / y      /        poids ', 
     1              ' ',dname)
      call pgvport(0.7,0.9,0.1,0.8)
      call pgwindow(0.,1.,1.,piv)
      call pgbox('bcnts',0.5,5,'bcmts',0.,0)
      call pgsch(1.)
      endif
c
      endif
c------------------------------------------
228     continue
c        do j=1,jje
c        do i=1,iitab
c        tab(i,j)=maxy
c        enddo
c        enddo

c      write(3,*)' avant boucle X,: ncx ',ncx
        
        write(3,*)'avant boucle X, jta=',jta 
c -----------------------------------------  boucle nxm
c     correction icorrec=4     intens.  pour ltz  tab1(i,ncli+3),tab2(i,ncli+3)
c                                          j=  (ja+jb)/2    (jc+jd)/2
        if(icorrec.eq.4)then
      do235 ncli=1,nxm
        iud=51
        dname=dnanfp(ncli,npo)
c            print*,dname
        call openold22(dname,sundec,51)
c            ---------
      rewind(iud)
      read(iud)head
       lmax=lm

c        jta=jtan(ncli)

        ia=i1(ncli)
        
        ja=jan(ncli)
        jb=jbn(ncli)
        jc=jcn(ncli)
        jd=jdn(ncli)

          if(ja.lt.jminus+1)then
           print *,' ja.lt.jminus+1'
          write(3,24)
          close(unit=iud)
          close(iue)
          close(3)
          close(8)
          if(iplot.ne.0)call pgend
              stop
          endif

          if(jd.gt.jm-jminus)then
           print *,' jd.gt.jm-jminus'
          write(3,24)
          close(unit=iud)
          close(iue)
          close(3)
          close(8)
          if(iplot.ne.0)call pgend
              stop
          endif

          do 234 l=1,lm
c               print*,'    saut avant ltz, lecture tableau ltz de d'
            do 233 j=1,jm
            read(iud)(lec(i),i=1,im)
c                ---
            if(l.ne.ltz)goto 233

          if(j.eq.(ja+jb)/2)then
              do i=1,im
              tab1(i,ncli+3)=lec(i)
              enddo
          endif
          if(j.eq.(jc+jd)/2)then
              do i=1,im
              tab2(i,ncli+3)=lec(i)
              enddo
          endif

233         continue
234         continue
      close(unit=iud)
235   continue

c corrections:  soustractions en tenant compte des decalages i
c                       ncli  <-- ncli+2,ncli+3,ncli+4
      do ncli=1,nxm
      do it=1,is

      if(ncli.ne.1)then
        i=it+i1(ncli-1)-1
        piv1=tab2(i,ncli+2)
      else
        i=it+i1(ncli)-1
        piv1=tab1(i,ncli+3)
      endif

        i=it+i1(ncli)-1
        piv2=tab1(i,ncli+3)
        piv3=tab2(i,ncli+3)

      if(ncli.ne.nxm)then
        i=it+i1(ncli+1)-1
        piv4=tab1(i,ncli+4)
      else
        piv4=tab2(i,ncli+3)
      endif 

      tab1(it,ncli)=(piv1-piv2)/2.
      tab2(it,ncli)=(piv4-piv3)/2.

      enddo
      enddo

      endif    !  icorrec 4 ?
c---------------
        if(nxm.lt.minimages_crec.and.icorrec.ne.4)icorrec=0 
c                                pas de correction petits scans

        do 80 ncli=1,nxm
c                   ---

         write(3,*)' nxm,ncli ',nxm,ncli
        iud=51
        dname=dnanfp(ncli,npo)
c            print*,dname
        call openold22(dname,sundec,51)
c            ---------
      rewind(iud)
      read(iud)head

        if(ldeb.eq.1)then
c        print 1,dname
c        write(3,1)dname
        endif

       lmax=lm

        jta=jtan(ncli)

        ia=i1(ncli)
        
        ja=jan(ncli)
        jb=jbn(ncli)
        jc=jcn(ncli)
        jd=jdn(ncli)
       
      if(ldeb.eq.1)then      
23    format(' ncli,ja,jb,jc,jd,jta,head(1,7) =',13i7) !i4,4i3,i4,
c     1         i2,3i4,i2,2i6)
      write(3,23)ncli,ja,jb,jc,jd, jta,(head(ih),ih=1,7)
c      print 23, ncli,ja,jb,jc,jd, jta
24    format(' jminus or jlap2 too large')
26    format(' jlap2 too large')
          if(ja.lt.jminus+1)then
           print *,' ja.lt.jminus+1'
          write(3,24)
          close(unit=iud)
          close(iue)
          close(3)
          close(8)
          if(iplot.ne.0)call pgend
              stop
          endif

          if(jd.gt.jm-jminus)then
           print *,' jd.gt.jm-jminus'
          write(3,24)
          close(unit=iud)
          close(iue)
          close(3)
          close(8)
          if(iplot.ne.0)call pgend
              stop
          endif


          endif

c          if(jc.lt.jb)then
c          write(3,26)
c          goto100
c          endif
  
c-----------------------------------------ecriture header q ou p
          do k=1,512
             qhead(k)=0
          enddo
        qhead(1)=head(1)          
        if(ldeb.eq.1.and.ncli.eq.1)then
        qhead(4)=lm !h          !   pour tenir compte promax eventuel
        qhead(2)=is
c          jjs=jqp2-jqp1+1
        qhead(3)=js  !jjs
           write(3,*)'header q: is,js,lm ',is,js,lm
         ipiv=lm+12    !8
c-------------------------------------------    champs reduits   
c      call par1('     iqa',1,iqa)
c      call par1('     iqb',1,iqb)
c      call par1('     jqa',1,jqa)
c      call par1('     jqb',1,jqb)
c       npix=1000/milsec
c        iqa=iqa*npix
c        iqb=iqb*npix
c        jqa=jqa*npix
c        jqb=jqb*npix
c                                                          non disk
c         if(iqcut1.ne.0.or.ifield.ne.0)then
c        qhead(4)=lm !h          !   pour tenir compte promax eventuel
c      write(3,*)'ifield,is',ifield,is
c      if(ifield.gt.is-1-iqcut1)then
c          print *,'ifield or iqcut1 too large'
c          stop
c      endif
c        qhead(2)=1+ifield !    coupures
c                --------------
c        jjs=1+ifield  ! jqp2-jqp1+1 -jqcut1-jqcut2
c                         --------------
c        qhead(3)=jjs
c        if(jmaxs.ne.0)then
c         jjs=jmax
c         qhead(3)=jmax
c        endif
c         ipiv=lm+12   !8
c       write(3,*)'im,jm, is,js,jjs, iqcut1,ifield ',
c     1            im,jm,is,jjs,jjs,iqcut1,ifield
c      endif
c--------------------------------------------   fin  iqcut1
       do l=1,lm
          dlb(l)=head(l+4)      !12) !qhead(l+12)
          qhead(l+4)=head(l+4)
       enddo
       
c       do lnouv=1,lm
c       l=lold(lnouv)
c       qhead(lnouv+12)=dlb(l)
c      enddo
           write(3,*)'line 1104 qhead ',(qhead(lw),lw=1,40)
c-------header q
c      if(iqa.ne.iqb)then
c         qhead(2)=iqb-iqa+1
c         qhead(3)=jqb-jqa+1
c      endif

27    format(' head file q ou p ',10i8)
        write(3,27)(qhead(i),i=1,40)
        write(iue)qhead                           ! header q
c             ---
271   format(' Dimensions  init/fin. q or p:')! is=',i5,'  jjs=',i5,'  lm =',i3
      write(7,271)
      write(2,271)
      write(7,*)is,jjs,lm,qhead(2),qhead(3),qhead(4)  !is,jjs,lm
      write(2,*)is,jjs,lm,qhead(2),qhead(3),qhead(4)  !is,jjs,lmjs,lm

        endif
c-----------------------------------------------------------
c                                                 boucle lm  de lecture de d
      write(3,*)' avant do60: ncx ',ncx

          do60 l=1,ltz
c             write(3,*)'l=',l,' saut avant tableau ltz de d'
            do50 j=1,jm
            read(iud)(lec(i),i=1,im)
c                ---
            if(l.ne.ltz)goto50
c--------
             ipiv=im/2
             jpiv=jm/2
c        if(j.eq.jpiv)print*,' dme: ncli,l  lec(im/2,jm/2) ',
c     1                            ncli,l, lec(ipiv)              
            jt=jta+j-ja

c correction icorrec=4
      if(icorrec.eq.4)then
            j1linc=(ja+jb)/2
            j2linc=(jc+jd)/2
      do it=1,is
      i=it+ia-1
      alp=float(j-j1linc)/float(j2linc-j1linc)
      lec(i)=tab1(it,ncli)*(1.-alp)+tab2(it,ncli)*alp+lec(i)
      enddo
      endif
c-----------------------------------------------1er intervalle
            if(j.ge.ja.and.j.lt.jb)then

            fac=1.
            if(jb.ne.ja)then
c              if(jlap2.lt.0)then
c              fac=float(fac100)/100.
c              else
              piv=pi*float(j-ja)/float(jb-ja)
              fac=(1.-cos(piv))/2.
c              endif
c        write(3,*)' ides,iplot,icorrec ',ides,iplot,icorrec
      if(ides.eq.1.and.iplot.ne.0.and.icorrec.ge.0)then
      if(icorrec.le.2.or.icorrec.eq.5)then
      yd=jt
      xd=1.-fac
      call pgpoint(1,xd,yd,2)
c          -------        
      xd=fac
      call pgpoint(1,xd,yd,2)
c          -------
      endif
      endif
c
            endif
c
              do30 it=1,is
              i=it+ia-1
                if(tab(it,jt).eq.32000.and.ncli.ne.1)then
                goto30
                endif
                if(lec(i).eq.32000)then
                 tab(it,jt)=32000
                 goto30
                endif
c
                piv1=tab(it,jt)
                piv2=lec(i)          
c                       
                  if(piv2.eq.32000.)then
                  tab(it,jt)=32000
                  goto30
                  endif                
c           if (icorrec.eq.1.or.icorrec.eq.2)piv2=piv2-ond(i,j,ides)
c           if(icorrec.eq.3)piv2=cormom(piv2,sig,sigmoy,j,l,modeg,jjtab)
c                               ------
c           if(icorrec.eq.5)piv2=piv2/ond(i,j,ides)
c                               ------------------
c
c                  corvit=convervx*ond(i,j,ides)
c                  jcorvit=j+corvit
c                  if(jcorvit.gt.jm)jcorvit=jcorvit-ltrj
c                  if(jcorvit.lt.1)jcorvit=jcorvit+ltrj
c                  piv2=lec(i)+ond(i,jcorvit,ides)
28    format(' ltz,jt,j,fac,ancien,nouveau,tab',
     1           3i5,      f6.3, 3f7.0, i7)
      call bisdme(piv1,piv2,fac,piv,tabmoy(l))
c          ------            

      if(ncli.eq.1)piv=piv*ratio
c                          -----
       if(piv.ge.0.)then
         kpiv=piv+0.5
       else 
         kpiv=piv-0.5
       endif

              tab(it,jt)=kpiv
c             ----------
      if(it.eq.is/2)then
      piv3=piv2-piv1
c      write(3,28)ltz,jt,j,fac,piv1,piv2,piv3,tab(it,jt)
      endif
30            continue
            endif
c----------------------------------------------2eme intervalle
            if(j.ge.jb.and.j.le.jc)then
      yd=jt
      xd=1.
      if(ides.eq.1.and.iplot.ne.0)call pgpoint(1,xd,yd,1)
c                                      -------
c
              do it=1,is
              i=it+ia-1
c
              if(lec(i).ne.32000)then
c               piv=lec(i)-tabmoy(ltz)
                piv=lec(i)
                if(ncli.eq.1)piv=piv*ratio

           if(icorrec.eq.1.or.icorrec.eq.2)piv=piv-ond(i,j,ides)
           if(icorrec.eq.3)piv=cormom(piv,sig,sigmoy,j,l,modeg,jjtab)
c                              ------
           if(icorrec.eq.5)piv=piv/ond(i,j,ides)
c                               ---------------
c                if (imuladd.eq.0) then
c                  tab(it,jt)=lec(i)*ond(i,j,ides)
c                else
c                  corvit=convervx*ond(i,j,ides)
c                  jcorvit=j+corvit
c                  if(jcorvit.gt.jm)jcorvit=jcorvit-ltrj
c                  if(jcorvit.lt.1)jcorvit=jcorvit+ltrj
c                  tab(it,jt)=lec(i)+ond(i,jcorvit,ides)
 
             tab(it,jt)=piv
c            ----------
              else
                tab(it,jt)=32000
              endif
              enddo
 
            endif
c-------------------------------------------------3eme intervalle
            if(j.gt.jc.and.j.le.jd)then

      yd=jt
      xd=1.
      if(ides.eq.1.and.iplot.ne.0)call pgpoint(1,xd,yd,1)
c                                      -------
c
              do it=1,is
                i=it+ia-1
                if(lec(i).ne.32000)then
c                  piv=lec(i)-tabmoy(ltz)
                   piv=lec(i)
              if(ncli.eq.1)piv=piv*ratio
c                  corvit=convervx*ond(i,j,ides)
c                  jcorvit=j+corvit
c                  if(jcorvit.gt.jm)jcorvit=jcorvit-ltrj
c                  if(jcorvit.lt.1)jcorvit=jcorvit+ltrj
c                  tab(it,jt)=lec(i)+ond(i,jcorvit,ides)
            if(icorrec.eq.1.or.icorrec.eq.2)piv=piv-ond(i,j,ides)
            if(icorrec.eq.3)piv=cormom(piv,sig,sigmoy,j,l,modeg,jjtab)
            if(icorrec.eq.5)piv=piv/ond(i,j,ides)
c                               ----------------
c
                  tab(it,jt)=piv
c                 ----------
                else
                  tab(it,jt)=32000
                endif
40            enddo
c                                   fin boucle i
  
            endif
c                                   fin 3eme intervalle
50          continue

c                                        fin boucle j
60        continue
c-----------------------------------------    fin boucle lm de lecture 
70        continue

        close(unit=iud)
c       -----
80    continue
c       print *,'fin boucle X'

c      write(3,*)' apres 80: ncx ',ncx

c-------------------------------------------------------- fin boucle nxm
      if(iz1.ne.0)then     !non
        if(iz4.eq.0)then
        iz4=is
        iz2=iz4/2
        iz3=iz2+1
        endif
      endif
c
c      print*,' dme: is,js,ltz,tabmoy(ltz) ',is,js,ltz,tabmoy(ltz)

c        do j=1,js
c          do i=1,is
c          lec(i)=tab(i,j)
c            if(lec(i).eq.32000)then
c             lec(i)=tabmoy(ltz)
c            else
c              if(ll(ltz).eq.1)lec(i)=2000.*lec(i)/tabmoy(ltz)
c                                 intensites non normalisees
c               if(ll(ltz).eq.-1)lec(i)=lec(i)-tabmoy(ltz)
c si ll=-1, vitesses recalees 
c            endif
c          enddo
c
      
c        call zero3(iz1,iz2,iz3,iz4,0,0,0,lec,1,is,1,
c            ----- si 0, inefficace
c     1             0,ar,br,cr,item,nrjm,nrej,piv1,piv2,ifin)
c               print *,j,ar,br,item,nrjm,piv1,piv2
c       sans effet si iz1=0
c
c          if(ifin.ne.0)then
c              do i=1,is
c              tab(i,j)=0
c              enddo
c          print 85
c85        format(' Sortie en erreur dans zero3')
c          write(3,85)
c
c           else
c          do i=1,is
c          tab(i,j)=lec(i)
cc         --------
c          enddo
c           endif
c
c        enddo   !j
c
c      if(ltz.eq.2)call srejec(tab,is,js,3000,vmoy)
c                      ------
c
c      print *,'is,js=',is,js
c----------                          calcul sana
c                                           ----
            if(mgrim.ne.0.and.ltz.eq.4)then
            sana(kana)=0.
              do j=1,js
              do i=1,is
              kpiv=tab(i,j)
              sana(kana)=sana(kana)+kpiv*kpiv
              enddo
              enddo
            piv=sana(kana)/(is*js)
            piv=sqrt(piv)
            sana(kana)=piv
            endif
c-----                                    ecriture sur q ou p

c      call ijlis2(tab,is,js,iitab,jje)   !non
c          ------
      call par1('  spimax',1,kpiv)     !    spicules
      if(kpiv.ne.0)then
      if(ltz.eq.1)call bord_exp(tab,is,js,iitab,jje,invi,invj,
c                      --------
     1                          jqp1,jqp2,milsec)
      endif

c      call cheveux(tab,iitab,jje,is,js,invi,invj)
c          -------

c      print*,' ltz,l,lnew, kind(ltz),kind(l),kind(lnew) ',
c     1         ltz,l,lnew,kind(ltz),kind(l),kind(lnew)

c------------------------------------------------centrage sur moyennes X
      if(icentre.ne.0)then   !   0
      if(lnew.eq.1)then   !  premiere intensite
       do j=jqp1,jqp2
       do i=1,is
         kdisc(i,j)=1
       if(mindisc.ne.0.and.tab(i,j).lt.mindisc)kdisc(i,j)=0
       if(minprot.ne.0.and.tab(i,j).lt.(-minprot))kdisc(i,j)=-1
       enddo
       enddo
      endif
        iv=0   !  int vit
        if(lnew.eq.1)iv=1
        if(lnew.eq.2)iv=2
c      call xcentre(tab,1,is,jqp1,jqp2,kdisc,iitab,jje,milsigc,iv)
      endif
c----------- soustraction lumière diffusée du ciel entre xsky1 et xsky2
c      if(xsky1*xsky2.ne.0..and.l.lt.lm)then
c      call scatsky(tab,iitab,jje,is,jqp1,jqp2,invi,milsec,xsky1,xsky2)
c      endif
c===-----------------------------  moyennes vitesses si tabint.ne.0
      call par1('  vrejec',1,kvrejec)
      if(lnew.eq.lint)then   !  intensites
       do jp=jqp1,jqp2
       do i=1,is
         tabint(i,jp)=tab(i,jp)
      enddo
      enddo
      endif

      if(lnew.eq.lint+1)then   !   vitesses
            nmoypro=0.
            vmoypro=0.
       do jp=jqp1,jqp2
       do i=1,is
        kpiv=tabint(i,jp)
        kvit=tab(i,jp)
        kvit=iabs(kvit)
         if(kpiv.gt.minint.and.kpiv.le.maxint)then
          if(kpiv.ne.0.and.kvit.lt.kvrejec)then
         vmoypro=vmoypro+tab(i,jp)
         nmoypro=nmoypro+1
         endif
         endif
      enddo
      enddo
               mcornew=0
c            if(nmoypro.ne.0)then
            vmoypro=vmoypro/float(nmoypro)
            piv=vmoypro   
            mcornew=mcorrec-piv
c            endif
 90   format('Averaged velocities (minint,maxint):lint+1,npts,',
     1       '   vmoy,mcorrec,mcornew')
 91   format(37x,i4,i8,f8.0,2i7)
 92   format(a22,i8,f8.0)
      write(3,90)
      write(3,91)lnew,nmoypro,vmoypro,mcorrec,mcornew
      write(2,90)
      write(2,91)lnew,nmoypro,vmoypro,mcorrec,mcornew
      write(7,90)
      write(7,91)lnew,nmoypro,vmoypro,mcorrec,mcornew
 93   format(i4,2X,a22,i8,f9.0,i8,i9)
      nvel=nvel+1
      scorrec=scorrec+float(mcornew)
      aver=scorrec/float(nvel)
      maver=aver
      write(8,93)nvel,qname,nmoypro,vmoypro,mcornew,maver
      print 90
      print 91,lnew,nmoypro,vmoypro,mcorrec,mcornew
c======--------------------------------             vit=0 si int<minint ou 
c                                                         int>maxint
      if(minmax.eq.1)then
        do jp=jqp1,jqp2
        do i=1,is
          kpiv=tabint(i,jp)
          if(kpiv.le.minint.or.kpiv.gt.maxint)tab(i,jp)=0.
        enddo
        enddo
       endif   !   minmax
      endif    !   vit
c=====--------------------------------------------------
      if(iminpro.ne.0)then                           ! protus
c        -------
        if(lnew.eq.lint)then
        do jp=jqp1,jqp2
        do i=1,is
          kpiv=tab(i,jp)
          if(kpiv.lt.iminpro)tab(i,jp)=0
        enddo
        enddo
        endif

        if(lnew.eq.lint+1)then
        do jp=jqp1,jqp2
        do i=1,is
          kpiv=tabint(i,jp)
          if(kpiv.lt.iminpro)tab(i,jp)=0
        enddo
        enddo
        endif
      endif
c---------------------------------
c            cas general (sans fixscat)
      ii1=1
      ii2=is  !qhead(2)
      jj1=1  !jqp1
      jj2=js  !qhead(3)
c=====--------------------------------------------
      write(3,*)'llimb,lbis ',llimb,lbis
      call par1(' fixscat',1,fixscat)
      if(fixscat.ne.0)then
      if(lnew.eq.llimb)then
            ii1=1+iqcut1
            ii2=qhead(2)+ii1-1  !is   ! -iqcut2
              if(ifield.ne.0) ii2=ii1+ifield   !is-iqcut2
            jj1=jqp1+jqcut1
            jj2=jj1+qhead(3)-1  !jqp2-jqcut2
      write(3,*)'is, ii1,ii2 ',is,ii1,ii2
c parabole limb
      call paralimb(tab,iitab,jje,ii1,ii2,jj1,jj2,
     1                               nmlimb,  jlimb)  
c                                   int limb  out
      endif
c centring + cut chrom
      call cutchrom(tab,iitab,jje,jlimb,jhchrom,ii1,ii2,
     1               jj1,jj2,jmax,lnew,lbis)
c                    new but same diff (transl)
      endif   ! fixscat
c---------------------------------liste scan.lis
      call par1('  milsec',1,milsec)
      call par1('  listx1',1,listx1)
      call par1('  listx2',1,listx2)
      call par1('  listy1',1,listy1)
      call par1('  listy2',1,listy2)
      call par1('  listl1',1,listl1)
      call par1('  listl2',1,listl2)
      if(listl1*listl2.ne.0)then
        coef=2. !1000./float(milsec)
        lx1=listx1*coef   !235*2
        lx2=listx2*coef
        ly1=listy1*coef
        ly2=listy2*coef
        iid=(lx2-lx1)/20
        jjd=(ly2-ly1)/20        
 96     format(21i5)
        if(lnew.ge.listl1.and.lnew.le.listl2)then
           write(7,*)'list Q lnew = ',lnew
           write(7,*)'lx1,lx2,ly1,ly2,iid,jjd',lx1,lx2,ly1,ly2,iid,jjd
           do jp=ly2,ly1,-jjd  
              write(7,96)(tab(ip,jp),ip=lx1,lx2,iid)
           enddo
         endif
      endif
c--------------------------------iqa,iqb,jqa,jqb
c      call par1('     iqa',1,iqa)
c      call par1('     iqb',1,iqb)
c      call par1('     jqa',1,jqa)
c      call par1('     jqb',1,jqb)
c      call par1('  milsec',1,milsec)
c        npix=1000/milsec
c        iqa=iqa*npix
c        iqb=iqb*npix
c        jqa=jqa*npix
c        jqb=jqb*npix
c      if(iqa.ne.iqb)then
c         ii1=ii1+iqa
c         ii2=ii1+iqb-iqa
c         jj1=jj1+jqa
c         jj2=jj1+jqb-jqa
c         write(3,*)'write q avt ii1,ii2,jj1,jj2',ii1,ii2,jj1,jj2
c      endif
c--------------------------------------------------------
      write(3,*)'write q apr ii1,ii2,jj1,jj2',ii1,ii2,jj1,jj2
       do95 jp=jj1,jj2  !jqp1,jqp2       new
                if(invj.eq.0)then
                j=jp
                else
                j=jj2-jp+jj1  !js-jp+1
                endif
c      jpss=jqp2/2
c      if(jp.eq.jpss)then
c      write(3,*)'lnew,q(i..10,jp2/2)'
c      write(3,*)lnew,(tab(i,jpss),i=ii1,ii2,10)
c     endif
c-------------------------------------------------------                  
c      if(lnewp.eq.lsqr)then
c        do i=ii1,ii2
c          piv=tab(i,j)
c             if(piv.lt.0.)piv=0.
c          piv=piv*100.
c          tab(i,j)=sqrt(piv)    !  sqrt pour lsqr
c        enddo
c      endif

          if(invi.eq.0)then
          write(iue)(tab(i,j),i=ii1,ii2)  !!  ecriture sur q ou p
c               -----           new           -------------------      
          else
          write(iue)(tab(i,j),i=ii2,ii1,-1)
          endif

 902      format(' ltz, q or p =',10i5)
            jtest=j-100*(j/100)
            if(jtest.eq.0)write(3,902)ltz,(tab(i,j),i=1,is,200)
      jtest=(j-1)*(j-101)*(j-201)*(j-301)*(j-401)*(j-501)
      if(jtest.eq.0)then
      write(3,*)'dme ltz lnew j tab ',ltz,lnew,j,(tab(i,j),i=ii1,ii2,75) 
      endif
95    continue     !!  fin boucle jp
c             jjj=(jqp1+jqp2)/2
c      write(3,*)'  kdisc  ',(kdisc(i,jjj),i=1,is,50)

100   continue
c      print *,'fin boucle lnew'
c==================================                             fin boucle ltz
      jjsaut=1
      if(jjsaut.eq.1)return

c      print *,'nrepetmax : ',nrepetmax,'jm : ',jm,'ltrj : ',ltrj
      nrepetmax=100*jm/ltrj+1
c      print *,'nrepetmax : ',nrepetmax !!!!!!!!!!!!!!!!!!!
      write(3,*)' nm,nrepetmax ',nm,nrepetmax
      den=nm*nrepetmax
c-------
c                              dessin des 2 derniers tableaux calcules
c       print *,'iplot : ',iplot 
        kpivdes1=lvp
        kpivdes2=lv
        if(iqp.eq.1)then
          kpivdes1=1
          kpivdes2=3
        endif

      if(iplot.le.0)goto102
      if(iplot.eq.3)goto102  
      if(iplot.eq.4)goto102

c       call sdesond(ond,poids,im,jm,den,tabmoy,kpivdes1,kpivdes2,
c              -------
c     1                 iitab,jjtab,icorrec,jq1,jq2)
 102  continue
      jjsaut=1
      if(jjsaut.eq.1)goto 202
c-------------------------------
c                                Creation du fichier j (vmoy et corr)
        print *,'jname: ',jname 
c        call system('rm '//jname)
c        call opennew22sf(jname,sundec,54)!opennew20 -> opennew20sf seq format
c            ---------
107   format(' mean doppler (if bis.),dxana,dyana,',
     1       'itananew,jtananew')
108   format(3f12.1,2i8)
109   format(a)
110   format(7i6)
111   format('    is    js    nm jminus   jm  jqp1  jqp2')
112   format(2i6,f6.3,5i6)
 115  format(2i6,f6.3,6i6)
113   format('    i1    jr  corm    ja    jb    jc    jd   jta   jc-jb')
114   format(3i4)
      write(iuj,109)jname
c           ---
      write(3,109)jname
      write(2,109)jname
c      write(iuj,114)2,nyy,npol
c      write(3,114)2,nny,npol
c        endif

      write(iuj,107)
c           ---
      write(3,107)
      if(kana.eq.kanam)write(7,107)
        print 107
c        vmoy(npo,ny)=tabmoy(2)
c        print 108,ny,npo,vmoy(npo,ny)

        call par1('   itana',1,itold)
        call par1('   jtana',1,jtold)
        itnew=itold+dxana(kana)
        jtnew=jtold+dyana(kana)

c                                lv=indice tableau vitesses
      write(iuj,108)tabmoy(lv),dxana(kana),dyana(kana),itnew,jtnew
c           ---     ----------

      write(3,108)tabmoy(lv),dxana(kana),dyana(kana),itnew,jtnew
      if(kana.eq.kanam)
     1 write(7,108)tabmoy(lv),dxana(kana),dyana(kana),itnew,jtnew

      write(iuj,111)
      write(iuj,110)is,js,nm,jminus,jm,jqp1,jqp2
c                                      attention nm
      write(3,111)
      write(3,110)is,js,nm,jminus,jm,jqp1,jqp2
      print 111
      print 110,is,js,nm,jminus,jm,jqp1,jqp2
      write(iuj,113)
                         write(2,113)
c      write(7,113)
c      print 113
       do n=1,nm
          ipiv=jcn(n)-jbn(n)
       write(iuj,112)i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
     1               jtan(n)
       write(2,115)i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
     1               jtan(n),ipiv
c       write(7,112)i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
c     1               jtan(n)
c       print 112, i1(n),jr(n),corm(n),jan(n),jbn(n),jcn(n),jdn(n),
c     1               jtan(n)
       enddo

c      close(unit=iuj)
c200   continue
c----------------------  fin boucle burst
c      write(3,*)' avant return ncx ',ncx
 202   continue
       write(2,*)' '
        write(2,*)' '
      write(7,90)
      write(7,91)lnew,nmoypro,vmoypro,mcorrec,mcornew
      return
      end
c***************************************************************************
      subroutine mulcor
c                ------
     1       (nfm,npo,dnanfp,sundec,pixstep,lcor,jrsom,ijcormax,ijc,
     2        corstep,im,jm,nm,i1,jr,iminus,jminus,i1i2,
     3        icor,jcor,corm,milcor,kedge,xcor,ycor,maxdecal,jdel,
     4        tab1,tab2,iitab,jjtab,icutcor)
c
      integer*4 i1(150),jr(150),kedge(150),head(512),
     1          icor(150),jcor(150),pixstep,corstep,sundec
      integer*2 tab1(iitab,jjtab),tab2(iitab,jjtab)
      dimension cor(201,201),corm(150),kcor(201),xcor(150),ycor(150)
      integer*2 lec(2048)

      character*22 dnanfp(500,2),name
c
c      if(lcor.le.0)return
c
1     format(a)
2     format(12i6)
c                                      boucle x      (nfm)
      iud=51
      idel=corstep
      call openold22(dnanfp(1,npo),sundec,iud)
c          ---------      
      rewind(iud)
      read(iud)head
 21   format('head',10i6)
      write(3,21)(head(k),k=1,30)
      im=head(2)
      jm=head(3)
      lm=head(4)
      npol=head(5)
c      nsto=head(6)
c                             !!!!!!!!!!!!! verifier
      nxx=1
      nyy=1
    
          jrsom=jm-pixstep
          if(jrsom.gt.jjtab)jrsom=jjtab
c          ijc=jm-pixstep   ! 21
c          if(ijc.gt.ijcormax)ijc=ijcormax
c          if(ijc.gt.51)ijc=51
            ijc=51

          write(3,*)'* debut mulcor'
5         format(' im,jm,lm,jjtab,pixstep,jrsom,ijcormax,ijc ',8i5)
          write(3,5)im,jm,lm,jjtab,pixstep,jrsom,ijcormax,ijc
c                   927 117 4  150       96    21     60   51
c                                    jjtab
c      nm=nfm
c      nmp=nfm-1
c         icor(1)=ijc
c         jcor(1)=ijc
c         corm(1)=1.
c------
      jj1=1
      jj2=jm-pixstep
      jj3=1+pixstep
      jj4=jm

c-----     
      do l=1,lcor
        do j=1,jm
          if(l.eq.lcor)then
c                                        modif 30 mars 2000  ncli --> nf
          read(iud)(tab2(i,j),i=1,im)
c                   ---------
          else
          read(iud)(lec(i),i=1,im)
          endif
        enddo
      enddo 
        close(unit=iud)

 4      format(20i4)
        write(3,*)'im,jm,jj1,jj2,jj3,jj4',
     1             im,jm,jj1,jj2,jj3,jj4
      write(3,*)'mulcor ncli   0   tab(i,jj*) '
      write(3,4)(tab2(i,jj1),i=1,im-idel,50)
      write(3,4)(tab2(i,jj2),i=1,im-idel,50)
      write(3,4)(tab2(i,jj3),i=1,im-idel,50)
      write(3,4)(tab2(i,jj4),i=1,im-idel,50)
c---
      nm=nfm
      nmp=nfm-1
         icor(1)=ijc
         jcor(1)=ijc
         corm(1)=1.
      cormin=float(milcor)/1000.
c---------------------------------------
c                           boucle X pour calcul des correlations
 
      do60 ncli=1,nfm-1
c      print *,'Dans boucle60 ncli : ',ncli

        do jp=1,jrsom
        jp1=jp-jrsom+jm     ! jp-(jm-pixstep)+jm = jp-pixstep
          do i=1,im
          tab1(i,jp)=tab2(i,jp1)
c         ----------
          enddo
        enddo
c
      name=dnanfp(ncli+1,npo)
      call openold22(name,sundec,iud)
c          ---------
      rewind(iud)
      read(iud)head

        do l=1,lcor
          do j=1,jm
            if(l.eq.lcor)then
            read(iud)(tab2(i,j),i=1,im)
c                     --------
            else
            read(iud)(lec(i),i=1,im)
            endif
          enddo
        enddo
      close(unit=iud)
      jj1=1
      jj2=jm-pixstep
      jj3=1+pixstep
      jj4=jm

      write(3,*)'ncli ',ncli,' tab(i,jj*) '
      write(3,4)(tab2(i,jj1),i=1,im-idel,50)
      write(3,4)(tab2(i,jj2),i=1,im-idel,50)
      write(3,4)(tab2(i,jj3),i=1,im-idel,50)
      write(3,4)(tab2(i,jj4),i=1,im-idel,50)
c---
c      print *,'avant cortab' !!!!!!!!!!!!!!!!!!!!!!!!!!!
      call CORTAB(im,jrsom,ijc,corstep,jdel,iminus,jminus,
c          ------
     1 tab1,tab2,cor,icor(ncli+1),jcor(ncli+1),corm(ncli+1),milcor,
c                                                           inutil
     2 kedge(ncli+1),iitab,jjtab)
c         print *,'apres cortab' !!!!!!!!!!!!!

 20       format(' ncli,corm(ncli+1),name(ncli+1) ',
     1             i5,f8.3,3x,a22)
           print 20,ncli,corm(ncli+1),name
          write(7,20)ncli,corm(ncli+1),name

      if(icutcor.eq.1.and.nm.eq.nfm)then
        if(corm(ncli+1).lt.cormin)then
        nm=ncli                    ! coupure
        write(7,*)' low correlation = end of scan'
        endif
      endif

40    format(12f6.3)
      ijcm=2*ijc+1

      if(icutcor.eq.1.and.nm.eq.nfm)then
        if(kedge(ncli+1).ne.0)then
          nm=ncli                    !   coupure
        write(7,52)ncli
        print 52,ncli
        endif
      endif

50    format(' ncli,  icb,  icor, jcor, corm',4i6,f8.3)
51    format(i3,2x,11i4) !     12i6)


52    format(' ncli=',i3,
     1       ' kedge=1 : jminus too large  or  ijc too small?',
     1       ' end of scan')
53    format(' correlations')
        npiv=ncli+1
      ica=icor(npiv)-5  !    1
      icb=icor(npiv)+5  !        2*ijc+1
      icpas=1 !4                   !1+(icb-1)/10
      jca=jcor(npiv)-5  !    1
      jcb=jcor(npiv)+5  !        2*ijc+1
      jcpas=1 !4            
 54   format(a22)
      write(3,54)name
      write(3,50)ncli,icb,icor(ncli+1),jcor(ncli+1),corm(ncli+1)
      write(2,50)ncli,icb,icor(ncli+1),jcor(ncli+1),corm(ncli+1)
      
      write(3,*)' '
      write(3,53)
      write(3,51)0,(i,i=ica,icb,icpas)
      write(3,*)' '
      do j=jca,jcb,jcpas
        do i=ica,icb,icpas
        kcor(i)=1000.*cor(i,j)
        enddo
        write(3,51)j,(kcor(i),i=ica,icb,icpas)
      enddo
c--------------------
c file correl.lis
          ipiva=icor(ncli+1)-1
          ipivb=ipiva+2
c      write(2,53)
c      write(2,51)0,(i,i=ipiva,ipivb)  !ica,icb,icpas)
          jpiva=jcor(ncli+1)-1
          jpivb=jpiva+2
      do j=jpiva,jpivb  !ica,icb,icpas      
        do i=ipiva,ipivb  !ica,icb,icpas
        kcor(i)=1000.*cor(i,j)
        enddo
        write(2,51)j,(kcor(i),i=ipiva,ipivb) !     ica,icb,icpas)


        
      enddo
c------
c      if(nm.eq.ncli)goto63

60    continue

61    format(/' Nombre de cliches correles =',i3)
c      print *,nm !!!!!!!!!!!!!!61->*
63    write(7,61)nm
c---------------------------------
c                                   calcul de i1(n) et jr(n)
62     if(nm.eq.1)then
       i1(1)=1
       i1i2=im-1
       jr(1)=0
        print *,'i1,i1i2,jr', i1(1),i1i2,jr(1)
       return
      endif
c
      kpiv=nm-1
        do n=2,nm
        xcor(n)=icor(n)-ijc
        ycor(n)=jcor(n)-ijc
        enddo

      if(maxdecal.ne.0) then
      call zerocorr(ijc,icor,jcor,kpiv,xcor,ycor)
c          --------
      endif

        sr=0.
        jr(1)=0
        sjr=0.
      do n=2,nm
        sr=sr+jrsom+ycor(n)
        jr(n)=sr-sjr+0.5
        sjr=sjr+jr(n)
c           jrsom=jm-pixstep
      enddo
c
      i1(1)=iminus+1+maxdecal
      sx=i1(1)
      si=i1(1)
      do n=2,nm
        sx=sx+xcor(n)
        i1(n)=i1(n-1)+sx-si+0.5
        si=si+i1(n)-i1(n-1)
      enddo
c
      imin=1000
      imax=0
      do n=1,nm
      imin=min0(imin,i1(n))
      imax=max0(imax,i1(n))
      enddo             
c
c      ipiv=maxdecal+iminus+1
c      if(ipiv.gt.imin)imin=ipiv
c      ipiv=im-maxdecal-iminus
c      if(imax.lt.ipiv)imax=ipiv
c
c      i1i2=imax-imin
       i1i2=im-2*iminus-1-imax+imin
c      i1i2=i1i2-imax+imin
       if(imin.lt.iminus+1)then
        idell=iminus+1-imin
        do n=1,nm
        i1(n)=i1(n)+idell
        enddo
       endif
c
c
70    format('ncli,  i1,i1i2,jr,kedge, corm(ncli) =',i4,2x,4i4,f8.3)
       do ncli=1,nm
       write(3,70)ncli,i1(ncli),i1i2,jr(ncli),kedge(ncli),corm(ncli)
       enddo
      return
      end
c-------------
      subroutine CORTAB(im,jrsom,ijc,corstep,jdel,iminus,jminus,
c                ------
     1                  tab1,tab2,cor,icc,jcc,corm,milcor,kedge,
     2                  iitab,jjtab)
      integer*2 tab1(iitab,jjtab),tab2(iitab,jjtab)
      dimension cor(201,201)
      integer corstep

      idel=corstep
      jdel=0
c                          modif 30 mars 2000 ecriture
      write(3,*)'debut cortab: jrsom,ijc,idel,jdel,iminus,jminus '
      write(3,*)               jrsom,ijc,idel,jdel,iminus,jminus
      write(3,*)' '
      
        kj1=1    !+jminus
        kj2=jrsom    !-jminus-jdel
        kj12=kj2-kj1
        ki1=1
        ki2=im-idel
        write(3,*) 'tab1,tab2'
        write(3,*)'  ki1,ki2 (tous les 50) ',ki1,ki2
        write(3,*)'  kj1,kj2 (tous les 1)  ',kj1,kj2
      write(3,5)
      do j=kj1,kj2   !,2     !,kj12
      write(3,4)(tab1(i,j),i=1,im-idel,50)
      enddo     

      write(3,6)
      do j=kj1,kj2   !,2     !,kj12
      write(3,4)(tab2(i,j),i=1,im-idel,50)
      enddo
c                          fin modif
c
1     format(10i5)
2     format(/)
c
c   differences premieres sur i
      idel=corstep
c
4     format(20i4)
5     format(' tab1:')
6     format(' tab2:')  
c      print *,'im,jrsom,ijcormax',im,jrsom,ijc
c
      den=im-idel
c
      do j=1,jrsom-jdel
        si1=0.
        si2=0.
c
        do i=1,im-idel
        kt1=tab1(i,j)
        kt2=tab1(i+idel,j+jdel)
          kpiv=kt2-kt1
          if(kt1.eq.32000.or.kt2.eq.32000)kpiv=0
          tab1(i,j)=kpiv
            si1=si1+kpiv
        kt1=tab2(i,j)
        kt2=tab2(i+idel,j+jdel)
          kpiv=kt2-kt1
          if(kt1.eq.32000.or.kt2.eq.32000)kpiv=0
          tab2(i,j)=kpiv
            si2=si2+kpiv
        enddo

          si1=si1/den
          si2=si2/den
        do i=1,im-idel
        tab1(i,j)=tab1(i,j)-si1
        tab2(i,j)=tab2(i,j)-si2
        enddo
c
      enddo
c---------------
      write(3,*)' '
      write(3,*) 'tab1,tab2, apres diff premieres et centrage'
        write(3,*)'  ki1,ki2 (tous les 50)  ',ki1,ki2
        write(3,*)'  kj1,kj2 (tous les 1) ',kj1,kj2
      write(3,5)
      do j=kj1,kj2     !,kj12
      write(3,4)(tab1(i,j),i=ki1,ki2,50)
      enddo     
      write(3,6)
      do j=kj1,kj2    !,kj12
      write(3,4)(tab2(i,j),i=ki1,ki2,50)
      enddo
c
      do i=1,101
      do j=1,101
      cor(i,j)=0.
      enddo
      enddo
c
c      jcopas=1+(jrsom-2*jminus)/20
       jcopas=1
      ijcm=2*ijc+1
        do jc=1,ijcm
        do ic=1,ijcm
        som=0.
        s1=0.
        s2=0.
        nt=0
          do12 j=kj1,kj2,jcopas
          jp=j+jc-ijc
          if(jp.lt.kj1.or.jp.gt.kj2)goto12
          somj=0.
          sj1=0.
          sj2=0.
            do10 i=iminus+1,im-idel-iminus  !,corstep
            ip=i+ic-ijc
            if(ip.lt.iminus+1.or.ip.gt.im-idel-iminus)goto10
            t1=tab1(i,j)
            if(t1.ge.32000.)goto10
            t2=tab2(ip,jp)
            if(t2.ge.32000.)goto10
            nt=nt+1
c              if(nt.gt.2000)goto10
            somj=somj+t1*t2
            sj1=sj1+t1**2
            sj2=sj2+t2**2
10          continue
          som=som+somj
          s1=s1+sj1
          s2=s2+sj2
12        continue
        piv=s1*s2
        if(piv.gt.0.)then
        cor(ic,jc)=som/sqrt(piv)
        else
        cor(ic,jc)=0.
        endif
      enddo
      enddo
c
      corm=0.
      do ic=1,ijcm
      do jc=1,ijcm
      corm=amax1(corm,cor(ic,jc))
        if(corm.eq.cor(ic,jc))then
        icc=ic
        jcc=jc
        endif
      enddo
      enddo
15    format(12f6.3)
c
      kedge=0
      if((icc-1)*(icc-ijcm)*(jcc-1)*(jcc-ijcm).eq.0)kedge=1
c      if((corm.lt.float(milcor)/1000.).or.(kedge.eq.1))then
c        icc=ijc
c        jcc=ijc
c        corm=0.
c      endif
      return
      end
c-----------------------
      subroutine lecj(jname,sundec,is,js,nm,jminus,jm,vmoy,
c                ----
     1                i1,jr,corm)
c                                           vmoy pour centrages q/p
      dimension corm(150)
      integer*4 i1(150),jr(150)
      integer sundec

      character*22 jname
      character*80 com80

1     format(a)
2     format(1x,a)
3     format(5i6)
4     format(2i6,f6.3)
5     format(f12.1)


      call openold22sf(jname,sundec,60)
c          -----------
      read(60,1)com80   ! 20
      write(3,1)com80

      read(60,1)com80   ! 20
      write(3,1)com80
      read(60,5)vmoy
      write(3,5)vmoy
      read(60,1)com80   ! 32
      write(3,1)com80
      read(60,3)is,js,nm,jminus,jm
      write(3,3)is,js,nm,jminus,jm
      read(60,1)com80    ! 32
      write(3,1)com80
c          print *,com80
        do n=1,nm
        read(60,4)i1(n),jr(n),corm(n)
          print 4,i1(n),jr(n),corm(n)
          write(3,4)i1(n),jr(n),corm(n)
        enddo

10      close(unit=60)
c
      return
      end
c----------------------------
      subroutine scorond(nfm,npo,dnanfp,sundec,jm,im,
c                -------
     1      lec,ltz,ltrjp,ond,poids,
     2      iminus,jminus,milsig,tabmoy,sigma,ll,   ilissond,
c                                                  ici sans dim
     3      iplot,ides,maxy,iitab,jjtab)
      integer*4 head(512),sundec
      integer*2 lec(2048)
      dimension ond(iitab,jjtab,3),poids(iitab,jjtab,3),tabmoy(60),
     1          sigma(60)
      character*22 dnanfp(500,2),name
c
         maxy=0
      i1=1+iminus
      i2=im-iminus
      j1=1+jminus
      j2=jm-jminus
      ltrj=iabs(ltrjp)
c          print*,' scorond: i1,i2,j1,j2,ltrj ',i1,i2,j1,j2,ltrj

        ablec1=-100000.
        ablec2=100000.
c 
      do j=1,jjtab
      do i=1,iitab
      ond(i,j,ides)=0.
      poids(i,j,ides)=0.
      enddo
      enddo
c                              1   calcul ablec (pour rejection milsig)
c                              2   calcul tabmoy
c                              3   calcul ond
c----------------------------------------------------   boucle moyond 
       do90 moyond=1,3 !!!!!!!!!!!!
c      print *,'                        valeur de moyond : ',moyond 
5     format(' moyond,milsig=',2i6)
      write(3,5)moyond,milsig

c--------------------------------------------------
c                                        etapes 2,3
            if(moyond.ne.1)then
              if(sijl.eq.0.)then
              print *,' No point left after rejection'
              stop
              endif
            tabmoy(ltz)=sijly/sijl
            sigma(ltz)=sijly2/sijl-tabmoy(ltz)**2
            sigma(ltz)=sqrt(sigma(ltz))
              ablec1=-sigma(ltz)*float(milsig)/1000.
c                                      ------
              ablec2=-ablec1
c          print *,'ltz,tabmoy,sigma,milsig,ablec1,ablec2'
c          print *, ltz,tabmoy(ltz),sigma(ltz),milsig,ablec1,ablec2
10    format(' ltz,tabmoy,sigma,milsig,ablec1,ablec2'/
     1        i3,2f8.0,i5,2f8.0)
      write(3,10)ltz,tabmoy(ltz),sigma(ltz),milsig,ablec1,ablec2
            endif
c----------------------------------------------------
c                                        etapes 1,2,3
c----------                       boucle X
           nm=nfm
c         print *,'avant do80 dans scorond nm,npo',nm,npo


         sijl=0.
         sijly=0.
         sijly2=0.
      do 80 ncli=1,nm

      write(3,*)' dme: ncli,dnanfp ',ncli,dnanfp(ncli,npo)
      iud=51
      name=dnanfp(ncli,npo)
      call openold22(name,sundec,iud)
c          ---------

      rewind(iud)
      read(iud)head

      lm=head(4)
c      write(3,*)' dme1: ncli,lm ',ncli,lm

c-------------------------------------------------
      if(moyond.eq.3)goto40
c                                       etapes 1,2

                  sijy=0.
                  sijy2=0.
                  sij=0.
        do35 l=1,ltz       ! on arrete la boucle au ltz qui est en argument
        do30 j=1,jm
           read(iud)(lec(i),i=1,im)
c               ---
c      write(3,*)' dme/scorond: l,j,lec(10) ',l,j,lec(10)
           if(l.ne.ltz)goto30

           if(j.lt.j1.or.j.gt.j2)goto30
                  si=0.
                  siy=0.
                  siy2=0.
c                     if(ll.gt.0)then
c                     suplec=0
c                     do i=i1,51
c                       piv=lec(i)
c                       suplec=amax1(piv,suplec)
c                     enddo
c                     do i=i2-51,i2
c                       piv=lec(i)
c                       suplec=amax1(suplec,piv)
c                     enddo
c                     endif
c                             if(j.eq.jm/2)print *,'suplec',suplec
15    format(' suplec,ll',f8.0,i5)
c                             if(j.eq.jm/2)write(3,15)suplec,ll
          do i=i1,i2
             if(lec(i).ne.32000)then
             piv=lec(i)
             kpiv=lec(i)
                maxy=max0(kpiv,maxy)
             test=-1.
c   -------------------------
               if(moyond.ne.1)then
               piv2=piv-tabmoy(ltz)
               test=(piv2-ablec1)*(piv2-ablec2)
               endif
c   -------------------------                          protus  inactif
c        if(ll.lt.0.or.(ll.gt.0.and.piv.gt.suplec/4..and.test.le.0.))
c     1        then
                 if(test.le.0.)then     ! rejection
                   siy=siy+piv
                   siy2=siy2+piv*piv
                   si=si+1.
                 endif
c              endif
c----------------------------
             endif
          enddo
                   sij=sij+si
                   sijy=sijy+siy
                   sijy2=sijy2+siy2 
30     continue
c              if(l.ne.ltz)goto35
                   sijl=sijl+sij
                   sijly=sijly+sijy
                   sijly2=sijly2+sijy2
35    continue
c
38    format(' sijl,sijly,sijly2=',3e12.3)
      write(3,38)sijl,sijly,sijly2

      goto70     !!!!!             ??????
c------------------------------------------------
c                                         etape 3   calcul ond

40    continue
c     print *,'passage a 40 avec ncli :',ncli
c
        do l=1,ltz    !  ltz
              jpm=1
          do 50 j=1,jm                    !  j
            read(iud)(lec(i),i=1,im)
c                ---
            if(l.ne.ltz)goto 50
c
               jtest=0
               if(j.ge.j1.and.j.le.j2)jtest=1

c              print*,' scorond: ltrj ',ltrj    ! tres grand si icorrec=2
c                                                          (non repliement)
            kpiv=(100*(j-j1))/ltrj
            jp=j-kpiv*float(ltrj)/100.
            jpm=max0(jp,jpm)
c
             do i=i1,i2                !  i
             if(jtest.eq.1)then
               piv=lec(i)-tabmoy(ltz)
45    format(' lec,tabmoy,ablec1,ablec2=',i6,3f8.0)
      if(i.eq.im/2.and.j.eq.jm/2)write(3,45)lec(i),tabmoy(ltz),
     1                           ablec1,ablec2
               if((piv-ablec1)*(piv-ablec2).le.0.)then  ! rejection
               poids(i,jp,ides)=poids(i,jp,ides)+1.
               ond(i,jp,ides)=ond(i,jp,ides)+piv
               endif
             endif
             enddo                    !  i
50         continue                      !  j
         enddo                        !  ltz
c      print *,'avant 70'

70      close(unit=iud)

80    continue
c       print *,'apres 80 continue'
c--------------------------------------------                                   
      if(moyond.ne.3)goto90
c                                                 etape 3
c        print*,' dme: moyond,jpm,ond(1,1,1),poids(1,1,1)',
c     1               moyond,jpm,ond(1,1,1),poids(1,1,1)
      do i=i1,i2
        do jp=j1,jpm
         if(poids(i,jp,ides).eq.0.)then
         ond(i,jp,ides)=0.
         else
         ond(i,jp,ides)=ond(i,jp,ides)/poids(i,jp,ides)
c        --------------
         endif
        enddo
      enddo
c
      if(j2.gt.jpm)then
      do i=i1,i2
        do j=jpm+1,j2
        kpiv=(100*(j-j1))/ltrj
        jpp=j-kpiv*float(ltrj)/100.
        ond(i,j,ides)=ond(i,jpp,ides)
        poids(i,j,ides)=poids(i,jpp,ides)
        enddo
      enddo
      endif
c
90    continue
c                                           fin boucle moyond
c-------------------------------------------------
c                                   Moy glissantes 
      call sgliss(ond,i1,i2,j1,j2,ides,ilissond,iitab,jjtab)
c          ------
c      print *, ' Fin sgliss'
100   format(/' ltz,j1,j2,ltrj,jpm,ides=',6i4,
     1      / ' j  ond(1,j) ond(im/2,j) ond(im,j)   poids...')
101   format(i4, 3f8.3, 6x,3f6.0,2f8.0)
      write(3,100)ltz,j1,j2,ltrj,jpm,ides
        ims2=im/2
      do j=1,jm,50
      write(3,101)j,ond(1,j,ides),ond(ims2,j,ides),ond(im,j,ides),
     1        poids(1,j,ides),poids(ims2,j,ides),poids(im,j,ides)
      enddo
c
      return
      end
c************************************************************
      subroutine scorond3(nfm,npo,dnanfp,sundec,jm,im,lm,
c                -------
     1      lec,ltrjp,sig,sigmoy,
     1      iminus,jminus,milsig,ll,ilissond,iplot,ides,jjtab) 
      integer*4 head(512),sundec
      integer*2 lec(2048),irejec(2048)
      dimension sig(jjtab,60,6),sigmoy(60,6),pds(200,60),ll(60),
     1          xdes(200),ydes(200),nr0(60)
      character*22 dnanfp(500,2)
      character*1 index(9)
      double precision s0,sx,sx2,sx3,sx4,sx5,sx6
      data index/'1','2','3','4','5','6','7','8','9'/

      write(3,*)' scorond3: im,jm,lm  iminus,jminus',
     1                      jm,im,lm, iminus,jminus 

        mm=6
      i1=1+iminus 
      i2=im-iminus
      j1=1+jminus
      j2=jm-jminus
      ltrj=iabs(ltrjp)
          
      do l=1,30
        do m=1,mm
          sigmoy(l,m)=0.
        enddo
      do j=1,jjtab
        pds(j,l)=0.
        do m=1,mm
          sig(j,l,m)=0.
        enddo
      enddo
      enddo
c-------------------------------   calculs des moments centres
      do86 moyond=1,2

c-------------------------------------                boucle nfm

      do 80 ncli=1,nfm

      iud=51
      call openold22(dnanfp(ncli,npo),sundec,iud)
c          ---------
      rewind(iud)
      read(iud)head

      lm=head(4)
c      print *,'lm :',lm

c-------------------------------------------------
        do35 l=1,lm
c               write(3,*)' scorond3: moyond,l,xmoy(1) ',
c     1                        moyond,l,sig(1,l,1)
           nr0(l)=0
        do30 j=1,jm
           read(iud)(lec(i),i=1,im)
c               ---
        call srejec(lec,i1,i2,milsig,irejec,nrejec)
c            ------
          nr0(l)=nr0(l)+nrejec
           if(j.lt.j1.or.j.gt.j2)goto30

                if(moyond.eq.2)xmoy=sig(j,l,1)
           s0=0
           sx=0
           sx2=0
           sx3=0
           sx4=0
           sx5=0.
           sx6=0.

         do25 i=i1,i2
            if(irejec(i).eq.1)goto 25

             if(lec(i).ne.32000)then
             x=lec(i)            

            if(moyond.eq.1)then
              s0=s0+1.
              sx=sx+x

            else
              x=x-xmoy
                      x2=x*x
              sx2=sx2+x2
c                      x3=x2*x
c              sx3=sx3+x3
c                      x4=x3*x
c              sx4=sx4+x4
c                      x5=x4*x
c              sx5=sx5+x5
c                      x6=x5*x
c              sx6=sx6+x6

            endif
            endif

25       continue

        if(moyond.eq.1)then
        pds(j,l)=pds(j,l)+s0
        sig(j,l,1)=sig(j,l,1)+sx

        else
          sig(j,l,2)=sig(j,l,2)+sx2
          sig(j,l,3)=sig(j,l,3)+sx3
          sig(j,l,4)=sig(j,l,4)+sx4
          sig(j,l,5)=sig(j,l,5)+sx5
          sig(j,l,6)=sig(j,l,6)+sx6
        endif


30     continue
35    continue

36    format(' scorond3: ncli,nrejec(l) ',20i5)
        if(moyond.eq.1)write(7,36) ncli,(nr0(l),l=1,lm)
70      close(unit=iud)

80    continue
c       print *,'apres 80 continue'
c-----------
      do85 l=1,lm
c        if(ll(l).ne.1)goto85

      do j=1,jm
           if(pds(j,l).eq.0)then
                     pds(j,l)=1.
                     print*,' attention pds=0'
                     write(3,*)' attention pds=0'
            endif

        if(moyond.eq.1)then
            if(pds(j,l).eq.0)then
                     pds(j,l)=1.
                     print*,' attention pds=0'
                     write(3,*)' attention pds=0'
            endif
        sig(j,l,1)=sig(j,l,1)/pds(j,l)
        
        else
          do84 m=2,mm
          piv=sig(j,l,m)/pds(j,l)
          expo=1./float(m)
          isign=1
            if(piv.lt.0.)then
c                                 modif 31 mars 2000
c            write(3,*)' attention moment.lt.0 '
c            print*,' attention moment.lt.0 '
c            sig(j,l,m)=1.
c            goto84
                     isign=-1
                     piv=-piv
            endif
          sig(j,l,m)=isign*piv**expo
84        continue
        endif
      enddo

85    continue
86    continue
c-----------------   fin boucle moyond

87    format(/'     l     m  sigmoy   sig(j=1,jm,10)')
88    format(2i6,           f8.0, 2x,    10f6.0)

      do l=1,lm
      do m=1,mm
        piv=0.
          do j=5,jm-4
          piv=piv+sig(j,l,m)
          enddo
        sigmoy(l,m)=piv/float(jm-8)
      enddo
      enddo
c-------
        do l=1,lm
                  write(3,87)
        do m=1,mm
        write(3,88)l,m,sigmoy(l,m),(sig(j,l,m),j=1,jm,20)
        enddo
        enddo
c--------
      if(ides.eq.1.and.iplot.ne.0)then
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)
      call pglabel('coupes/j pour l=1,lm','moments',
     1             'moyennes (1) et moments centres (2 a 6) ')

      jm2=jm/2
c                     modif 30 mars 2000
      ymin=10000.
      ymax=-10000.
        do l=1,lm
        do m=1,mm
        do j=1,jm
          ymin=amin1(ymin,sig(j,l,m))
          ymax=amax1(ymax,sig(j,l,m))
        enddo
        enddo
        enddo
c                     fin modif 
c      piv1=sig(jm2,lm,1)
c      piv2=sig(jm2,lm,mm)
c      ymax=1.2*amax1(piv1,piv2)

c      piv1=sig(jm2,1,1)
c      piv2=sig(jm2,lm,3)
c      ymin=amin1(piv1,piv2)

      do j=1,jm
      xdes(j)=j
      enddo

      xjm=float(jm)

      do l=1,lm
        xmin=0.1+(l-1)*0.8/float(lm)
        xmax=0.1+l*0.8/float(lm)
        call pgvport(xmin,xmax,0.1,0.8)

        call pgwindow(1.,xjm,ymin,ymax)
c      if(l.eq.1)then
c                                                 modif 30 mars 2000 100->500
        call pgbox('bcnts',20.,2,'bcnts',500.,5)
c      else
c        call pgbox('bcnts',20.,2,'bcnts',500.,5)
c      endif

        do m=1,mm
                          xind=jm2
                          yind=sig(jm2,l,m)+4.
                          call pgtext(xind,yind,index(m))
        call pgsls(m)
          do j=1,jm
          ydes(j)=sig(j,l,m)
          enddo
        call pgline(jm,xdes,ydes)
        enddo
          call pgsls(1)
      enddo
      endif
c      if(iplot.eq.1)call pgend
c-------------          
      return
      end
c***************************************
      function cormom(y,sig,sigmoy,j,l,modeg,jjtab)
c              ------

      dimension sig(jjtab,60,6),sigmoy(60,6)
c                                            modif 1 avril 2000
      cormom=sigmoy(l,1)+(y-sig(j,l,1))
c     1                   *(sigmoy(l,modeg)/sig(j,l,modeg))

      return
      end
c***************************************
      subroutine srejec(lec,i1,i2,milsig,irejec,nr)
c                ------
      integer*2 lec(2048),irejec(2048)

        nr=0
      seuil=0.001*milsig
      seuil=seuil*seuil
      den=i2-i1+1
      sx=0.
      sx2=0.

      do i=i1,i2
      sx=sx+lec(i)
      enddo
      xmoy=sx/den

      do i=i1,i2
      x=lec(i)-xmoy
      sx2=sx2+x*x
      enddo
      sig2=sx2/den

      do i=i1,i2
        irejec(i)=0
      x=lec(i)-xmoy
      s=x*x/sig2
      if(s.gt.seuil)then
        irejec(i)=1
        nr=nr+1
      endif
      enddo

c      write(3,*)' irejec ',(irejec(i),i=i1,i2)
      return
      end
c*************************************************************
      subroutine sgliss(x,i1,i2,j1,j2,ides,ilissond,iitab,jjtab)
c                ------
      dimension x(iitab,jjtab,3),xx(2000)
      liss=ilissond
      liss2=2*liss+1
      fliss2=float(liss2)
c
c      print *,'i1,i2,j1,j2,ides',i1,i2,j1,j2,ides
        do j=j1,j2
          do i=i1,i2
          xx(i)=x(i,j,ides)
          if(xx(i).eq.0..and.i.ne.i1)xx(i)=xx(i-1)
          enddo

        s=0.
          do i=i1,i1+liss2-1
          s=s+xx(i)
          enddo

        do i=i1,i1+liss
        x(i,j,ides)=s/fliss2
        enddo
c             if(j.eq.1)print *,' i,somme',i,s

        do i=i1+liss+1,i2-liss
        s=s+xx(i+liss)-xx(i-liss-1)
        x(i,j,ides)=s/fliss2
        enddo
c
        do i=i2-liss+1,i2
        x(i,j,ides)=x(i2-liss,j,ides)
        enddo

      enddo
      return
      end
c*****************************************************************
      subroutine sdescor(ijc,icor,jcor,corm,nm,xcor,ycor)
c                -------
      dimension x(150),y(150),icor(150),jcor(150),corm(150),
     1          xcor(150),ycor(150),x1(150)

      call pgvport(0.1,0.2,0.1,0.8)
        ynm=nm
        if(nm.lt.2)ynm=2.
c                             modif 1 avril 2000 ...
      call pgwindow(0.,1.1,1.,ynm)
      call pgbox('bcnst',0.5,5,'bcnst',0.,0)
c      call pglabel(' ',' ','Correl max / pix(i) / pix(j)')
c
      if(nm.lt.2)return
      nm1=nm-1
      do n=1,nm1
      y(n)=n+1
      x(n)=corm(n+1)
      enddo
      call pgline(nm1,x,y)
c
c      xmax=ijc
      xmax=15.
      call pgvport(0.2,0.4,0.1,0.8)
      call pgwindow(-xmax,xmax,1.,ynm)
c                         modif 31 mars 2000      5.
      call pgbox('abcnst',10.,5,'abcst',0.,0)
c
      do n=1,nm
      x(n)=icor(n)-ijc
      x1(n)=xcor(n)
      y(n)=n
      enddo
      call pgsls(2)
      call pgline(nm,x1,y)
      call pgsls(1)
      call pgline(nm,x,y)
c
      call pgvport(0.4,0.6,0.1,0.8)
      call pgwindow(-xmax,xmax,1.,ynm)
c                         modif 31 mars 2000      5.
      call pgbox('abcnst',10.,5,'abcmst',0.,0)
c
      do n=1,nm
      x(n)=jcor(n)-ijc
      x1(n)=ycor(n)
      enddo
      call pgsls(2)
      call pgline(nm,x1,y)      
      call pgsls(1)
      call pgline(nm,x,y)
c
      return
      end
c********************************************************
      subroutine sdesond(ond,poids,im,jm,den,tabmoy,lvp,lv,iitab,jjtab,
c                -------   (tabmoy pour normalisation intensites dessins)
     1                   icorrec,jq1,jq2)
c dessin des tableaux lvp et lv
c    3 coupes en i de chacun
c          
      dimension x(2000),y(200),ond(iitab,jjtab,3),
     1          poids(iitab,jjtab,3),xp(200),tabmoy(90)
      character*2 clvp,clv
c     dimension icoupe(3)
      write(clvp(1:2),'(i2)')lvp
      write(clv (1:2),'(i2)')lv
        write(3,*)' dme: clvp,clv ',clvp,clv

      call pgadvance
c
             xjm=jm
c
c      print *,'avant boucle50' !!!!!!!!!!!!!
c      print *,'den : ',den !!!!!!!!!!

      call pgvport(0.1,0.9,0.1,0.8)
      call pglabel('coupes .1 .5 .9*im',
     1'Tableau'//clvp//' (int)   ecarts/moy    Tableau'//clv//
     2 ' (vit ou int)','Corrections')
c---------
      do 50 idess=1,2
      ypiv1=0.1+0.35*(idess-1)
      ypiv2=ypiv1+0.35
c
      do icoup=1,3
          do j=1,jm
          y(j)=j
          enddo
        xpiv1=0.1+(icoup-1)*0.8/3.
        xpiv2=xpiv1+0.8/3.
      call pgvport(xpiv1,xpiv2,ypiv1,ypiv2)

c        if(idess.eq.1)then
c        piv1=-0.30
c        piv2=0.30
c        else
c        piv1=-1500.
c        piv2=1500.
c        endif

        i=im*(0.1+0.4*(icoup-1))
             xmin=100000.
             xmax=-100000.
          do j=1,jm
          x(j)=ond(i,j,idess)
c             print *,'tabmoy(idess) :',tabmoy(idess) !!!!!!!!!!!!!
c            if(idess.eq.1)x(j)=x(j)/tabmoy(idess)
          xp(j)=poids(i,j,idess)
             xmin=amin1(xmin,x(j))
             xmax=amax1(xmax,x(j))
          enddo
             xmin=xmin-(xmax-xmin)/5.
             xmax=xmax+(xmax-xmin)/5.
      if(icorrec.eq.5.or.xmin.eq.xmax)then
        xmin=0.
        xmax=2.
      endif
          call pgwindow(xmin,xmax,1.,xjm)
        if(idess.eq.1)then
          call pgbox('bcnts',0.,0,'bcnts',0.,0)
        else
          call pgbox('bcmts',0.,0,'bcnts',0.,0)
        endif
        call pgline(jm,x,y)
c
      call pgwindow(0.,2.,1.,xjm)
      call pgsls(2)
      call pgline(jm,xp,y)
c
      x(1)=0.
      x(2)=2.
      y(1)=jq1
      y(2)=jq1
      call pgline(2,x,y)
      y(1)=jq2
      y(2)=jq2
      call pgline(2,x,y)
      call pgsls(1)
      enddo
50    continue
      return
      end
c------------------------------------------
      subroutine zerocorr(ijc,icor,jcor,nm1,xcor,ycor)
c                --------
      dimension icor(150),jcor(150),xcor(150),ycor(150)
c
        nm=nm1+1
      if(nm1.le.2)then
        do n=2,nm
        xcor(n)=icor(n)-ijc
        ycor(n)=jcor(n)-ijc
        enddo
      xcor(1)=0.
      ycor(1)=0.
      icor(1)=ijc
      jcor(1)=ijc
      return
      endif
c
      na=2
      nb=nm
      ab=nb-na+1.
      siab=0.
      sjab=0.
c
      icor(1)=ijc
      jcor(1)=ijc
      xcor(1)=0.
      ycor(1)=0.
        do n=1,nm1
        xcor(n+1)=icor(n+1)-ijc
        ycor(n+1)=jcor(n+1)-ijc
        enddo
c
      do n=na,nb
      siab=siab+xcor(n)
      sjab=sjab+ycor(n)
      enddo
        siab=siab/ab
        sjab=sjab/ab
c
      do n=2,nm
      xcor(n)=xcor(n)-siab
      ycor(n)=ycor(n)-sjab
      enddo
c
10    format('   icor-ijc/ xcor/ jcor-ijc/ ycor')
      write(3,10)
11    format(i10,f6.2,i10,f6.2)
      do n=1,nm
        kpiv1=icor(n)-ijc
        kpiv2=jcor(n)-ijc
      write(3,11)kpiv1,xcor(n),kpiv2,ycor(n)
      enddo
c      
      return
      end
c-------------------------------------------
c      subroutine srejec(tab,is,js,milsig,ymoy)
c                ------
c      integer*2 tab(1024,4000)
c
c      double precision sy,sy2,pds
c
c        ymoy=0.
c        ec=0.
c      do n=1,5
c      sy=0.
c      sy2=0.
c      pds=0.
c      do i=1,is,5
c      do j=1,js,5
c        y=tab(i,j)
c        if(y.ne.32000.)then
c          if(n.eq.1.or.abs(y-ymoy).lt.ec) then
c          sy=sy+y
c          sy2=sy2+y*y
c          pds=pds+1.
c          endif
c        endif
c      enddo
c      enddo
c
c        if(pds.eq.0.)then
c          print *,' Rejection impossible pour le calcul de moyennes'
c          return
c        endif
c
c        ymoy=sy/pds
c        var=sy2/pds-ymoy*ymoy
c        var=sqrt(var)
c        ec=(var/1000.)*float(milsig)
c        print *,' rejec: n,ymoy,pds,var,ec ='
c        print *,         n,ymoy,pds,var,ec
c      enddo
c
c      return
c      end
c************************************************
      subroutine newscan(priscan,kburst,nxmtot,dnap,nbu,nxm,dnanfp)
c                -------
      integer burst,priscan
      integer*2 nxb(500),nx(500),k25(25),k125(25),k225(25)
      character*22 dnap(500,2),dnanfp(500,2)
c     integer*2 nxt(500)
      data k25/4,9,14,19,24,  5,10,15,20,25,  1,6,11,16,21, 
     1        3,8,13,18,23,    2,7,12,17,22/           
c      data k125/1,2,3,4,5,   10,9,8,7,6,   11,12,13,14,15, 
c     1     20,19,18,17,16,     21,22,23,24,25/   
      data k125/20,19,18,17,16,  21,22,23,24,25,  1,2,3,4,5,  !25 
     1      11,12,13,14,15,   10,9,8,7,6/
      data k225/1,2,3,4,5,  10,9,8,7,6,      11,12,13,14,15,   
     1     20,19,18,17,16,     21,22,23,24,25 /

      burst=1
      nbu=1
      nxm=nxmtot/burst
      do n=1,nxm
      nxb(n)=nbu+burst*(n-1)    !    n
      nx(n)=nxb(n)              !   n
      enddo

      if(priscan.eq.0)goto10 

      nxc=(nxm+1)/2   ! 3 
      nxa=nxm-nxc     ! 2
c----
      if(priscan.eq.1000)then      !   1 prisme
         nx(1)=nxb(1)
      endif
c----
      if(priscan.eq.2000)then      !   2 prismes successifs
         nx(1)=nxb(1)
         nx(2)=nxb(2)
      endif
c----------
      if(priscan.eq.2031)then      !   2 prismes  3 puis 1
         nx(1)=nxb(3)
         nx(2)=nxb(1)
      endif      
c-----------
      if(priscan.eq.3000)then   !   3 prismes   2,3,1
         nx(1)=nxb(2)
         nx(2)=nxb(3)
         nx(3)=nxb(1)
      endif
c-----------------------------      
      if(priscan.eq.-1)then
        write(7,*)' Champs alternativement droite-gauche'
c  n1  1   2   3   4   5
c  n2  5   3   1   2   4
      nx(nxc)=1
      do nd=1,nxa
        n1=nxc-nd   !  2  1
        n2=1+2*nd   !  3  5
        nx(n1)=nxb(n2)
      enddo
      do nd=1,nxa
        n1=nxc+nd   !  4  5
        n2=2*nd     !  2  4
        nx(n1)=nxb(n2)
      enddo
      endif
c----
      if(priscan.eq.1)then
c                            Pic du Midi
        write(7,*)' Champs alternativement gauche-droite'
c  n1  1   2   3   4   5
c  n2  4   2   1   3   5
      nx(nxc)=1
      do nd=1,nxa
        n1=nxc-nd   !  2  1
        n2=2*nd     !  2  4
        nx(n1)=nxb(n2)
      enddo
      do nd=1,nxa
        n1=nxc+nd   !  4  5
        n2=1+2*nd   !  3  5
        nx(n1)=nxb(n2)
      enddo
      endif
c----
      if(priscan.eq.4)then
c                     Tour Meudon
      nx(1)=nxb(2)
      nx(2)=nxb(3)
      nx(3)=nxb(1)
      nx(4)=nxb(5)
      nx(5)=nxb(4)
      endif
c----
      if(priscan.eq.25)then  ! Meudon chariot-prismes
      do k=1,25
        kpiv=k25(k)
      nx(k)=nxb(kpiv)
      enddo
      endif
c----
      if(priscan.eq.125)then  ! Meudon chariot-prismes
      do k=1,25   !5
        kpiv=k125(k)
      nx(k)=nxb(kpiv)
      enddo
      endif
c----
      if(priscan.eq.225)then  ! Meudon chariot-prismes
      do k=1,25   !5
        kpiv=k225(k)
      nx(k)=nxb(kpiv)
      enddo
      write(3,*)'priscan 225 chariot: nx(k)= ',(nx(k),k=1,25)
      endif

10    do n=1,nxm
      np=nx(n)
      dnanfp(n,1)=dnap(np,1)
      enddo
c----
      if(priscan.eq.23000)then
      nx(1)=nxb(2)
      nx(2)=nxb(3)
      endif
c----
      write(3,*)' nx(n),n=1,nxm ',(nx(n),n=1,nxm)
      return
      end
c*******************************************
      subroutine bisdme(piv1,piv2,fac,piv,tabmoy)
c                ------
      if(piv1.ne.32000)then
        if(piv2.ne.32000)then
           piv=(1.-fac)*piv1+fac*piv2
        else
           piv=piv1
        endif
      else
        if(piv2.ne.32000)then
          piv=piv2
        else
          piv=tabmoy
        endif
      endif
      return
      end
c*********************************************
      subroutine ijlis2(tab,is,js,iitab,jje)
c                ------                       appelle klis dans cmd1.f
      integer*2 tab(1500,1500)   !iitab,jje)
      integer*4 mem(2000)

      call par1('  ilisqp',1,ilis)      ! non
      call par1('  jlisqp',1,jlis)

      if(ilis.gt.1)then
        do j=1,js
          do i=1,is
          mem(i)=tab(i,j)
          enddo
        call klis(mem,is,ilis)
c            ----
          do i=1,is
          tab(i,j)=mem(i)
          enddo
        enddo
      endif

      if(jlis.gt.1)then
        do i=1,is
          do j=1,js
          mem(j)=tab(i,j)
          enddo
        call klis(mem,js,jlis)
c            ----
          do j=1,js
          tab(i,j)=mem(j)
          enddo
        enddo
      endif

      return
      end
c----------------------------------
      subroutine bord_exp(tab,is,js,iitab,jje,invi,invj,jqp1,jqp2,
c                --------
     1                    milsec)
      integer*2 tab(iitab,jje)

      call par1('  x0bord',1,kpiv)   ! pour intensites spicules 1er tableau
        x0bord=kpiv
      call par1('  y0bord',1,kpiv)
        y0bord=kpiv
      call par1('   hbord',1,kpiv)   ! echelle de hauteur spicules
        hbord=kpiv
      call par1('  ampmax',1,kpiv)
        ampmax=kpiv

      a2=2.*960000.*y0bord
      a=sqrt(a2)
      write(3,*)' a ',a

        x0=x0bord
        if(invi.eq.1)x0=(is-1)*milsec-x0

      do i=1,is
        x=(i-1)*milsec-x0
        y=(a+x)*(a-x)/(2.*960000.)

      do jp=1,js
         j=jp
         if(invj.eq.1)j=js-jp+1
           yj=milsec*(j-1)-y
         ampli=exp(yj/hbord)
         if(ampli.gt.ampmax)ampli=ampmax

         kpiv=tab(i,j)*ampli
         if(kpiv.gt.32000)kpiv=32000
         tab(i,j)=kpiv

      if(j.eq.js)then
        if(i-10*(i/10).eq.0)
     1    write(3,*)' i,y,ampli,tab(i,js) ',
     2                i,y,ampli,tab(i,js)
      endif

      enddo
      enddo

      return
      end
c--------------------------------------------
      subroutine cheveux(tab,iitab,jje,is,js,invi,invj)
c                -------                          inusite
      integer*2 tab(iitab,jje)
      dimension ihair(10),iw2hair(10)

      do k=1,10
      call par1('    hair',k,ihair(k))
      if(ihair(k).eq.0)then
        km=k-1
        goto 5
      endif
      call par1('  w2hair',k,iw2hair(k))
      enddo
 5    continue

      if(ihair(1).eq.0)return

      call par1('    ipos',1,ipos)
        if(ipos.eq.0)ipos=1
      call par1('   istep',1,istep)
      call par1('  milsec',1,milsec)
        ipas=float(istep)/float(milsec)+0.5

      do k=1,km
      do ip=1,ipos
        if(invi.eq.0)then
          i0=ihair(k)+ipas*(ip-1)
        else
          i0=is-ihair(k)+1-ipas*(ip-1)
        endif
        i1=i0-iw2hair(k)
        i2=i0+iw2hair(k)

      do j=1,js
        t1=tab(i1-1,j)
        t2=tab(i2+1,j)
        dt=(t2-t1)/(i2-i1+2)
        do i=i1,i2
        t=t1+dt*(i-i1+1)
          if(t.ge.0.)then
          tab(i,j)=t+0.5
          else
          tab(i,j)=t-0.5
          endif
        enddo
      enddo

      enddo
      enddo

      return
      end
c----------------------------------------------
      subroutine scorond5(nfm,npo,dnanfp,sundec,jm,im,
c                --------
     1      lec,ltz,ltrjp,ond,poids,
     2      iminus,jminus,milsig,tabmoy,sigma,ll,   ilissond,
c                                                  ici sans dim
     3      iplot,ides,maxy,iitab,jjtab,kind,nquv)
      integer*4 head(512),sundec
      integer*2 lec(2048),lecij(2000,200)
      dimension ond(iitab,jjtab,3),poids(iitab,jjtab,3),tabmoy(60),
     1          sigma(60),kind(60),tmoyin(2000,500)
      character*22 dnanfp(500,2),name
c
 5    format(a)
      write(3,*)' debut scorond5  nfm=',nfm

      i1=1+iminus
      i2=im-iminus
      j1=1+jminus
      j2=jm-jminus
           j1j2=j2-j1
        j3=j1+j1j2/2
        j4=j2-j1j2/2
      write(3,*)' i1,i2,j1,j2 ',i1,i2,j1,j2

      do i=i1,i2
      do j=j1,j2
      ond(i,j,ides)=1.
      poids(i,j,ides)=1.
      enddo
      enddo
        if(kind(ltz).ne.0)goto 105
c------------
c                     calcul tmoyin(i,ncli)
      do ncli=1,nfm
      do i=1,im
       tmoyin(i,ncli)=0.
      enddo
      enddo
c                                        boucle ncli
      do 50  ncli=1,nfm 
      iud=51
      name=dnanfp(ncli,npo)
      write(3,5)name
      call openold22(name,sundec,iud)
c          ---------

      rewind(iud)
      read(iud)head
      lm=head(4)
        do 20 l=1,ltz                  ! boucle l jusqu'a ltz
         do 10 j=1,jm
            read(iud)(lecij(i,j),i=1,im)
c                ---
 10      continue
            if(l.ne.ltz)goto 20
c                                         moy  tmoy(i,n) sur j
              den=float(j4-j3+1)
            do i=i1,i2
              piv=0.
            do j=j3,j4
              xpiv=lecij(i,j)
              piv=piv+abs(xpiv)
            enddo
              tmoyin(i,ncli)=tmoyin(i,ncli)+piv/den
            enddo
 20     continue

c      do i=i1,i2
c      tmoyin(i,ncli)=sqrt(tmoyin(i,ncli))
c      enddo

      write(3,*)' ncli',ncli,' tmoyin '
      write(3,*)(tmoyin(i,ncli),i=i1,i2,100)
      close(iud)
 50   continue
c-----------------------------   calcul ond(i,j,ides)
        den=float(nfm)
      do i=i1,i2
      do j=j1,j2
      ond(i,j,ides)=0.
      enddo
      enddo

      do 100  ncli=1,nfm     ! boucle ncli 
      iud=51
      name=dnanfp(ncli,npo)
      call openold22(name,sundec,iud)
c          ---------

      rewind(iud)
      read(iud)head
c      lm=head(4)
        do 70 l=1,ltz                  ! boucle l jusqu'a ltz
         do 60 j=1,jm
            read(iud)(lecij(i,j),i=1,im)
c                ---
 60      continue
            if(l.ne.ltz)goto 70

           do i=i1,i2
            ondpiv=tmoyin(i,ncli)
c                           ----
            do j=j1,j2
            xpiv=lecij(i,j)
            ond(i,j,ides)=ond(i,j,ides)+abs(xpiv)/(ondpiv*den)
c           ------------                                  nfm
            poids(i,j,ides)=1.
            enddo
           enddo
 70     continue
      close(iud)
 100  continue
 
c      do i=i1,i2
c      do j=j1,j2
c      piv=ond(i,j,ides)
c      ond(i,j,ides)=sqrt(piv)
c      enddo
c      enddo

      write(3,*)' ltz ',ltz,' ond(i,j,ides) '
 102  format(10f6.3)
      do j=j1,j2,10
      write(3,102)(ond(i,j,ides),i=i1,i2,100)
      enddo     
c--------------------------------
c                                   Moy glissantes 
 105  call sgliss(ond,i1,i2,j1,j2,ides,ilissond,iitab,jjtab)
c          ------
c      print *, ' Fin sgliss'
110   format(/' ltz,j1,j2,ltrj,jpm,ides=',6i4,
     1      / ' j  ond(1,j) ond(im/2,j) ond(im,j)   poids...')
111   format(i4, 3f8.3, 6x,3f6.0,2f8.0)
      write(3,110)ltz,j1,j2,ltrj,jpm,ides
        ims2=im/2
      do j=1,jm,50
      write(3,111)j,ond(1,j,ides),ond(ims2,j,ides),ond(im,j,ides),
     1        poids(1,j,ides),poids(ims2,j,ides),poids(im,j,ides)
      enddo
c
      return
      end
c************************************************************
      subroutine xcentre(ijtab,i1,i2,j1,j2,kdisc,iitab,jje,milsigc,iv)
      integer*2 ijtab(iitab,jje),kdisc(iitab,jje)
c                                disque 1 fond 0 protu -1
      dimension xmoy(5000),deni(5000),sigj(5000)
      double precision discmoy,denj

      if(iv.eq.0)return

      seuil=float(milsigc)/1000.
c     seuil= ecart/sigma  max pour calcul moyennes
           print *,'seuil',seuil

c  calcul xmoy(j)
      do 10 j=j1,j2          ! boucle j
       xmoy(j)=0.
c piv
       kpiv=0
       deni(j)=0.
      do i=i1,i2
         if(kdisc(i,j).eq.1)then
             kpiv=kpiv+ijtab(i,j)
             deni(j)=deni(j)+1.
         endif
      enddo
         if(deni(j).eq.0.)goto 10
         kpiv=float(kpiv)/deni(j)
c sig
      do iter=1,2
        sig=0.
      do i=i1,i2
         if(kdisc(i,j).eq.1)sig=sig+(ijtab(i,j)-kpiv)**2
      enddo
         sig=sig/deni(j)
         sig=sqrt(sig)
      sigj(j)=sig
c xmoy
         deni(j)=0.
      do i=i1,i2
             ecart=ijtab(i,j)-kpiv
             s=abs(ecart)/sig
         if(kdisc(i,j).eq.1.and.s.lt.seuil)then
            xmoy(j)=xmoy(j)+ijtab(i,j)
            deni(j)=deni(j)+1.
         endif
      enddo
      if(deni(j).ne.0.)xmoy(j)=xmoy(j)/deni(j)
      kpiv=xmoy(j)
      enddo    !  iter
 10   continue                     !    j
        print *,'disc:  sig ',(sigj(j),j=j1,j2,50)

c calcul discmoy
        denj=0.
        discmoy=0.
      do j=j1,j2
      if(deni(j).ne.0.)then
            discmoy=discmoy+xmoy(j)*deni(j)
            denj=denj+deni(j)
      endif
      enddo
        discmoy=discmoy/denj
c centrage
      do j=j1,j2
        dy=0.
        if(deni(j).ne.0.)then
           if(iv.eq.1)dy=xmoy(j)-discmoy
           if(iv.eq.2)dy=xmoy(j)
        endif
         kdy=dy
      do i=i1,i2
        if(kdisc(i,j).eq.1)ijtab(i,j)=ijtab(i,j)-kdy
      enddo
      enddo

      if(iv.eq.2)then
c------  protus
c promoy
        promoy=0.
        kproden=0
      do i=i1,i2
      do j=j1,j2
       if(kdisc(i,j).eq.-1.)then
         promoy=promoy+ijtab(i,j)
         kproden=kproden+1
       endif
      enddo
      enddo
        if(kproden.ne.0)promoy=promoy/float(kproden)
        kpiv=promoy
c sigma
      do iter=1,2    !    iter
        sig=0.
        kdsig=0
      do i=i1,i2
      do j=j1,j2
         if(kdisc(i,j).eq.-1.)then
             sig=sig+(ijtab(i,j)-kpiv)**2
             kdsig=kdsig+1
         endif
      enddo
      enddo
         if(kdsig.ne.0)sig=sig/float(kdsig)
         sig=sqrt(sig)
c promoy
         promoy=0.
         kproden=0
      do i=i1,i2
      do j=j1,j2
             ecart=ijtab(i,j)-kpiv
             s=abs(ecart)/sig
         if(kdisc(i,j).eq.-1.and.s.lt.seuil)then
            promoy=promoy+ijtab(i,j)
            kproden=kproden+1
         endif
      enddo
      enddo
      if(kproden.ne.0)promoy=promoy/float(kproden)
      kpiv=promoy
        print *,'iter,kdsig,sig,kpiv',iter,kdsig,sig,kpiv
       enddo    !  iter
c centrage
      do j=j1,j2
      do i=i1,i2
        if(kdisc(i,j).eq.-1.) ijtab(i,j)=ijtab(i,j)-promoy
      enddo
      enddo
      endif   !  iv

      print *,' iv ',iv
      print *,'kdisc  ',(kdisc(i,50),i=i1,i2,50)
      print *,'deni ',(deni(j),j=j1,j2,50)
      print *,'xmoy ',(xmoy(j),j=j1,j2,50)
      print *,'denj,discmoy   ',denj,discmoy
      print *,'kproden,promoy  ',kproden,promoy

      print *,' '
      return
      end
c----------------------------------
      subroutine scatsky(tab,iitab,jje,is,jqp1,jqp2,invi,milsec,
     1                   xsky1,xsky2)
      integer*2 tab(iitab,jje),kdif(500)

      pix=float(milsec)/1000.
      if(invi.eq.0)then
        mean1=xsky1/pix+1
        mean2=xsky2/pix+1
      else
        mean1=is-xsky2/pix
        mean2=is-xsky1/pix
      endif  !  invi
        den=mean2-mean1+1.

      do j=jqp1,jqp2
          dif=0.
        do i=mean1,mean2
          dif=dif+tab(i,j)
        enddo
        kdif(j)=dif/den+0.5

        do i=1,is
        tab(i,j)=tab(i,j)-kdif(j)
        if(tab(i,j).le.0)tab(i,j)=1
        enddo
      enddo  !  j

      do j=jqp1,jqp2,20
      write(3,*)'scatsky: mean1,mean2,j,kdif(j)  ',mean1,mean2,j,kdif(j)
      enddo
      return
      end
c===============================================
      subroutine gray1(iqp,iu,name,page1,page2,hfin,nfsm,milsec,
c                ----      52
     1     tab,iitab,jje,igray,lmh)
c                              lm head      
c               dimensions max cartes = iitab,jje
c------------------------------------------------------------- 
      double precision moy
      dimension black(60),white(60),tab(iitab,jje)
      integer*4 bl(60),wh(60),zer(60),page1,page2,llb(60),
     1          ctrast(60)
      integer*4 head(512),headmean(512)
      integer*2 lec(2048),mmoy(60)
      character name*22,meanname*26
      character*8 hfin
      maxdim=jje
      lmax=lmh
      lm=lmh
      write(3,*)'gray1  lmax=',lmax 
1     format(a)
3     format(10i8)

      write(3,1)' gray1'
      print *,'            gray:iitab,jje',iitab,jje

      write(meanname(5:26),'(a)')name
      write(meanname(1:4),'(a)')'mean'
      write(3,*)' meanname ',meanname
      
      call system('rm '//meanname) 
      call opennew26(meanname,60)
c          ---------
      call par1('smallplo',1,ksmallplo)
      call par1('     iqa',1,iqa)
      call par1('     iqb',1,iqb)
      call par1('     jqa',1,jqa)
      call par1('     jqb',1,jqb)
        write(3,*)'iqa,iqb,jqa,jqb ',iqa,iqb,jqa,jqb
      npix=1000/milsec
c exe
      call par1('    imax',1,imax)

      inveri=0
      inverj=0
      npy=0

        if(page1.eq.0)page1=2
        if(page2.eq.0)page2=1
        if(npy.eq.0)npy=1
        npage=page1*page2*npy

c fix
      do l=1,lmax
      bl(l)=0
      wh(l)=1
      enddo

      if(iqp.eq.0)then
      call par1('  blackq',1,kpiv0)
      call par1('  whiteq',1,kpiv1)

c        if(kpiv0.eq.0.and.kpiv1.eq.0)goto5
        bl(1)=kpiv0
        wh(1)=kpiv1
      do l=2,lmax
      call par1('  blackq',l,bl(l))
      call par1('  whiteq',l,wh(l))
      enddo

      else
      call par1('  blackp',1,kpiv0)
      call par1('  whitep',1,kpiv1)
c        if(kpiv0.eq.0.and.kpiv1.eq.0)goto5
        bl(1)=kpiv0
        wh(1)=kpiv1
      do l=2,lmax
      call par1('  blackp',l,bl(l))
      call par1('  whitep',l,wh(l))
      enddo   
      endif
      write(3,*)' black ',(bl(l),l=1,20)
      write(3,*)' white ',(wh(l),l=1,20)
5     continue
c                    les lmax positions recoivent des valeurs correspondant
c                    successivement a tous les tableaux rencontres dans
c                    le fichier q (ou p)
c                    ordre: Y,npol,lambda (j,i)

      do l=1,lmax
      ctrast(l)=0
      zer(l)=0
      enddo 


c      lm=0
          do l=1,lmax
          black(l)=bl(l)
          white(l)=wh(l)
            if(bl(l).ne.wh(l))then
            lsup=l
            endif
          enddo

       ii1=0
       ii2=0
       jj1=0
       jj2=0
      iz1=0
      iz2=0
      iz3=0
      iz4=0
      milsig=0
      iprej=0
      nt=0


      nplot=0
      ihv=1
      np=0
c
      nf=1
      nfm=1
6     format(/' plot of ',a22,' -> s',a22,'.ps')
          print 6,name,name
          write(7,6)name,name
          write(3,6)name,name
c------------------------------------------------------------------------
c     !!!!!!!!!!!                            lecture header (iu)
          write(3,*)' gray1: iu ',iu
      rewind(iu)
      read(iu)head!!!!!!!!!!!!!
c        do i=1,40!!!!!!!!!!!!!!!
c        print *,'head(i) :',head(i)!!!!!!!!!!!!!!!!
c        enddo!!!!!!!!!!!!!!!!!
      im=head(2)!!!!!!!!!!!!!!!!
      jm=head(3)!!!!!!!!!!!!!!!!!
      lm=head(4)    !!!!!!!!!!!!!!!  
      do l=1,lm !!!!!!!!!!!!!!
      llb(l)=head(4+l)!!!!!!!!!!!!!!!
      enddo                     !!!!!!!!!!!!!!!!!!
10    format(' header q: im,jm,lm,lb(1),lb(2)... ')
      write(3,10)
        write(3,*)im,jm,lm,(llb(l),l=1,lm)

        if(ii1.eq.0)then        
          if(ksmallplo.ne.0)then
          i1=1+iqa*npix
          i2=1+iqb*npix   !im
          j1=1+jqa*npix
          j2=1+jqb*npix         !jm
          else
          i1=1
          i2=im
          j1=1
          j2=jm
          endif
        else
          i1=ii1
          i2=ii2
          j1=jj1
          j2=jj2
          endif
c
c        llsup=lsup
c        if(nm.lt.llsup)llsup=nm
c---
c          header de  mean
      do k=1,512
      headmean(k)=0
      enddo
        headmean(1)=4 !1
        headmean(2)=1
        headmean(3)=1
        headmean(4)=lm
c     write(60)(headmean(k),k=1,512)
      write(3,*)' headmean: ',(headmean(l),l=1,4)  
c                              moyennes
      write(3,*)'mean im,jm,lm ',im,jm,lm

      do l=1,lm
        moy=0
        norme=0
        n32000=0

        do j=1,jm
        read(iu)(lec(i),i=1,im)
          do i=1,im
          if(lec(i).ne.32000)then
            norme=norme+1
            moy=moy+lec(i)
          else
            n32000=n32000+1
          endif
          enddo
       enddo
       
        piv=moy
        mmoy(l)=piv/float(norme)
           write(3,*)'l,moy,norme,mmoy(l) ',l,moy,norme,mmoy(l)
      enddo

15    format(' meanq')
16    format(' meanp')
18    format(15i6)
      if(iqp.eq.0)write(7,15)   !  oui
      if(iqp.eq.1)write(7,16)
      write(7,18)(mmoy(l),l=1,lm) 
      write(7,*)' total number of bad points =',n32000

      write(60)  (mmoy(l),l=1,lm)   !  dans meanq ou meanp
      close(60)
c---------------------------------------------------  lecture q head
      rewind(iu)
      read(iu)head

      write(7,*)' '
      write(7,*)' Header Q-file'
 180  format(10i5)
      write(7,180)(head(l),l=1,40)
      write(7,*)' '

      print *,' '
      print *,' Header Q-file'
      print 180,(head(l),l=1,40)
      print *,' '  
      
      im=head(2)
      imax=im
      jm=head(3)
      lm=head(4)
      lmh=lm
c------
      call par1('  cented',1,icented)
      call par1('    lmpd',1,lmpd1)
      call par1('    lmpd',2,lmpd2)
         lint=icented+2*(lmpd1+lmpd2)+1      !    first map int doppler
         print *,'lint,lmh',lint,lmh
         
        do l=1,lm
        llb(l)=head(l+4)/10             ! pm
c        if(l.eq.lmh-1)llb=iabs(llb)
c        if(l.eq.lmh)llb=head(l-2+4)/10
        write(3,*)' l llb(l) ',l,llb(l)
c        if(l.eq.llsup.and.l.eq.lint+2)llb=lb(l-1)
c
        if(bl(l).ne.wh(l))np=np+1
        if(np.gt.npy)np=1
          ihv=1
       write(3,*)'subgray: l,i1,i2,im,imax,j1,j2,jm,llb,black,white ',
     1                  l,i1,i2,im,imax,j1,j2,jm,llb,black(l),white(l)

      call SUBGRAY(iu,lec,tab,l,i1,i2,im,imax,j1,j2,jm,llb,
c          -------                                     fct l
     1          inveri,inverj,page1,page2,np,npy,milsec,
     2          black,white,ctrast,zer,
     3          iz1,iz2,iz3,iz4,milsig,iprej,nt,name,ihv,hfin,nfsm,
     4          iitab,jje,mmoy,igray,lm,lint)

        nplot=nplot+1
        enddo

      ijtot=page1*page2*npy
      jpiv=nplot/ijtot
      jsaut=ijtot-(nplot-jpiv*ijtot)
         write(3,*)' nplot,ijtot,jsaut ',nplot,ijtot,jsaut

      if(jsaut.ne.0.and.jsaut.ne.ijtot)then
        do jst=1,jsaut
        call pgadvance
        enddo
      endif

c      if(las.ne.0)then
c         if(nplot.ne.npage.and.nf.ne.nfm)goto200
c         call pgend
c         if(las.eq.1)then
c             print *,'Print new page?'
c             pause
c             call system('lpr -Pqms gray.ps')
c             pause
c         endif        
c         if(las.eq.-1)call system('ghostview gray.ps')
c         call system('rm gray.ps')
c         if(displ.eq.4)call pgbegin(0,'gray.ps/ps',page1,page2)
c         if(displ.eq.5)call pgbegin(0,'gray.ps/vps',page1,page2)
c         np=0
c         nplot=0
c      endif

      return
      end
c-------------------------------------
      subroutine SUBGRAY(iu,lec,tab,l,i1,i2,im,imax,j1,j2,jm,llb,
c                -------            in                       in pm
     1            inveri,inverj,page1,page2,np,npy,milsec,
     2            bl,wh,ctrast,zer,
     3            iz1,iz2,iz3,iz4,milsig,iprej,nt,nom,ihv,hfin,nfsm,
     4            iitab,jje,mmoy,igray,lm,lint)
c                                      in  in
      integer*2 lec(2048),mmoy(60)
      integer*4 zer(10),nrej(90),page1,page2,ctrast(10)
      dimension tab(iitab,jje),tr(6),cr(10),bl(60),wh(60),llb(60)
c modif 1 mai 00  titre a 48
      character*45 titre
      character*22 nom
      character*8 hfin
c      character*4 blancs
        pm=llb(l)                                   ! picometres
      write(3,*)' subgray l,pm,lint  iitab,jje ',
     1 l,pm,lint,iitab,jje
      call par1(' iprodis',1,iprodis)
      maxdim=4000
c      blancs='pm  '
c
      if(bl(l).eq.wh(l))goto10
      write(titre(1:22),'(a)')nom
      write(titre(23:28),'(i4,2x)')l
         kpico=pm
         kpicoa=iabs(kpico)
            
      if(l.lt.lint)then                         !  profils
           write(titre(29:36),'(a8)')'  int   '
           write(titre(37:41),'(i3,2X)')kpico      !llb  
            write(titre(42:45),'(a4)')'pm  '
      else                   !   IV
            
      call par1(' iminpro',1,iminpro)       ! m2403
      call par1('   ilin1',1,ilin1)

      if(iminpro.ne.0.and.ilin1.ne.0)then       ! protu scattering
        if(l.eq.lint)then
           write(titre(29:37),'(a9)')'  int +/-'
           write(titre(38:41),'(i4)')kpicoa
           write(titre(42:45),'(a4)')' pm '
        endif
        if(l.eq.lint+1)then
           write(titre(29:37),'(a9)')'  vel +/-'
           write(titre(38:41),'(i4)')kpicoa
           write(titre(42:45),'(a4)')' pm '
        endif
        if(l.eq.lint+2)then
           write(titre(29:37),'(a9)')'  int +/-'
           write(titre(38:41),'(i4)')kpicoa
           write(titre(42:45),'(a4)')' pm '
        endif
        
      else                                      ! disk or prom no scattering
         if(llb(l).gt.0)write(titre(29:37),'(a9)')'  int +/-'    
         if(llb(l).lt.0)write(titre(29:37),'(a9)')'  vel +/-'
           write(titre(38:41),'(i4)')kpicoa
           write(titre(42:45),'(a4)')' pm '
      endif                                    ! scattering
      endif                                    ! profils/IV

        tr(1)=0. 
        tr(2)=1.
        tr(3)=0.
        tr(4)=0.
        tr(5)=0.
        tr(6)=1.
c      do i=1,38 !!!!!!!!!!!!!!!!
c      if(titre(i:i).eq.' ')titre(i:i)='0'!!!!!!!!!!!!!
c      enddo!!!!!!!!!!!!!!
c
      call par1('     vps',1,kvps)
      ratio=(0.880/0.653)*float(page1)/float(page2)
c     if(ihv.eq.1)ratio=(0.653/.880)*float(page1)/float(page2)
       if(kvps.eq.0)ratio=(0.653/.880)*float(page1)/float(page2)
      is=i2-i1+1
      js=j2-j1+1
c
      ipiv=(is-1)/maxdim+1
      jpiv=(js-1)/maxdim+1
      ijpas=max0(ipiv,jpiv)
      is=(is-1)/ijpas+1
      js=(js-1)/ijpas+1
      xi1=1.
      xi2=is
      yj1=1.
      yj2=js
               piv=float(milsec)/1000.
      xxi1=0.
      xxi2=piv*(xi2-1.)*ijpas
      yyj1=0.
      yyj2=piv*(yj2-1.)*ijpas
c
      xl=0.75     
      if(npy.gt.1)xl=0.85
      if(imax.ne.0)xl=xl*float(is-1)/float(imax-1)
      yl=xl*(j2-j1)/(ratio*float(i2-i1))
c        print *, xl,yl
c
        if(npy.gt.1.and.yl.gt.0.85/float(npy))then
        piv=xl/yl
        yl=0.85/float(npy)
        xl=yl*piv
        endif
c
        if(npy.le.1.and.yl.gt.0.8)then
        piv=xl/yl
        yl=0.75
        xl=yl*piv
        endif
c
      x1=.5-0.5*xl
      x2=x1+xl
        if(npy.gt.1)then
        y1=0.04+0.90*(np-0.5)/(float(npy)+0.05)-0.5*yl
        else
        y1=0.5-0.5*yl
        endif
      y2=y1+yl
c

10    do20 j=1,jm
      read(iu)(lec(i),i=1,im)
c          --

      if(bl(l).eq.wh(l))goto20
        jtest=j-100*(j/100)
12      format(' im,jm,tab=',10i6)
c        if(jtest.eq.0)write(3,12)im,jm,(lec(i),i=1,im,200)
      if(j.lt.j1.or.j.gt.j2)goto20
      if(zer(l).ne.0)call ZERO3(iz1,iz2,iz3,iz4,milsig,iprej,nt,
     1                      lec,1,im,1,0,
     2                      ar,br,cr,item,nrjm,nrej,yy1,yy2,ifin)
       pasij=ijpas
       if(inverj.eq.0)then
        yp=(j-j1)/pasij+1
        jp=yp+0.001
        if(yp-jp.ne.0.)goto20
       else
        yp=(j2-j)/pasij+1
        jp=yp+0.001
       if(yp-jp.ne.0.)goto20
       endif
c
        do15 i=i1,i2,ijpas      ! i
        if(inveri.eq.0)then
         xp=(i-i1)/pasij+1
         ip=xp
         if(xp-ip.ne.0.)goto15
        else
         xp=(i2-i)/pasij+1
         ip=xp
         if(xp-ip.ne.0.)goto15
        endif
          tab(ip,jp)=lec(i)
c         ----------
c       dessin intensites second plot (cas IV)
c          if(l.eq.2)then   !  Premier tableau > 0   
c             piv=lec(i)
c             if(iprodis.eq.0)tab(ip,jp)=abs(piv)
c             if(iprodis.eq.1)then
c                         if(piv.lt.0.)then
c                              tab(ip,jp)=-piv
c                         else
c                              tab(ip,jp)=0.
c                         endif
c             endif
c             if(iprodis.eq.2)then
c                         if(piv.lt.0.)then
c                              tab(ip,jp)=0.
c                         else
c                              tab(ip,jp)=piv
c                         endif
c          endif
            
c          if(lec(i).eq.32000)tab(ip,jp)=32000.
15      continue
c
20    continue    !  j
c
      if(bl(l).eq.wh(l))return
      if(np.eq.1)call PGADVANCE
c      call PGIDEN
        call par1('     slw',1,kslw)    !line width
        call par1('  milsch',1,milsch)    !size charact
           sch=float(milsch)/1000.       
        write(3,*)' x1,x2,y1,y2 before ',x1,x2,y1,y2
        ratio=0.90
        xxc=(x1+x2)/2.
        xd=(x2-x1)*ratio
        yyc=(y1+y2)/2.
        yd=(y2-y1)*ratio
      x1=xxc-xd/2.
      x2=xxc+xd/2.
      y1=yyc-yd/2.
      y2=yyc+yd/2.

      write(3,*)' x1,x2,y1,y2 after  ',x1,x2,y1,y2
      write(3,*)' msdp4 4097 xi1..  xxi1..',xi1,xi2,yj1,yj2
      write(3,*)  xxi1,xxi2,yyj1,yyj2
        call PGVPORT(x1,x2,y1,y2)
      call PGWINDOW(xi1,xi2,yj1,yj2)
c        print *,x1,x2,y1,y2
c     print *,xi1,xi2,yj1,yj2
c      piv=float(page1)
c      piv=2.5/piv !3./piv
c      if(page1.eq.1)piv=1.5
c         piv=page1*page2
c         piv=sqrt(piv)
      call pgsch(sch)   !    (piv)                sch
c      if(npy.ne.1)call pgsch(piv)
      if(np.eq.1.and.npy.eq.1)then
       call pgwindow(xxi1,xxi2,yyj1,yyj2)
       call pgslw(kslw)       !       2)          slw
            piv=yyj2-yyj1
       if(piv.gt.150.)then
          call PGBOX('BCInts',100.,2,'BCInts',100.,2)
       else
          call pgbox('bcints',50.,5,'bcints',50.,5)
       endif
c          call pgslw(1)
       call pgwindow(xi1,xi2,yj1,yj2)
      endif

c      call pgslw(2)
c      call pgsch(1.5)
      if(np.eq.npy)call PGLABEL('arcsec','arcsec',titre)  !' ')  
c                               ' ','arcsec',titre)
c      call pgsch(2.)
c      call pgslw(1)
c
      tabmax=-10000.
      tabmin=10000.
        iimin=1
        iimax=1
        jjmin=1
        jjmax=1
      do j=1,js
      do30 i=1,is

        if(igray.eq.3)then
        piv=abs(tab(i,j))
        if(piv.lt.1.)piv=1.
        tab(i,j)=1000.*alog10(piv)   !   ******  log
        endif

      piv=tab(i,j)
c         --------
        if(abs(piv).gt.31999.)goto30
      tabmin=amin1(piv,tabmin)
      tabmax=amax1(piv,tabmax)
      if(piv.eq.tabmin)then
          iimin=i+i1-1
          jjmin=j+j1-1
      endif     
      if(piv.eq.tabmax)then
          iimax=i+i1-1
          jjmax=j+j1-1  
      endif
30    continue
31    format(' is,js,tab=',2i6,1x,8f6.0)
c      jtest=j-100*(j/100)
c      if(jtest.eq.0)write(3,31)is,js,(tab(i,j),i=1,is,200)
      enddo
32    format
     1('   l,   bl,   wh, imin,jmin,tabmin, imax,jmax,tabmax,  mean'/
     2  i4,    2f6.0,     i6,  i5,  f7.0,   i6,  i5,  f7.0,  i7)
        print 32, l,bl(l),wh(l),iimin,jjmin,tabmin,iimax,jjmax,tabmax,
     1                                                         mmoy(l)
        write(3,32)l,bl(l),wh(l),iimin,jjmin,tabmin,iimax,jjmax,tabmax,
     1                                                         mmoy(l)
        write(7,32) l,bl(l),wh(l),iimin,jjmin,tabmin,iimax,jjmax,tabmax,
     1                                                         mmoy(l)
c     endif
      if(bl(l).eq.0.and.wh(l).eq.1)then
           bbl=tabmin
           wwh=tabmax
        goto 50
      endif

      if(wh(l).eq.100.)then
         if(bl(l).le.100.)then
            wwh=tabmax
            piv=bl(l)/100.
            bbl=tabmin+(tabmax-tabmin)*piv
            goto 50
         endif
      endif
      
      bbl=bl(l)
      wwh=wh(l)

c      if(abs(bl(l)).le.100..and.abs(wh(l)).le.100.)then
c        piv=bl(l)/100.
c        bbl=tabmin+(tabmax-tabmin)*piv
c        piv=wh(l)/100.
c        wwh=tabmin+(tabmax-tabmin)*piv
c        goto50
c      endif


50    write(3,*)' gray: tabmin,bbl,wwh,tabmax ',
     1                  tabmin,bbl,wwh,tabmax

c      alp=(ctrast(l)+100.)/200.
c      den=wwh-bbl
c      print *,alp,den
c
c      do j=1,js
c      do i=1,is
c        if(tab(i,j).eq.32000)then
c        tab(i,j)=0.5*(bbl+wwh)
c        goto40
c        endif
c      if(alp.ne.0.5)then
c      bet=(tab(i,j)-bbl)/den
c        if(bet.le.alp)then
c        tab(i,j)=bbl+0.5*den*bet/alp
c        else
c        tab(i,j)=wwh-0.5*den*(1.-bet)/(1.-alp)
c        endif
c      endif
c40    continue
c      enddo
c      enddo
c
         write(3,*)'pggray: is,js,bbl,wwh:  ',is,js,bbl,wwh

         call PGGRAY(tab,iitab,jje,1,is,1,js,bbl,wwh,tr)
      return
      end
c------------------------------------
      subroutine opennew26(name,iu)
c                ---------
      character*26 name
        write(3,*)name
      open(unit=iu,status='new',form='unformatted',file=name)
c        write(3,*)'ouvert'
      return
      end
c------------------------------------
      subroutine prostok(iu,name,page1,page2,hfin,nfsm,milsec,
c                ----
     1                 tab,iitab,jje,igray)
c               dimensions max cartes = iitab,jje
c-------------------------------------------------------------
      dimension tab(iitab,jje),
     1          proi(30,18,51),prok(30,18,51),
     2          xdes(60),ydes(60)
      integer*4 page1,page2
      integer*4 head(512)
      integer*2 lec(2048) ! ,mmoy(60)
      character*22 name
      character*8 hfin
      character*22 titre1
      character*16 titre2
      maxdim=jje

      write(titre1(1:22),'(a)')name

1     format(a)
3     format(10i8)

      write(3,1)' prostok'

      read(iu)head

        pix=milsec*0.001
      im=head(2)
        sim=(im-1)*pix
      jm=head(3)
        sjm=(jm-1)*pix
      write(3,*)' im,jm,sim,sjm ',im,jm,sim,sjm

      call par1('     x1k',1,is1k)
      call par1('     x2k',1,is2k)
      call par1('     y1k',1,js1k)
      call par1('     y2k',1,js2k)
      call par1(' ijstepk',1,ijstepk)
        if(is2k.eq.0)then
        is1k=0
        is2k=sim
        js1k=0
        js2k=sjm
        endif
      x1k=is1k
      x2k=is2k
      y1k=js1k
      y2k=js2k
        if(ijstepk.eq.0)ijstepk=(im-1)/float(page1-1)
      write(3,*)' x1k,x2k,y1k,y2k,ijstepk ',
     1            x1k,x2k,y1k,y2k,ijstepk
      write(7,*)' prosrok: x1k,x2k,y1k,y2k,ijstepk ',
     1            x1k,x2k,y1k,y2k,ijstepk
      iprom=(is2k-is1k+0.1)/(ijstepk*pix)+1
      jprom=(js2k-js1k+0.1)/(ijstepk*pix)+1
      write(3,*)' iprom,jprom ',iprom,jprom      
         if(iprom.gt.11.or.jprom.gt.51)then
         print *,' trop de points solaires pour profils stokes'
         return
         endif
      call par1('    lmpr',1,lmpr)
      call par1('  lbpasr',1,lbpasr)
      call par1('  center',1,kcenter)
      call par1('    dlbd',1,kdlbd)
      if(kcenter.eq.0)then
        print *,' pas de calcul centre raie pour profils stokes'
        return
      endif
      lmp=2*lmpr+1
      lm=2*lmp-1
      write(3,*)' lmp,lm ',lmp,lm
      if(lm.gt.60)then
        print *,' trop de lambdas pour profils stokes'
        return
      endif

      xmax=lmpr*kdlbd*lbpasr*0.001    ! mA
      ux=kdlbd*lbpasr*0.001/2.

      write(3,*)' prostok:sim,sjm,ijstepk,xmax,ux ',
     1                    sim,sjm,ijstepk,xmax,ux 

c stockage des profils a tracer (l=1,3,5,...lm)
c  intensites
        maxint=0
      do l=1,lm,2
        lpro=(l+1)/2

      do j=1,jm
      read(iu)(lec(i),i=1,im)
        do i=1,im
        tab(i,j)=lec(i)
        enddo
      enddo

      do jpro=1,jprom
      j=1+(jpro-1)*ijstepk
      if(j.gt.jm)j=jm

        do ipro=1,iprom
        i=1+(ipro-1)*ijstepk
        if(i.gt.im)i=im
        kpiv=tab(i,j)
        proi(lpro,ipro,jpro)=kpiv
        maxint=max0(maxint,kpiv)
        enddo
      enddo 

      enddo    ! l
        write(3,*)' maxint: ',maxint
c  stokes
        amaxk=0.
        amink=10000.
      do l=1,lm,2
        lpro=(l+1)/2

      do j=1,jm
      read(iu)(lec(i),i=1,im)
        do i=1,im
        tab(i,j)=lec(i)
        enddo
      enddo

      do jpro=1,jprom
      j=1+(jpro-1)*ijstepk
      if(j.gt.jm)j=jm

        do ipro=1,iprom
        i=1+(ipro-1)*ijstepk
        if(i.gt.im)i=im
        pivk=tab(i,j)
c        prok(lpro,ipro,jpro)=pivk
          pivi=proi(lpro,ipro,jpro)
          piv=0.
          if(pivi.ne.0.)piv=pivk/pivi
          prok(lpro,ipro,jpro)=piv
        amaxk=amax1(amaxk,piv)
        amink=amin1(amink,piv)
        enddo
      enddo

      enddo  ! l
        write(3,*)' amaxk,amink: ',amaxk,amink

c lignes incompletes
      manque=page1-(iprom-page1*(iprom/page1))
      if(manque.eq.page1)manque=0
        write(3,*)' manque: ',manque

c dessin
         ymax=maxint

      do jpro=jprom,1,-1
      do ipro=1,iprom
        xpro=x1k+ijstepk*(ipro-1)*pix
        ypro=y1k+ijstepk*(jpro-1)*pix
      write(titre2(1:8),'(f8.2)')xpro
      write(titre2(9:16),'(f8.2)')ypro
c       if(ipro.eq.1.and.jpro.eq.jprom)
c      write(3,*)' xpro,ypro ',xpro,ypro

c intensites
      call pgadvance
      call pgvport(0.1,0.9,0.2,0.8)
         piv=-xmax
      call pgwindow(piv,xmax,0.,ymax)
      call pgsch(2.5)
      call pglabel(titre1,' ',' ')
c      call pgbox('bcnts',100.,1,'bc',10000.,1)
      call pgsch(3.5)
      call pglabel(' ',' ',titre2)
      call pgsch(2.5)

      do l=1,lm
      xdes(l)=-xmax+(l-1)*ux
      enddo

c interpol
      do l=1,lm,2
        lp=(l+1)/2
      ydes(l)=proi(lp,ipro,jpro)
      enddo
        ydes(2)=0.5*(ydes(1)+ydes(3))
        ydes(lm-1)=0.5*(ydes(lm-2)+ydes(lm))

      do l=4,lm-3,2            
      a=ydes(l-3)
      b=ydes(l-1)
      c=ydes(l+1)
      d=ydes(l+3)
      ydes(l)=(-a+9.*b+9.*c-d)/16.
      enddo      

c dessin
      call pgsls(4)
      call pgline(lm,xdes,ydes)
c          ------
      call pgsls(1)
c stokes
        piv=-xmax
      call pgwindow(piv,xmax,amink,amaxk)
      call pgsls(1)
      if(amink.lt.-0.5.or.amaxk.gt.0.5)then
        call pgbox('bcnts',100.,1,'bcnts',0.5,5)
      else
        call pgbox('bcnts',100.,1,'bcnts',0.1,1)
      endif
      call pgsls(2)
      call pgbox('a',0.,0,'',0.,0)
      call pgsls(1)
c interpol
      do l=1,lm,2
        lp=(l+1)/2
      ydes(l)=prok(lp,ipro,jpro)
      enddo
        ydes(2)=0.5*(ydes(1)+ydes(3))
        ydes(lm-1)=0.5*(ydes(lm-2)+ydes(lm))

      do l=4,lm-3,2            
      a=ydes(l-3)
      b=ydes(l-1)
      c=ydes(l+1)
      d=ydes(l+3)
      ydes(l)=(-a+9.*b+9.*c-d)/16.
      enddo      

c dessin
      call pgsls(1)
      call pgline(lm,xdes,ydes)
c          ------
      enddo   !  ipro

      if(manque.ne.0)then
        do mm=1,manque
        call pgadvance
        enddo
      endif
      enddo   !  jpro

      return
      end
c*********************************************
c    i3scan.f                  balayage 3 images stokes en i
c********************************************
c     cree un tableau d (ou r) pour bande complete, avec 1 en caract 21
c         le ss/progr est appele dans une boucle nquv exterieure
c         nfa correspond a la 1ere position de nq
c*************************************************************************
      subroutine i3scan(mgrim,nquv,nq,ipos,nfa,rafburst,dnanfp,iud,iui,
c                ------                                         inutile
     1                sundec,tab,icorrel,milsec,iitab,jjtab,lltab,
c                            eco in
     2                nxfirst,nxm,select)

      integer sundec,head(512),dlb(60),ideb1(10,4),ideb2(10, 4),! dlbd
c                                                      >mgrim, poses
     1                                      jdeb1(10,4),rafburst,select
      integer*2 tab(iitab,jjtab,lltab),   tabx(2000,200,4)
c                                 pour chaque ltz
c                              
c                                    images d1,d2,d3 / d4,d5,d6 / d7,d8,d9
c                                      ny=   1          2          3
c                                      nq=   1,2,3   /  1,2,3   /  1,2,3
c                                      nb=  1,..1..1../...
c     calcul des bandes completes 
c  chargees dans le tableau ltz de tab       1,2,3
c  ecriture dans      "         de d1,d2,d3

c      character cqp*6,comm*80
      character*22 dnanfp(10000,2),dname

        npolar=1
12    format(14i6)  
14    format(2x,10i6)
                                 idelc=0
                                 mar=0
c obs
      call par1('   istep',1,kstep)
                             kstep=iabs(kstep)
c exe
      call par1('    igri',1,iigri)
      call par1('   itgri',1,itgri)

            pixel=float(milsec)
 
        xgri=float(iigri)/pixel
              igri=xgri
        txgri=float(itgri)/pixel
              itgri=txgri

        istep=float(kstep)/pixel+0.5
        taux=0.

c      print *,'debut i3scan'
      write(3,*)' Debut i3scan'
c-----------------------------------------
c                            lecture du header premier fichier d
c                                             (sans select)
      dname=dnanfp(nfa,npolar)
      call openold22(dname,sundec,iud)
c          ---------                 
       print *,'dname:',dname

      rewind(iud)
      read(iud)head
c              ----           
      close(unit=iud)

      imm=head(2)
      jm=head(3)    
      lm=head(4)
c      npolar=head(5)

       do l=1,lm
       dlb(l)=head(l+4)
       enddo

       print*,' i3scan: lecture header d ',(head(i),i=1,lm+12) 
      
c----------------------------------------------------------

        igri=imm/mgrim  ! largeur des fentes de la grille
        igri=igri-2*mar

c       txgri           ! pas des fentes
c        ilapana=igri-istep ! recouvrement entre les 3 poses
c                            0 si jointives

            is=2*istep+igri+              (mgrim-1)*itgri
c                       igri=longueur utile
            js=jm-2*mar

c--------------------------------------
c                                      boucle ltz
      do 90 ltz=1,lm
c                         print *,' ltz=',ltz
          do i=1,is
          do j=1,jm
          tabx(i,j,1)=0
          tabx(i,j,2)=0
          tabx(i,j,3)=0
          tabx(i,j,4)=0
          tab(i,j,ltz)=0
          enddo
          enddo

        do 50 ny=1,ipos
c                                     on explore les ipos positions 

        if(nxfirst.eq.0)then
           nf=nfa+(ny-1)*nquv*rafburst
        else
           nf=nfa+(ny-1)*nquv*rafburst*nxm
        endif

      if(ltz.eq.1)then
        write(3,*)' i3scan:nfa,nquv,rafburst,nxm,nxfirst,ny,nf ',
     1                     nfa,nquv,rafburst,nxm,nxfirst,ny,nf
      endif

          dname=dnanfp(nf,npolar)
        write(dname(18:19),'(i2)')select
        if(select.ge.0.and.select.le.9)write(dname(18:18),'(a1)')'0'
c          if(select.lt.0) write(dname(18:19),'(a2)')'-1'  ! select
        write(dname(22:22),'(i1)')nq
        write(3,*)'nq,ltz,ny,nf,dname  ',
     1             nq,ltz,ny,nf,dname

        call openold22(dname,sundec,iud)
c            --------- 
        rewind(iud)
        read(iud)head        
c                                              boucle de lecture lm
          do30 l=1,lm
            do j=1,jm
            read(iud)(tabx(i,j,ny),i=1,imm)
c                ---
            enddo
            if(l.eq.ltz)then
            close(unit=iud)      
            goto50
            endif
30        continue
50      continue

c----                         chargement dans tab(...ltz) 
c calcul des indices des debuts d'images dans les images  ideb1(10,4)
c                                     et dans la bande    ideb2(10,4)

      if(ltz.eq.1)then

      do mgri=1,mgrim
      do ny=1,ipos

        ideb1(mgri,ny)=1+(mgri-1)*igri+mar
        jdeb1(mgri,ny)=1+mar

        if(istep.gt.0)then
        ideb2(mgri,ny)=1+itgri*(mgri-1)+istep*(ny-1)
        else
        ideb2(mgri,ny)=1+2*istep+itgri*(mgri-1)-istep*(ny-1)
        endif

      enddo
      enddo

      endif  !! test ltz=1

c chargement dans tab
      call bande(tabx,tab,ltz,mgrim,igri,ideb1,ideb2,jdeb1,js,
c          -----
     1           mar,istep,iitab,jjtab,lltab,ipos)
c                    signe

90    continue  !! fin boucle ltz

c---------------------------- ecriture dans fichier d (1 en caract 21)
      
c                                  header pour bande complete

        head(4)=lm
        head(2)=is
        head(3)=js

      dname=dnanfp(nfa,npolar)
      nns=1
      write(dname(21:21),'(i1)')nns

        write(dname(18:19),'(i2)')select
        if(select.ge.0.and.select.le.9)write(dname(18:18),'(a1)')'0'
c        if(select.lt.0)write(dname(18:19),'(a2)')'-1'  ! select
        write(dname(22:22),'(i1)')nq
        call system('rm '//dname)
      call opennew22(dname,sundec,iud)
c          ---------                 
       print *,'dname:',dname
  
27    format(' i3scan: head file bande d complete ',(a))
28    format(10i8)
        write(3,27)dname
        write(3,28)(head(i),i=1,30)


      write(iud)head

      do l=1,lm
      do j=1,jm
      write(iud)(tab(i,j,l),i=1,is)
      enddo
      enddo

      close(unit=iud)

      return
      end
c************************************************
      subroutine bande(tabx,tab,ltz,mgrim,igri,ideb1,ideb2,
c                -----
     1                 jdeb1,js,mar,istep,iitab,jjtab,lltab,ipos)
c                                   signe
      integer*2 tab(iitab,jjtab,lltab),tabx(iitab,jjtab,4)
      integer ideb1(10,4),ideb2(10,4),jdeb1(10,4),
     1        indmgri(60),indipo(60)
c            ordre des images pour i croissants
1     format(10i6)
      write(3,*)' bande: ideb1 '
      write(3,1)ideb1
      write(3,*)' bande: ideb2 '
      write(3,1)ideb2

      if(istep.gt.0)then
        n=0
      do mgri=1,mgrim
      do ipo=1,ipos
        n=n+1
        nm=n
        indmgri(n)=mgri
        indipo(n)=ipo
      enddo
      enddo
      endif

      if(istep.lt.0)then
        n=0
      do mgri=1,mgrim
      do ipo=ipos,1,-1
        n=n+1
        nm=n
        indmgri(n)=mgri
        indipo(n)=ipo
      enddo
      enddo
      endif

5     format(/' bande: indmgri')
6     format(/' bande: indipo')
7     format(10i6)
      write(3,5)
      write(3,7)(indmgri(n),n=1,nm)
      write(3,6)
      write(3,7)(indipo(n),n=1,nm)

c---     

      do 10 n=1,nm
        mgri=indmgri(n)
        if(n.gt.1)mgrig=indmgri(n-1)
        if(n.lt.nm)mgrid=indmgri(n+1)
        ipo=indipo(n)
        if(n.gt.1)ipog=indipo(n-1)
        if(n.lt.nm)ipod=indipo(n+1)

            iia=ideb1(mgri,ipo)
            jja=jdeb1(mgri,ipo)
      ia=ideb2(mgri,ipo)

         if(n.ne.1)then
         ib=ideb2(mgrig,ipog)+igri-1-2*mar
         else
         ib=ia
         endif

      id=ia+igri-1-2*mar

         if(n.ne.nm)then
         ic=ideb2(mgrid,ipod)
         else
         ic=id
         endif
     
10    call biseau(tabx,tab,ltz,mgri,ipo,
c          ------
     1             iia,jja,  ia,ib,ic,id, js,iitab,jjtab,lltab)
c                  depart    arrivee

      return
      end
c********************************************
      subroutine biseau(tabx,tab,ltz,
c                ------
     1                   mgri,ipo, 
     2                   iia,jja,  ia,ib,ic,id, js,iitab,jjtab,lltab)

      integer*2 tab(iitab,jjtab,lltab),tabx(iitab,jjtab,4)

c        ind initiaux   iia    iib     iic    iid
c        ind finaux     ia     ib      ic     id
c                        biseau         biseau

                iib=iia+ib-ia
                iic=iia+ic-ia
                iid=iia+id-ia
1     format(' biseau: iia iib iic iid  ia  ib  ic  id')
2     format(8x,8i4)
      write(3,1)
      write(3,2)iia,iib,iic,iid,ia,ib,ic,id

      do j=1,js
      jj=j+jja-1

c  ia-ib
      if(ia.ne.ib)then      
        den=ib-ia
      do ii=iia,iib
            i=ii+ia-iia
         alp=float(ii-iia)/den
         tab(i,j,ltz)=tab(i,j,ltz)+(tabx(ii,jj,ipo)*alp+0.5)
      enddo
      endif

c   ib-ic
        iibp=iib+1
        if(ia.eq.ib)iibp=iib
        iicp=iic-1
        if(ic.eq.id)iicp=iic
      do ii=iibp,iicp
          i=ii+ia-iia
        tab(i,j,ltz)=tab(i,j,ltz)+tabx(ii,jj,ipo)
      enddo

c   ic-id
      if(ic.ne.id)then
        den=id-ic
      do ii=iic,iid
          i=ii+ia-iia
        alp=float(iid-ii)/den
        tab(i,j,ltz)=tab(i,j,ltz)+(tabx(ii,jj,ipo)*alp+0.5)
      enddo
      endif

      enddo

      return
      end
c*****************************************
c  mslib.f
c******************************************
c            mslib.f
c            *******
c--------------------------------
      subroutine SNAME(iu,name,ier)   !  inutilise
      character name*8,nx*8
1     format(a8)
      ier=0
c
      do n=1,10000
      read(iu,1,END=10)nx
      if(nx.eq.name)return
      enddo
 10   ier=-1
      return
      end
c----------------------------
c                -----
      subroutine ZERO3(ia,ib,ic,id,milsig,iprej,nt,lec,i1,i2,ksubs,
     1                jecri,ar,br,cr,item,nrjm,nrej,y1,y2,ifin)
      integer*2 lec(2048)
      dimension nrej(90),z1(3000),z2(3000),cr(10)
      ifin=0
      ar=0.
      br=0.
      y1=0.
      y2=0.
      if(ia.eq.0)return
c
      ifin=1
      if(iprej.ne.0)then
        if(milsig.eq.0)milsig=3000
        call REJEC3(lec,i1,i2,ia,ib,ic,id,milsig,iprej,nt,
c            ------
     1             jecri,cr,item,nrjm,nrej,ifin)
        if(ifin.ne.0)return
        y1=fzero(i1,nt,cr)
        y2=fzero(i2,nt,cr)
c          -----
        goto30
      endif
c
      if(milsig.ne.0)then
        do i=i1,i2
        z1(i)=lec(i)
        enddo
      call parafit(z1,i1,i2,milsig,z2)
c          -------
      endif
c
      xab=0.5*(ia+ib)
      ipiv=(ib-ia)/100+1
      sn=0.
      sy=0.
        do10 i=ia,ib,ipiv
        if(lec(i).eq.32000)goto10
        sn=sn+1.
          if(milsig.eq.0)then
          PIV=LEC(I)
          sy=sy+PIV
          goto10
          endif
        sy=amax1(sy,z2(i))
10      continue
      if(sn.eq.0.)return
      if(milsig.ne.0)sn=1.
      yab=sy/sn
c
      if((ia.eq.ic).and.(ib.eq.id))then
      ar=0.
      br=yab
      y1=yab
      y2=yab
      ifin=0
      goto30
      endif
c
      xcd=0.5*(ic+id)
      ipiv=(id-ic)/100+1
      sn=0.
      sy=0.
        do20 i=ic,id,ipiv
        if(lec(i).eq.32000)goto20
        sn=sn+1.
          if(milsig.eq.0)then
          PIV=LEC(I)
          sy=sy+PIV
          goto20
          endif
        sy=amax1(sy,z2(i))
20      continue
      if(sn.eq.0.)return
      if(milsig.ne.0)sn=1.
      ifin=0
      ycd=sy/sn
      ar=(ycd-yab)/(xcd-xab)
      br=yab-ar*xab
      y1=ar*i1+br
      y2=ar*i2+br
c
30    continue
      if(jecri.ne.0)then
35    format(' xab,xcd,yab,ycd, ar,br =',6e14.6)
      write(6,35)xab,xcd,yab,ycd,ar,br
36    format(' ia,ib,ic,id,milsig,iprej,i1,i2,ksubs,item,nrjm,y1,y2,',
     1       'ifin')
37    format(11i6,2f8.1,i2)
      write(6,36)
      write(6,37)ia,ib,ic,id,milsig,iprej,i1,i2,ksubs,item,nrjm,y1,y2,
     1           ifin
38    format(' lec avant/apres(si substitution)  tous les 10')
39    format(20i6)
      write(6,38)
      write(6,39)(lec(i),i=i1,i2,10)
      endif
c
      if(ksubs.eq.0)return
        do40 i=i1,i2
        if(lec(i).eq.32000)goto40
          if(iprej.ne.0)then
          kpiv=lec(i)-fzero(i,nt,cr)
c                     -----
          lec(i)=32000
          if(iabs(kpiv).lt.32000)lec(i)=kpiv
          goto40
          endif
        kpiv=lec(i)-ar*FLOAT(i)-br
        lec(i)=32000
        if(iabs(kpiv).lt.32000)lec(i)=kpiv
40      continue
      if(jecri.ne.0)write(6,39)(lec(i),i=i1,i2,10)
      return
      end 
c------------------------------
      subroutine DPMCAR(X,Y,P,ND,NT,COEF)
c                ------
c Ce sous-programme [ Schneider 75 ] calcule par une 
c methode de moindres carres en double precision le 
c polynome de NT termes (10 max) Y=F(X) associe a ND mesures.
C
C P=poids
c
      DOUBLE PRECISION C,F,PIVOT
      DIMENSION X(2000),Y(2000),P(2000),COEF(10),F(10),C(11,11)
      IF(NT.GT.10)NT=10
      NTP=NT+1
C
      DO 2 I=1,NT
      DO 2 L=1,NTP
2     C(L,I)=0.
C
      DO10 L=1,ND
      F(1)=1
        DO7 I=2,NT
C                   CHANGEMENT D'ECHELLE X
7       F(I)=F(I-1)*X(L)/1000.
      DO10 M=1,NT
        DO9 K=M,NT
9       C(K,M)=C(K,M)+F(K)*F(M)*P(L)
10    C(NTP,M)=C(NTP,M)+F(M)*P(L)*Y(L)
C
      DO20 M=1,NTP
      DO20 K=M,NTP
20    C(M,K)=C(K,M)
C
      DO104 I=1,NT
      PIVOT=C(I,I)
        DO103 M=1,NT
103     C(M+1,I)=C(M+1,I)/PIVOT
      DO104 K=1,NT
      IF(I.EQ.K)GOTO104
      PIVOT=C(I,K)
        DO105 M=1,NT
105     C(M+1,K)=C(M+1,K)-C(M+1,I)*PIVOT
104   CONTINUE
C
      COEF(1)=C(NTP,1)
      DO106 I=2,NT
106   COEF(I)=C(NTP,I)/(1000.**(I-1))
      RETURN
      END
c----------------------
C                -------
      SUBROUTINE parafit(Y,I1,I2,L,Z)
      DIMENSION Y(*),Z(*)      ! m2403
        IF(L.EQ.0)THEN
          DO I=I1,I2
          Z(I)=Y(I)
          ENDDO
        RETURN
        ENDIF
C
        IF(L.GE.10000)THEN
        X=0.
          DO I=I1,I2
          X=X+Y(I)
          ENDDO
        X=X/FLOAT(I2-I1+1)
          DO I=I1,I2
          Z(I)=X
          ENDDO
        RETURN
        ENDIF
C
      SX2=0.
      SX4=0.
        DO N=1,L
        N2=N*N
        SX2=SX2+N2
        SX4=SX4+N2*N2
        ENDDO
        SX2=2*SX2
        SX4=2*SX4
      D=(2*L+1)*SX4-SX2**2
      IA=I1+L
      IB=I2-L
      IF(IB.LT.IA)RETURN
C
      DO100 I=IA,IB
      SY=0.
      SYX=0.
      SYX2=0.
      IL1=I-L
      IL2=I+L
        DO IP=IL1,IL2
        DI=IP-I
        SY=SY+Y(IP)
        SYX=SYX+Y(IP)*DI
        SYX2=SYX2+Y(IP)*DI*DI
        ENDDO
      A=(SY*SX4-SYX2*SX2)/D
      Z(I)=A
C
      IF(I.EQ.IA.OR.I.EQ.IB)THEN
      B=SYX/SX2
      C=((2*L+1)*SYX2-SY*SX2)/D
      ENDIF
C
      IF(I.EQ.IA)THEN
        DO IP=I1,IA
        DI=IP-IA
        Z(IP)=A+DI*(B+DI*C)
        ENDDO
      ENDIF
C
      IF(I.EQ.IB)THEN
        DO IP=IB,I2
        DI=IP-IB
        Z(IP)=A+DI*(B+DI*C)
        ENDDO
      ENDIF
C
100   CONTINUE
      RETURN
      END
c----------------------
      subroutine IV(q,kmp,xlb,xi,xv,jabs,xc,spimin,spimax,sminmax,
c                                  iabsor     int min/max spicules
     1  dismin,fond,vfond,cosinus,sinus,  l,iph, cn,sn, nosol,itest,
c                        inutiles               inut.
     2  icontv,                minpro,mincont,idr,jemabs)
c                                                  emiss absor
c       min cont pour calcul v,prominences    fichiers D ou R
c                                                      0    1
c    sn=sinus(alpha)  avec
c           alpha=ecart(pt gauche corde-point mesure) / resolution spectrale
	dimension p(130),q(129)
c
c xv en unite canal   1<xv<125
c
c jabs=0  priorite a la valeur plus proche de xc
c      1  absorption seulement,autour minimum priorite intensites faibles
c      -1 emission seulement,priorite intensites fortes
c      2  option barycentre, absorption
c      -2 option barycentre, emission
c      3  barycentre derivee premiere ou seconde suivant dlambda
c si pas de solution  xi=xv=0
c
      dimension sinus(629),cosinus(629),cn(60),sn(60)
c prominences: min continuum for v calculation
        contv=icontv
      if(icontv.ne.0.and.idr.eq.0)then !   non
c                            D
       if(q(1).lt.contv.or.q(kmp).lt.contv)then  ! disque seul pour D
c                                                                   *
         nosol=1
         xi=0.
         xv=0.
         return
       endif
      endif

c prominences
      if(minpro.ne.0.and.idr.eq.1)then   !  fichiers R                non
c                                                    *
c                      contvd  pris en compte dans IV pour disque
        contin=0.5*(q(1)+q(kmp))
          pro=0.
        do n=1,kmp
          piv=abs(q(n))
        pro=amax1(piv,pro)
        enddo
          protu=pro-contin

c      if(i.eq.jp.and.protu.gt.200.)
c     1 print *,'    maxcont,minpro,contin,protu',
c     2                   maxcont,minpro,contin,protu

      if(protu.lt.float(minpro).and.q(n).lt.0.)then !.or.contin.gt.mincont)then
         nosol=1
         xi=0.
         xv=0.
      return
      endif  !  protu
      endif  !  minpro


c---
      lb=2.*xlb+0.00001   !   corde entiere
      nosol=1
      km=kmp+2
        do k=2,kmp+1
        p(k)=q(k-1)
        enddo
      p(1)=2.*q(1)-q(2)
      p(km)=2.*q(kmp)-q(kmp-1)

      cn(l)=0.
      sn(l)=0.
      cn(l+1)=0.
      sn(l+1)=0.

c      if(lb.eq.100)then  !    baryc carres de pente
c      sdp2=0.
c      sxdp2=0.
c      spdp2=0.
c        do k=2,km-1
c        dp2=(p(k+1)-p(k-1))**2
c        sdp2=sdp2+dp2
c        sxdp2=sxdp2+k*dp2
c        spdp2=spdp2+p(k)*dp2
c        enddo
c      xi=spdp2/sdp2
c      xv=sxdp2/sdp2
c      nosol=0
c      return
c      endif
c
c	pte=0.

        ddx=100000.
        yy=1000000.
        zz=0.
	xv=0.
	xi=0.

c spicules
      if(spimax.ne.0.)then  !  non
c baryc
        xi=fond
        xv=vfond
      piv=0.
      xpiv=0.      
      pivmax=0.
       ailes=0.
       ailes=amax1(p(2),p(km-1))

        do k=2,km-1
        if(p(k).gt.ailes)piv=piv+p(k)-ailes
        pivmax=amax1(pivmax,p(k))
        xpiv=xpiv+float(k-1)*(p(k)-ailes)
        enddo
   
      if(pivmax.lt.spimin)return
        cont=0.5*(p(2)+p(km-1))
      if((cont/pivmax).gt.sminmax)return
      if(pivmax.le.spimax)then
        xv=xpiv/piv
        xi=pivmax
        nosol=0
c       write(3,*)' pivmax,piv,xpiv,xv,xi ',
c     1             pivmax,piv,xpiv,xv,xi
        return
c      else
c minimum absor
c10        if(p(2).lt.discmin.or.p(km-1).lt.discmin)return
c      piv=p(2)
c      do k=3,km-1
c          piv=amin1(piv,p(k))
c      if(piv.eq.p(k))kc=k
c                    ----
c      enddo
c         xi=p(kc)
c         xv=kc
c         if(kc.gt.1.and.kc.lt.km)call parabole(p,kc,xi,xv)
c                                     --------
c         nosol=0
c           xv=xv-1.
c         return
      endif
      endif    ! fin spicules
c-------------------------------------------------------------
      if(iabs(jabs).ge.1)then     !    jabs= -1,1,-2,2,3
c        ---------------
      piv=p(1)
      do k=2,km-1
        if(jabs.gt.0)then
          piv=amin1(piv,p(k))  !  disque
        else
          piv=amax1(piv,p(k))  !  protu
        endif
      if(piv.eq.p(k))kc=k
c                    ----
      enddo

c centre raie             
         if(lb.eq.0)then                
c           -------
         xi=p(kc)
         xv=kc
         if(kc.gt.1.and.kc.lt.km)call parabole(p,kc,xi,xv)
c                                     --------
         nosol=0
           xv=xv-1.
         return     !  fin lb=0
         endif
c---------------------------------------------
c barycentre
         if(iabs(jabs).ge.2)then
c           ---------------
        if(spimax.ne.0.)then
           if(p(kc).lt.spimin.or.p(kc).gt.spimax)return
        endif
         nosol=0
         call baryc(p,km,xlb,kc,jabs,xi,xv,itest)
c             -----
            xv=xv-1.
         return     !  fin jabs=-2,2,3
         endif
      endif
c----------------------------------------------
c bissecteur   jabs = 0, 1 ou -1      
     
	ksol=0
	k1=lb+2
	  do100 k=k1,km
	  ka=k-lb-1
	  kb=ka+lb
	  d1=p(kb)-p(ka)
	  d2=p(kb+1)-p(ka+1)
	  if((d1*d2).gt.0.) goto100
	  if(d1.eq.d2)goto100
	  if((jabs.eq.1).and.(d1.gt.d2))goto100
	  if((jabs.eq.-1).and.(d1.lt.d2))goto100
        y=p(ka)+p(kb)+p(ka+1)+p(kb+1)
c
      if(jabs.eq.-1)then
      zz=amax1(y,zz)
      if(zz.eq.y)ksol=k   !  pente max
      goto100
      endif
c
      if(jabs.eq.1)then
      yy=amin1(y,yy)
      if(yy.eq.y.and.(kc-ka)*(kc-kb).le.0)ksol=k   !  pente min
      goto100
      endif
c
c 	  pt=p(kb+1)-p(kb)+p(ka)-p(ka+1)
c	  pt=abs(pt)
c         pte=amax1(pt,pte)
c         if(pt.eq.pte)ksol=k
      dx=float(ka+kb+1)/2.-xc
      dx=abs(dx)
      ddx=amin1(dx,ddx)
      if(iabs(jabs).ne.1.and.dx.eq.ddx)ksol=k   ! point le plus pres de xc
100	  continue
c
c               write(3,*)' xc, ddx ',xc,ddx
	if(ksol.eq.0)return


c                  dseuil=0.0
        nosol=0
	ka=ksol-lb-1
	kb=ka+lb
	d1=p(kb)-p(ka)
	d2=p(kb+1)-p(ka+1)

        if(d1.gt.d2)jemabs=-1  !  emission
        if(d1.eq.d2)jemabs=0
        if(d1.lt.d2)jemabs=+1  !  absorption

	del=d2-d1
c	if(abs(del)/p(ka).lt.dseuil)then
c           nosol=1
c           xi=0.
c           xv=0.
c           return
c        endif

	x=-d1/del
	xv=ka+float(lb)/2.+x
	xi=(p(ka)+p(ka+lb))*(1.-x)+(p(ka+1)+p(ka+1+lb))*x
	xi=0.5*xi

c          abscisse=(ka+x-1)/4.
c            if(abscisse.lt.0.)abscisse=0.
c          piv=abscisse-int(abscisse)
c          alpha=piv*6.28
c            ideg=piv*360.

            iph=(ka+x-1)*1000./4.
            if(iph.lt.0)iph=0

c         n      1...  2...    3...   4
c         k      1234  56...
c        iph     0     1000... 2000   3000...

c          nsn=100*alpha+1.5
c              if(nsn.lt.1.or.nsn.gt.629)then
c              print *,' depassement cos/sin   n=',nsn
c              return
c              endif

c          sn(l)=sinus(nsn)
c          cn(l)=cosinus(nsn)
c          sn(l+1)=sn(l)
c          cn(l+1)=cn(l)
              xv=xv-1.
	return
	end
c-----------------------------
C                  ------
	SUBROUTINE REJEC3(LEC,I1,I2,IA,IB,IC,ID,MILSIG,IPAS,NT,
     1                   JW,CR,ITEM,NRJ,NREJ,IFIN)
	INTEGER*2 LEC(2048),IBON(3000)
	DIMENSION XR(3000),YR(3000),PR(3000),SIGMA(20),CR(10),
     1            NREJ(90)
2       FORMAT(' PLUS DE 30 POINTS A REJETER')
3       FORMAT(' PLUS DE 20 ITERATIONS POUR LA REJECTION')
	IFIN=1
      DO N=1,10
      CR(N)=0.
      ENDDO
      DO I=1,3000
      PR(I)=1.
      ENDDO
        NS=0
        NSM=0
	  DO10 ITER=1,20
          ITEM=ITER
	  IF(ITER.NE.1)EM=SIG*MILSIG/1000.
          NSP=NSM
          NS=0
	    DO4 I=IA,ID,IPAS
            X=I
            Y=LEC(I)
	    IBON(I)=0
            IF(Y.EQ.32000.)GOTO4
	      IF(ITER.NE.1)THEN
	      E=Y-FZERO(I,NT,CR)
C                 -----
	      IF(ABS(E).GT.EM)GOTO4
	      ENDIF
	    IBON(I)=1
            NS=NS+1
            XR(NS)=X
            YR(NS)=Y
4	    CONTINUE
        if(NS.lt.10) return
        NSM=NS
        CALL DPMCAR(XR,YR,PR,NSM,NT,CR)
C            ------
5       IF(NSP.EQ.NSM)GOTO20
C
C CALCUL DE SIG, ET CHARGEMENT DE NREJ
	SIG=0.
          DO NRJ=1,30
          NREJ(NRJ)=0
          ENDDO
        NRJ=0
        S=0.
        NS=0
	  DO6 I=IA,ID,IPAS
	    IF(IBON(I).EQ.0)THEN
            NRJ=NRJ+1
              IF(NRJ.GT.90)THEN
                IF(JW.EQ.1)WRITE(6,2)
                RETURN
              ENDIF
            NREJ(NRJ)=I
            GOTO6
            ENDIF
          S=S+1.
          NS=NS+1
	  SIG=SIG+(YR(NS)-FZERO(I,NT,CR))**2
C                       -----
6	  CONTINUE
	SIG=SIG/S
        SIG=SQRT(SIG)
        SIGMA(ITER)=SIG
          IF(JW.EQ.1)WRITE(6,12)SIG
C
10	CONTINUE
12      FORMAT(' SIGMA REJECTION=',10F8.1)
        WRITE(6,12)SIGMA
          IF(JW.EQ.1)WRITE(6,3)
	RETURN
20	IFIN=0
          IF(JW.EQ.1)THEN
30        FORMAT('  CR(10),SIG  /  I REJETES  '/11E11.3)
32        FORMAT(2X,30I4)
          WRITE(6,30)CR,SIG
          WRITE(6,32)NREJ
          ENDIF      
	RETURN
	END
C-----------------------------
      function fzero(i,nt,c)
c              -----
      dimension c(10)
      fzero=c(1)
      if(nt.lt.2)return
      do n=2,nt
      fzero=fzero+c(n)*i**(n-1)
      enddo
      return
      end
c---------------------------------------
c                ---------
      subroutine zerofloat(ia,ib,ic,id,milsig,iprej,nt,lec,i1,i2,ksubs,
     1                jecri,ar,br,cr,item,nrjm,nrej,y1,y2,ifin)
      real*4 lec(2048)
      dimension nrej(90),z1(3000),z2(3000),cr(10)
      ifin=0
      ar=0.
      br=0.
      y1=0.
      y2=0.
      if(ia.eq.0)return
c
      ifin=1
      if(iprej.ne.0)then
        if(milsig.eq.0)milsig=3000
        call rejecfloat(lec,i1,i2,ia,ib,ic,id,milsig,iprej,nt,
c            ----------
     1             jecri,cr,item,nrjm,nrej,ifin)
        if(ifin.ne.0)return
        y1=fzero(i1,nt,cr)
        y2=fzero(i2,nt,cr)
c          -----
        goto30
      endif
c
      if(milsig.ne.0)then
        do i=i1,i2
        z1(i)=lec(i)
        enddo
      call parafit(z1,i1,i2,milsig,z2)
c          -------
      endif
c
      xab=0.5*(ia+ib)
      ipiv=(ib-ia)/100+1
      sn=0.
      sy=0.
        do10 i=ia,ib,ipiv
        if(lec(i).eq.32000)goto10
        sn=sn+1.
          if(milsig.eq.0)then
          PIV=LEC(I)
          sy=sy+PIV
          goto10
          endif
        sy=amax1(sy,z2(i))
10      continue
      if(sn.eq.0.)return
      if(milsig.ne.0)sn=1.
      yab=sy/sn
c
      if((ia.eq.ic).and.(ib.eq.id))then
      ar=0.
      br=yab
      y1=yab
      y2=yab
      ifin=0
      goto30
      endif
c
      xcd=0.5*(ic+id)
      ipiv=(id-ic)/100+1
      sn=0.
      sy=0.
        do20 i=ic,id,ipiv
        if(lec(i).eq.32000)goto20
        sn=sn+1.
          if(milsig.eq.0)then
          PIV=LEC(I)
          sy=sy+PIV
          goto20
          endif
        sy=amax1(sy,z2(i))
20      continue
      if(sn.eq.0.)return
      if(milsig.ne.0)sn=1.
      ifin=0
      ycd=sy/sn
      ar=(ycd-yab)/(xcd-xab)
      br=yab-ar*xab
      y1=ar*i1+br
      y2=ar*i2+br
c
30    continue
      if(jecri.ne.0)then
35    format(' xab,xcd,yab,ycd, ar,br =',6e14.6)
      write(6,35)xab,xcd,yab,ycd,ar,br
36    format(' ia,ib,ic,id,milsig,iprej,i1,i2,ksubs,item,nrjm,y1,y2,',
     1       'ifin')
37    format(11i6,2f8.1,i2)
      write(6,36)
      write(6,37)ia,ib,ic,id,milsig,iprej,i1,i2,ksubs,item,nrjm,y1,y2,
     1           ifin
38    format(' lec avant/apres(si substitution)  tous les 10')
39    format(20i6)
      write(6,38)
      write(6,39)(lec(i),i=i1,i2,10)
      endif
c
      if(ksubs.eq.0)return
        do40 i=i1,i2
        if(lec(i).eq.32000)goto40
          if(iprej.ne.0)then
          kpiv=lec(i)-fzero(i,nt,cr)
c                       -----
          lec(i)=32000
          if(iabs(kpiv).lt.32000)lec(i)=kpiv
          goto40
          endif
        kpiv=lec(i)-ar*FLOAT(i)-br
        lec(i)=32000
        if(iabs(kpiv).lt.32000)lec(i)=kpiv
40      continue
      if(jecri.ne.0)write(6,39)(lec(i),i=i1,i2,10)
      return
      end 
c------------------------------
C                  ----------
        SUBROUTINE rejecfloat(LEC,I1,I2,IA,IB,IC,ID,MILSIG,IPAS,NT,
     1                   JW,CR,ITEM,NRJ,NREJ,IFIN)
        real*4 lec(2048)
        INTEGER*2 IBON(3000)
        DIMENSION XR(3000),YR(3000),PR(3000),SIGMA(20),CR(10),
     1            NREJ(90)
2       FORMAT(' PLUS DE 30 POINTS A REJETER')
3       FORMAT(' PLUS DE 20 ITERATIONS POUR LA REJECTION')
        IFIN=1
      DO N=1,10
      CR(N)=0.
      ENDDO
      DO I=1,3000
      PR(I)=1.
      ENDDO
        NS=0
        NSM=0
          DO10 ITER=1,20
          ITEM=ITER
          IF(ITER.NE.1)EM=SIG*MILSIG/1000.
          NSP=NSM
          NS=0
            DO4 I=IA,ID,IPAS
            X=I
            Y=LEC(I)
            IBON(I)=0
            IF(Y.EQ.32000.)GOTO4
              IF(ITER.NE.1)THEN
              E=Y-FZERO(I,NT,CR)
C                 -----
              IF(ABS(E).GT.EM)GOTO4
              ENDIF
            IBON(I)=1
            NS=NS+1
            XR(NS)=X
            YR(NS)=Y
4           CONTINUE
        if(NS.lt.10) return
        NSM=NS
        CALL DPMCAR(XR,YR,PR,NSM,NT,CR)
C            ------
5       IF(NSP.EQ.NSM)GOTO20
C
C CALCUL DE SIG, ET CHARGEMENT DE NREJ
        SIG=0.
          DO NRJ=1,30
          NREJ(NRJ)=0
          ENDDO
        NRJ=0
        S=0.
        NS=0
          DO6 I=IA,ID,IPAS
            IF(IBON(I).EQ.0)THEN
            NRJ=NRJ+1
              IF(NRJ.GT.90)THEN
                IF(JW.EQ.1)WRITE(6,2)
                RETURN
              ENDIF
            NREJ(NRJ)=I
            GOTO6
            ENDIF
          S=S+1.
          NS=NS+1
          SIG=SIG+(YR(NS)-FZERO(I,NT,CR))**2
C                       -----
6         CONTINUE
        SIG=SIG/S
        SIG=SQRT(SIG)
        SIGMA(ITER)=SIG
          IF(JW.EQ.1)WRITE(6,12)SIG
C
10      CONTINUE
12      FORMAT(' SIGMA REJECTION=',10F8.1)
        WRITE(6,12)SIGMA
          IF(JW.EQ.1)WRITE(6,3)
        RETURN
20      IFIN=0
          IF(JW.EQ.1)THEN
30        FORMAT('  CR(10),SIG  /  I REJETES  '/11E11.3)
32        FORMAT(2X,30I4)
          WRITE(6,30)CR,SIG
          WRITE(6,32)NREJ
          ENDIF      
        RETURN
        END
C-----------------------------
      subroutine parabole(p,kc,xi,xv)
c                --------
      dimension p(125)
      y1=p(kc-1)
      y2=p(kc)
      y3=p(kc+1)

      a=(y1+y3-2.*y2)
        if(a.eq.0.)return
      b=(y3-y1)/2.
      c=y2

      x0=-b/(2.*a)
      xv=kc+x0
      xi=c+x0*(b+x0*a)

      return
      end
c---------------------------------
      subroutine baryc(p,km,xlb,kc,jabs,xi,xv,itest)
c                -----                     xv entre 1 et kc
      dimension p(1),xdes(125),ydes(2),q(125)
      coef=xlb/4.

      if(jabs.eq.3)then
c        ---------
      qmax=0.
        do k=2,km-1
        if(coef.lt.1.5)then
          q(k)=p(k+1)-p(k-1)
        else
          q(k)=2.*p(k)-p(k-1)-p(k+1)          
        endif
          q(k)=q(k)**2
          qmax=amax1(q(k),qmax)
        enddo

      q(1)=q(2)
      q(km)=q(km-1)
      y0=amax1(q(1),q(km))

      szg=0.     !  aires
      sxg=0.     !  abscisse G
      syg=0.     !  ordonnee G

      do k=1,km-1
      a=k
      b=k+1
      y1=q(k)
      y2=q(k+1)

      call trapeze(a,b,y0,y1,y2,xg,yg,zg)
c          -------      
      sxg=sxg+xg*zg
      syg=syg+yg*zg
      szg=szg+zg
      enddo

      if(szg.eq.0)then
        xi=0.
        xv=0.
      else
        xi=syg/szg
        xv=sxg/szg
      endif

      if(itest.eq.1)then   !   dessin
           xkm=km
        do k=1,km
        xdes(k)=k
        enddo
      call pgwindow(1.,xkm,0.,qmax)
        if(coef.lt.1.5)then
        call pgsls(2)
        else
        call pgsls(3)
        endif
      call pgline(km,xdes,q)
c          ------
        call pgsls(1)
        xdes(1)=xv
        ydes(1)=xi
        call pgpoint(1,xdes,ydes,2)
c            -------
      endif    !  fin dessin
      return
      endif    !  fin jabs = 3
c                 ------------------------------        
c      sdp2=0.
c      sxdp2=0.
c      spdp2=0.
c        do k=2,km-1
c        dp2=(p(k+1)-p(k-1))**2
c        sdp2=sdp2+dp2
c        sxdp2=sxdp2+k*dp2
c        spdp2=spdp2+p(k)*dp2
c        enddo
c      xi=spdp2/sdp2
c      xv=sxdp2/sdp2
c      nosol=0
c      return
c                              la suite pour jabs= -1,1,-2,2
c                                            ---------------
      if(jabs.gt.0)then
c        ----
        y0=p(kc)*(1.+coef)     ! absorption
c       --
      else
c        y0=p(kc)*(1.-coef)     ! emission
c        if(y0.lt.0.)return
      endif

      if(itest.eq.1)then   !   dessin
        xdes(1)=1
        xdes(2)=float(km)
        ydes(1)=y0
        ydes(2)=y0
        call pgsls(2)
        call pgline(2,xdes,ydes)
        call pgsls(1)
      endif

      szg=0.     !  aires
      sxg=0.     !  abscisse G
      syg=0.     !  ordonnee G

      do10 k=1,km-1
      yy1=p(k)
      yy2=p(k+1)
        if(jabs.lt.0)then
          y1=yy1
          y2=yy2
        else
          y1=2.*y0-yy1
          y2=2.*y0-yy2
        endif
      a=k
      b=k+1

      call trapeze(a,b,y0,y1,y2,xg,yg,zg)
c          -------
        if(itest.eq.1.and.k.eq.kc)write(3,*)
     1' trapeze k=kc : a,b,y0,y1,y2,xg,yg,zg ',
     2                 a,b,y0,y1,y2,xg,yg,zg                                               

      sxg=sxg+xg*zg
      syg=syg+yg*zg
      szg=szg+zg
10    continue

      if(szg.eq.0)then
        xi=0.
        xv=0.
      else
        xi=syg/szg
        xv=sxg/szg
      endif
        if(jabs.gt.0) xi=2.*y0-xi

      if(itest.eq.1)then
        xdes(1)=xv
        ydes(1)=xi
        call pgpoint(1,xdes,ydes,2)
      endif

      return
      end
c-------------------------------------------------
       subroutine trapeze(a,b,y0,y1,y2,xg,yg,zg)
c                 -------
      if(y0.eq.y1.and.y1.eq.y2)return

c 1-2
      if(y0.le.y1.and.y0.le.y2)then

c 1
        if(y1.le.y2)then
          x1=a
          x2=b
          xg1=(x1+x2)/2.
          xg2=x1/3.+2.*x2/3.
          yg1=(y0+y1)/2.
          yg2=2.*y1/3.+y2/3.
          zg1=(x2-x1)*(y1-y0)
          zg2=(x2-x1)*(y2-y1)/2.
c 2
        else
          x1=a
          x2=b
          xg1=(x1+x2)/2.
          xg2=x2/3.+2.*x1/3.
          yg1=(y0+y2)/2.
          yg2=y1/3.+2.*y2/3.
          zg1=(x2-x1)*(y2-y0)
          zg2=(x2-x1)*(y1-y2)/2.
        endif
C 1-2
        zg=zg1+zg2
        if(zg.le.0.)goto10    
        xg=(xg1*zg1+xg2*zg2)/zg
        yg=(yg1*zg1+yg2*zg2)/zg
        return

      endif
c 3
      if(y1.le.y0.and.y0.le.y2)then
        x1=a+(b-a)*(y0-y1)/(y2-y1)
        x2=b
        xg=x1/3.+2.*x2/3.
        yg=2.*y0/3.+y2/3.
        zg=(x2-x1)*(y2-y0)/2.
        if(zg.eq.0.)goto10
        return
      endif
c 4
      if(y2.le.y0.and.y0.le.y1)then
        x1=a
        x2=a+(b-a)*(y0-y1)/(y2-y1)
        xg=2.*x1/3.+x2/3.
        yg=2.*y0/3.+y1/3.
        zg=(x2-x1)*(y1-y0)/2.
        if(zg.eq.0.)goto10
        return
      endif
c 5-6
10    xg=0.
      yg=0.
      zg=0.
      return
      end
c-------------------------------------------
c-------------------------
      subroutine proscat(cna,txyn,im,jm,nm,milsec,inveri,txynp)
c          corrige txyn 
      integer*4 dlimb,dspic,dicalc,dijspic,inflex(150),kbord(150),
c                -20    4   40     8                   existence bord
     1         ilimb(150),ispic(150),limb(150)
c                    j         j  
      dimension xdes(1600),ydes(1600)
      dimension coefint(150,18),pte(1600),ptel(150)
c                          j  n      i
      integer*2 txyn(1600,150,18),txynp(1600,150,18)
      character*22 cna

      call par1('   dlimb',1,dlimb)  !  -20 arcsec
      if(dlimb.eq.0)then
        do i=1,im
        do j=1,jm
        do n=1,nm
          txynp(i,j,n)=txyn(i,j,n)
        enddo
        enddo
        enddo
      return
      endif

      call par1(' minprot',1,minprot)  !   25
      call par1(' mincont',1,mincont)  !  1000
      call par1('   dspic',1,dspic)  !   4 arcsec

      imarge=0

        arcsec=1000./float(milsec)   !   2.
      write(3,*)'arcsec  ',arcsec
c        karcsec=arcsec+0.5
          jc=(1+jm)/2
          nc=(1+nm)/2

c soustraction fond                          sur txyn
      do n=1,nm
      do j=1,jm
          s=0.
        do i=1,10
        kpiv=txyn(i,j,n)
c          if(kpiv.gt.2*mincont)goto 5
        s=s+kpiv
        enddo
          s=s/10.
        do i=1,im
          txyn(i,j,n)=txyn(i,j,n)-s
c          ****
          if(txyn(i,j,n).lt.0)txyn(i,j,n)=0
         txynp(i,j,n)=txyn(i,j,n)  !  presque toujours > 0
c            *              
        enddo
      enddo  !  j
      enddo  !  n
c 5    continue

c  annulation pour prot < minprot     sur txynp
      do j=1,jm
      do i=1,im
       piv1=txyn(i,j,1)
       piv2=txyn(i,j,nm)
       cont=0.5*(piv1+piv2)
          pivmax=0.
          pivmin=10000.
        do n=1,nm   !  boucle n
        piv=txyn(i,j,n)
        pivmax=amax1(piv,pivmax)
        pivmin=amin1(piv,pivmin)
        enddo
           piva=abs(pivmax-cont)
           pivb=abs(cont-pivmin)
c        write(3,*)'i,j,nm,piv1,piv2,piva,pivb,minprot',
c     1             i,j,nm,piv1,piv2,piva,pivb,minprot
      if(piva.lt.float(minprot).and.pivb.lt.float(minprot))then
           do n=1,nm         
           txynp(i,j,n)=0.
c           ****
           enddo
      endif
      enddo   !  i
      enddo   !  j
c decalages
      if(inveri.eq.1)dlimb=-dlimb     ! unites i   40

      dijspic=float(dspic)*arcsec+0.5   !   8
          dicalc=float(dlimb)*arcsec+0.5

       write(3,*)'dicalc  ',dicalc
c   inveri      di         situation fichier D (inversion fin cmd)
c        0       +           di       disk\0    
c        1       +           di       disk\0    
c        0       -                   0/disk    di
c        1       -                   0/disk    di

c  inflexion  (j)
        write(3,*)'proscat: txyn',(txyn(i,jc,1),i=1,im,100)
c-----
      do  j=1,jm                         !   j
      pte(j)=0.
c test existence de bord
          kbord(j)=0
        do i=1,im
        kpiv=txyn(i,j,1)+txyn(i,j,nm)
        if(kpiv.gt.2*mincont)kbord(j)=1
c            ******
        enddo
c calcul 
      if(kbord(j).eq.0)then
          ilimb(j)=0
          inflex(j)=0
c------------------------------
      else           !             calcul limb(j) unites i
      do i=5,im-10         !   i
      kpiv=txyn(i+2,j,1)+txyn(i+2,j,nm)-txyn(i-2,j,1)-txyn(i-2,j,nm)
       piv=kpiv
       piv=abs(piv)
       pte(j)=amax1(pte(j),piv)
          if(piv.eq.pte(j))limb(j)=i   !  indep de n
c                       -------
c      if(j.eq.jc)write(3,*)'i,piv,pte(j)',i,piv,pte(j)    ! oui
      enddo              !      i
c         if(i.eq.450)write(3,*)'j,txyn(i+2,j,1),piv450,pte',
c     1                           j,txyn(i+2,j,1),piv,pte
c       write(3,*)'j,pte(j),limb(j)',j,pte(j),limb(j)   !   oui

          ipiv=limb(j)
       inflex(j)=0.5*(txyn(ipiv,j,1)+txyn(ipiv,j,nm))+0.5! y du cont a inflex
c        ---------
      endif    !                bord
      enddo    !                 j

c calcul de ja,jb intervalle de j avec bord
        ja=0
        jb=jm
      do j=1,jm
        if(kbord(j).eq.1)then
            ja=j
            goto 10
        endif
      enddo
 10   continue
      do j=ja,jm
        if(kbord(j).eq.0)then
            jb=j-1
            goto 20
        endif
      enddo
 20   continue

c coefint(j,n)
      do j=1,jm
      do n=1,nm
          id=limb(j)+dicalc
          cd=0.5*(txyn(id,j,1)+txyn(id,j,nm))
       if(limb(j).eq.0.or.cd.eq.0)then
          coefint(j,n)=0.
       else
        coefint(j,n)=float(txyn(id,j,n))/cd
c       ----------- 
       endif
      enddo  !  n
      enddo  !  j

c calcul ispic(j)  bord spicules
      call bordspic(limb,im,ja,jb,jm,dijspic,ptel,ilimb,ispic)
c                                            out
c      do j=1,jm
c       ilimb(j)=limb(j)
c       ispic(j)=ilimb(j)
c      enddo
         write(3,*)' '
         write(3,*)'cna   ',cna
c      write(3,*)
c     1'jc,kbord,limb,inflex,ja,jb,coefint(jc,nc),ilimb,ispic',
c     2 jc,kbord(jc),limb(jc),inflex(jc),ja,jb,coefint(jc,nc),
c     3 ilimb(jc),ispic(jc)  

c     dessin
            call pgbegin(1,'limb'//cna//'.ps/ps',3,3)
c----------------------------------             boucle j
      do jp=1,jm
        j=jm-jp+1
        jpiv=(j-3)*(j-jc)*(j-jm+2)
           xbord=(float(ilimb(j))-1.)/arcsec
        xa=xbord-40.
        xb=xbord+25.
        xai=xa*arcsec+1
        xbi=xb*arcsec+1
c----------------------                         boucle n
      do n=1,nm
        npiv=(n-2)*(n-5)*(n-8)
          yb=inflex(j)*coefint(j,n)*2.2
c          ya=0.
        ya=-1000.
c---
      if(jpiv.eq.0.and.npiv.eq.0)then
           write(3,*)'j,n,limb(j),ptel(j),ilimb(j),ispic(j)',
     1                j,n,limb(j),ptel(j),ilimb(j),ispic(j)
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)
      call pgsch(2.)
      call pgslw(1)
        if(j.eq.jm-2)then
      call pglabel('arcsec','Intensity',' '//cna//' ')
        else
      call pglabel('arcsec','Intensity',' ')
        endif

         print *,'arcsec:xbord,xa,xb,ya,yb  ',xbord,xa,xb,ya,yb
      call pgwindow(xa,xb,ya,yb)   !            arcsec
      call pgsls(1)
      call pgbox('abcints',50.,5,'abcints',500.,5)
          
         print *,'index i: ilimb,xai,xbi,ya,yb  ',ilimb(j),xai,xbi,ya,yb
      call pgwindow(xai,xbi,ya,yb)
      do i=1,im
        xdes(i)=float(i)
        ydes(i)=0.5*(txyn(i,j,1)+txyn(i,j,nm))*coefint(j,n)
      enddo   
        call pgsls(1)
      call pgline(im,xdes,ydes)   !                int  cont * coefint

c---   limb
      xdes(1)=float(limb(j))
      xdes(2)=xdes(1)
      ydes(1)=0.
      ydes(2)=inflex(j)*coefint(j,n)
           print *,'xdes,ydes',xdes(1),xdes(2),ydes(1),ydes(2)
        call pgsls(2)
      call pgline(2,xdes,ydes)
      xdes(1)=0.
      ydes(1)=inflex(j)*coefint(j,n)
      ydes(2)=ydes(1)           
         print *,'xdes,ydes',xdes(1),xdes(2),ydes(1),ydes(2)
      call pgline(2,xdes,ydes)

c      limb+dicalc
        ipiv=limb(j)+dicalc
      xdes(1)=ipiv
      xdes(2)=xdes(1)
      ydes(1)=0.
      ydes(2)=0.5*(txyn(ipiv,j,1)+txyn(ipiv,j,nm))*coefint(j,n)
           print *,'xdes,ydes',xdes(1),xdes(2),ydes(1),ydes(2)
      call pgline(2,xdes,ydes)

c---             dessin                      int line
      print *,' '
      print *,'mslib1251 j,n, txyn(i,j,n)     ',
     1                 j,n,(txynp(i,j,n),i=1,im,100)
      do i=1,im
        xdes(i)=float(i)
        ydes(i)=txyn(i,j,n) 
      enddo   
        call pgslw(2)
        call pgsls(2)
      call pgline(im,xdes,ydes)   !       int 

      endif   !    jpiv,npiv
c---
c                                        nouveau txyn
        imax=ispic(j)  !limb(j)  !  nouveau bord
c            *
        if(imax.gt.1)then   !    protu+ciel

      do i=1,imax
        kpiv=txynp(i,j,n)
     1           -0.5*(txynp(i,j,1)+txynp(i,j,nm))*coefint(j,n) ! correction
      if(kpiv.le.0)then
          txynp(i,j,n)=-1
      else
        txynp(i,j,n)=-kpiv   !  protu et ciel  int < 0 
      endif
      enddo   !   i
        endif ! imax
c annulation entre ilimb et ispic
              imarge=0
      do i=1,im      
        kpiv=(i-limb(j)-imarge)*(i-ispic(j))
        if(kpiv.le.0)then
           txynp(i,j,n)=0.
c              *
        endif   
      enddo
c---            dessin int corrigee
      if(jpiv.eq.0.and.npiv.eq.0)then  !  tests dessins
      print *,'mslib1282 j,n, txyn(i,j,n) corr ',
     1                 j,n,(txynp(i,j,n),i=1,im,100)
      do i=1,im
        xdes(i)=float(i)
        ydes(i)=txynp(i,j,n)   !   dessin  > 0  oui neg pour protu
c                       *
      enddo   
        call pgslw(3)
        call pgsls(1)
      call pgline(im,xdes,ydes)     !     int corr    
      endif   !   jpiv,npiv
c-------------------------
      enddo   !    n
c-----------------------------------------
      enddo   !    j
      call pgend
      return
      end
c====================================
      subroutine bordspic(limb,im,ja,jb,jm,dijspic,ptel,ilimb,ispic)
c                                               out
      dimension limb(150),ilimb(150),ispic(150),xj(2000),yj(2000),
     1             p(2000),climb(10),ptel(150)
c               coef
      integer*4 dijspic
c             ecart en pixels ppd bord

c      ilimb(0)=0
c      do j=1,jm
c       ilimb(j)=limb(j)
c       ispic(j)=limb(j)   !   -4*dijspic
c      enddo
c         return

        nd=10 ! nbre Donnees
        nt=3  ! nbre Termes
            interv=(jb-ja)/20.
         do n=1,nd
           p(n)=1.
           jpiv=(2*n-1)*interv
         xj(n)=jpiv
         yj(n)=limb(jpiv)
          enddo
      write(3,*)' '
      write(3,*)'xj  ',xj
      write(3,*)'yj  ',yj

      call dpmcar(xj,yj,p,nd,nt,climb)
c Ce sous-programme [ Schneider 75 ] calcule par une 
c methode de moindres carres en double precision le 
c polynome de NT termes (10 max) Y=F(X) associe a ND mesures.

      do j=1,jm
          piv=climb(nt)             ! c3
        do kp=2,nt
          k=nt-kp+1
        piv=piv*float(j)+climb(k)  !j*c3 + c2
c                                   j2*c3 + c2*j +c1
        enddo
          ilimb(j)=piv

c          ispic(j)=ilimb(j)-4*dijspic
c      ilimb(j)=climb(1)+float(j)*climb(2)+climb(3)*float(j)**2

      ptel(j)=2.*climb(3)*float(j)+climb(2)
        dspi=dijspic    !  8  entier 
      dj=dspi*sqrt(1.+ptel(j)**2)
      ispic(j)=ilimb(j)-dj+0.5
      enddo

      write(3,*)'limb  ',(limb(j),j=1,jm,20)
      write(3,*)'ilimb ',(ilimb(j),j=1,jm,20)
      write(3,*)'ispic ',(ispic(j),j=1,jm,20)

      return
      end
c=======================================
      subroutine paralimb(tab,iitab,jje,ii1,ii2,jj1,jj2,
     1                               nmlimb,  jlimb)
c                                   int limb  out
      integer*2  tab(iitab,jje)
      dimension xj(2000),yj(2000),p(2000),coef(10),
c                  i        i       i
     1          jlimb(2000),jpara(2000)
c                     ii          ii
      write(3,*)'paralimb iitab,jje,ii1,ii2,jj1,jj2,nmlimb ',
     1                    iitab,jje,ii1,ii2,jj1,jj2,nmlimb
      zlimb=nmlimb
      im=ii2-ii1+1
c calcul jlimb
      do i=1,im
        ii=i+ii1-1   !  dans tab
      xj(i)=ii1+i-1
      yj(i)=0.
      jlimb(ii)=0
      p(i)=0.
        do jj=jj1+1,jj2
        dif=(tab(ii,jj)-zlimb)*(tab(ii,jj-1)-zlimb)
        if(dif.le.0)then
           yj(i)=jj
           jlimb(ii)=jj
           p(i)=1.
        endif
        enddo
      enddo
c calcul para
        nd=im
        nt=3
      call dpmcar(xj,yj,p,nd,nt,coef)
c Ce sous-programme [ Schneider 75 ] calcule par une 
c methode de moindres carres en double precision le 
c polynome de NT termes (10 max) Y=F(X) associe a ND mesures.
      do ii=ii1,ii2
        i=ii-ii1+1   !    dans DPMCAR
        jpara(ii)=coef(1)+float(ii)*(coef(2)+float(ii)*coef(3)) ! dans tab
      enddo

 1    format(15i6)
 2    format(15f6.3)
        isaut=20
      write(3,*)'paralimb coef,ii,poids,jlimb,jpara'
      write(3,*)(coef(k),k=1,3)
      write(3,1)(ii,ii=ii1,ii2,isaut)
      write(3,2)(p(i),i=1,im,isaut)
      write(3,1)(jlimb(ii),ii=ii1,ii2,isaut)
      write(3,1)(jpara(ii),ii=ii1,ii2,isaut)
      return
      end
c----------------------------
      subroutine cutchrom(tab,iitab,jje,jlimb,jhchrom,ii1,ii2,
     1                    jj1,jj2,jmax,lnew,lbis)
c                         new but same diff (transl)
      integer*2  tab(iitab,jje)
      dimension jlimb(2000)

c cut chrom+disk
      if(lnew.eq.lbis.or.lnew.eq.lbis+1)then
         do i=ii1,ii2
         jchrom=jlimb(i)-jhchrom
           do j=jj1,jj2
           if(j.gt.jchrom)tab(i,j)=0.
           enddo
         enddo
      endif

c new jj1,jj2
      if(lnew.eq.1.and.jmax.ne.0)then
      call par1('mildecal',1,mildecal)
      ilimb0=(ii1+ii2)/2
      jlimb0=jlimb(ilimb0)
      jdecal=float(jmax)*float(mildecal)/1000.
      j1=jlimb0-jdecal
      j2=j1+jmax-1
      jj1=j1
      jj2=j2
      endif

      return
      end
c-------------------------------------------------------


        



