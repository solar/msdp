c msdp2.f
c========
c                            bmg1.f, bmc1.f, cmf1.f
c**************************
c  Programme bmg1 - UNIX
c*************************************************************************
      subroutine bmg1(nw,win,nm,iux,iuy,iug,gname,istop,ima,ijcam)
c                              iux inutilise
c       character comm*80,fin*3,end*3
       character*22 gname
       dimension kz(512)
       integer si,sgi,sj,sgj,distor,win(8)
c       integer winp,sgj1,sgj2
       integer*2 ima(ijcam,ijcam) ! ,zmi(2000,3)
c                     attention images < 1536*1536
c---------------------
c                       voir SRECT
c      is=1536
c      js=1536
c     leps    ! intervalle max entre point probable (seuil) et gradient max
c      milanglei=60
c        anglei=milanglei*0.001
c      idangle=0.2*(i2-i1)
c              demi-disrtance entre les 2 coupes utilisees pour estimer 
c                                                          l'angle/i   
c                  coupes a ic+/-idangle, integration sur +/-intvi
c     lip         pourcentage ecarts en I pour parabole haut/bas (bords J)
c          ex: 30
c      intercan=6  ! nombre de pixels entre canaux <---------- modifiable
c     icp       inutilise:
c         coupes //j autour de ic=(i1+i2)/2
c     jeps        pour la determination precise des points en J,
c               recherche +/-jeps  autour des valeurs approchees
c     intvi  intervalle integration en i pour detection bords j
c     intvj                            j                      i
c--------------------------------------
       print *,'debut bmg1: nw,gname',nw,gname
       write(3,*)'debut bmg1: nw,gname',nw,gname
1      format(a)
2      format(1x,a)
3      format(12i6)
4      format(1x,12i6)
5      format(//)
6      format(3a4,3i8)
7      format(2x,3a4,3i8)

      call par1('   nbcln',nw,nbcln)
c            coef=float(nbcln)/1000.
      call par1('  interc',1,interc)
c                                     interc=interc*coef
      call par1('      si',nw,si)
      call par1('      sj',nw,sj)
      call par1('     sgi',nw,sgi)
      call par1('     sgj',nw,sgj)
      call par1(' milangi',nw,milangi)
      call par1(' milangj',nw,milangj)
      call par1('  milgeo',nw,milgeo)

      call par1('      i1',nw,i1)
      call par1('     i2m',nw,i2m)
      call par1('      j1',nw,j1)
      call par1('     j2m',nw,j2m)
      call par1('     lip',nw,lip)
c                                    lip=lip*coef
      call par1('    jeps',nw,jeps)
c                                    jeps=jeps*coef
      call par1('   intvi',nw,intvi)
c                                    intvi=intvi*coef
      call par1('   intvj',nw,intvj)
c                                    intvj=intvj*coef
      call par1('    leps',nw,leps) 
c                                    leps=leps*coef
      call par1('      n1',nw,n1)
      call par1('  distor',nw,distor)

      call par1('  normsq',nw,normsq)
      call par1('    norm',nw,norm)
      call par1(' largrid',nw,largrid)

       n2=nm-n1+1
       nc=(n1+n2)/2
       n3n4=0

       rewind(iuy)
       read(iuy)kz
       write(3,3)(kz(i),i=1,8)
       im=kz(2)
       jm=kz(3)
c
      write(3,*)' bmg: i1,i2m,im,  j1,j2m,jm ',i1,i2m,im,  j1,j2m,jm
          write(3,5)
      call srect(milangi,milangj,i1,i2m,im,icp,j1,j2m,jm,lip,jeps,
c          -----
     1           intvi,intvj,interc,
     2           si,sgi,sj,sgj,leps,
     3           nm,n1,n2,nc,n3n4,distor,
     4           iux,iuy,iug,gname,
     5           ima,milgeo,istop,ijcam,nw,normsq,norm,largrid)
           write(3,5)

c        if(istop.eq.1)stop
       return
       end    

c----------------------
      subroutine SRECT(milangi,milangj,i1,i2m,im,icp,j1,j2m,jm,lip,jeps,
     1                 intvi,intvj,intercan,
     2                 sip,sgip,sj,sgj,leps,nm,n1,n2,nc,n3n4,
     3                 distor,
     4                 iux,iuy,iug,gname,
     5                 ima,milgeo,istop,ijcam,nw,normsq,norm,largrid)
      integer*2 lec(1536),ima(ijcam,ijcam),lecx(1536)
      dimension z(1536),zg(1536),x(1536),zmoy(1536),zmoyg(1536),
     1        xr(18,3,2),yr(18,3,2),yc(18,2,2),yca(18,2),
     2        nkbon(18,2),zmi(2000,3),y(1536),
     3        vi(18,6,6),vj(18,6,6),vmod(18,6,6),flech(18,2),ili(18,2),
     4        xdes(7),ydes(7),avi(6,6,3),avj(6,6,3),avmod(6,6,3),

     5       kz(512),zd(1536),xbord(18,5,2),ybord(18,5,2),
     6       integ(2,3),ecarti(18,6,6),ecartj(18,6,6),ecartmod(18,6,6),
c                deb-fin
     7       ksi(20),ksj(20),ksgi(20),ksgj(20)
      integer sip,sgip,si,sgi,sj,sgj,sgj1,sgj2,distor,displ
      character titre*54,gname*22
c      integer*2 listj(1536)
c      dimension cis(3,2),ypar(11,3,2)
c      character nomps(24),gameo(22)

      data ksi /2,2,4,4,4,6,6,6,9,9,9, 12,12,12,15,15,15,20,20,20/
      data ksgi/2,2,4,4,4,6,6,6,9,9,9, 12,12,12,15,15,15,20,20,20/
      data ksj /2,4,2,4,6,4,6,9,6,9,12, 9,12,15,12,15,20,15,20,25/
      data ksgj/2,4,2,4,6,4,6,9,6,9,12, 9,12,15,12,15,20,15,20,25/


      call system('rm channel.lis')
      open(unit=95,file='channel.lis',status='new')

      displ=0

c          indices      xbord (X//i)
c                       ybord (y//j)
c                  (canal n)
c                 
c                                  n,5,1   n,5,2
c                                  --+-------+--
c                                  |           |
c                           n,3,1  +           +  n,3,2
c                                  |           |
c                           n,2,1  +           +  n,2,2
c                                  |           |
c                           n,1,1  +           +  n,1,2
c                                  |           |
c                                  --+-------+--
c   
c                                  n,4,1   n,4,2

c        nsm=21
         nsm=20
        if(sip.ne.0)nsm=1
        ecmin=10000.
        nsol=0
        ipl=0
        ecartbis=1000.

      call par1('     idc',1,idc)
      call par1(' kdangle',1,kdangle)
c        if(kdangle.eq.0)kdangle=200
c                     idangle=kdangle*(i2-i1)/1000.
c=================================================================
      do 190 nseuils=1,nsm  !      ----------------------- nseuils
       if(nseuils.eq.nsm)ipl=1
c                        threshold loop
1     format(' bmg: nseuils, si,sgi,sj,sgj',i3,2x,4i4)

      if(sip.eq.0)then
c      si=3*nseuils
c        if(nseuils.eq.nsm)si=3*nsol
      si=ksi(nseuils)
         if(nseuils.eq.nsm)si=ksi(nsol)
      sgi=ksgi(nseuils)
         if(nseuils.eq.nsm)sgi=ksgi(nsol)
         if(sgip.lt.0)sgi=-sgi
      sj=ksj(nseuils)
         if(nseuils.eq.nsm)sj=ksj(nsol)
      sgj=ksgj(nseuils)
         if(nseuils.eq.nsm)sgj=ksgj(nsol)

      else
      si=sip
      sgi=sgip
      endif
               print 1,  nseuils,si,sgi,sj,sgj
               write(3,1)nseuils,si,sgi,sj,sgj
      is=ijcam
      js=ijcam
      n3=n3n4/10
      n4=n3n4-10*n3
        if(n3.eq.0)n3=n1
        if(n4.eq.0)n4=n1

c     leps    ! intervalle max entre point probable (seuil) et gradient max

        anglei=milangi*0.001
        anglej=milangj*0.001

c      kdangle=  lu sur ms.par  (idangle=kdangle*(i2-i1)/1000)
c                  coupes a ic+/-idangle, integration sur +/-intvi
c     lip         pourcentage ecarts en I pour parabole haut/bas (bords J)
c      intercan= nombre de pixels entre canaux
c     icp         inutilise


      xa=0.1
      xb=0.6
       
      xc=0.65 !0.7
      xd=0.95 !0.9
      xcd=0.80  !0.5*(xc+xd)

        xcd1=0.75
        xcd2=0.85

        do2 n=1,18 !                             ????????????
c                    10 bords detectes sur 4 cotes (+2 inutilises)
        do kg=1,2
          do ihb=1,5
          xbord(n,ihb,kg)=0.
          ybord(n,ihb,kg)=0.
          enddo
c                     6 points de reference sur 2 cotes
          do ihb=1,3
          xr(n,ihb,kg)=0.
          yr(n,ihb,kg)=0.
          enddo
        enddo
2       continue

      i2=im-i2m
      idangle=float(kdangle)*(i2-i1)/1000.
c                            0.2*(i2-i1)
          write(3,*)'idangle',idangle
      ic=0.5*(i1+i2)+0.5
      centre=0.5*(i1+i2)
      icentre=ic  

      j2=jm-j2m
c      ia=ic-intvi
c      ib=ic+intvi
         iap=ic-intvi
         ibp=ic+intvi
c      write(3,*)' im,jm,i2,ic,j2,ia,ib ',im,jm,i2,ic,j2,ia,ib
c
        do j=1,ijcam
        do ihmb=1,3
        zmi(j,ihmb)=0.
        enddo
        enddo
c
          den=normsq
          den=den/10.
      do4 ilec=1,3
        den=den*10
      rewind(iuy)
      read(iuy)kz
c---------------------          stockage de l'image entiere

       rewind(iux)
       read(iux)kz
       write(3,*)(kz(i),i=1,8)
c       im=kz(2)
c       jm=kz(3)

      iia=i1
      iib=i1+4
      iic=i2-4
      iid=i2

         print *,' Soustraction y-x    im,jm ',im,jm
      do j=1,jm
      read(iuy)(lec(i),i=1,im)
        if(idc.eq.1)then
        read(iux)(lecx(i),i=1,im)
         do i=1,im
           lec(i)=lec(i)-lecx(i)
           if(lec(i).lt.0)lec(i)=1
c                 piv=abs(lec(i))
c                 lec(i)=sqrt(piv)
         enddo
        endif
c      if(j.eq.200)write(3,*)' bmg:lecx ',(lecx(i),i=1,100,10)
      if(j.eq.200)write(3,*)' bmg:lec  ',(lec(i),i=1,100,10)

c      call zero3(iia,iib,iic,iid,0,0,0,lec,1,im,1,0,
c     1          az1,az2,az3,az4,az5,az6,az7,az8,az9)
c                          on retranche une fonction lineaire joignant
c                          les fonds en debut et fin de ligne
c        if(j.eq.200)write(3,*)' apres zero'
c        if(j.eq.200)write(3,*)(lec(i),i=1,286)
        do i=1,im
          if(normsq.eq.0)then
          ima(i,j)=lec(i)
          else
          kpiv=lec(i)
          ipiv=kpiv*kpiv/den
            if(ipiv.gt.32000)goto4
          ima(i,j)=ipiv
          endif
        enddo
      enddo
        goto5
4     continue
5     continue
         write(3,*)'ima', ima(1,1),ima(im,jm)
c--------------
c                                  dessin ima
      nplot=1
      iperm=1
      if(nseuils.eq.1)then
         call xyplot(nplot,ima,ijcam,1,im,1,jm,iperm)
         call pgbegin(0,'geo.ps/ps',1,1) 
         call pgslw(2)
         call pgsch(1.)
         call pgsls(1)      
      endif
c-------------------------------------------------------------
c      if(displ.eq.1)call pgbegin(0,'?',1,1)
c      if(displ.eq.2)call pgbegin(0,'/xwin',1,1)
c      if(displ.eq.3)then
c        nomps=gname//'.ps'
c        call system('rm '//nomps)
c        if(ipl.eq.0)if(ipl.ne.0)call pgbegin(0,nomps//'/ps',1,1)
c      endif

      if(ipl.ne.0)call pgsch(1.) !     0.6)
c
        fond=100000.
        zmax=0.
c      print *,'jm :',jm
c------------------------------------------------------------
      do24 iangle=1,2      !!!  boucle iangle------------
10    format(/' iangle=',i1)
      write(3,10)iangle
        ia=iap+(2*iangle-3)*idangle
        ib=ibp+(2*iangle-3)*idangle

      call scanj(ima,is,js,ia,ib,j1,j2,anglei,anglej,z,ijcam)
c          -----

      if(iangle.eq.1)then  !!  on definit zmax,zfond a la premiere coupe  --
      fond=amax1(z(j1),z(j2))
        do j=j1,j2
c        fond=amin1(z(j),fond)
        zmax=amax1(z(j),zmax)
        enddo
      endif

c3     continue
      write(3,*)' fond,zmax ',fond,zmax

      if(ipl.ne.0)call pgvport(xa,xb,.70,.90)  !   pgvport
        if(ipl.ne.0)call pgslw(1)
        if(ipl.ne.0)call pgsch(1.)  !0.8)
      x1=1
      x2=jm
      z1=0
      z2=(zmax-fond)*1.

      if(ipl.ne.0)then
c        call pgsch(1.)
        call pgslw(2)
      call pgwindow(x1,x2,0.,100.)
      call pgbox('bcts',200.,2,'bcnts',20.,2)
      call pglabel(' ','Intensity',' ')
        piv=float(si)+5.
      call pgtext(120.,piv,'sj')
      endif

c      call pgwindow(x1,x2,z1,z2)
      xdes(1)=j1
      xdes(2)=j1
      ydes(1)=z1
      ydes(2)=z2
      if(ipl.ne.0)call pgsls(2)
      if(ipl.ne.0)call pgline(2,xdes,ydes)
      xdes(1)=j2
      xdes(2)=j2
      if(ipl.ne.0)call pgline(2,xdes,ydes)
      if(ipl.ne.0)call pgsls(1)
        do j=1,jm
        x(j)=j
        zd(j)=(z(j)-fond)*100./(zmax-fond)     !  courbes z
        enddo
        write(3,*)'iangle,zmax,fond,zd ',
     1    iangle,zmax,fond,(zd(j),j=1,jm,50)

      if(iangle.eq.2.and.ipl.ne.0)call pgsls(2)
      if(ipl.ne.0)call pgline(jm,x,zd)
      if(ipl.ne.0)call pgsls(1)
c
      zseuil=sj  !*sj*(zmax-fond)/100.  !!  pour dessin  zseuil
 
      x(2)=jm
      y(1)=zseuil
      y(2)=y(1)
      if(ipl.ne.0)call pgsls(2)
      if(ipl.ne.0)call pgline(2,x,y)
      if(ipl.ne.0)call pgsls(1)
      x(2)=2
c
        if(iangle.eq.1)zgmax=0.
        do j=2,jm
        zg(j-1)=(z(j)-z(j-1))
        piv=abs(zg(j-1))
        if(iangle.eq.1)zgmax=amax1(zgmax,piv)  !!  premiere coupe
        enddo

c                                                 gradients
c        write(3,*)' zgmax',zgmax
      z2=zgmax
      z1=-z2
      jm1=jm-1

      if(ipl.ne.0)then
        call pgsch(1.)
        call pgslw(2)
      call pgvport(xa,xb,.50,.70)
      call pgwindow(x1,x2,-100.,100.)
      call pgbox('abcnts',200.,2,'bcnts',50.,5)
        sgi2=sgi+10
        sgi3=-sgi-25
      call pgtext(120.,sgi2,'sgj')
      call pgtext(70.,sgi3,'-sgj')

      call pgwindow(x1,x2,z1,z2)
      call pglabel(' ','Intensity gradient',' ')
      call pgline(jm1,x,zg)
      endif
c
      sgj1=sgj
      sgj2=sgj

      zgseuil=sgj1*zgmax/100.                 !  zgseuil

      if(ipl.ne.0)call pgsls(2)
      x(2)=jm
      y(1)=zgseuil
      y(2)=y(1)
      if(ipl.ne.0)call pgline(2,x,y)
      y(1)=-y(1)
      y(2)=y(1)
      if(ipl.ne.0)call pgline(2,x,y)
      if(ipl.ne.0)call pgsls(1)
      x(2)=2
c
c                                      Bords approches en J   YC(N,KG)
c      xb1=xb+0.02
       xb1=xb+0.01
       xb2=xb1+0.1

c      xb2=xb1+0.1
      piva=0.15
      pivb=piva+0.1
      if(ipl.ne.0)call pgvport(xb1,xb2,piva,pivb)
      if(ipl.ne.0)call pgwindow(0.,1.,0.,1.)
c      if(ipl.ne.0)call pgtext(0.,0.,'A-D')
      piva=piva+0.10
      pivb=piva+0.1
      if(ipl.ne.0)call pgvport(xb1,xb2,piva,pivb)
      if(ipl.ne.0)call pgwindow(0.,1.,0.,1.)
c      if(ipl.ne.0)call pgtext(0.,0.,'B-E')
      piva=piva+0.1
      pivb=piva+0.1
      if(ipl.ne.0)call pgvport(xb1,xb2,piva,pivb)
      if(ipl.ne.0)call pgwindow(0.,1.,0.,1.)
c      if(ipl.ne.0)call pgtext(0.,0.,'C-F')
c
      if(ipl.ne.0)call pgvport(xa,xb,.10,.40)
      y1=1.
      y2=im
      if(ipl.ne.0)call pgwindow(x1,x2,y1,y2)
      if(ipl.ne.0)call pgbox('bcnts',200.,2,'bcnts',200.,2)
      if(ipl.ne.0)call pglabel('J','I',' ')
      xdes(1)=1
      xdes(2)=jm
      ydes(1)=i1
      ydes(2)=i1
      if(ipl.ne.0)call pgsls(2)
      if(ipl.ne.0)call pgline(2,xdes,ydes)
      ydes(1)=i2
      ydes(2)=i2
      if(ipl.ne.0)call pgline(2,xdes,ydes)
      if(ipl.ne.0)call pgsls(1)
c
      zseuil=zseuil+fond
      zgseuil=zgseuil
        write(3,*)' zseuil,zgseuil ',zseuil,zgseuil

c                                    bords extremes
        isig=-1
        icroiss=2
      call maxgrad(z,j1,j2,isig,icroiss,zseuil,zgseuil,
c          -------
     1             yc(n2,2,iangle),1,0,nodet,ijcam)
c           if(nodet.eq.1) goto 190

c                                            kg droite
        write(3,*)' droite',yc(n2,2,iangle)

        isig=+1
        icroiss=1
         write(3,*) ' j1,j2,isig,icroiss ',j1,j2,isig,icroiss
      call maxgrad(z,j1,j2,isig,icroiss,zseuil,zgseuil,
c          -------
     1             yc(n1,1,iangle),2,0,nodet,ijcam)
c           if(nodet.eq.1) goto 190

c                                            kg gauche
        write(3,*)' gauche',yc(n1,1,iangle)

      can=(yc(n2,2,iangle)-yc(n1,1,iangle)+intercan)/float(n2-n1+1)
c         largeur moy canaux
          write(3,*)' can ',can
c                                    bords KG=2   (droite canaux)
c                      
      do n=n1,n2-1
      iprobable=yc(n2,2,iangle)-(n2-n)*can  !      i probables
      ideb=iprobable-leps
      ifin=iprobable+leps
          ideb=max0(ideb,i1)
          ifin=min0(ifin,i2)
        isig=-1
        icroiss=0
      call maxgrad(z,ideb,ifin,isig,icroiss,zseuil,zgseuil,
c          -------
     1             yc(n,2,iangle),3,n,nodet,ijcam)
c           if(nodet.eq.1) goto 190

        if(yc(n,2,iangle).eq.0.)then
        yc(n,2,iangle)=iprobable
        write(3,*)' attention point yc: iprobable=',iprobable
        endif
      enddo

c                                    bords kg=1   (gauche canaux)
      do n=n1+1,n2
      iprobable=yc(n1,1,iangle)+(n-n1)*can
      ideb=iprobable-leps
      ifin=iprobable+leps
          ideb=max0(ideb,i1)
          ifin=min0(ifin,i2)

        isig=1
        icroiss=0
      call maxgrad(z,ideb,ifin,isig,icroiss,zseuil,zgseuil,
c          -------
     1             yc(n,1,iangle),4,n,nodet,ijcam)
c            if(nodet.eq.1) goto 190

        if(yc(n,1,iangle).eq.0.)then
          write(3,*)'  attention point yc: iprobable=',iprobable
          yc(n,1,iangle)=iprobable
        endif
      enddo  

      do n=n1,n2
        piv=yc(n,2,iangle)
        dpiv=(ib-ia)*0.5*anglei
        x(1)=piv-dpiv
        x(2)=piv+dpiv
        y(1)=ia
        y(2)=ib
c        if(ipl.ne.0.and.iangle.eq.2)call pgline(2,x,y)
c      write(3,*)'Left,edge  n,x,x,y,y ',n,x(1),x(2),y(1),y(2)
      enddo

      do n=n1,n2
        piv=yc(n,1,iangle)
        x(1)=piv-dpiv
        x(2)=piv+dpiv
c        if(ipl.ne.0.and.iangle.eq.2)call pgline(2,x,y)
c      write(3,*)'Left,edge  n,x,x,y,y ',n,x(1),x(2),y(1),y(2)
      enddo
 
20    format(/'  n1=',i2,'  n2=',i2)
c      write(3,20)n1,n2
22    format(/'  YC(N,KG,iangle)'/18f7.1/18f7.1/18f7.1/18f7.1)
c      write(3,22)yc

24    continue   !!! fin boucle iangle-----
c------------------------------------------------------------
c      write(3,*)' End loop angle'
c      print *,' End loop angle'

      if(idangle.ne.0) call sangle(yc,n1,n2,idangle,angle)
c                           ------
      write(3,*)' angle ',angle


c      anglei=angle  !   conserve anglei


      do kg=1,2
      do n=n1,n2
        yca(n,kg)=0.5*(yc(n,kg,1)+yc(n,kg,2))
c                                              en ic=0.5*(i1+i2)+0.5
      enddo
      enddo
c-------------------------------------------------------------------
c                           Bords en I    xbord(N,IHMB,KG)
      
        if(ipl.ne.0)call pgslw(3)
        Idessin=0
c
      do40 n=n1,n2  !!!  boucle canaux -------------
        print *,'bords I: n,yca(n,1),yca(n,2):',n,(yca(n,kg),kg=1,2)

           if(n.gt.n3.and.n.lt.n4)goto40

      do38 kg=1,2   !!!  boucle gauche-droite ---

      if(n.eq.n2.and.kg.eq.2.and.idessin.ne.2)idessin=1
      if(yca(n,kg).eq.0.)goto38

      nkbon(n,kg)=1

c                        dj = decalage en j entre ic et (i1+i2)*0.5 
       dj=(0.5*(i1+i2)-ic)*anglei
c                                    voisin de 0
      jdeb=yca(n,kg)+dj
      jfin=jdeb+intvj
        if(kg.eq.2)then
        jdeb=jdeb-intvj
        jfin=jfin-intvj
c                      rapportes a 0.5*(i1+i2)
        endif

      call scani(ima,is,js,i1,i2,jdeb,jfin,anglei,anglej,
c          -----
     1           zmoy,ijcam,fond,zmax,norm)

        zseuili=fond+si*(zmax-fond)/100.
        zgseuili=sgi*zgmax/100.
        igsign=1
        if(sgi.lt.0)igsign=-1
        zgseuili=abs(zgseuili)
c          write(3,*)' zseuili,zgseuili ',zseuili,zgseuili

      do i=i1+1,i2
      zmoyg(i)=zmoy(i)-zmoy(i-1)
      enddo
      goto260
c
c                                      dessin coupe  en  I
250   Idessin=2
      if(ipl.ne.0)call pgsls(1)
      if(ipl.ne.0)call pgslw(2)
      x(1)=yca(n,kg)+dj+(i1-icentre)*anglei
      x(2)=yca(n,kg)+dj+(i2-1-icentre)*anglei
      y(1)=i1
      y(2)=i2-1

 203  format(2i4,6f8.1)
c      write(3,*)'Left,Right  n,kg,x,x,y,y ',n,kg,x(1),x(2),y(1),y(2)
c      write(95,*)'Left,Right'
c      write(95,203) n,kg,x(1),x(2),y(1),y(2),0.,0.

      if(ipl.ne.0)call pgline(2,x,y)  !!! ligne non trouvee
        do i=i1,i2-1
        y(i)=i
        enddo
        zcmin=10000.
        zcmax=0.
        zcgmin=10000.
        zcgmax=-10000.
c-------------
      if(ipl.ne.0)call pgvport(xcd,xd,.10,.40)
        piv=1.9*si
      if(ipl.ne.0)call pgwindow(0.,piv,y1,y2)
       xsi=si
      if(ipl.ne.0)call pgbox('bcmts',xsi,1,'bc',0.,0)
      if(ipl.ne.0)call pglabel('si',' ',' ')

        piv=1.9*zseuili
      if(ipl.ne.0)call pgwindow(0.,piv,y1,y2)
      if(ipl.ne.0)call pgline(i2-1,zmoy,y)
c                      --
      if(ipl.ne.0)call pgsls(2)
        x(1)=zseuili
        x(2)=x(1)
        y(2)=im
        if(ipl.ne.0)call pgline(2,x,y)
      if(ipl.ne.0)call pgsls(1)
c--------------
      if(ipl.ne.0)call pgvport(xc,xcd,.10,.40)
        piv=1.9*sgi
      if(ipl.ne.0)call pgwindow(-piv,piv,y1,y2)
        xsgi=sgi
      if(ipl.ne.0)call pgbox('bcmts',xsgi,1,'bc',0.,0)
      if(ipl.ne.0)call pglabel('-sgi     sgi',' ',' ')

        piv=1.9*zgseuili
      if(ipl.ne.0)call pgwindow(-piv,piv,y1,y2)
      if(ipl.ne.0)call pgsls(2)
        y(2)=im/2
        x(1)=zgseuili*igsign
        x(2)=x(1)
        if(ipl.ne.0)call pgline(2,x,y)
        y(1)=im/2
        y(2)=im
        x(1)=-x(1)
        x(2)=x(1)
        if(ipl.ne.0)call pgline(2,x,y)
      y(1)=1
      y(2)=2
      if(ipl.ne.0)call pgsls(1)
c-------------------
      if(ipl.ne.0)call pgline(i2-1,zmoyg,y)
c                      --
      if(ipl.ne.0)call pgslw(3)
      if(ipl.ne.0)call pgvport(xa,xb,.10,.40)
      if(ipl.ne.0)call pgwindow(x1,x2,y1,y2)
      goto38
c
c                                     Detection bords en I
c                                       xbord,ybord (n,4a5,kg)
260   continue 
c      write(3,*)' i1,icentre,i2 ',i1,icentre,i2
c                        if(khb.eq.2)goto27  !!!              bas
      do26 i=i1+2,icentre-1
      if((zmoyg(i)-zmoyg(i-1))*igsign.lt.0.)goto26
      if((zmoyg(i)-zmoyg(i+1))*igsign.lt.0.)goto26
      if((zmoyg(i)-zgseuili*igsign)*igsign.lt.0.)goto26
c      if((zmoy(i+leps)-zseuili)*igsign.lt.0.)goto26
c      if((zmoy(i-leps)-zseuili)*igsign.gt.0.)goto26
        call SMAX(zmoyg,i,eps)
          if(sgi.lt.0.and.zmoy(i+largrid).gt.zmoy(i))goto26  
c                                            elimine transversalium

      piv=i+eps-0.5

      jdeb=yca(n,kg)+dj
      jfin=jdeb+intvj
        if(kg.eq.2)then   !  gauche ->    droite <-
        jdeb=jdeb-intvj
        jfin=jfin-intvj
        endif
      x(1)=jdeb+(piv-centre)*anglei
      x(2)=jfin+(piv-centre)*anglei
        xcentre=0.5*(x(1)+x(2))
      y(1)=piv+(x(1)-xcentre)*anglej
      y(2)=piv+(x(2)-xcentre)*anglej

          xbord(n,4,kg)=y(1)
          ybord(n,4,kg)=x(1)
             if(sgi.lt.0)xbord(n,4,kg)=y(1)-10.   !   marge pour grid

c         ybord(n,4,kg)=0.5*(x(1)+x(2))
c         xbord(n,4,kg)=y(1)

c      print *,' bas: n,kg,i, x1,x2,y1,y2 ',n,kg,i,x(1),x(2),y(1),y(2)
      write(3,*)'Bottom  n,kg  x,x,y,y ',n,kg,x(1),x(2),y(1),y(2)
      write(95,*)'Bottom    '
      write(95,203) n,kg,x(1),x(2),y(1),y(2),0.,0.
      if(ipl.ne.0)call pgline(2,x,y)
      goto27
26    continue
        Idessin=Idessin+1
27    continue
c
c                        if(khb.eq.1)goto29   !!!              haut
      do28 ip=icentre+2,i2-1
      i=icentre+i2+1-ip
      if((zmoyg(i)-zmoyg(i-1))*igsign.gt.0.)goto28
      if((zmoyg(i)-zmoyg(i+1))*igsign.gt.0.)goto28
      if((zmoyg(i)+zgseuili*igsign)*igsign.gt.0.)goto28
c      if((zmoy(i+leps)-zseuili)*igsign.gt.0.)goto28
c      if((zmoy(i-leps)-zseuili)*igsign.lt.0.)goto28
270   format(' z, zg ',4f8.0,2x,4f8.0)
c      write(3,270)zmoy(i-leps),zmoy(i),zmoy(i+leps),zseuili,
c     1            zmoyg(i-1),zmoyg(i),zmoyg(i+1),zgseuili
        call SMAX(zmoyg,i,eps)
          if(sgi.lt.0.and.zmoy(i-largrid).gt.zmoy(i))goto28

      piv=i+eps-0.5

      jdeb=yca(n,kg)+dj
      jfin=jdeb+intvj
        if(kg.eq.2)then
        jdeb=jdeb-intvj
        jfin=jfin-intvj
        endif
      x(1)=jdeb+(piv-centre)*anglei
      x(2)=jfin+(piv-centre)*anglei
        xcentre=0.5*(x(1)+x(2))
      y(1)=piv+(x(1)-xcentre)*anglej
      y(2)=piv+(x(2)-xcentre)*anglej

          xbord(n,5,kg)=y(1)
          ybord(n,5,kg)=x(1)
              if(sgi.lt.0)xbord(n,5,kg)=y(1)+10.  !   marge pour grid

c      print *,'haut: n,kg,i, x1,x2,y1,y2 ',n,kg,i,x(1),x(2),y(1),y(2)
c      y(1)=piv
c      y(2)=y(1)
c         ybord(n,5,kg)=0.5*(x(1)+x(2))
c         xbord(n,5,kg)=y(1)
      write(3,*)'Top  n,kg x,x,y,y ',n,kg,x(1),x(2),y(1),y(2)
      write(95,*)'Top       '
      write(95,203) n,kg,x(1),x(2),y(1),y(2),0.,0.
      if(ipl.ne.0)call pgline(2,x,y)
      goto29
28    continue
        if(Idessin.eq.0)Idessin=1
29    continue
c      if(mark.eq.1.or.mark.eq.-1)goto250
c
        if(Idessin.eq.1)goto250
c
38    continue !!!  fin boucle gche-droite ---
40    continue !!!  fin boucle canaux ----------------------------

      if(n3n4.ne.0)call interpo(xr,n3,n4)
c                       -------
c
c                         Bords en J  xbord,ybord   (n,1a3,kg)
c                                         pour paraboles eventuelles

      do60 n=n1,n2
c                                                 boucle n
                   if(n.gt.n3.and.n.lt.n4)goto60
      write(3,*)' n ',n
         den=2*intvi+1

      do58 kg=1,2
c                                                 boucle kg
         if(ipl.ne.0)call pgslw(3)
c      if(nkbon(n,kg).ne.1)goto58
c      if(xr(n,1,kg).eq.0..or.xr(n,3,kg).eq.0.)goto58

      xcc=0.5*(xbord(n,4,kg)+xbord(n,5,kg))
        icc=xcc
      xl=xbord(n,5,kg)-xbord(n,4,kg) 
      ili(n,kg)=lip*xl/100.

        do ihb=1,3
        integ(1,ihb)=icc+ili(n,kg)*(ihb-2)-intvi
        integ(2,ihb)=integ(1,ihb)+2*intvi
        enddo

      jdeb=yca(n,kg)-jeps
      jfin=yca(n,kg)+jeps
c
      jdeb=max0(jdeb,1)
      jfin=min0(jfin,jm)

51    format(' n=',i2,'  kg=',i2,' jdeb=',i4,'   jfin=',i4)
c      write(3,51)n,kg,jdeb,jfin

c

52    do j=jdeb,jfin
        do ihb=1,3
           zmi(j,ihb)=0
           ip1=integ(1,ihb)
           ip2=integ(2,ihb)
              do ip=ip1,ip2
              yy=j+(ip-xcc)*anglei
              jp=yy
              dy=yy-jp
              zmi(j,ihb)=zmi(j,ihb)+ima(ip,jp)*(1.-dy)+
     1                              ima(ip,jp+1)*dy
              enddo
        enddo
      enddo
      do ihmb=1,3
      zmi(j,ihmb)=zmi(j,ihmb)/den
      enddo
c
      do54 ihb=1,3
      piv=0.
        do jp=jdeb+2,jfin-1
        zmoyg(jp)=zmi(jp,ihb)-zmi(jp-1,ihb)
        if(kg.eq.1)piv=amax1(piv,zmoyg(jp))
        if(kg.eq.2)piv=amin1(piv ,zmoyg(jp))
        if(zmoyg(jp).eq.piv)jmax=jp
        enddo
      if(abs(zmoyg(jmax)).lt.zgseuil)jmax=0
      if(jmax.eq.(jdeb+2).or.jmax.eq.(jfin-1))jmax=0
      eps=0.
      if(jmax.eq.0)goto54
      call SMAX(zmoyg,jmax,eps)

      ybord(n,ihb,kg)=jmax+eps+
     1                (ili(n,kg)*(ihb-2)+xcc-icc)*anglei
      xbord(n,ihb,kg)=ili(n,kg)*(ihb-2)+xcc
c                                              flottants equidistants de xcc

53    format(' n,kg,ihb,jmax,eps,ili,xcc ',3i3,2x,i5,f7.2,i5,f6.1)
c      write(3,53)n,kg,ihb,jmax,eps,ili(n,kg),xcc

        y(1)=integ(1,ihb)
        y(2)=integ(2,ihb)
c                                 dessin
             x(1)=ybord(n,ihb,kg)-intvi*anglei
             x(2)=ybord(n,ihb,kg)+intvi*anglei
535   format(' x1,x2,y1,y2 ',4f6.1)
      write(3,535)x(1),x(2),y(1),y(2)

      write(3,*)'Left,Right  n,kg,x,x,y,y ',n,kg,x(1),x(2),y(1),y(2)
      write(95,*)'Left,Right'
      write(95,203) n,kg,x(1),x(2),y(1),y(2),0.,0.

      if(ipl.ne.0)call pgline(2,x,y)
c          ------
54    continue
c              fin boucle ihb
58    continue
c                  fin boucle kg
60    continue
c                      fin boucle n
c-------------------------------------------------
      if(n3n4.ne.0)then
      call interpob(xbord,n3,n4)
      call interpob(ybord,n3,n4)
c          --------
      endif

611   format(/' xbord(n,ihb,kg) 11,5,2 ')
      write(3,611)
612   format(11f6.1)
      write(3,612)xbord
613   format(/' ybord(n,ihb,kg) 11,5 2 ')
      write(3,613)
      write(3,612)ybord

c
c                         cisaillement  cis(ihmb,kg)
c
c      den=yca(nc,2)-yca(nc,1)-intvj
c      if(den.eq.0.)goto620
c        do ihmb=1,3,2
c        alp=(xr(nc,ihmb,2)-xr(nc,ihmb,1))/den
c        cis(ihmb,1)=-alp*intvj/2.
c        cis(ihmb,2)=alp*intvj/2.
c        enddo
c      cis(2,1)=0.5*(cis(1,1)+cis(3,1))
c      cis(2,2)=0.5*(cis(1,2)+cis(3,2))
62    format(' Cisaillements  ',6f6.1/)
620   continue
c      write(3,62)cis

c                     on dispose de 10 points xbord,ybord centres
c                        ( xbord(2)=0.5*(xbord(1)+xbord(3)) )
c
c                     calcul de 6 points references xr,yr
c                     -----------------------------------
      if(ipl.ne.0)call pgslw(1)
      do n=1,nm
c                                              calcul et dessin, cas lineaire
      if(distor.eq.0)then
      call interseclin(xbord,ybord,n,xr,yr)
c          -----------

      do63 kg=1,2
        xr(n,2,kg)=0.5*(xr(n,1,kg)+xr(n,3,kg))
        yr(n,2,kg)=0.5*(yr(n,1,kg)+yr(n,3,kg))

         do ihmb=1,3
         xx=yr(n,ihmb,kg)
         yy=xr(n,ihmb,kg)
         if(ipl.ne.0)call pgpoint(1,xx,yy,5)
c             -------
         enddo
63      continue
      else
c                                              calcul et dessin, cas parabole
c                          on remplace xbord,ybord (1) et (3)
c                          par xbord,ybord (1) et (3) en (bas) et (haut)

      do66 kg=1,2
      x1=xbord(n,4,kg)
      x2=xbord(n,1,kg)
      x3=xbord(n,2,kg)
      x4=xbord(n,3,kg)
      x5=xbord(n,5,kg)

      if(x2.eq.0.or.x4.eq.0.)goto190
        rap=(x5-x1)/(x4-x2)
      
c      write(3,*)' n,kg,rap ',n,kg,rap

      y3=ybord(n,2,kg)
      y2=ybord(n,1,kg)
      y4=ybord(n,3,kg)
      dy=0.5*(y2+y4-2.*y3)*rap**2
      y1=y3+(y2-y4)*rap/2.+dy
      y5=y3+(y4-y2)*rap/2.+dy
c         write(3,*)' y1,y2,y3,y4,y5 ',y1,y2,y3,y4,y5

      xbord(n,1,kg)=x1
      xbord(n,3,kg)=x5

      ybord(n,1,kg)=y1
      ybord(n,3,kg)=y5
66    continue

      call interseclin(xbord,ybord,n,xr,yr)
c          -----------

      do kg=1,2
      xr(n,2,kg)=0.5*(xr(n,1,kg)+xr(n,3,kg))
      dxcc=xr(n,2,kg)-x3
      yr(n,2,kg)=ybord(n,2,kg)+dxcc*anglei
      enddo

      endif
c                       fin calcul xr,yr
      do kg=1,2
          flech(n,kg)=yr(n,2,kg)-0.5*(yr(n,1,kg)+yr(n,3,kg))
      enddo

c                          dessin contour canaux

      xdes(1)=yr(n,1,1)
      ydes(1)=xr(n,1,1)
      xdes(2)=yr(n,2,1)
      ydes(2)=xr(n,2,1)
      xdes(3)=yr(n,3,1)
      ydes(3)=xr(n,3,1)
      xdes(4)=yr(n,3,2)
      ydes(4)=xr(n,3,2)
      xdes(5)=yr(n,2,2)
      ydes(5)=xr(n,2,2)
      xdes(6)=yr(n,1,2)
      ydes(6)=xr(n,1,2)
      xdes(7)=xdes(1)
      ydes(7)=ydes(1)

      write(95,*)'Channel   '
      write(95,203) n,kg,(xdes(nn),nn=1,6)
      write(95,203) n,kg,(ydes(nn),nn=1,6)
      if(ipl.ne.0)call pgsls(4)
      if(ipl.ne.0)call pgline(7,xdes,ydes)  !  contour en pointillés
      if(ipl.ne.0)call pgsls(1)

      enddo    !    -------------------  boucle n
      close(95)
            call par1('   nleft',nw,nleft)
            call par1('  nright',nw,nright)

          if(nleft.lt.100.and.nright.lt.100)then
           call edge(xr,yr,1,nm,nw)
c               ----
          else
           call edge1(xr,yr,1,nm,nw)
          endif

c                         fin boucle n
64    format(/'  XR(N,IHMB,KG)'/18f7.1/18f7.1/18f7.1/
     1                          18f7.1/18f7.1/18f7.1)
c                 9,18  3   2
      write(3,64)xr
65    format(/'  YR(N,IHMB,KG)'/18f7.1/18f7.1/18f7.1/
     1                          18f7.1/18f7.1/18f7.1)
      write(3,65)yr
68    format('  Fleches(n,kg)'/18f7.1/18f7.1)
      write(3,68)flech
      if(ipl.eq.1)write(7,68)flech
c-------------------------------
70    format(a22)
72    format(18f7.1)
74    format(12i6)
        icp=0
75    format(' displ    i1   i2m   icp    j1   j2m   lip  jeps',
     1       ' intvi intvj')
76    format('    si   sgi    sj  sgj1   sgj2 leps    nm    n1    n2',
     1       '    nc distor')

78    format('  xr(n,ihmb,kg)  / yr(n,ihmb,kg)     ')
c      write(iug,70)kj,kh,kr,jan,mois,jour,gname
      rewind(iug)
      write(iug,70)gname
      write(iug,75)
      write(iug,74)displ,i1,i2m,icp,j1,j2m,lip,jeps,intvi,intvj
      write(iug,76)
      write(iug,74)si,sgi,sj,sgj1,sgj2,leps,nm,n1,n2,nc,distor

      write(iug,78)
      write(iug,72)xr
      write(iug,72)yr

      write(7,*)' '
      write(7,78)
      write(7,*)' '
      write(7,72)xr
      write(7,*)' '
      write(7,72)yr
c----------------------------  calcul long/court
      call par1('      lj',1,lj)
       ylarg=lj
        xxa=0.
      do n=1,nm
      do kg=1,2
      xxa=xxa+xr(n,3,kg)-xr(n,1,kg)
      enddo
      enddo
         yya=0.
      do n=1,nm
      do ihmb=1,3,2
      yya=yya+yr(n,ihmb,2)-yr(n,ihmb,1)
      enddo
      enddo
        rap=xxa/yya
        long=ylarg*rap+0.5
      write(7,*)' if larg=',lj,'  long=',long,' (ratio ',rap,')'
      write(7,*)' '
c-------------------------------
c                          vecteurs  en  fonction de N
      do n=1,nm
      do ihmb1=1,3
      do kg1=1,2
      ind1=ihmb1+3*(kg1-1)
      do ihmb2=1,3
      do kg2=1,2
      ind2=ihmb2+3*(kg2-1)
        vi(n,ind1,ind2)=0.
        u=xr(n,ihmb1,kg1)
        v=xr(n,ihmb2,kg2)
        if(u*v.ne.0.)vi(n,ind1,ind2)=v-u
        vj(n,ind1,ind2)=0.
        u=yr(n,ihmb1,kg1)
        v=yr(n,ihmb2,kg2)
        if(u*v.ne.0.)vj(n,ind1,ind2)=v-u
          piv=vi(n,ind1,ind2)**2+vj(n,ind1,ind2)**2
        vmod(n,ind1,ind2)=sqrt(piv)
      enddo
      enddo
      enddo
      enddo
      enddo
c
80    format(' Vecteurs  X       ABCDEF * ABCDEF  * n '/)
81    format(' Vecteurs  Y       ABCDEF * ABCDEF  * n '/)
82    format(2x,18f7.1/2x,18f7.1/2x,18f7.1/2x,18f7.1/2x,18f7.1/
     12x,18f7.1//)
c      write(7,80)
c      write(7,82)vi
c      write(7,81)
c      write(7,82)vj
c  
        call pgslw(2)
        call pgsch(1.)
      if(ipl.ne.0)call pgvport(xc,xcd1,0.5,0.9)
      if(ipl.ne.0)call pglabel(' ',' ','I')
      if(ipl.ne.0)call pgvport(xcd1,xcd2,0.5,0.9)
      if(ipl.ne.0)call pglabel(' ',' ','J')
      if(ipl.ne.0)call pgvport(xcd2,xd,0.5,0.9)
      if(ipl.ne.0)call pglabel(' ',' ','modulus')
c
      xp1=xc
      xp2=xd
      yp1=0.50
      h=(.90-.50)/7.
      yp1=yp1-h
c
      call slin(1,vi,nm,avi,ecarti)
      call slin(2,vj,nm,avj,ecartj)
      call slin(3,vmod,nm,avmod,ecartmod) ! calcul ecarts
c          ----

      call ecartqm(nm,ecarti,ecartj,ecartmod, ecart,ecqm,ecmaxmod)

      write(3,*)'ecmaxmod',ecmaxmod
c      ecart=amax1(ecarti,ecartj)
c
      angle1=0.
      piv=avj(1,4,1)+nc*(avj(1,4,2)+nc*avj(1,4,3))
      if(piv.ne.0.)
     1 angle1=10000.*(avi(1,4,1)+nc*(avi(1,4,2)+nc*avi(1,4,3)))/piv

      angle2=0.
      piv=avj(3,6,1)+nc*(avj(3,6,2)+nc*avj(3,6,3))
      if(piv.ne.0.)
     1 angle2=10000.*(avi(3,6,1)+nc*(avi(3,6,2)+nc*avi(3,6,3)))/piv
c
      call sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,1,4,'AD',avi,avj,avmod,
     1            ipl)
      call sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,2,5,'BE',avi,avj,avmod,
     1            ipl)
      call sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,3,6,'CF',avi,avj,avmod,
     1            ipl)
      call sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,1,3,'AC',avi,avj,avmod,
     1            ipl)
      call sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,4,6,'DF',avi,avj,avmod,
     1            ipl)
      call sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,1,2,'AB',avi,avj,avmod,
     1            ipl)
      call sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,4,5,'DE',avi,avj,avmod,
     1            ipl)
c     
c                                             calcul points manquants 
      if(ipl.ne.0)call pgvport(xa,xb,.10,.40)
      if(ipl.ne.0)call pgwindow(x1,x2,y1,y2)
      do84 n=1,nm
      do84 ihmb=1,3
      do84 kg=1,2
      xpiv=xr(n,ihmb,kg)
      ypiv=yr(n,ihmb,kg)

      if(xpiv*ypiv.ne.0.)goto84
      kg2=3-kg
      xpiv2=xr(n,ihmb,kg2)
      ypiv2=yr(n,ihmb,kg2)
      if(xpiv2*ypiv2.eq.0.)goto84
      ind1=ihmb+3*(kg-1)
      ind2=ihmb+3*(kg2-1)
      vvi=avi(ind1,ind2,1)+n*(avi(ind1,ind2,2)+n*avi(ind1,ind2,3))
      vvj=avj(ind1,ind2,1)+n*(avj(ind1,ind2,2)+n*avj(ind1,ind2,3))
      xpiv=xpiv2-vvi
      ypiv=ypiv2-vvj
      xr(n,ihmb,kg)=xpiv
      yr(n,ihmb,kg)=ypiv
      if(ipl.ne.0)call pgpoint(1,ypiv,xpiv,4)
84    continue
c
94    format('  xr / yr  with complementary points')
940   write(3,94)
      write(3,64)xr
      write(3,65)yr
      write(iug,94)
      write(iug,72)xr
      write(iug,72)yr
      call angi(nm,xr,yr,milangi)

      write(3,*)'milangi ',milangi
c
95    continue

97    format(' arrows')
98    format(/)
99    format(' max disc. rms,angle1,angle2')
      write(3,98)
      write(3,99)
      write(3,115)ecart,ecqm,angle1,angle2
      write(iug,97)
      write(iug,72)flech
c
c      kgs=2
c      if(mark.eq.-1)kgs=1
c      khbs=khb+1
c      if(khb.eq.1)khbs=1
c

      rewind(iuy)  

c110   continue
      write(iug,99)
115   format(2f7.2,2f7.1)
      write(iug,115)ecart,ecqm,angle1,angle2
c120   continue
c                             dessin titre

c      write(titre(1:22),'(a22)')gname
c      write(titre(23:25),'(a)')'   '
c      write(titre(26:30),'(f5.2)')ecart
c      write(titre(31:37),'(f7.2)')ecqm
c      write(titre(38:45),'(a)')'  maxmod'
c      write(titre(46:50),'(f5.2)')ecmaxmod
      write(titre(1:16),'(a)')'  si,sgi,sj,sgj '
      write(titre(17:28),'(4i3)') si,sgi,sj,sgj
      write(titre(29:38),'(a)')'          '      !milangi
      write(titre(39:42),'(a)')'    '          !'(i4)')milangi
      write(titre(43:50),'(a)')'        '      !' accuracy '
      write(titre(51:54),'(a)')'    '          !'(f4.2)')ecmaxmod

      if(ipl.ne.0)call pgvport(xa,xb,.1,.9)
      if(ipl.ne.0)call pgsch(1.)   !0.7)
      if(ipl.ne.0)call pgslw(2)
      if(ipl.ne.0)call pglabel(' ',' ',titre)
           istop=0
           piv=float(milgeo)/1000.
             print *,' milgeo,ecart,nsol ',milgeo,ecart,nsol

      if(nseuils.lt.nsm)then
        ecmin=amin1(ecart,ecmin)
         if(ecmin.eq.ecart.and.ecart.lt.ecartbis)nsol=nseuils
c                                                ****
         ecartbis=ecart
      else
        write(7,*)' Max error geo =',ecart,' pixels',' rms =',ecqm
           if(nm.lt.18. and.ecart.le.piv)goto 200
           if(nm.ge.18. and.ecmaxmod.le.piv)goto 200
      endif

190   continue  !    -------------------------  nseuils
c=========================================================
         print *,' Error too large '
         print *,' Check geo.ps'
         print *,' Modify the parameters or increase milgeo'
         print *,' if another g-file is not available'
               istop=1
c               gname0=gname
c               write(gname0(9:9),'(a)')'0'
c               call system('mv '//gname//gname0//)
c                  fin boucles seuils
200   continue
      return
      end
c--------------------------
      subroutine SMAX(z,i,eps)
      dimension z(*)     !  m2403
      eps=0.
      b=z(i+1)-z(i-1)
      a=z(i+1)+z(i-1)-2.*z(i)
        if(a.eq.0.)return
      eps=-b/(2.*a)
      return
      end
c---------------------------
      subroutine sgraph(xp1,xp2,yp1,h,vi,vj,vmod,nm,ind1,ind2,tit,
     1                  avi,avj,avmod,ipl)
      dimension vi(18,6,6),vj(18,6,6),vmod(18,6,6),v(18),
     1          avi(6,6,3),avj(6,6,3),avmod(6,6,3),
     2          xdes(18),ydes(18)
      character tit*2
1     format(/' vecteurs  ind1=',i1,'  ind2=',i1,2x,11f7.1)
      yp1=yp1+h
      yp2=yp1+h
c
          pivx=(xp2-xp1)/3.
c--------
      do100 ij=1,3    !     vi  vj  mod
      u1=xp1+pivx*float(ij-1)
      u2=u1+pivx
c      if(ij.eq.1)u2=u10.5*(u1+u2)
c      if(ij.eq.2)u1=0.5*(u1+u2)
      vmin=10000.
      vmax=-10000.
c
        do50 n=1,nm
        if(ij.eq.1)v(n)=vi(n,ind1,ind2)
        if(ij.eq.2)v(n)=vj(n,ind1,ind2)
        if(ij.eq.3)v(n)=vmod(n,ind1,ind2)
c           piv=vi(n,ind1,ind2)**2+vj(n,ind1,ind2)**2
c           v(n)=sqrt(piv)
        if(v(n).eq.0.)goto50
        vmin=amin1(vmin,v(n))
        vmax=amax1(vmax,v(n))
50      continue
c      write(3,1)ind1,ind2,v
c
      if(abs(vmax-vmin).gt.20.)goto100      vmin=vmin-2
      vmax=vmax+2
      if(ipl.ne.0)call pgvport(u1,u2,yp1,yp2)
      xnm1=nm+1
      if(ipl.ne.0)call pgwindow(0.,xnm1,vmin,vmax)
      if(ipl.ne.0)call pgbox('bcs',10.,1,'bcs',10.,10)
      test=vmax-vmin
      if(test.lt.10.and.ipl.ne.0)
     1      call pgbox(' ',1.,0,'bcs',10.,10)
c
        do60 n=1,nm
        if(v(n).eq.0.)goto60
        xx=n
        if(ipl.ne.0)call pgpoint(1,xx,v(n),5)
60      continue

      do n=1,nm
        xdes(n)=n
      if(ij.eq.1)then
        ydes(n)=avi(ind1,ind2,1)+n*(avi(ind1,ind2,2)+n*avi(ind1,ind2,3))
      endif
      if(ij.eq.2)then
        ydes(n)=avj(ind1,ind2,1)+n*(avj(ind1,ind2,2)+n*avj(ind1,ind2,3))
      endif
      if(ij.eq.3)then
        ydes(n)=avmod(ind1,ind2,1)
     1          +n*(avmod(ind1,ind2,2)+n*avmod(ind1,ind2,3))
      endif

      enddo
      if(ipl.ne.0)call pgline(nm,xdes,ydes)

100   continue
c
      u1=xp2
      u2=xp2+0.05
      if(ipl.ne.0)call pgvport(u1,u2,yp1,yp2)
      if(ipl.ne.0)call pgwindow(0.,1.,0.,1.)    
      if(ipl.ne.0)call pgtext(0.4,0.4,tit)
      return
      end
c------------------------
      subroutine slin(ij,v,nm,av,ecart)
c                ----     
c          call slin(3,vmod,nm,avmod,ecartmod)
      dimension v(18,6,6),av(6,6,3),
     1          x(18),y(18),p(18),coef(10),ecart(18,6,6)
c                               
      do5 i1=1,6
      do4 i2=1,6
        do ico=1,3
        av(i1,i2,ico)=0.
        enddo
c
          nd=0
        do2 n=1,nm
        x(n)=n
        y(n)=v(n,i1,i2)
        p(n)=0.
        ecart(n,i1,i2)=0.
        if(y(n).eq.0.)goto4
        p(n)=1.
        nd=nd+1
2       continue
c
      if(nd.lt.3)goto4
      call DPMCAR(x,y,p,nm,3,coef)
c          ------
      av(i1,i2,1)=coef(1)
      av(i1,i2,2)=coef(2)
      av(i1,i2,3)=coef(3)
c
c        if(ij.eq.2)then
c        if(iabs(i1-i2).ne.3)goto4
c        endif

      do3 n=1,nm
      yy=v(n,i1,i2)
      if(yy.eq.0.)goto3
      pivreg=av(i1,i2,1)+n*(av(i1,i2,2)+n*av(i1,i2,3))
        piv=pivreg-yy
        piv=abs(piv)
      ecart(n,i1,i2)=piv
c     -----

c      write(3,*)'i1,i2,n,regression,vect',i1,i2,n,pivreg,yy

3     continue
c
4     continue
5     continue
      return
      end
c--------------------
      subroutine LISTEJ(listj,ijcam)
      integer*2 listj(1536)
        do i=1,ijcam
        listj(i)=listj(i)/100
        enddo
1     format(i6,2x,16i4)
2     format('     j')
c      write(3,2)
      do n=1,64
      i1=1+(n-1)*16
      i2=i1+15
c      write(3,1)i1,(listj(i),i=i1,i2)
      enddo
      return
      end
c---------------------
      subroutine sangle(yc,n1,n2,id,a)
c                ------
      dimension yc(18,2,   2)
c                     g/d  iangle
c      write(3,*)' n1,n2,id ',n1,n2,id

         a=0.
      do n=n1,n2
      a=a+yc(n,1,2)+yc(n,2,2)-yc(n,1,1)-yc(n,2,1)
      enddo

      a=a/(4.*(n2-n1+1)*float(id))
      return
      end
c-----------------------
      subroutine maxgrad(z,i1,i2,isig,icroiss,zseuil,zgseuil,x,index,n,
c                -------
     1                   nodet,ijcam)
c                          on cherche entre i1 et i2
c                               le point x de gradient max  de signe isig
c           icroiss=0   max absolu sur l'intervalle
c                   1   premier max dans le sens des i croissants
c                   2                                  decroissants
c           on ne retient que les points pour lesquels
c                                                     z>zseuil
c                                                 abs(zg)>zgseuil
      dimension z(1536),zg(1536)
c      write(3,*)' '
c      write(3,*)' isig,icroiss ',isig,icroiss
c      write(3,*)' i1,i2, zseuil,zgseuil ',i1,i2,zseuil,zgseuil

      do i=i1,i2-1
      zg(i)=z(i+1)-z(i)
      enddo
        zg(i2)=zg(i2-1)
c      do i=i1,i2-1
c       write(3,*)' i,z,zg', i,z(i),zg(i)
c      enddo

        zmax=0.

      do10 ip=i1+1,i2-1
        i=ip
        if(icroiss.eq.2)i=i2-ip+i1
c                              pour icroiss=0, on garde ip
        
      if(z(i).lt.zseuil)goto10
        piv2=isig*zg(i)
      if(piv2.lt.zgseuil)goto10
      if(piv2.lt.zgseuil)goto10
        piv1=isig*zg(i-1)
        piv3=isig*zg(i+1)
      if(piv2.lt.piv1.or.piv2.lt.piv3)goto10
c        write(3,*),' z, zg(i-1/i/i+1) ',z(i),zg(i-1),zg(i),zg(i+1)

      if(zmax.le.piv2)imax=i
      zmax=amax1(zmax,piv2)
      if(icroiss.ne.0)goto12
10    continue
11    format(' Edge of channel not detected, index=',i1,
     1                                       ' n=',i2)
        nodet=0
      if(zmax.eq.0.)then
c        print 11,index,n
        write(3,11)index,n
        x=0.

        nodet=1
        return
      endif

12    continue

      call smax(zg,imax,eps)
      nodet=0
      x=imax+0.5+eps
        write(3,*)' x ',x
      return
      end
c----------------------
      subroutine scanj(ima,is,js,i1,i2,j1,j2,anglei,anglej,z,ijcam)
c                -----
      integer*2 ima(ijcam,ijcam)
      dimension z(1536)

      xc=0.5*(i1+i2)
c                         integ entre i1 et i2
      do j=j1,j2
      zp=0.
      den=0.
          
        do 10 i=i1,i2
        yy=j+(i-xc)*anglei
        jp=yy
          if(jp.lt.j1.or.jp.gt.j2-1)goto 10
        dy=yy-jp
        zp=zp+ima(i,jp)*(1.-dy)+ima(i,jp+1)*dy
          den=den+1.
10      continue
      
      if(den.ne.0.)z(j)=zp/den
      enddo
        if(anglei.eq.0.)z(j2)=z(j2-1)
      return
      end
c------------------------
      subroutine scani(ima,is,js,i1,i2,j1,j2,anglei,anglej,z,ijcam,
c                -----
     1                 fond,zmax,norm)
      integer*2 ima(ijcam,ijcam)
      dimension z(1536)

c      write(3,*)' i1,i2,j1,j2 ',i1,i2,j1,j2

      den=j2-j1+1
      xc=0.5*(i1+i2)
      yc=0.5*(j1+j2)
c                         integ entre j1 et j2 autour de xc
      do i=i1,i2
      zp=0.
        y1=j1+(i-xc)*anglei
        y2=j2+(i-xc)*anglei
        jp1=y1
        jp2=y2
        dy=y1-jp1

          do jp=jp1,jp2
            x=i+(jp-yc)*anglej
            ip=x
            dx=x-ip
          zp=zp+(ima(i,jp)*(1.-dy)+ima(i,jp+1)*dy) *(1.-dx)+
     1          (ima(i+1,jp)*(1.-dy)+ima(i+1,jp+1)*dy) *dx
          enddo
      z(i)=zp/den
      enddo

      if(norm.eq.1)then
      zma=0.
        do i=i1,i2
        zma=amax1(zma,z(i))
        enddo
          piv=(zmax-fond)/(zma-fond)
        do i=i1,i2
        z(i)=fond+(z(i)-fond)*piv
        enddo
      endif
      return
      end
c-------------------------------
      subroutine interseclin(xb,yb,n,xr,yr)
c                -----------
c recherche des sommets xr,yr

      dimension xb(18,5,2),yb(18,5,2),xr(18,3,2),yr(18,3,2)

      call inters(xb,yb,n,1,1,3,1,4,1,4,2,xr,yr,1,1)
      call inters(xb,yb,n,1,1,3,1,5,1,5,2,xr,yr,3,1)
      call inters(xb,yb,n,1,2,3,2,4,1,4,2,xr,yr,1,2)
      call inters(xb,yb,n,1,2,3,2,5,1,5,2,xr,yr,3,2)

      return
      end
c--------------------------------
      subroutine inters(xb,yb,n,l1,k1,l2,k2,l3,k3,l4,k4,xr,yr,lr,kr)
c                ------
      dimension xb(18,5,2),yb(18,5,2),xr(18,3,2),yr(18,3,2)

      x1=xb(n,l1,k1)
      x2=xb(n,l2,k2)
      x3=xb(n,l3,k3)
      x4=xb(n,l4,k4)
c         write(3,*)' n,x1,x2,x3,x4 ',n,x1,x2,x3,x4

      y1=yb(n,l1,k1)
      y2=yb(n,l2,k2)
      y3=yb(n,l3,k3)
      y4=yb(n,l4,k4)
c         write(3,*)' n,y1,y2,y3,y4 ',n,y1,y2,y3,y4

      if(x1.eq.x2)return
      if(y1.eq.y2)return
      if(x3.eq.x4)return
      if(y3.eq.y4)return

        ayx=(y2-y1)/(x2-x1)
        byx=(y4-y3)/(x4-x3)
        axy=1./ayx
        bxy=1./byx

      xx=(x3*byx-x1*ayx+y1-y3)/(byx-ayx)
      yy=(y3*bxy-y1*axy+x1-x3)/(bxy-axy)

      xr(n,lr,kr)=xx
      yr(n,lr,kr)=yy

      return
      end
c-------------------------------------
      subroutine interpo(z,n3,n4)
c                -------
      dimension z(18,3,2)

      den=n4-n3
        do kg=1,2
        do ihmb=1,3
          do n=n3+1,n4-1
          x=float(n-n3)/den
          z(n,ihmb,kg)=(1.-x)*z(n3,ihmb,kg)+x*z(n4,ihmb,kg)
          enddo
        enddo
        enddo
      return
      end
  
c-------------------------------------
      subroutine interpob(z,n3,n4)
c                --------
      dimension z(18,5,2)

      den=n4-n3
        do kg=1,2
        do ihmb=1,5
          do n=n3+1,n4-1
          x=float(n-n3)/den
          z(n,ihmb,kg)=(1.-x)*z(n3,ihmb,kg)+x*z(n4,ihmb,kg)
          enddo
        enddo
        enddo
      return
      end
c-------------------------------------
      subroutine edge(xr,yr,n1,n2,nw)
c                ----
      dimension xr(18,3,2),yr(18,3,2),nlec(2),isig(2),nlr(2)

      nlec(1)=0
      nlec(2)=0
      call par1('   nleft',nw,nlec(1))
      call par1('  nright',nw,nlec(2))

      do10 lr=1,2

      if(nlec(lr).eq.0)then
        goto10
      else
        if(nlec(lr).gt.0)then
          nlr(lr)=nlec(lr)
          isig(lr)=+1
        else
          nlr(lr)=-nlec(lr)
          isig(lr)=-1
        endif
      endif

      do ihb=1,3
        n=nlr(lr)
        np=n+isig(lr)
        jgd=lr
        jgdp=3-lr
      xr(n,ihb,jgd)=xr(n,ihb,jgdp)+xr(np,ihb,jgd)-xr(np,ihb,jgdp)
      yr(n,ihb,jgd)=yr(n,ihb,jgdp)+yr(np,ihb,jgd)-yr(np,ihb,jgdp)
      enddo

10    continue
      return
      end
c-----------------------
      subroutine edge1(xr,yr,n1,n2,nw)
c                ----
c  inutilise soft0410b
      dimension xr(18,3,2),yr(18,3,2),nlec(2)
c      dimension nlr(2)

      nlec(1)=0
      nlec(2)=0
      call par1('   nleft',nw,nlec(1))
      call par1('  nright',nw,nlec(2))

      do10 lr=1,2

      if(nlec(lr).eq.0)then
        goto10
      else
c       nleft = 030506  ->  edge left de 5 deduit de
c                           right de 5
c                           largeur (left-right) interpolee entre 3 et 6
        na=nlec(lr)/10000
        n=nlec(lr)/100-100*na
        nb=nlec(lr)-10000*na-100*n
 
        xb=float(n-na)/float(nb-na)
        xa=1.-xb
      write(3,*)' bmg: na,n,nb,xa,xb ',na,n,nb,xa,xb

      endif

      do ihb=1,3
        jgd=lr
        jgdp=3-lr
      xr(n,ihb,jgd)=xr(n,ihb,jgdp)+
     1              xa*(xr(na,ihb,jgd)-xr(na,ihb,jgdp))+
     2              xb*(xr(nb,ihb,jgd)-xr(nb,ihb,jgdp))

      yr(n,ihb,jgd)=yr(n,ihb,jgdp)+
     1              xa*(yr(na,ihb,jgd)-yr(na,ihb,jgdp))+
     2              xb*(yr(nb,ihb,jgd)-yr(nb,ihb,jgdp))
      enddo

10    continue

      return
      end      
c-----------------------------------------------
      subroutine ecartqm(nm,eci,ecj,ecmod, ecart,ecqm,   ecmaxmod)
c                -------                  max i,j qmoy i,j
      dimension eci(18,6,6),ecj(18,6,6),ecmod(18,6,6)

        ecart=0.
        ecmaxmod=0.
        piv=0.
      do 5 i1=1,6
      do 4 i2=1,6
        if(i1.eq.i2)goto4
      do n=1,nm
      ecart=amax1(ecart,eci(n,i1,i2))
      ecart=amax1(ecart,ecj(n,i1,i2))
      ecmaxmod=amax1(ecmaxmod,ecmod(n,i1,i2))

c      write(3,*)'n,i1,i2,ecmod',n,i1,i2,ecmod(n,i1,i2)

      piv=piv+eci(n,i1,i2)**2+ecj(n,i1,i2)**2
      enddo
4     continue
5     continue    
        ecqm=0.5*piv/(float(nm)*30.)
        ecqm=sqrt(ecqm)
      return
      end
c=================================================   bmc1.f
c                                  bmc1.f
c                                  ------
      subroutine bmc1(nw,win,nm,milsec,
c                ----         
     1               xna,izyb,yna,bna,   gna,cna, 
c                        0/1 y,z ou b        z ou c         pour calib
c                               in            out   
     2               sundec,iswap,ipermu,icalib,fna,      
c                            si b               pour calib
     3               fl, repert,lbda,nq,xrecad,yrecad,istop,
c                    economie        in in     in
     4    ibs,jbs,iitab,jjtab,nntab,nnfl,ijcam,tab,tabxy,dc,dxr,dyr,
c         dim fichiers b
     5    message,istray,stray,xnormf,jhair,  fcont)
c                 in     eco   in             calib cont

c en entree  bna 
c en sortie canaux de cna apres soustraction de dc 
c        ou canaux de cna            "             +calib si iyb=1
c------------------------------------------------------------------------

c        si iyb=1  noms de b a 41 caracteres,  b est un fits
c                                               (avant etape sequence,  "bmc")
c        si iyb=0  noms de y a 22 caracteres   b n'est pas un fits 
c                                               (avant etape flatfield, "ymz")
c
c        si icalib=1, le fichier c est calibre avec fna
      double precision coef(6,2)
      dimension xr(18,3,2),yr(18,3,2),
     1          kz(512),
     2          xrecad(2,6),yrecad(2,6),
     3          csh1(2),csh2(2),csh3(2),stray(2000,18),
     4                      fcont(iitab,jjtab,nntab)         !    hair(21),
c                           calib cont
      integer*4 smooth,dayf,timef,win(8),distor,dsky1,dsky2,disk
    
      integer*2 tab(ijcam,ijcam),tabxy(iitab,jjtab),lec(1536),
     1     dc(ijcam,ijcam),txyn(1600,150,18),
c                                               1600  150   18
     2          fl(iitab,jjtab,nnfl),jhair(200)

      character*41 bna
      character*22 xna,yna,fna,gna,cna
 
      Character*80 Repert
c      write(3,*)' msdp2.f 1846 début bmc1: inverj=',inverj   ! m2403
      print *,' '
      print *,' bmc1:  izyb=',izyb
      print *,' call bmc1  ibs,jbs, ipermu ',ibs,jbs,ipermu  

      nclim=1
c      nseqdir=0
      smooth=1    
      kbseuil=500      ! <------ modifiable

        do i=1,512
        kz(i)=0
        enddo
c---- prominences
      call par1('    disk',1,disk)
      call par1('   dsky1',1,dsky1)
      call par1('   dsky2',1,dsky2)
      call par1('  inveri',1,inveri)
  
c----        
      call par1('      li',nw,li)
      call par1('      lj',nw,lj)
      call par1('  invern',nw,invern)
      call par1('     idc',1,idc)
      call par1('  distor',1,distor)

      call par1('  icalct',1,icalcont)  !  calib par continu

      call par1('   ndust',1,ndust)
      call par1('  dxdust',1,mildx)
      call par1('  dydust',1,mildy)
      call par1('  x1dust',1,milx1)
      call par1('  x2dust',1,milx2)
      call par1('  y1dust',1,mily1)
      call par1('  y2dust',1,mily2)

      dxdust=float(mildx)/float(milsec)
      dydust=float(mildy)/float(milsec)
      x1dust=float(milx1)/float(milsec)
      x2dust=float(milx2)/float(milsec)
      y1dust=float(mily1)/float(milsec)
      y2dust=float(mily2)/float(milsec)

        n1=1
          n2=nm
          im=float(li)/float(milsec)+1.5
          jmp=float(lj)/float(milsec)+1.5
c             print *,' bmc: li,lj,milsec, im,jmp ',
c     1                     li,lj,milsec,im,jmp
      istop=0
      if(im.gt.ijcam.or.jmp.gt.jjtab)then
      print *,' Increase the output pixel size'
      istop=1
      return
      endif

          maxlev=32000
          iscat=0      
c      iscat=1
          smooth=1

c      facmax=float(maxlev)/1000.
c      if(maxlev.eq.0)facmax=1000.

12    format(48x,2i6)

      iux=23
c                     modif 3 avril 2000
      if(idc.ge.0)then
      call openold22(xna,sundec,23)
c          ---------                             x  en  23
      endif

      iug=24
      call openold22sf(gna,sundec,24)
c          -----------                           g  en  24

      iuy=25
      iuz=26
      iub=25
      iuc=26

      if(izyb.le.2)then
      call openold22(yna,sundec,25)!                      izyb=1 ou 2
c          ---------                             b  en  25     z ou y
      call opennew22(cna,sundec,26)
c          ---------                             c  en  26     w ou v 
      endif
c----
      if(izyb.eq.3)then 
      call openold41(bna,sundec,25, repert)
c          ---------                                b en 25   sequence
c      print *,'bna ouvert'

      call opennew22(cna,sundec,26)  !sf  
c          --------- avec format                    c en 26   sequence
c         print*,' cna ouvert'
      endif
c----

      if(icalib.eq.1.and.izyb.eq.3)then
      iuf=29
      call openold22(fna,sundec,29)
c          ---------                             f  en  29
      endif
c================================  lecture fichier g
      do n=1,6
      read(iug,14)
      enddo
c        read(iug,12)jcor1,jcor2
        jcor1=0
        jcor2=0
      do n=1,13
      read(iug,14)
      enddo
c
13    format(18f7.1)
14    format(1x)
      read(iug,13)xr  !!!! 2 -> iug
      read(iug,13)yr   !!! 2 -> iug

c      if(iyb.eq.1)then
c      do kg=1,2
c      do ihb=1,3
c      do n=1,18
c        xr(n,ihb,kg)=xr(n,ihb,kg)
c        yr(n,ihb,kg)=yr(n,ihb,kg)
c      enddo
c      enddo
c      enddo
c      endif

16    continue 
c
      nc=(n1+n2)/2
      xlong=0.5*(xr(nc,3,1)-xr(nc,1,1)+xr(nc,3,2)-xr(nc,1,2))
      ylarg=0.5*(yr(nc,1,2)-yr(nc,1,1)+yr(nc,3,2)-yr(nc,3,1))
      yespace=0.5*(yr(nc+1,2,1)-yr(nc,2,2)+yr(nc,2,1)-yr(nc-1,2,2))
        if(im.ne.1.and.jmp.ne.1)then
        jm=jmp
        endif
        if(im.ne.1.and.jmp.eq.1)then
        jm=im*ylarg/xlong
        endif
        if(im.eq.1.and.jmp.ne.1)then
        jm=jmp
        im=jm*xlong/ylarg
        endif
        if(im.eq.1.and.jmp.eq.1)then
        print *,' Donnees incorrectes dans bmc'
        return
        endif

      if(im.gt.ijcam-1)im=ijcam-1
      if(jm.gt.jjtab-1)jm=jjtab-1
      ism=im/smooth
      jsm=jm/smooth

      jespace=yespace*jm/ylarg   !!! coordonnees nouvelles de z ou c

18    format(' im,jm,ism,jsm,jespace  =',5i6)
      write(3,18)im,jm,ism,jsm,jespace
c============================================
c  Chargement de TAB

      if(izyb.le.2)then
c        --------                       Cas de fichier sequentiel z ou y
       print *,'y : ',yna
       rewind(iuy)  
c                    header  fichier z ou y
        read(iuy)kz

        is=kz(2)
        js=kz(3)
c       --
        idate=kz(6)!!!!!!!!!!
        itime=kz(7)!!!!!!!!!!!!
        read(yna(2:7),'(i6)')idate
        read(yna(9:16),'(i8)')itime
c        print *,'idate : ',idate !!!!!!!!!!!
c        print *,'itime',itime !!!!!!!!!!!!
c        iline=kz(8)
        read(yna(21:21),'(i1)')iline
c        print *,' ymz: iline:',iline

      do j=1,js
       read(iuy)(lec(i),i=1,is)
        do i=1,is
        tab(i,j)=lec(i)    !                        bon
c       --------
        enddo  !i

      enddo  ! j
      endif  ! izyb

c-------------------------
       if(izyb.eq.3)then
c      ----                      Cas de fichier fits direct b         izyb=3
c Fichier d'entree est un fichier B donc un fits a acces direct:
c                                                   header + tableau
        print *,'   lecture de bna: ',bna       

        read(bna(2:7),'(i6)')idate
        read(bna(30:37),'(i8)')itime
        read(bna(21:21),'(i1)')iline
c        print *,'idate : ',idate
c        print *,'itime :',itime
c        print *,'iline :',iline
c Lecture du nombre de blocs headers
        call comptehead(25,inbhead)

c        call readifits(25,'NAXIS1  ',naxis1,1)
c        call readifits(25,'NAXIS2  ',naxis2,1)
        naxis1=ibs
        naxis2=jbs

c        print *,' bmc: naxis1: ',naxis1
c        print *,' bmc: naxis2: ',naxis2
c        print *,'inbhead : ',inbhead
c        print *,'nclim : ',nclim

      if(ipermu.eq.0)then
         call readtabfits(iub,inbhead,iswap,naxis1,naxis2,nclim,tab,
c            -----------
     1                   ijcam)
      else
c                    modif 22 avril 00
        call readtabfits(iub,inbhead,iswap,naxis1,naxis2,nclim,dc,
c            -----------                                       economie
     1                   ijcam)

        ipiv=naxis1
        naxis1=naxis2
        naxis2=ipiv

        do i=1,naxis1
          ip=naxis1+1-i
        do j=1,naxis2
          tab(ip,j)=dc(j,i)
        enddo
        enddo        

      endif
  
        is=naxis1
        js=naxis2

c        write(3,*)' tab: is,js ',is,js
c        do j=1,js,20
c        write(3,*)(tab(i,j),i=1,is,100)
c        enddo

      endif  !                                              fin   iyb

c      call corseuil(tab,ijcam,is,js,kbseuil)
c          --------

c========================================
c Soustraction du DARK CURRENT dc

      if(idc.ge.0)then
        rewind(iux)
        read(iux)kz
c        print *,' kz ',(kz(i),i=1,8)
        is=kz(2)
        js=kz(3)
 
            message=0
          do j=1,js
          read(iux)(dc(i,j),i=1,is)
          enddo

c          call corseuil(dc,ijcam,is,js,kbseuil)
c -------------------------------------------------liste en js2
          js2=js/2
          do i=1,is,40
                  kdiff=tab(i,js2)-dc(i,js2)
              write(3,*)'bmc1: liste en js2    tab,dc,diff (i)',
     1               tab(i,js2),dc(i,js2),kdiff
          enddo
         write(3,*)' '
c--------------------------------------------------liste en is2
            is2=is/2
c          do j=1,js,20                        !  tous les 20 points
c            if((j-jcor1)*(j-jcor2).lt.0)then
c            do i=1,is
c            dc(i,j)=dc(i,j-1)
c            enddo
c            endif
               
       do j=1,js
         krj=j-(j/20)*20       
          do i=1,is
             if(i.eq.is2.and.krj.eq.0)then
                    kdiff=tab(i,j)-dc(i,j)
             write(3,*)' bmc1:   liste en is2    tab,dc dif mess',
     1                          tab(i,j),dc(i,j),kdiff,message
           endif
            tab(i,j)=tab(i,j)-dc(i,j)
c           ---      
            if(tab(i,j).lt.0)then
               if(tab(i,j).lt.-20)message=message+1
               tab(i,j)=0
            endif
          enddo
c---------------------------------------            
       if(j.eq.js)write(3,*)' bmc: tab(i tous 50,js) ',
     1    (tab(i,js),i=1,is,50)     !     bon

          enddo    !                       j
        if(message.ne.0)then
        write(7,*)'warning: dark > signal+20 for',message,'points'
        print  *, 'warning: dark > signal+20 for',message,'points'
        endif

       print *,'bmc1:iitab,jjtab',iitab,jjtab  


      endif          !                               fin    idc

c======================================================
c                              lectures pour icalib=1 et izyb=3
c                             avant d'entrer dans la boucle canaux

c                                   Lecture de fl   ********************
      if(icalib.eq.1.and.izyb.eq.3)then
      rewind(iuf)
      read(iuf)na3,imf,jmf,nmf  ,imm,jmm,nmm,ndf,dayf,timef,ilinef,
c                  -----------
     1          lbda,ncf,jt100,ja100,jb100,jz100,nc100,kminm,
     2       (kz(i),i=20,512)
c               *****
c      print *,iuf
c      print *,'apres read(iuf) lbda:',lbda!!!!!!!!!!!!!
c      print *,'dayf,timef',dayf,timef
c     print *,'jz100',jz100
      write(3,*)'iuf,imf,jmf,nmf,dayf,timef,ilinef,lbda,
     1          ncf,jt100,ja100,jb100,jz100,nc100,kminm,backb'
      write(3,*)iuf,imf,jmf,nmf,dayf,timef,ilinef,lbda,
     1     ncf,jt100,ja100,jb100,jz100,nc100,kminm,
     2     (kz(i),i=20,30)  !512)
c                                                    
c      ltrjf=jt100
c     ncf est pris dans le header
c      ncf=(1+nmf)/2
c             average line-center = n=ncf and
c                                   j=(ja100+jz100)/100 in i=1
c                                            jz100/100     i=(1+imf)/2
c                                     (jb100+jz100)/100    i=imf
         do n=1,nmf
         do j=1,jmf
         read(iuf)(fl(i,j,n),i=1,imf)
         enddo
         write(3,*)'bmc n fl(i,50,n) ',n,(fl(i,50,n),i=1,1000,150)
         enddo

       if(istray.ne.0)then
         do n=1,nmf
         read(iuf)(stray(i,n),i=1,imf)
         enddo
       endif

      endif   !                                        fin icalib=iyb=1
c=========================================================
c                                Ecriture header fichier c
      do i=1,512
         kz(i)=0
      enddo
      
      kz(1)=8 !!!!!!!!!!!!!!!! 5
      kz(2)=im
      kz(3)=jm
      kz(4)=nm
      kz(5)=im  !! a supprimer
      kz(6)=nm  !! a supprimer
      kz(7)=2   !!  .....
      kz(8)=1
      kz(9)=1
      kz(10)=4
      kz(11)=idate
      kz(12)=itime
      kz(13)=iline
      kz(14)=lbda

c      if(izyb.eq.3)then
      kz(10)=10
      kz(15)=ncf
      kz(16)=jt100
      kz(17)=ja100
      kz(18)=jb100
      kz(19)=jz100
      kz(20)=nc100
      kz(21)=kminm
c      endif

      print *,'kz(1 a 21) :', (kz(i),i=1,21)
      if(izyb.le.2)then
      write(iuc)kz
c           ---
      else
      write(iuc)kz  !,*)kz
      endif
      print *,'bmc1 avant canaux: iitab,jjtab',iitab,jjtab  

c==============================================================
c                                               boucle canaux
c corrections dust
      iddust=0
      jddust=0

        if(dxdust.ne.0..and.dydust.ne.0.)then
           if(dxdust.ge.0.)then
           iddust=dxdust+0.5
           else
           iddust=dxdust-0.5
           endif

           if(dydust.ge.0.)then
           jddust=dydust+0.5
           else
           jddust=dydust-0.5
           endif        
           
           i1dust=x1dust+1.
           i2dust=x2dust+1.
             if(i2dust.gt.im)i2dust=im
           j1dust=y1dust+1.
           j2dust=y2dust+1.
             if(j2dust.gt.jm)j2dust=jm     
        endif

      i1dust=x1dust+1.5
      i2dust=x2dust+1.5
      j1dust=y1dust+1.5
      j2dust=y2dust+1.5
        if(i2dust.gt.im)i2dust=im
        if(j2dust.gt.jm)j2dust=jm

70    format(' bmc: iddust,jddust,i1dust,i2dust,j1dust,j2dust ',
     1         6i8)
      write(3,70) iddust,jddust,i1dust,i2dust,j1dust,j2dust
c======================================
         call par1('    dlxy',1,ldxy)
c            write(3,*)'bmc avant calib: n,tabxy(i,50)'
      do 90 np=1,nm   !                                     ****** canaux
c     -------------
c        print *,' bmc canaux n1,n2,n=',n1,n2,np
        n=np
        if(invern.ne.0)n=nm-np+1
c                                              n initial non inverse
c                                              np final  inverse eventuellement
c      print *,'bmc: n=',n
c Correction shift

      call channel_shift(nw,win,np,im,milsec,csh1,csh2,csh3)
c          -------------       final         out  out  out

c Calcul de la matrice de transformation
c      write(3,97) !!!!!!!!!!!!!!!!!!!
c      print*,' bmc: im,jm,n ',im,jm,n
      call COEFF(im,jm,xr,yr,n,coef,distor)
c          -----             *         n.ne.np si inversion

c      print *,'apres coeff: iitab,jjtab',iitab,jjtab 

c      print *,' bmc: coef ',coef
c       write(7,*)' coef: ',coef
c-------------------------
c                     Interpolation du fichier  b
c      print *,'av interpol b,iitab,jjtab,im,jm',iitab,jjtab,im,jm 

          xc=float(1+im)/2.
c          call par1('    dlxy',1,ldxy)
          dlx=float(ldxy)/float(milsec)

        do ii=1,im
        u=(ii-xc)/(xc-1.)
        shiftx=u*dlx
        shift1=-(csh1(1)+u*(csh2(1)+u*csh3(1)))
        shift2=-(csh1(2)+u*(csh2(2)+u*csh3(2)))

          do jj=1,jm
          alp=float(jj-1)/float(jm-1)
          shifty=shift1*(1.-alp)+shift2*alp
            if(ii.eq.1.or.ii.eq.im)then
            if(jj.eq.1.or.jj.eq.jm)then
80    format(' channel_shift: nw,n,ii,jj,shiftx,shifty ',2i2,2i5,2f5.2)
c            write(3,80)nw,n,ii,jj,shiftx,shifty

c            print 80,nw,n,ii,jj,shiftx,shifty
            endif
            endif
                xx=ii+xrecad(nw,nq)-1.+dxr/float(milsec)+shiftx
                yy=jj+yrecad(nw,nq)-1.+dyr/float(milsec)+shifty
          call PIX(xx,yy,coef,i,j,di,dj)
c              ---  in         out

      if(iitab.gt.2000)print *,'ii,jj,iitab',ii,jj,iitab  !  oui

        jflag=0
      if(j.lt.1)then
        j=1
        dj=0.
        jflag=jflag+1
      endif
      if(j.ge.js)then
        j=js-1
        dj=0.
        jflag=jflag+1
      endif
       if(jflag.gt.0)write(3,*)' jflag=',jflag
       if(jflag.gt.0)print *,' jflag=',jflag

          tabxy(ii,jj)=(tab(i,j)*(1.-di)+tab(i+1,j)*di)*(1.-dj)
c            iitab,jjtab
     1            +(tab(i,j+1)*(1.-di)+tab(i+1,j+1)*di)*dj


c------------fcont
        if(izyb.eq.1)fcont(ii,jj,n)=tabxy(ii,jj) ! calcule fcont pour izyb=2,3

        if(izyb.ge.2.and.icalcont.eq.1)then
          piv=10000./fcont(ii,jj,n)
          tabxy(ii,jj)=tabxy(ii,jj)*piv +0.5  ! utilise fcont pour corriger v,b 
        endif
c--------------------------

      if(iitab.gt.2000)then
      print *,'ii,jj: xx,yy,i,j,iitab,jjtab,ijcam,tabxy',
     1         ii,jj,xx,yy,i,j,iitab,jjtab,ijcam,tabxy(ii,jj)
      stop
      endif


          enddo   !                         jj
        enddo     !                         ii

c      write(3,*)n,(tabxy(i,50),i=1,1000,150)
        
      write(9,*)' '
      write(9,*)' bmc calcule z ou c: nw, np, tab(187,25,np) ',
     1                                 nw, np, tabxy(187,25)


c      print *,'bmc1:apres tabxy: iitab,jjtab',iitab,jjtab  ! non


c      ii=im/2
c      write(9,*)' bmc: n=',n,' tabxy(im/2,j tous 10)'
c      write(9,*)(tabxy(ii,jj),jj=1,jm,10)

c-------------------------
c                          Calcul des backgrounds 
c                                 (deux courbes par canal)

c      call COEFF(im,jm,xr,yr,n,coef,distor)
c          -----             -
        jesp2=jespace/2  !!     coordonnees nouvelles de z ou c


c-------------------
c                  lissage, soustract. fond, et ecriture dans le cas icalib=0
c==================     pour cmf
      if(icalib.eq.0.and.izyb.le.2)then
c       call ssmooth(tabxy,1536,200,ism,jsm,smooth)  !!! remettre eventuellement
c           -------

c        if(iscat.ne.0)then
c      do i=1,im
c        kpiv=0.5*(tabx(i,1,np)+tabx(i,2,np))
c          do j=1,jm
c          tabxy(i,j)=tabxy(i,j)-kpiv
c          enddo
c      enddo
c        endif
c-----
      do j=1,jm
      write(iuc)(tabxy(i,j),i=1,im)    !  canal n

c      if(j.eq.50)write(3,*)'bmc n tabxy(i,50) ',
c     1                         n,(tabxy(i,50),i=1,1000,150)
c            do i=1,im
c            fcont(i,j,n)=tabxy(i,j)   ! cont calib
c            enddo

      enddo


c      print *,'bmc1 apres ecriture iuc:iitab,jjtab',iitab,jjtab ! non

      write(9,*)' bmc ecrit z: nw,np, tabxy(187,25) ',
     1                         nw,np, tabxy(187,25)

      endif    !       pas calib, iyb=0  (inclus boucle n)
c===================
c                                          Calibration dans le cas fichiers b
c                                          **********************************
      if(icalib.eq.1)then

c  soustraction des backgrounds (differents de backg) et division par fl 
c      print *,'etape soustraction du fond' !!!!!!!!!!!!!!!!
        fscat=1.
        if(iscat.eq.0)fscat=0.

c          if(np.eq.5)then
c          do j=1,jm,10
c          write(3,*)'bmc:n,j,tabxy(i,i)  ',n,j,(tabxy(i,i),i=1,jm,100)
c          enddo
c          endif

      n32000=0
       do j=1,jm    !                                   boucle j
          do i=1,im !                               boucle i
             piv=tabxy(i,j)     !-0.5*(tabx(i,1,np)+tabx(i,2,np))*fscat
c             -----
c            if(istray.eq.2....

          pivd=fl(i,j,np)
c              *********
            if(pivd.eq.0)pivd=1
c1 tab=fl*(prof+stray)/xnormf
c1         prof+stray=tab*xnormf/fl
               xnormf=10000.
            pivt=piv*xnormf/pivd+0.5   !-backb+0.5   !   *******************  
c  nouveau tab   tab ****** fl   *****        
c                                      
c      if(istray.eq.1)pivt=pivt-stray(i,np)
c                              -----
              if(abs(pivt).gt.32000)then
                n32000=n32000+1
                if(i.ne.1)tabxy(i,j)=tabxy(i-1,j)
              else
                tabxy(i,j)=pivt    !                    *****
c               -----
              endif
          enddo      !      boucle i
        enddo        !                  boucle j

      if(n32000.ne.0)write(7,*)' bmc: ',n32000,' intensities > 32000 ,',
     1                         ' reduce minpro'

      write(9,*)' bmc ecrit c: nw,np, fl(187,25,np),tabxy(187,25) ',
     1                         nw,np, fl(187,25,np),tabxy(187,25)

c                                                lissage et ecriture
c       call ssmooth(tabxy,1536,200,im,jm,smooth)
c           -------
c-------------------------------
c correction hairline
c      call par1('   haira',1,kpiv)
c        ihaira=float(kpiv)/float(milsec)
c      if(ihaira.ne.0)then

c      call par1('   hairb',1,kpiv)
c        ihairb=float(kpiv)/float(milsec)
c      call par1('  hairhw',1,kpiv)          ! demi-largeur cheveu
c        ihairhw=float(kpiv)/float(milsec)
c      if(ihairhw.lt.10.and.ihairhw.ne.0)then
c
c  recherche cheveu 
c      do j=1,jm                    ! boucle j
c        ih1=ihaira+ihairhw
c        ih2=ihairb-ihairhw
c      if(ih2.gt.ih1)then
c         if(j.eq.jm/2)
c     1   write(3,*)' ihaira,ihairb,ihairhw ',ihaira,ihairb,ihairhw
c         khair=32000
c           do i=ih1,ih2
c              kpiv=tabxy(i,j)
c              khair=min0(khair,kpiv)
c              if(khair.eq.kpiv)ih=i
c           enddo

c           if(ih.eq.ih1)then
c           print *,' haira too large '
c           stop
c           endif
c           if(ih.eq.ih2)then
c           print *,' hairb too small '
c           stop
c           endif

c        ih=jhair(j)
c        iha=ih-ihairhw
cc        ihb=ih+ihairhw

c interpolation    1  ih1  iha  ih  ihb  ih2  is
c                           ..........
      
c        pas=(tabxy(ihb,j)-tabxy(iha,j))/float(ihb-iha)
c      do i=iha,ihb
c        ip=i-iha
c        hair(ip)=tabxy(iha,j)+ip*pas
c      enddo
c        if(j.eq.jm/2)then
c        do i=iha,ihb
c        write(3,*)' i,tabxy(i,jm/2),hair(i-iha) ',
c     1              i,tabxy(i,j),hair(i-iha)
c        enddo
c        endif

c      do i=iha+1,ihb-1
c        ip=i-iha
c        tabxy(i,j)=hair(ip)+0.5
c      enddo

c      endif
c      enddo     ! fin boucle j

c      endif
c      endif
c           fin cheveu
c------------------------- chargement txyn
      do j=1,jm
         do i=1,im
            kpiv=tabxy(i,j)
          if(kpiv.lt.0)kpiv=0
         txyn(i,j,n)=kpiv
         enddo
      enddo    
c       write(3,*)'bmc n txyn(i,50,n)',
c     1           n,(txyn(i,50,n),i=1,im,150)
c--------------------     fin calib du canal

c             Il n'est pas necessaire de rajouter les backgrounds
c             cmd ne les utilisera pas.

          endif   !    icalib  fichiers b
c=======
c        print *,'bmc743 n',n
90    continue !   n
        write(3,*)' bmc  fin boucle canaux'
        print *,' bmc  fin boucle canaux'
c=====
        write(3,*)' msdp2.f 2582 bmc1: icalib,inveri,inverj ',
     1                 icalib,inveri,inverj   ! m2403
      if(icalib.eq.1)then
          jc=jm/2
c--------------------------  prominences
c      call proscat(cna,txyn,im,jm,nm,milsec,inveri,txynp)
c          corrige txyn par extrapolation ciel entre dsky1 et dsky2
c---------------  ecriture txyn
      do n=1,nm
        do j=1,jm
           jp=j

c            if(inverj.ne.0)jp=jm-j+1   ! m2403
        if(inveri.eq.0)then
          if(izyb.le.2)then
          write(iuc)(txyn(i,j,n),i=1,im)  !  txynp
          else
          write(iuc)(txyn(i,j,n),i=1,im) !  txynp  *
          endif
        else
          if(izyb.le.2)then
          write(iuc)(txyn(i,j,n),i=im,1,-1)  !  txynp
          else
          write(iuc)(txyn(i,j,n),i=im,1,-1) !  txynp  *
          endif
        endif
        enddo
      write(3,*)'fin bmc n,txyn:',n,(txyn(i,jc,n),i=1,im,100)  ! txynp
      enddo

      endif  !   icalib   
c-----------------------------------------
      close(unit=23)
c        write(3,*)' 23 ferme'
      close(unit=24)
c        write(3,*)' 24 ferme'
      close(unit=25)
c        write(3,*)' 25 ferme'
      close(unit=26)
c        write(3,*)' 26 ferme'

      if(icalib.eq.1.and.izyb.eq.3)close(unit=29)

      return
      end
c********************************************************************
      subroutine COEFF(im,jm,xr,yr,n,coef,distor)
      dimension xr(18,3,2),yr(18,3,2)
      double precision coef(6,2),xl,yl,x1,x2,x3,x4,x5,x6,
     1                                 y1,y2,y3,y4,y5,y6
      integer distor

      xl=im-1
      yl=jm-1
c
      x1=xr(n,1,1)
      x2=xr(n,3,1)
      x3=xr(n,1,2)
      x4=xr(n,3,2)
      x5=xr(n,2,1)
      x6=xr(n,2,2)

      y1=yr(n,1,1)
      y2=yr(n,3,1)
      y3=yr(n,1,2)
      y4=yr(n,3,2)
      y5=yr(n,2,1)
      y6=yr(n,2,2)
c
      coef(1,1)=x1
      coef(3,1)=(x3-x1)/yl

      if(distor.eq.0)then
      coef(2,1)=(x2-x1)/xl
      coef(4,1)=(x1+x4-x2-x3)/(xl*yl)
      coef(5,1)=0.
      coef(6,1)=0.
      else
      coef(2,1)=(-3.*x1-x2+4.*x5)/xl
      coef(4,1)=(3.*x1+x2-3.*x3-x4-4.*x5+4.*x6)/(xl*yl)
      coef(5,1)=2.*(x1+x2-2.*x5)/(xl*xl)
      coef(6,1)=2.*(-x1-x2+x3+x4+2.*x5-2.*x6)/(xl*xl*yl)
      endif
c
      coef(1,2)=y1
      coef(3,2)=(y3-y1)/yl

      if(distor.eq.0)then
      coef(2,2)=(y2-y1)/xl
      coef(4,2)=(y1+y4-y2-y3)/(xl*yl)
      coef(5,2)=0.
      coef(6,2)=0.
      else
      coef(2,2)=(-3.*y1-y2+4.*y5)/xl
      coef(4,2)=(3.*y1+y2-3.*y3-y4-4.*y5+4.*y6)/(xl*yl)
      coef(5,2)=2.*(y1+y2-2.*y5)/(xl*xl)
      coef(6,2)=2.*(-y1-y2+y3+y4+2.*y5-2.*y6)/(xl*xl*yl)
      endif

      return
      end
c--------------
      subroutine pix(x,y,coef,i,j,di,dj)
c                ---
      double precision coef(6,2),xx,yy
        xx=x
        yy=y
      xi=coef(1,1)+xx*coef(2,1)+yy*coef(3,1)+xx*yy*coef(4,1)+
     1   (xx*xx)*(coef(5,1)+yy*coef(6,1))
      yj=coef(1,2)+xx*coef(2,2)+yy*coef(3,2)+xx*yy*coef(4,2)+
     1   (xx*xx)*(coef(5,2)+yy*coef(6,2))

      i=xi
      j=yj
      di=xi-i
      dj=yj-j
      return
      end      
c----------------------------------------------------
       Subroutine readtabfits(iu,inbhead,iswap,is,js,nclim,tabent,
c                 -----------
     1                        ijcam)
       integer*2 tabent(ijcam,ijcam)
       integer*2 sort(1440),in(1),out(1),ku
       integer t,uint
       
       call par1('    uint',1,uint)       
c                               0
c Lecture de tabent(is,js)
        n=inbhead
        i=0
        j=1
        k=1
100      n=n+1
        read(iu,rec=n,iostat=ios)  (sort(t),t=1,1440)
c        print *,'apres read n =',n
        if(ios.lt.0) go to 1000
        t=1
200     i=i+1
        if(i.le.is) go to 500
         if(j.lt.js) then
           j=j+1
           i=1
         else
          goto1000
c            if(k.lt.nclim) then
c               k=k+1
c               j=1
c               i=1
c            else
c               go to 1000
c            endif
          endif

500       continue

          if(iswap.eq.1)then
              in(1)=sort(t)
              call swap(in,1,out)
              sort(t)=out(1)
          endif

          if(uint.eq.1)then
             ku=sort(t)
             call sint(ku)
             sort(t)=ku
          endif

        tabent(i,j)= sort(t)

       t=t+1

       if (t.eq.1441) go to 100
       go to 200
1000   continue
c      print *,'lecture des donnees en I2 effectuee'

          return
          end
c----------------------------------                 
      subroutine channel_shift(nw,win,n,im,milsec,c1,c2,c3)
c                -------------
      integer win(2),acs1,bcs1,zcs1,acs2,bcs2,zcs2
      dimension c1(2),c2(2),c3(2)

        lm=0
        acs1=0
        zcs1=0
        bcs1=0
        acs2=0
        zcs2=0
        bcs2=0

        do jj=1,2
        c1(jj)=0
        c2(jj)=0
        c3(jj)=0
        enddo

      return

c      do8 l=1,23
c      call par1('     wcs',l,wcs)
c        if(wcs.eq.0)goto10
c        lm=l
c        if(wcs.ne.win(nw))goto8
c      call par1('     ncs',l,ncs)
c        if(ncs.ne.n)goto8
c      call par1('    acs1',l,acs1)
c      call par1('    bcs1',l,bcs1)
c      call par1('    zcs1',l,zcs1)
c      call par1('    acs2',l,acs2)
c      call par1('    bcs2',l,bcs2)
c      call par1('    zcs2',l,zcs2)
c        goto10
c8     continue
c10    continue
ccc        if(lm.eq.0)return

c      call par1('    dlxy',1,ldy)
c      dly=ldy

c          pix=milsec
c      a(1)=(float(acs1)+dly)/pix
c      b(1)=(float(bcs1)+dly)/pix
c      z(1)=(float(zcs1)+dly)/pix
c      a(2)=(float(acs2)-dly)/pix
c      b(2)=(float(bcs2)-dly)/pix
c      z(2)=(float(zcs2)-dly)/pix

c      do jj=1,2
c      c1(jj)=z(jj)
c      c2(jj)=0.5*(b(jj)-a(jj))
c      c3(jj)=0.5*(a(jj)+b(jj)-2.*z(jj))
c      enddo

c      return
      end
c----------------------------------------
      subroutine corseuil(tab,ijcam,is,js,ks)
c                --------
c correction par seuil relatif dans fichiers b
      integer*2 tab(ijcam,ijcam)
      ns=0

c bords
      do i=1,is
      if(tab(i,1)-tab(i,2).gt.ks)then
        ns=ns+1
        tab(i,1)=tab(i,2)
      endif
      if(tab(i,js)-tab(i,js-1).gt.ks)then
        ns=ns+1
        tab(i,js)=tab(i,js-1)
      endif
      enddo

      do j=1,js
      if(tab(1,j)-tab(2,j).gt.ks)then
        ns=ns+1
        tab(1,j)=tab(2,j)
      endif
      if(tab(is,j)-tab(is-1,j).gt.ks)then
        ns=ns+1
        tab(is,j)=tab(is-1,j)
      endif
      enddo

c hors bords
      do i=2,is-1
      do2 j=2,js-1
      ky=tab(i,j)

      ka=ky-tab(i-1,j)
      kb=ky-tab(i+1,j)
      if(ka.gt.ks.and.kb.gt.ks)then
        ns=ns+1
        tab(i,j)=0.5*(ka+kb)+0.5
        goto2
      endif

      ka=ky-tab(i,j-1)
      kb=ky-tab(i,j+1)
      if(ka.gt.ks.and.kb.gt.ks)then
        ns=ns+1
        tab(i,j)=0.5*(ka+kb)+0.5
      endif

2     continue
      enddo

      if(ns.gt.0)then
      write(7,*)' bmc:',ns,' points with intensity excess > ',ks
      print   *,' bmc:',ns,' points with intensity excess > ',ks
      endif

      return
      end
c==============================================   cmf1.f
c******************************************************************************
c  Programme cmf1 - UNIX     
c******************************************************************************
        subroutine cmf1(nw1,nw2,win,nm,nq1,nq2,kdecal,gna,vna,wna,fna,
     1                 fl,tab,nrecad,xrecad,yrecad,idebf,
c                              in 2
c                             la boucle recadrage est dans ms1
     2                 iitab,jjtab,nnfl,lltab,nntab,im,jm,x,y,
     3                 jt100,ibdm,sigbd,ides,
c                      out   in     out   in  
     4                 iterfluc,deljt,delja,deljb,idy,dyln,trjm,
c                      in                         in  out  out  
     5                 istray,stray,xnormf,iplotf)
c     in     eco   out    in
       dimension x(2000),y(2000),xrecad(2,6),yrecad(2,6),idebf(2,6),
c                                       nw nq
     1            cr(10),profm(2000,2,6),prof100(101,18),coef(18),
c                                  nw nq
     2            cf(18,2),kz(512),profmm(2000),yln(2000),xk(2000),            
     3            xr(18,3,2),yr(18,3,2),tabp(200),tabq(200),
     4      yline(2000, 2,6),grad(18),ylinem(2000),ypiv(2000,2),
c                i    nw nq    
     5            trj(2,6),ncw(2,6),kmin(2,6),kdecal(2),xncw(2,6), 
c                    nw nq   
     6         sigbd( 3,18,2,6),dyln(iitab,18,2,7),stray(2000,18),
c                   ibd  n nw nq       i    n  nw nq
     7         profj(2000)
       integer*4 nrej(90),smoothi,smoothj,day,time,curv,
     1            win(2), som1, som2,!   xmin(10),xmax(10),
c                 nw
     2            calfs,yz(2,6),dya(2,6),dyb(2,6),
c                               z       a-z      b-z (arcsec/1000)
     3                        dyzm(2),dyam(2),dybm(2)
        integer*2 tab(iitab,jjtab,lltab),
     1            rec(2000),fl(iitab,jjtab,nnfl),pm(2000)

        character nom*15,titre*58,canal*3
        character*22 gna(26),vna(6,26),wna(6,26),fna(6,26)
      do i=1,2
      do j=1,6
        ncw(i,j)=0
      enddo
      enddo
           
c     integer displ,winp,caldeb
c     character comm*80,fname(22),fprint(9),hdeb(6),tit(25)

c      print*,' cmf1: kdecal ',kdecal

c Input: .DAT and C-file
c Output: F-file = Header = 15 values (6,im,jm,nm, im,jm,1,
c                                      7,day, time, lbda,
c                                      jt1000, ja1000, jb1000, jz1000)
c                                       en arcsec/1000
c                  Table Flat field = im*jm*nm
c                  Mean Profile = im values
C
C     8=max number channels
        print *,'entree cmf1 ides=',ides

      ides=1
      trk=100. !50.  !   translation entre canaux pour profm
      trkc=50.
      ktr=100
      ktrc=50

      ic=(1+im)/2

3       format(1x)
13      format(18f7.1)
16	FORMAT(' donnees sur .par')
22	FORMAT(/)

c obs
      call par1('     jy1',1,jy1)
      call par1('     jy2',1,jy2)
      call par1('  mupris',1,mupris)
      call par1('  mustep',1,mustep)
      call par1(' minprof',1,minprof)
      call par1('  contff',1,kcontff)
        contff=kcontff
      call par1('   mgrim',1,mgrim)
c exe
      call par1('  inclin',1,inclin)
              jpiv=iabs(inclin)
           if(jpiv.le.3)then
              jdb=0
           else
              jdb=inclin
              inclin=1
           endif

c      call par1('  milrec',1,milrecad)
c        milrecad=0
      call par1('  milsec',1,milsec)
      call par1('   calfs',1,calfs)
      call par1('   itgri',1,itgri)
      call par1('   itana',1,itana) 
      call par1('    ideb',1,ideb)
        if(itgri.eq.0)itgri=34000
        if(itana.eq.0)itana=17000
        largsec=4.*float(itana)/1000.+0.5
        largpix=4.*float(itgri)/float(milsec)+0.5
        largmin=largpix/4
        itpix4=itgri/(4*milsec)

c fix
      call par1(' smoothi',1,smoothi)
      call par1(' smoothj',1,smoothj)
      call par1('    il1p',1,il1p)
      call par1('    il2p',1,il2p)
      call par1('    jl1p',1,jl1p)
      call par1('    jl2p',1,jl2p)
      call par1('    isym',1,isym)
      call par1('  iextra',1,iextra)
c      call par1(' ncentre',1,ncm)
c                            inutilise

         n1=1
         n2=nm
c      if(ncm.eq.0)then
          nc=(1+nm)/2
c      endif
c          nc=ncm

c                canal de reference pour le calcul de trj 

c  smoothi=1     constant zero function versus i
c  smoothi=2     linear zero function versus i
c  smoothj=1     constant zero function versus j
c  smoothj=2     linear zero function versus j
c  if mupris.ne.0
c                 read file g....  before c....
c                 mupris= translation by PRISms (microns)
c                 mustep= STEP between slits before prisms
c  isym          1 symetrisation du profil
c                2 ajustement entre ailes bleue et rouge
c                        (voir param.txt)
c  iextra        1 extrapolation a partir des canaux n1+1 et n2-1
c*************************************************


c                      Etapes
c                      ======

c      boucle ntl

c         boucle nq,nw
c           lecture tab(nw,nq)  (=vna ou wna suivant calfs)
c           si ntl=1    calcul trj et yline pour le canal
c                              ---    -----
c                              ncw=xncw= (nm+1)/2        pour inclin=0
c                              ---
c                                   canal min (subr. ncentre) inclin=1
c                                   moy canaux ailes raies    inclin=2 
c                       dessins A1 et A2        goto endif 259
c           else        calculs coef de canal a canal,
c                               ----
c                       prof100,profm,
c                       ------- -----
c                         (n'utilisent que la pente et la courbure de yline) 
c                       dessins B                |
c           endif 259                            |
c         enddo nw,nq 260                        |
c                                                V
c           moyennes trj,yline
c             et corrections des yline (variation/ic)
c         si ntl=2 goto 280
c           moyennes profilmm
c           dessin entrelac (si nec), profm et profmm

c      enddo ntl 280
c
c      nouvelles moyennes (redondantes) et calcul profmm
c      boucle nq,nw pour calcul et dessin fl


c                 Etapes pour option idy=1    non
c                 ========================


c nrecad=2
c Calcul de trjm = moyenne nw,nq a partir de la geometrie

c Boucles
c      do280 ntl=1,2  
c      do270 nq=nq1,nq2
c      do 260 nw=nw1,nw2
c         calcul de yline(i,nw,nq)  par plot_line
c                   chargement dans dyln(i,n,nw,nq)
c                   centrage sur jc
c         goto260
          
c------------------------------------------------------------------------
      kw=1
      call par1('  jt1000',kw,jt1000)
      call par1('  ja1000',kw,ja1000)
      call par1('  jb1000',kw,jb1000)
      call par1('  jz1000',kw,jz1000)

      jt100=      jt1000*100./float(milsec)
      jz100=100.* (yyc+jz1000/float(milsec))
      ja100=jz100+ja1000*100./float(milsec)+delja
c                                           -----
      jb100=jz100+jb1000*100./float(milsec)+deljb
c     -----
      
      if(jt100.ne.0)jt100=jt100+deljt
c                               -----
c            ces nouvelles valeurs (en centiemes de pixels)
c            seront utilisees
c            dans le calcul, et pour les headers des fichiers fl
c            (mais jz100 est en fait superflu)
c            Toutefois, ja100,jb100,jz100 sont recalcules
c            d'apres les donnees si inclin=1 ou 2

c--------------------------------------------------------------------
      if(idy.ne.0)then                                               !  non
c                                     Calcul de trjm  pour idy=1
c                                     **************
c                                               avec moyennes nq,nw

c lecture de im,jm,nm

      if(calfs.le.1)then
c vna
      iuc=26

      call openold22(vna(1,1),sundec,iuc)
c          ---------
      rewind(iuc) !!!!!!!!!!!!
      read(iuc)na3,im,jm,nm,imm,jmm,nmm,naxis3,naxis4,nd,day,time,
     1 iline,lbda,(kz(i),i=15,512)    !    oui
      close(iuc)
      endif      !    calfs

c wna
      if(calfs.ge.1)then     !  non
      iuc=26
      call openold22(wna(1,1),sundec,iuc)
c          ---------

      rewind(iuc) !!!!!!!!!!!!
      read(iuc)na3,im,jm,nm,imm,jmm,nmm,naxis3,naxis4,nd,day,time,
     1 iline,lbda,(kz(i),i=15,512) 
      close(iuc)
      endif    !  calfs

c                    boucle nw
      do nw=nw1,nw2

         write(3,*)' jt100',jt100
c---
      if(jt100.ne.0)then                    !17/4/04
      trj(nw,nq)=jt100*0.01     ! deja corrige de deljt dans cas 
c                                 jt100.ne.0

      else    !    jt100
c---
c lecture de vecjb et chajb
c            transl et largeur canaux  en unites fichier b
c
      iug=25
      open(unit=25,status='old',file=gna(nw)) !!!!!!!!!!!!!!!!!!
c                               -----------
         rewind(iug)    !!!!!!!!!!!!!!!
        do n=1,19
        read(iug,3) !!!!!!!!!!!!!!!
        enddo
      read(iug,13)xr !!!!!!!!!!!!!!
      read(iug,13)yr !!!!!!!!!!!!!!
      close(unit=iug) !!!!!!!!!!!!!!
c
      chajb=0.5*(yr(nc,1,2)-yr(nc,1,1)+yr(nc,3,2)-yr(nc,3,1))
      vecjb=0.125*(yr(nc+1,1,1)+yr(nc+1,3,1)+yr(nc+1,1,2)+yr(nc+1,3,2)
     1            -yr(nc-1,1,1)-yr(nc-1,3,1)-yr(nc-1,1,2)-yr(nc-1,3,2))
c      print *,chajb,vecjb
c
      write(3,*)' jm,mustep,vecjb,mupris,chajb ',
     1     jm,mustep,vecjb,mupris,chajb
      write (3,*)'  trj:  xr(nc,1,1) ',xr(nc,1,1)
      
      do nq=nq1,nq2
        trj(nw,nq)=float(jm-1)*mustep*vecjb/(float(mupris)*chajb)
c     -------        
c                                        correction deljt avant yline
        trj(nw,nq)=trj(nw,nq)+deljt/100.
c                             -----
c---                                        correction jtcor
        call par1('   jtcor',nw,jtcor)
        piv=float(jtcor)/float(milsec)
        trj(nw,nq)=trj(nw,nq)+piv

c        write(3,*)' nw, correction trj, trj ',nw,piv,trj
        write(3,*)' 3175 nw,nq,chajb,vecjb,icor,trj,deljt ', 
     1              nw,nq,chajb,vecjb,icor,trj,deljt
      enddo
    
      endif    !  jt100

      enddo
c---
      trjm=0.
      den=(nw2-nw1+1)*(nq2-nq1+1)
      do nw=nw1,nw2
      do nq=nq1,nq2
        trjm=trjm+trj(nw,nq)
      enddo
      enddo
        trjm=trjm/den
c       ****
      write(3,*)' option idy=1  trjm=',trjm

       endif                                  !     fin idy=1 ?
c--------------------------------------

c                                  boucle pour moyennes trj et yline
c                                  ---------------------------------
c                         boucle moyennes trj,line
      do 280 ntl=1,2 !                                     1,2****** 

       if(ntl.eq.2.and.idy.ne.0)      !                             non
     1 call centrage_dyln(dyln,im,nm,nw1,nw2,nq1,nq2,trjm,nrecad,
c           -------------
     1                    iitab)
c     boucle nq
      do 270 nq=nq1,nq2
c                                                     boucle nw
      do 260 nw=nw1,nw2
             kw=win(nw)
        write(3,*)'  cmf1:  ntl=',ntl,'  nq=',nq,'  nw=',nw

      call par1('    curv',kw,curv)   !  1
      call par1('   iliss',kw,iliss)  !  20
      call par1('  jparli',kw,jparli)  !  5 
      call par1('  lispro',kw,lissprofm)  !  2
c      call par1('  jmarge',kw,jmarge)    !  0

c        xncw(nw,nq)=canal ou sera cherche le centre raie (option inclin=1)
c             ou bien milieu entre les canaux a plus forte pente (inclin=2)
c        inclin=0   forme des raies lue par jt1000,ja1000,jb1000,jz1000
c                                           en arcsec/1000
c                                  convertis en pixels/100:
c                      jz100 = 100 fois le j  de la raie en (1+im)/2
c                      ja100 = 100 fois le J  de la raie en i=1   
c                      jb100 = 100 fois le j  de la raie en i=im 
c                   (ceci est la traduction des parametres dans le calcul:
c                    voir param.txt pour la traduction en donnees
c                    et milliarcsec)
c        jt100 = 100 fois
c                    la translation en j correspondant a la meme difference
c                    en lambda qu'entre deux canaux successifs (initiaux)
c                    (meme remarque)
c        curv = 0/1  (prise en compte de la courbure de raie)
c        iliss    lissage en i sur 2*iliss+1 avant detection centre raie
c        jparli       "      j parabolique        "   "        "     "
c        lissprofm    "      k parabolique  sur le profil moyen qui sert
c                                          pour calculer fl

      do n=1,nm
c            nm.lt.30
      do j=1,jjtab
      do i=1,iitab
       tab(i,j,n)=0
       fl(i,j,n)=0
      enddo
      enddo
      enddo
c
      do i=1,512
      kz(i)=0
      enddo
c      print*,' cmf3: kdecal ',kdecal
c*****************
c                                                     images TAB
c                    (les backgrounds sont deja soustraits dans bmc)

c       calfs     tab     fl
c          -1       v
c           0       v     
c           1       v     w
c           2       w     w

      if(calfs.le.1)then
c        ----------
c vna
c ---
      print*,' cmf3:nw, ',nw,vna(nq,nw)
      iuc=26
      
      call openold22(vna(nq,nw),sundec,iuc)
c          ---------
c       print*,' cmf3a ',kdecal


      rewind(iuc) !!!!!!!!!!!!
      read(iuc)na3,im,jm,nm,imm,jmm,nmm,naxis3,naxis4,nd,day,time,
     1 iline,lbda,(kz(i),i=15,512) 
      print *,im,jm,nm,day,time,iline,lbda
      
      ic=(1+im)/2
      jc=(1+jm)/2
      nc=(1+nm)/2
c rel -----------
      yyc=(1.+jm)*0.5

110   format(1x,i6,8x)
120   format(9x,i6)

      write(3,*)'ntl,nq,nw,kw',ntl,nq,nw,kw

125   format(' lecc: header z ou c'/6i8/6i8/6i8) !!!!!!!!!!!!!!!10 -> 11
      write(3,125)
     1     na3,im,jm,nm,imm,jmm,nmm,naxis3,naxis4,nd,day,time,lbda
c
      if(calfs.ne.2)then
        do n=1,nm
           write(3,*)'iuc,n,im,jm',iuc,n,im,jm

           do j=1,jm
c              write(3,*)'j ',j
        read(iuc)(rec(i),i=1,im) !!!!!!!!!!!!!!!
          do i=1,im
          tab(i,j,n)=rec(i)
c         ----------
          enddo
        enddo
             write(9,*)' cmf lit z: n, tab(187,25,n) ',n,tab(187,25,n)
c             write(3,*)' cmf lit z: n, tab(187,25,n) ',n,tab(187,25,n)
      enddo
      endif

      im2=im/2
      im2t4=im2+itpix4
       write(3,*)'3207  im2,itpix4',im2,itpix4


      jm2=jm/2
      write(3,*)' tab ',(tab(im2,jm2,n),n=1,nm)
      print    *,' tab ',(tab(im2,jm2,n),n=1,nm)  

      close(unit=26)
      endif   !   calfs

c wna
c ---
      print*,' cmf3:nw, ',nw,wna(nq,nw)
      if(calfs.ge.1)then
c        ----------
      iuc=26
      call openold22(wna(nq,nw),sundec,iuc)
c          ---------
c       print*,' cmf3a ',kdecal

      rewind(iuc) !!!!!!!!!!!!
      read(iuc)na3,im,jm,nm,imm,jmm,nmm,naxis3,naxis4,nd,day,time,
     1 iline,lbda,(kz(i),i=15,512) 
c      print *,im,jm,nm,day,time,iline,lbda
 
c      write(3,125)na3,im,jm,nm,imm,jmm,nmm,naxis3,naxis4,nd,day,time,lbda
c
      do n=1,nm
        do j=1,jm
        read(iuc)(rec(i),i=1,im) !!!!!!!!!!!!!!!
          do i=1,im
          fl(i,j,n)=rec(i)
c         ---------         fl pour economie de memoires
          if(calfs.eq.2)tab(i,j,n)=rec(i)
c                  ---- ----------
          enddo
        enddo

c        write(9,*)' cmf lit z: n, fl(187,25,n) ',n,fl(187,25,n)
      enddo

c          print *,' lecc2 ',kdecal

      im2=im/2
      jm2=jm/2
      write(3,*)'459  fl ',(fl(im2,jm2,n),n=1,nm)        ! non 2013
      print    *,'459  fl ',(fl(im2,jm2,n),n=1,nm)  

      close(unit=26)
      endif

c*****************
      print *,'ntl,idy',ntl,idy

      if(ntl.eq.1)then
      if(idy.eq.0)then
c                                     Calcul de TRJ pour nw,nq
c                                     *************
         print *,' jt100',jt100

      if(jt100.ne.0)then                    !17/4/04
c      if(jt100.ne.0.and.inclin.eq.0)then
      trj(nw,nq)=jt100*0.01     ! deja corrige de deljt dans cas 
c                                 jt100.ne.0

      else

c lecture de vecjb et chajb
c            transl et largeur canaux  en unites fichier b
c
      iug=25
      open(unit=25,status='old',file=gna(nw)) !!!!!!!!!!!!!!!!!!
c                               -----------
         rewind(iug)    !!!!!!!!!!!!!!!
        do n=1,19
        read(iug,3) !!!!!!!!!!!!!!!
        enddo
      read(iug,13)xr !!!!!!!!!!!!!!
      read(iug,13)yr !!!!!!!!!!!!!!
      close(unit=iug) !!!!!!!!!!!!!!
c
      chajb=0.5*(yr(nc,1,2)-yr(nc,1,1)+yr(nc,3,2)-yr(nc,3,1))
      vecjb=0.125*(yr(nc+1,1,1)+yr(nc+1,3,1)+yr(nc+1,1,2)+yr(nc+1,3,2)
     1            -yr(nc-1,1,1)-yr(nc-1,3,1)-yr(nc-1,1,2)-yr(nc-1,3,2))
c      print *,chajb,vecjb
           write (3,*)'  trj:  xr(nc,1,1) ',xr(nc,1,1)
      trj(nw,nq)=float(jm-1)*mustep*vecjb/(float(mupris)*chajb)
c     -------
c                                      correction deljt avant yline
      trj(nw,nq)=trj(nw,nq)+deljt/100.
c                           -----
c                                      correction jtcor
      call par1('   jtcor',nw,jtcor)
      piv=float(jtcor)/float(milsec)
      trj(nw,nq)=trj(nw,nq)+piv

c      write(3,*)' nw, correction trj, trj ',nw,piv,trj
        write(3,*)' 3413  nw,nq,chajb,vecjb,icor,trj,deljt ', 
     1              nw,nq,chajb,vecjb,icor,trj,deljt,deljt
       endif   !  jt100

      endif   !  idy
c-------------
      if(istray.ne.0)
     1 call sstray(tab,iitab,jjtab,lltab,im,jm,nm,nw,nq,
c           ------
     2            stray,nrecad,vna(nq,nw))

      print *,'calcul de yline100'    !!!!!!!!!!!!!!
c                        Calculation of YLINE100(1536,2,6) 
c                        ---------------------------------  

      i1=1
      i2=im
      write(3,*)' im,jm ',im,jm

              im16=im-16
              im8=im-8
 
        xl=im-1
        yl=jm-1
        jc=(1+jm)/2

      il1=1+(im-1)*float(il1p)/100.
      il2=1+(im-1)*float(il2p)/100.
      jl1=1+(jm-1)*float(jl1p)/100.
      jl2=1+(jm-1)*float(jl2p)/100.
        if(jl2p.eq.0)jl2=jm
      iliss2=2*iliss+1
            write(3,*)' il1,il2,iliss2 ',il1,il2,iliss2
      iil1=il1+iliss
      iil2=il2-iliss
      iilm=iil2-iil1+1
c                                              precautions jmarge
c      if(jmarge.eq.0) jmarge=(jm-trj(nw,nq))/4.
       jmarge=2*jparli
                                     
c      if(jmarge.lt.jparli)jmarge=jparli

c-----------------
      if(inclin.ne.0)then   !  1
      ldes=0

      if(ides.ne.0.and.ntl.eq.1)ldes=1
      call plot_line(tab,iitab,jjtab,lltab,im,jm,nm,
c          ---------
     1                              fna(nq,nw),x,y,
c                                              economie
     2              il1,il2,jl1,jl2,iil1,iil2,iliss,jmarge,jparli,
     3              tabp,tabq,nw,win,milsec, curv,cr,yln,ngood,ldes,
c                                                 out    out   <- canal choisi
     4              dyln,nq,idy,trjm,nw1,nw2)
      write(3,*)' call plot_line im,jm,nm,il1,il2,jl1,jl2',
     1                           im,jm,nm,il1,il2,jl1,jl2
      write(3,*)' iil1,iil2,iliss,jmarge,jparli,  nq,idy,trjm',
     1            iil1,iil2,iliss,jmarge,jparli,  nq,idy,trjm

      endif

c      call cross_talk(tab,iitab,jjtab,lltab,im,jm,nm,nw,win,lmctk)
c          ----------
c      if(nrecad.eq.2.and.ntl.eq.1.and.lmctk.ne.0)then
c      if(inclin.eq.3)call plot_line(tab,iitab,jjtab,lltab,im,jm,nm,
c                         ---------
c     1                              fna(nq,nw),x,y,
c                                              economie
c     2              il1,il2,jl1,jl2,iil1,iil2,iliss,jmarge,jparli,
c     3              tabp,tabq,nw,win,milsec, curv,cr,yln,ngood,ldes,
c                                                 out
c     4              dyln,nq,idy,trjm,nw1,nw2)
c      endif


c*********************************
c      if(inclin.eq.0)then       !                               non
c     ---------------
c        xxl=xl*0.5
c      al=0.01*jz100
c      bl=0.005*(jb100-ja100)/xxl
c      cl=0.005*(ja100+jb100-2*jz100)/xxl**2
c        write(3,*)' al,bl,cl=  ',al,bl,cl

c        do i=1,im
c        xi=i-xxl
c        yline(i,nw,nq)=al+xi*(bl+xi*cl)
c                      avec correction delja,deljb
c        rec(i)=yline(i,nw,nq)
c        enddo
c            write(3,*)' inclin=0  yline(ic,nw,nq)= ',yline(ic,nw,nq)
c      ncw(nw,nq)=(nm+1)/2+nw-1
c
      xncw(nw,nq)=ncw(nw,nq)

c      goto200
c              le dessin sera le meme que dans l'option inclin=1
c      endif

      if(inclin.eq.1.or.inclin.eq.3)then
c     --------------------------------------------
c                           recherche canal ou centre champ minimum
c                           reference canal xncw (entier ici)
1000  format(10i8)
1004  format(/' nw=',i2,' n=4')
1005  format(/' nw=',i2,' n=5')
      write(3,1004)nw
      write(3,1000)(tab(ic,j,4),j=1,jm)
      write(3,1005)nw
      write(3,1000)(tab(ic,j,5),j=1,jm)
      
      ncw(nw,nq)=ngood
c      call ncentre(tab,iitab,jjtab,lltab,im,jm,nm,ncw(nw,nq))
c          -------
      xncw(nw,nq)=ncw(nw,nq)
c rel --------
      write(3,*)' cmf: nw,nq,ncw(nw,nq) ',nw,nq,ncw(nw,nq)
      nncw=ncw(nw,nq)
c---
      print *,'            3534              call linecurv, n= ',nncw
      call linecurv(tab,iitab,jjtab,lltab,im,jm,nm,
c          --------
     1          il1,il2,jl1,jl2,iil1,iil2,iliss,jmarge,jparli,nncw,
     2          tabp,tabq,nw,win,milsec, curv,cr,yln)
c                                             out
c                            yln=raie lissee
      do i=1,im
      yline(i,nw,nq)=cr(1)+i*(cr(2)+i*cr(3))
      enddo

      endif
c         yline est centree si inclin=1 ou 3
c-----------------------------
c                                      Dessin A1 options 0 et 1
c200   continue
c
      if(inclin.eq.0.or.inclin.eq.1.or.inclin.eq.3)then
c        -----------------------------------------
c      if(nrecad.eq.2)then
        if(ides.ne.0)then
c*      call pgadvance
c          ---------
c*      call pgvport(0.1,0.9,0.1,0.7) !  (0.1,0.5,0.1,0.7)

c*      call pgwindow(0.,yl,0.,xl)    

        write(canal(1:3),'(f3.1)')xncw(nw,nq)
c*        call pgsch(2.)   !   2.

c*      call pgbox('bcnts',50.,5,'bcnts',200.,2)
c      call pglabel(' ',' ',canal)

      if(inclin.eq.1.or.inclin.eq.3)then
c               ----
      do i=iil1,iil2
      iip=i-iil1+1
      x(iip)=yln(i)
      y(iip)=i
      enddo
c*      call pgline(iilm,x,y)
c          ------
      endif
c                    0 ou 1
c                    ------
         write(3,*)'im,nw,nq ',im,nw,nq

      do i=1,im
      x(i)=yline(i,nw,nq)
      y(i)=float(i)
c        write(3,*)' i,nw,nq,im,x,y ',i,nw,nq,im,x(i),y(i)
      enddo
        write(3,*)' im, x(i) ',im,(x(i),i=1,im,100)
c*      call pgline(im,x,y)
c          ------
         
         pte=float(jdb)/float(milsec)
         pte=pte/float(ic-1)
      do i=1,im
        piv=(i-ic)*pte
        x(i)=x(i)+piv
      enddo
c*      call pgline(im,x,y)
c          ------

      endif
      endif

c---------------------------------
      if(inclin.eq.2)then !                                     non
c     ---------------
      if(ides.ne.0)call pgadvance
        call pgsch(1.5)

c   Recherche des canaux a abs(pente) maximum au centre du canal
c   reference         barycentre des canaux xncw

      jpiv1=jc-jm/10
      jpiv2=jc+jm/10
c
      gradp=-100000.
      gradm=100000.    
      do n=1,nm
      grad(n)=tab(ic,jpiv2,n)-tab(ic,jpiv1,n)
c      print *,'n: ',n
c      print *,' tab(ic,jpiv2,n): ',tab(ic,jpiv2,n)
c      print *,' tab(ic,jpiv1,n): ',tab(ic,jpiv1,n)
      gradm=amin1(grad(n),gradm)
      gradp=amax1(grad(n),gradp)
      enddo

      do n=1,nm
      if(grad(n).eq.gradm)nfte2=n
      if(grad(n).eq.gradp)nfte1=n
      enddo

c      print *,' nfte1,nfte2 ',nfte1,nfte2

c                                  boucle 2 canaux
      xncw(nw,nq)=0.5*(nfte1+nfte2)
  
c   pour option inclin=2     (non)
            ndiff=nfte2-nfte1
      do n=nfte1,nfte2,ndiff
        xa=0.1+(n-nfte1)*0.267/ndiff
        xb=xa+0.267
        call pgvport(xa,xb,.1,.7)
        call pgwindow(0.,yl,0.,xl)
          write(canal(1:2),'(i2)')n
c          call pgsch(2.)
        call pgbox('bcnts',50.,5,'bcnts',200.,2)
        call pglabel(' ',' ',canal)

      iliss2=2*iliss+1
      iil1=il1+iliss
      iil2=il2-iliss
      iilm=iil2-iil1+1

      zlevel=tab(ic,jc,n)
c                      -
          do210 i=iil1,iil2   !!!!!--------- boucle en i
          iip=i-iil1+1
          y(iip)=i
                                 
              jmax=jc+(jm-jc)*0.25
c             ----                          valeur modifiable
          do j=jc,jmax
           do igd=-1,1,2
            jj1=jc+(j-jc)*igd
            jj2=jj1+igd
                        z1=0.
                        z2=0.
              do id=-iliss,iliss
              z1=z1+tab(i+id,jj1,n)
              z2=z2+tab(i+id,jj2,n)
              enddo
                    deni=2*iliss+1.
                    z1=z1/deni
                    z2=z2/deni

            test=(z1-zlevel)*(z2-zlevel)
                yln(i)=jc
            if(test.le.0..and.z1.ne.z2)then
               yln(i)=(jj1*(zlevel-z2)+jj2*(z1-zlevel))/(z1-z2)
               x(iip)=yln(i)
               goto210
             endif
            enddo
           enddo
      if(iip.ne.1)yln(i)=yln(i-1)
210   continue
c                        -----------      fin boucle en i

c       print*,' cmf: n, rec(i)',n,(rec(i),i=iil1,iil2,10)
        im8=im-8
        im16=im-16
        jm2=jm/2
      iic=0.5*(il1+il2)
      iic1=iic+1
c      print *,il1,il2,iil1,iil2
      if(curv.eq.0)then
c        ---------
      call zerofloat(iil1,iic,iic1,iil2,0,0,0,yln,1,im,0,0,
c          ---------                          ---
     1           ar,br,cr,item,nrjm,nrej,y1,y2,ifin)
c                      out    
      cr(1)=br
      cr(2)=ar
      cr(3)=0.
      else
c
      call zerofloat(iil1,0,0,iil2,3000,5,3,yln,1,im,0,0,
c          ---------                        ---
     1           ar,br,cr,item,nrjm,nrej,y1,y2,ifin) 
c                      out    
      endif
c-------------------------------------- L1,L2,L3,L4
      write(3,*)' L1,L2,L3,L4'
      xl1=yln(iil1)
      yl1=i-10.
      call pgtext(xl1,yl1,'L1')


c220   continue
c                        dessin A1 pour option 2   
      do i=1,im
      y(i)=i
      x(i)=cr(1)+i*(cr(2)+i*cr(3))
          if(n.eq.nfte1)npiv=1
          if(n.eq.nfte2)npiv=2
      ypiv(i,npiv)=x(i)
c rel ----------------
      enddo
      call pgline(im,x,y)
c          ------
c      print *,(x(i),i=1,im,20)
      do i=iil1,iil2
      iip=i-iil1+1
      y(iip)=i
      x(iip)=yln(i)
c            ---
      enddo
      call pgline(iilm,x,y)
c          ------
      enddo
C                                    fin boucle canaux
      den=2.
      do i=1,im
        piv=0.
      do n=1,2
        piv=piv+ypiv(i,n)
      enddo
      yline(i,nw,nq)=piv/den
c rel ----------------            
      enddo

      endif
c                                    fin option inclin=2
c
c---------------------------------
c                          calcul de jz,ja,jb, jt pour toutes options inclin
c                                    ------------      ---------------------   
c                                    yline corrige de deljt,delja,deljb
c                                    ------------- 
      write(3,*)' calcul de jt,jz,ja,jb pour toutes options'
      
      if(inclin.ne.0)then
      jt100=100.*trj(nw,nq)+deljt
c                           -----
       jz100=100.*yline(ic,nw,nq)
      ja100=yline(1,nw,nq)*100.+delja
c                               -----
      jb100=yline(im,nw,nq)*100.+deljb
c                                -----

      xxl=xl*0.5
      bl=0.005*(deljb-delja)/xxl
      cl=0.005*(delja+deljb)/xxl**2

c      write(7,*)' im,xxl,bl,cl ',im,xxl,bl,cl

      do i=1,im
      xi=i-xxl
      yline(i,nw,nq)=yline(i,nw,nq)
     1               +xi*(bl+xi*cl)
      enddo

      endif

c      print *, jt100,ja100,jb100,jz100
240   format(/' jt100,ja100,jb100,jz100 = ',4i8) 
      write(3,240)jt100,ja100,jb100,jz100
250   continue
c      print*,' cmf: kdecal ',kdecal
c***************
c dessin                                        titre et dessin A2
c      if(nrecad.eq.2)then
      
       yz(nw,nq)=jz100*milsec/100
      dya(nw,nq)=(ja100-jz100)*milsec/100
      dyb(nw,nq)=(jb100-jz100)*milsec/100

      write(3,*)' nw,nq,  trj,ncw,yz,dya,dyb ',
     1            nw,nq,trj(nw,nq),ncw(nw,nq),
     2                  yz(nw,nq),dya(nw,nq),dyb(nw,nq)
         write(3,*)'3797 ides ',ides
      if(ides.ne.0)then
      write(nom(1:1),'(a1)')'f'
c        write(titre(1:22),'(a)')fna(nq,nw) !!!!!!!!!!!!!!!!
c        write(titre(23:27),'(a5)')'     ' !!!!!!!!!!!!!
c        write(titre(28:33),'(i6)')jt100*milsec/100 !!!!!!!!!!!!!!!
c        write(titre(34:39),'(i6)')(ja100-jz100)*milsec/100 !!!!!!!!!!!!!!!
c        write(titre(40:45),'(i6)')(jb100-jz100)*milsec/100 !!!!!!!!!!!!!!!
c        write(titre(46:51),'(i6)')jz100*milsec/100 !!!!!!!!!!!!!!
c        write(titre(52:58),'(a)')'       '
c            print *,' titre ',titre

      call pgslw(2)
      call pgsch(2.5)
      call pgadvance
      call pgvport(0.2,0.9,0.2,0.8) ! .8
      call pglabel('Y','X','B  Same wavelength, channels n,n+1')  !    //titre)
c      call pgvport(0.2,0.8,0.1,0.8)      !(0.632,0.90,.1,.7)
      call pgwindow(0.,yl,0.,xl)

      print *,'   3817 pgbox'
      call pgbox('bcnts',50.,5,'bcnts',200.,2)
      endif
c      stop


      
c***************
      call pgsls(1)
c      piv=0.5*yl-yline(ic,nw,nq)+1.
c      x(1)=piv+yline(iil1,nw,nq)-trj(nw,nq)/2.
c      x(2)=piv+yline(iil1,nw,nq)+trj(nw,nq)/2.
c      y(1)=iil1
c      y(2)=iil1
c      call pgline(2,x,y)
c      x(1)=piv+yline(iil2,nw,nq)-trj(nw,nq)/2.
c      x(2)=piv+yline(iil2,nw,nq)+trj(nw,nq)/2.
c      y(1)=iil2
c      y(2)=iil2
c      call pgline(2,x,y)
      print *,'xl,yl',xl,yl
      print *,'  3838  trj,yline(iil1),yline(iil2),nw,nq,iilm',  
     1trj(nw,nq),yline(iil1,nw,nq),yline(iil2,nw,nq),nw,nq,iilm
      call pgslw(3)
      iilm=iil2-iil1+1
      print *,'  3842   iil1,iil2,xl,yl',iil1,iil2,xl,yl
      
      do i=iil1,iil2  ! L1,L2
      iip=i-iil1+1
c      x(iip)=yline(i,nw,nq)+piv-trj(nw,nq)/2. 
c     yl/2-yline(ic)


c      x(iip)=yline(i,nw,nq)+piv-trj(nw,nq)/2. 
c                           yl/2-yline(ic)
      x(iip)=yline(i,nw,nq)-yline(ic,nw,nq)+yl/2.-trj(nw,nq)/2.
c                                    nx      
      y(iip)=i+1
      if(i.eq.iil1.or.i.eq.iil2)print *,
     1 '  3856  x(iip),y(iip) ',x(iip),y(iip)
      enddo

      
      call pgline(iilm,x,y)
c     ------
      xx=x(1)-10
      yy=iil1
      call pgtext(xx,yy,'L1')
      xx=x(iilm)-10
      yy=iilm
      call pgtext(xx,yy,'L2   n')
      
c                    
      do i=iil1,iil2            !  L3,L4
      iip=i-iil1+1
c      x(i-iil1+1)=yline(i,nw,nq)+yl/2.+trj(nw,nq)/2.   !piv+trj(nw,nq)/2.
      x(iip)=yline(i,nw,nq)-yline(ic,nw,nq)+yl/2.+trj(nw,nq)/2.
      enddo
      call pgline(iilm,x,y)
c          ------
      xx=x(1)-10
      yy=iil1-10
      call pgtext(xx,yy,'L3')
      xx=x(iilm)-10
      yy=iilm
      call pgtext(xx,yy,'L4   n+1')

c      yy=iilm*0.75      ! (iil1+iilm)/2.+20.
c      call pgtext(45.,yy,'n')     !45.,yy,'n')
c      call pgtext(87.,yy,'n+1')   !87.,yy,'n+1')
c     tracé de Ts
      write(3,*)'plot Ts'
      trj2=trj(nw,nq)/2.
      x(1)=yl/2.-trj2
      x(2)=yl/2.+trj2
      y(1)=xl/2.
      y(2)=y(1)
      call pgsls(2)
      call pgline(2,x,y)
      call pgsls(1)

      xx=yl/2.-10
      yy=xl/2.+50
      ktrj=trj(nw,nq)+0.5
      write(titre(1:5),'(a)')'Ts = '
      write(titre(6:8),'(i3)')Ktrj
      call pgslw(2)
      call pgtext(xx,yy,titre(1:8))
      
c      if(ides.ne.0)then
c      if((n2-n1+1).le.8.and.istray.eq.0)call pgadvance



c      endif

c      goto 259   !!   fin du dessin A2

c      else      !!   fin des calculs trj,yline   (if ntl)
c******************************
c                                  calcul de coef(n)
c                                  *****************
c                                                     pour ntl=2:
c     Calcul des coefficients de canal a canal par la moyenne sur le canal
      cf(n1,1) = 1.0
      cf(n1,2) = 1.0

c      if(idy.ne.0) trj(nw,nq)=trjm     
c                  ---------           
      idec = trj(nw,nq)+0.5  !!  moy dans ntl=2
c            ---------
c      if(trj(nw,nq).lt.0.)idec = trj(nw,nq)-0.5
c                     pour precaution si variation de lambda en i
c                     interpolation ulterieure

          iia=im/3
          iib=im-iia
      kpiv=jm-idec-1
      kpiv=kpiv/2
      jmar=min0(kpiv,8)   !    ????????????,

c                             calcul sur trj et trj+1
      do ib=1,2
c         print *,ib
         id= idec + ib - 1
         ic=(im+1)/2
      do n= n1+1, n2
c           --------

c      if(idy.ne.0)id=id+dyln(ic,n,nw,7)-dyln(ic,n-1,nw,7)
c                       ---------------------------------

c      jja1=1+jmar
c      jjb1=jm-id-jmar
c      jja2=1+jmar+id
c      jjb2=jm-jmar
      jja1=jc-id/2-5
      jjb1=jja1+9
      jja2=jja1+id
      jjb2=jjb1+id

      write(3,*)' pour coef: nw,nq,n,iia,iib,jja1,jja2,jjb1,jjb2 ',
     1            nw,nq,n,iia,iib,jja1,jja2,jjb1,jjb2

         som1=0

c       calfs     tab     fl
c          -1       v
c           0       v     
c           1       v     w
c           2       w     w

         if(calfs.le.0)then
c           ----------
         do j= jja1,jjb1   
            do i= iia, iib
               som1= som1 + tab(i,j,n-1)
c                           ---
            enddo
         enddo
         endif

         if(calfs.ge.1)then
c           ----------
         do j= jja1,jjb1   
            do i= iia, iib
               som1= som1 + fl(i,j,n-1)
c                           --
c                           economie
            enddo
         enddo
         endif

         som2=0

         if(calfs.le.0)then
c           ----------
         do j= jja2,jjb2
            do i= iia,iib
               som2= som2 + tab(i,j,n)
c                           ---
            enddo
         enddo
         endif

        if(calfs.ge.1)then
c          ----------
         do j= jja2,jjb2
            do i= iia,iib
               som2= som2 + fl(i,j,n)
c                           --
c                           economie
            enddo
         enddo
         endif

         cn= float(som1)/float(som2)
         cf(n,ib) = cf(n-1,ib) * cn
      enddo   !   n
      enddo   !   ib

      piv1=cf(nc,1)
      piv2=cf(nc,2)
      rap= trj(nw,nq) - idec
c                                 interpolation entre trj et trj+1
      do n=n1,n2
      coef(n)=cf(n,1)/piv1 + ((cf(n,2)/piv2-cf(n,1)/piv1) * rap)
c rel -------

      write(3,*)'n,piv1,piv2,rap,coef',n,piv1,piv2,rap,coef(n)
      enddo

c***************
      print *,'calcul de prof100'      !!!!!!!!!!!!!!!!!!!!!!!
c                                      calcul de PROF100(101,18)
c                                                ---------------  
      profmax=0.
c                             boucle n
        do n=n1,n2
           write(3,*)' n,trj(nw,nq) ',n,trj(nw,nq)

c                                 boucle l
           do l=1,ktr+1         ! 1,51  !101
                      xx=float(l-ktrc-1)
        piv=-trj(nw,nq)*xx/trk +float(jc) 
c     rel   ---------------------------
        sum=0.
c                           moyennes sur i
      ipiv1=im/3                                    !   *******
      ipiv2=im-ipiv1+1

           do i=ipiv1,ipiv2
c          do i=9,im8
c            ici on suit les variations de lambda avec i
c        if(idy.eq.0)then
          yj=yline(i,nw,nq)+piv-yline(ic,nw,nq)
c     rel     -----------------------------------
c            moy pour ntl=2
c        else
c          yj=piv+dyln(i,n,nw,7)
c         ---------------------
c        endif
c            j= 1+ (jm-1)/2  -(l-51)*trj/100
c bord canal j= 1+ (jm-1)/2  -49    *trj/100

          j=yj
          alp=yj-j
          t=tab(i,j,n)*(1.-alp)+tab(i,j+1,n)*alp
c     rel     ------------          ------------
          t=t*coef(n)
          tt=t
c                    suppression du biseau si trj > jm/2      non
c        jtest=trj(nw,nq)  
c        if(2*jtest.gt.jm)goto255

c       biseau
c       canal droit
c      if(l.gt.51.and.n.ne.n2)then
c      yjd=yj+trj(nw,nq)
c      if(idy.ne.0)yjd=yjd+dyln(i,n+1,nw,7)
c                         ----------------
c        jd=yjd
c                  if(jd.gt.jm-1)goto255
c        alpd=yjd-jd
c      td=tab(i,jd,n+1)*(1.-alpd)+tab(i,jd+1,n+1)*alpd
c      td=td*coef(n+1)
c        bis=l-51
c        biseaud=0.5*bis/50.
c      tt=t*(1.-biseaud)+td*biseaud
c      endif

c       canal gauche
c      if(l.lt.51.and.n.ne.n1)then
c      yjg=yj-trj(nw,nq)
c      if(idy.ne.0)yjg=yjg+dyln(i,n-1,nw,7)
c                         ----------------
c        jg=yjg
c                 if(jg.lt.1)goto255
c        alpg=yjg-jg
c      tg=tab(i,jg,n-1)*(1.-alpg)+tab(i,jg+1,n-1)*alpg
c      tg=tg*coef(n-1)
c        bis=51-l
c        biseaug=0.5*bis/50.
c      tt=t*(1.-biseaug)+tg*biseaug
c      endif
                   if(n.eq.n1.or.n.eq.n2.or.n.eq.(n1+n2+1)/2)then
                     if(i.eq.im/2.and.l.eq.40*(l/40))then
                     write(3,*)' l,yj,t,td,biseaud,tg,biseaug,tt ',
     1                           l,yj,t,td,biseaud,tg,biseaug,tt
                     endif
                   endif
255   sum=sum+tt
          enddo                 ! boucle i

        impiv=ipiv2-ipiv1+1
c        impiv=im16
        prof100(l,n)=sum/(coef(n)*float(impiv))
c rel   ------------                           pour chaque nw
        enddo         ! boucle l
      enddo           ! boucle n
c*****************************************************************
c      print*,' cmf: kdecal ',kdecal


      print *,'calcul de profm' !!!!!!!!!!!!!!!
c                                     calcul de PROFM(2000,2,6)
c                                      -----------------------
c             print *,'calcul profm,n1,n2',n1,n2
      do k=1,2000
        profm(k,nw,nq)=0.
      enddo

      do n=n1,n2
        do l=1,ktr+1    !   101
c          nc=xncw(nw,nq)
        k=1000+(n-nc)*ktr+(l-ktrc-1)    !             100.+l-51
c rel   ----------------------- 
        profm(k,nw,nq)=prof100(l,n)*coef(n)  !           profm(k,nw,nq)
c rel   ----------------------------------
        enddo
      enddo
      write(3,*)' prof100(ktrc,n)= ',(prof100(ktrc,n),n=n1,n2)
               kpiv=ktr-1
      write(3,*)' profm(k)= ',(profm(k,1,1),k=ktrc,2000,kpiv)
      print *,' profm(k)= ',(profm(k,1,1),k=1,2000,50)
c
c lissage, extrapolation et dessin de PROFM(2000)
c------------------------------------------------
c                                     lissage
c               1 10
      kprof1=1000+(n1-nc)*ktr-ktrc  !                100.-50
c              18 10 
      kprof2=1000+(n2-nc)*trk+trkc  !                100.+50
c      kproft=kprof2-kprof1+1
c        do k=1,2000
c        x(k)=profm(k,nw,nq)
c        enddo
      print *,'              kprof1,kprof2',kprof1,kprof2
      
c      call parafit(x,i1,i2,lissprofm,y)
c          -------
c                                 extrapol tangentes milieux canx n1 et n2
c      i1extra=i1+50
c      i2extra=i2-50


c      i1extra=i1
c      i2extra=i2
c        if(iextra.eq.1)then
c                                                          ou n1+1 et n2-1   
c        i1extra=i1+105     ! debut n1+1 
c        i2extra=i2-105     ! fin   n2-1
c        endif

c      piva=(y(i1extra+10)-y(i1extra))/10.
c      pivb=(y(i2extra)-y(i2extra-10))/10.
c      piv=0.5*(piva-pivb)
c        do i=1,i1extra
c        y(i)=y(i1extra)+(i-i1extra)*piva
c        y(i)=y(i1extra)
c        enddo
c        do i=i2extra,2000
c        y(i)=y(i2extra)+(i-i2extra)*pivb
c        y(i)=y(i2extra)
c        enddo

c         profmin=100000.
c         profmax=0.
c        do i=i1,i2
c        profmin=amin1(profmin,y(i))   !!     minimum profm pour nw
c        profmax=amax1(profmin,y(i)) 
c        if(y(i).eq.profmin)imin=i
c                          ----
c        enddo

c          cnorma=1.
c                                 normalisation si  minprof.ne.0
c                                               ou countff.ne.0
c      if(minpro.ne.0.or.contff.ne.0.)then
c      if(minpro.ne.0)then
c        cnorma=float(minpro)/profmin
c      else
c        aile=amax1(y(i1),y(i2))
c        cnorma=contff/aile
c      endif
c        write(3,*)' contff,aile,cnorma ',contff,aile,cnorma
c          do i=1,2000
c          y(i)=y(i)*cnorma
c          enddo
c      endif

c        do i=1,2000
c          profm(i,nw,nq)=y(i)
c        enddo
c                                       stockage kmin
c          kmin(nw,nq)=imin
c          if(inclin.eq.0)kmin(nw,nq)=kdecal(nw)+1000


c      write(3,*)'symetrie: imin, profmin ',imin,profmin

c                         symetrie
c      if(isym.eq.1)then 
c        ---------
c      do i=1,2000
c      ip=2*imin-i
c          ----
c      if(ip.ge.1.and.ip.le.2000)x(i)=0.5*(y(i)+y(ip))
c      enddo

c      do i=1,2000
c      profm(i,nw,nq)=x(i)
c      enddo
c      endif

c                                    ajustement ailes  (soft0403)
c      if(isym.eq.2)then
c        ---------
c        ipiv1=imin-i1extra
c        ipiv2=i2extra-imin
c                     ----
c          idelta=min0(ipiv1,ipiv2)

c        just1=imin-idelta
c        just2=imin+idelta

c        piva=profm(just1,nw,nq)
c        pivb=profm(just2,nw,nq)
c        pte=(piva-pivb)/float(just2-just1)
c257   format(' cmf:isym,nw,nq,imin,i1extra,i2extra,idelta,just1,just2 ',
c     1     9i6/' piva,pivb,pte ',2f6.0,f8.3)
c      write(3,257) isym,nw,nq,imin,i1extra,i2extra,idelta,just1,just2,
c     1           piva,pivb,pte
      
c        do i=just1,just2
c        y(i)=profm(i,nw,nq)+(i-imin)*pte
c        enddo

c          ydelta=y(just1)-profm(just1,nw,nq)
c        do i=1,just1-1
c        y(i)=profm(i,nw,nq)+ydelta
c        enddo

c          ydelta=y(just2)-profm(just2,nw,nq)
c        do i=just2+1,2000
c        y(i)=profm(i,nw,nq)+ydelta
c        enddo

c        do i=1,2000
c        profm(i,nw,nq)=y(i)
c       -----
c        enddo
c      endif

c------------------------------- 
c                                                          dessin B
c      trk=50.  !   translation entre canaux pour profm
c      trkc=26.
c      ktr=51
c      ktrc=26
      profmax=0.
      do n=n1,n2
        do l=1,ktr+1
           profmax=amax1(profmax,prof100(l,n))  
        enddo
      enddo
      
      unit=2000.
      if(profmax.lt.3000.)unit=1000.   ! 
      if(profmax.lt.1500.)unit=500.    !  2500.   500.
      if(profmax.lt.600.)unit=200.     !  600.    100.
c---
c      if(nrecad.eq.2)then
      if(ides.ne.0)then
      call pgadvance
c          ---------
      call pgvport(0.2,0.9,.2,.8)
      xa=n1-1. !2.
      xb=n2    !+1.
      dmax=profmax*1.5  !1.2
      call pgwindow(xa,xb,0.,dmax)  !dmax)
      call pgbox('bcgint',1.,0,'bcints',unit,2) !5000.,5)
      call pglabel('Channels, j-range Ts','Intensity',
     1 'B       mean profile')   !    'B      '//fna(nq,nw))

      call pgsls(2)
      call pgslw(2)

        do n=n1,n2
          do l=1,ktr  !    101
          x(l)=n-1.+(l-1)/trk        !100.
          y(l)=prof100(l,n)    !*cnorma
       enddo
        call pgline(ktr,x,y)       !101,x,y)
c            ------
      enddo
      call pgsls(1)
c
      xa=kprof1  !i1-trk  !      -100.
      xb=kprof2  !i2+trk  !        +100.
      call pgwindow(xa,xb,0.,dmax)
      call pgslw(3)

        do k=1,2000
        x(k)=k    !1+float(k-k1)/trk
        y(k)=profm(k,nw,nq)
        enddo

      call pgline(2000,x,y)
c          ------
c        call pgslw(1)
c        call pgsls(5)
c      x(1)=imin
c      x(2)=x(1)
c      y(1)=0
c      y(2)=dmax
c      call pgline(2,x,y)
c          ------
        call pgsls(1)
        call pgslw(2)
      endif    !  ides
c----
c259   continue
      endif    !  idy

260   continue      !!                                  nw
270   continue      !!                                  nq
c  On a seulement calcule profm et yline pour chaque n
c****************************************************************
c      print*,' cmf: kdecal ',kdecal
c                      moyennes des trj et yline
c                      -------------------------
c      
c                       attention le signe de trj depend de invern

c      call par1('   leadw',1,leadw)
c          ----
        nwa=nw1
        nwb=nw2

c      if(leadw.ne.0)then
c      nwa=win(leadw)
c      nwb=win(leadw)
c      endif

      trjm=0.            
c                                                         nw1=2   !17/4
c                                       moyenne trj   
         den=(nwb-nwa+1.)*(nq2-nq1+1.)
c                                         print*,' den ',den
      do  nq=nq1,nq2
      do  nw=nwa,nwb
      trjm=trjm+trj(nw,nq)/den
c rel ----
      enddo
      enddo      

      do nq=nq1,nq2
      do nw=nw1,nw2
        trj(nw,nq)=trjm   !  *1.08
      enddo
      enddo
c      write(3,*)' trjm= ',trjm
c ecarts
c           -(z(2)-z(1))/2      0        (z(2)-z(1))/2
c                         -tr4      +tr4
c                  dyzm(1)               dyzm(2)
c                  dyam(1)               dyam(2)
c                  dybm(1)               dybm(2)

         denq=nq2-nq1+1

      if(nw1.ne.nw2)then
        pivz1=0.
        pivz2=0.
        piva1=0.
        piva2=0.
        pivb1=0.
        pivb2=0.
      do nq=nq1,nq2
          tr4=(0.25+(ncw(1,nq)-ncw(2,nq))*0.5)*trjm*milsec
            write(3,*)' nq,tr4 ',nq,tr4
          pivdz1=(-yz(1,nq)+yz(2,nq))/2.+tr4
          pivdz2=(+yz(1,nq)-yz(2,nq))/2.-tr4
        pivz1=pivz1+pivdz1
        pivz2=pivz2+pivdz2
        piva1=piva1+dya(1,nq)+pivdz1
        piva2=piva2+dya(2,nq)+pivdz2
        pivb1=pivb1+dyb(1,nq)+pivdz1
        pivb2=pivb2+dyb(2,nq)+pivdz2
      enddo
        dyzm(1)=pivz1/denq
        dyzm(2)=pivz2/denq
        dyam(1)=piva1/denq
        dyam(2)=piva2/denq
        dybm(1)=pivb1/denq
        dybm(2)=pivb2/denq

      else
        dyzm(nw1)=0
        piva=0
        pivb=0
      do nq=nq1,nq2
        piva=piva+dya(nw1,nq)
        pivb=pivb+dyb(nw1,nq)
      enddo
        dyam(nw1)=piva/denq
        dyam(nw1)=pivb/denq

      endif
      write(3,*)' ncw (2,6)    ',ncw
      write(3,*)' dyzm(nw=1,2) ',dyzm
      write(3,*)' dyam         ',dyam
      write(3,*)' dybm         ',dybm

      if(nrecad.eq.2.and.ntl.eq.2)then
      write(7,*)' ncw (2,6)    ',ncw
      write(7,*)' dyzm(nw=1,2) ',dyzm
      write(7,*)' dyam         ',dyam
      write(7,*)' dybm         ',dybm
      endif
c                             moyenne ylines (seules servent
c                                             pente et courbure)
      do i=1,im
            ylinem(i)=0.
        do nq=nq1,nq2
        do nw=nwa,nwb  
          ylinem(i)=ylinem(i)+yline(i,nw,nq)/den
c rel     ---------
        enddo
        enddo
      enddo
      write(3,*)' ylinem(ic) = ',ylinem(ic)

        do nq=nq1,nq2
        do nw=nw1,nw2
             piv=yline(ic,nw,nq)
          do i=1,im
          yline(i,nw,nq)=piv+ylinem(i)-ylinem(ic)
          enddo
        enddo
        enddo

c      write(3,*)' yline(ic) pour nq ',yline(ic,1,1),yline(ic,1,2)

      if(ntl.eq.1)goto280
c--------------------------
      den=(nq2-nq1+1)*(nwb-nwa+1)
         print *,'den=',den




c                  moyenne xnc (canaux finaux)
         xnc=0.
      do nq=nq1,nq2
      do nw=nwa,nwb
      piv=(xncw(nw,nq)-1.)*(nw2-nw1+1)+1.+nw2-nw
c                          intercal
      xnc=xnc+piv/den
c rel ---                    
      enddo
      enddo
        write(3,*)' moy xnc=',xnc
c======================================================================
c                    moyenne  profmm de toutes polars et fenetres
c                             ------
c  le point k=1000 de profmm correspond a la longueur d'onde au point 
c  ic  du centre du canal xnc, canal du centre pour nw1 
c  --     ------          ---                       --- 

c autre i                                          deltak= ylinem(i)*100/trj  
c         autre j                                          -(j-jc)*100/trj
c                   autre n                                -(n-xnc)*100.
c                                       autre  nw          +kdecal(nw)

      isaut=1
      if(isaut.eq.1)goto 271
      do i=1,2000
      profmm(i)=0.
      enddo

         mink=10000
         maxk=0

c      write(3,273)

      do k=1,2000   !   k
c        piv=1.
         piv=0.
        dden=0.
      do nq=nq1,nq2
      do nw=nwa,nwb

          kp=k+kdecal(nw)
crel      ---------------
c                        kdecal = decalage theorique en l
c                                             moyenne geom valable pour 2!!!!
          if(kp.ge.i1.and.kp.le.i2)then
            mink=min0(mink,k)
            maxk=max0(maxk,k)
          endif

273   format('    k   kp   nq   nw mink maxk profm')
274   format(6i5,f6.0)      
c      write(3,274) k,kp,nq,nw,mink,maxk,profm(kp,nw,nq)

          if(kp.ge.1.and.kp.le.2000)then
c              piv=piv*profm(kp,nw,nq)
             piv=piv+profm(kp,nw,nq)
            dden=dden+1.
          endif            

      enddo
      enddo
          if(dden.eq.0)goto 275
          if(dden.eq.1.)then
             profmm(k)=piv
          else
c               profmm(k)=piv**(1./dden)
              profmm(k)=piv/dden
          endif
 275  continue
      enddo   !   k
c                                                      nw1=1  !17/4
c deconvolution
      call par1('  deconv',1,kdeconv)
      do k=1+kdeconv,2000-kdeconv
      xk(k)=0.5*(profmm(k-kdeconv)+profmm(k+kdeconv))-
     1           profmm(k)
      enddo

      do k=1+kdeconv,2000-kdeconv
      profmm(k)=profmm(k)-xk(k)
      enddo

c                        minimum et nouvelle normalisation
      profmin=100000.
        do i=700,1300
        profmin=amin1(profmm(i),profmin)
        if(profmm(i).eq.profmin)imin=i
c          ------               ----
        enddo

      write(3,*)' profmin,imin ',profmin,imin

      xminpro=profmin

      if(minprof.ne.0)then
      xminpro=minprof
285          piv=xminpro/profmin
          do i=1,2000
          profmm(i)=profmm(i)*piv
            if(profmm(i).gt.32000)then
            xminpro=xminpro/2
              print *,' newxminpro=',xminpro 
            goto285
            endif
          enddo
      endif

      write(3,*)'symetrie: imin, profmin ',imin,profmin

c                         symetrie
c      if(isym.eq.1)then 
c        ---------
c      do i=1,2000
c      ip=2*imin-i
c      if(ip.le.2000)x(i)=0.5*(profmm(i)+profmm(ip))
c      enddo

c      do i=1,2000
c      profmm(i)=x(i)
c      enddo
c      endif

c                                    ajustement ailes  (soft0403)
c      if(isym.eq.2)then
c        ---------
c        piva=profmm(minkp)
c        pivb=profmm(maxkp)
c        pte=(pivb-piva)/float(maxkp-minkp)

c        do i=1,2000
c        y(i)=profmm(i)+(i-imin)*pte
c        enddo

c        do i=1,2000
c        profmm(i)=y(i)
c        enddo
c      endif
 271   continue   !                                 saut
      do k=1,2000
      profmm(k)=profm(k,1,1)
      enddo
c=========================================================
        write(3,*)' canal median  xnc=',xnc
      xnc=xnc-(nw2-nw1+1)*(ylinem(ic)-jc)/trjm
c     ---
        write(3,*)' apres correction ylinem(ic) xnc=',xnc
      nc100=xnc*100.      
c rel -----          ne sert que pour preciser le centre raie dans cmd
c       ce centre raie est pris en j=jc
c       il est legerement different du minimum de profmm
c                                                 (moyenne sur j) 

      if(idy.ne.0)goto294

      jt1000=trjm*milsec     !!!  pour sortie scan.lis 
c                                 --------------------
       write(3,*)' y(ic),yyc,milsec ',ylinem(ic),yyc,milsec
      jz1000=(ylinem(ic)-yyc)*milsec
      ja1000=(ylinem(1)-ylinem(ic))*milsec-jdb  ! correction jdb
      jb1000=(ylinem(im)-ylinem(ic))*milsec+jdb
292   format(/' cmf: nc100=',i6,'  mean values (unit = arcsec/1000)'/
     1 ' jt1000 =',i6,
     2 ' ja1000 =',i6,' jb1000 =',i6,' jz1000 =',i6/) 
      write(7,292)nc100,jt1000,ja1000,jb1000,jz1000

c                 ylinem est defini pour nc100 final

      write(3,*)'ntl,jdb ',ntl,jdb

      
      write(3,*)' cmf: jt1000,jz1000,ja1000,jb1000,nc100 ',
     1                 jt1000,jz1000,ja1000,jb1000,nc100
c                                  pour sortie FL
c     --------------      
      jt100=100.*trjm
      jz100=100.*ylinem(ic)
      ja100=100.*ylinem(1)
      jb100=100.*ylinem(im)

      write(3,*)' cmf: jt100,jz100,ja100,jb100,nc100 ',
     1                 jt100,jz100,ja100,jb100,nc100
294   continue
c------------------------
c                              Dessin C               non
c     if(nrecad.eq.2)then
      write(3,*)'ides,nw1,nw2',ides,nw1,nw2
      
      if(ides.ne.0)then
        if(nw1.ne.nw2)then
      call pgadvance
      call pgvport(0.3,0.7,0.1,0.7)
      call pgwindow(-150.,+150.,700.,1300.)
        call pgsch(2.)
      call pgbox('bcnts',100.,4,'bcnts',200.,4)
      call pglabel('kdecal','kmin',' C0    Entrelac')

         call pgsch(4.)
      do nq=nq1,nq2
      do nw=nw1,nw2
      xp=kdecal(nw)
      yp=kmin(nw,nq)
      write(3,*)' nq,nw, kdecal,kmin ',nq,nw,xp,y
      call pgpoint(1,xp,yp,2)
c          -------                      Graphique Entrelac
      enddo
      enddo
        call pgsch(2.)
c        print*,' cmf: apres dessin entrelac'

c                                        bissectrice
        xgr=0.
        ygr=0.
      do nq=nq1,nq2
      do nw=nw1,nw2
      xgr=xgr+kdecal(nw)/den
      ygr=ygr+kmin(nw,nq)/den
      enddo
      enddo

        call pgsls(2)
      do ip=1,2000
         i=ip-1000
      x(ip)=i
      y(ip)=ygr+i-xgr
      enddo
c        write(3,*)' Entrelac: x,y ',x(1000),y(1000)

        call pgline(2000,x,y)
c            ------
        call pgsls(1)
      endif
      endif
c-------------------------
c                              Dessin D
      isaut=1
      if(isaut.eq.1)goto 265
        kdelta=0
        if(nw1.ne.nw2)kdelta=kdecal(nw2)-kdecal(nw1)

c      xk1=1000+(n1-nc)*trk-kdelta-(jm-jc)*trk/trjm
c      xk2=1000+(n2-nc)*trk       +(jc-1) *trk/trjm
c      k1=xk1
c      k2=xk2
      xk1=kprof1
      xk2=kprof2
c      if(nrecad.eq.2)then
c        if(ides.ne.0)then
c      call comparatlas(profmm,imin,k1,k2)
c          -----------
c      endif

           pmax=0.
        do k=kprof1,kprof2
        piv=profmm(k)
        pmax=amax1(pmax,piv)
        enddo
          ppmax=1.2*pmax
c---
c      if(nrecad.eq.2)then
      if(ides.ne.0)then
      write(3,*)' Dessin D: mink=',mink,' maxk=',maxk,
     1' ppmax',ppmax

        call pgadvance

      call pgvport(.1,.9,.1,.7)
        do nt=1,58
        write(titre(nt:nt),'(a)')' '
        enddo
      write(titre(10:29),'(a)')'min. aver. profile ='
      write(titre(30:36),'(f7.0)')xminpro
      call pgsch(2.)
      call pglabel(' ',' ',titre)

      call pgvport(.1,.9,.1,.8)
        write(titre(1:11),'(a11)')' D0 jt1000='
        write(titre(12:16),'(i5)')jt1000
        write(titre(17:25),'(a9)')'  ja1000='
        write(titre(26:30),'(i5)')ja1000
        write(titre(31:39),'(a9)')'  jb1000='
        write(titre(40:44),'(i5)')jb1000
        write(titre(45:53),'(a9)')'   nc100='
        write(titre(54:58),'(i5)')nc100
      call pgsch(1.5)
      call pglabel(' ',' ',titre)
      call pgsch(1.5)

      call pgvport(.1,.9,.1,.7)
      call pgwindow(xk1,xk2,0.,ppmax)
      call pgbox('bcnts',200.,4,'bcnts',0.,0)

c                                                   courbes profm
      do nq=nq1,nq2
      do nw=nw1,nw2
       call pgsls(1)
       call pgslw(1)
c        kp1=1000+(n1-nc)*100-50
c        kp2=1000+(n2-nc)*100+50
c        kdel=kdecal(nw)

      write(3,*)' Dessin D: nq,nw,kp1,kp2,kdel ',nq,nw,kp1,kp2,kdel
      write(3,*)' profm ',(profm(kp,nw,nq),kp=kp1,kp2,50)
c            kxx=0
c        do k=k1,k2                        ! m2403
c            kxx=kxx   !+1                 !
c            x(k)=k   !-kdel               !
c            y(k)=profm(k,nw,nq)           !
c        enddo                             !
        
c      call pgline(k,x,y)                  
c          ------
c      call pgsls(nw)                      !
c        y(1)=0                            !
c        y(2)=ppmax                        !
c      do n=n1,n2                          !
c        x(1)=k1+trk+trk*(n-n1)-kdecal(nw) !
c        x(2)=x(1)                         !
c        call pgline(2,x,y)                !
c            ------
c      enddo                               ! m2403
      call pgsls(1)

      enddo
      enddo
c                                                  courbe moyenne profmm
        call pgsls(1)
        call pgslw(2)         
      do k=kprof1,kprof2
      x(k)=k
      y(k)=profmm(k)
      enddo

      call pgline(kproft,x,y)
c          ------
c suppression raies genantes pour 6302
      call par1('     kl1',1,kl1)
      call par1('     kl2',1,kl2)
      if(kl1.ne.0)then
        do k=1,kl1-1
        profmm(k)=profmm(kl1)
        enddo
      endif
      if(kl2.ne.0)then
        do k=kl2+1,2000
        profmm(k)=profmm(kl2)
        enddo
      endif
c                                    blends
      call par1('   blend',1,kblend)
      if(kblend.ne.0)then
      if(kl1*kl2.ne.0)then
        profmin=32000.
        do k=kl1,kl2
        profmin=amin1(profmin,profmm(k))
        if(profmm(k).eq.profmin)kminm=k
        enddo
      write(7,*)' profmin,kminm ',profmin,kminm
      if(kblend.eq.1)then
           kl3=2*kminm-kl1
           kl3=min0(kl3,1999)
        do k=kminm+1,kl3
         kp=2*kminm-k
        profmm(k)=profmm(kp)
        enddo
        do k=kl3+1,2000
        profmm(k)=profmm(kl3)
        enddo
      else
           kl3=2*kminm-kl2
           kl3=max0(2,kl3)
        do k=kl3,kminm-1
         kp=2*kminm-k
        profmm(k)=profmm(kp)
        enddo
        do k=1,kl3-1
        profmm(k)=profmm(kl3)
        enddo
      endif
      endif
      endif
c                        ---
      do k=1,2000
      x(k)=k
      y(k)=profmm(k)
      enddo
        call pgsls(2)
      call pgline(2000,x,y)
c          ------
        call pgsls(1)
      
        call pgslw(1)

      nqw=(nq2-nq1+1)*(nw2-nw1+1)+2
        if(nw1.eq.nw2)nqw=nqw-1
      nqwr=nqw-iplotf*(nqw/iplotf)
      nqadv=iplotf-nqwr

c        if(nqadv.ne.0.and.nqwr.ne.0)then
c          do nnnn=1,nqadv
c          call pgadvance
c          enddo
c        endif

      endif
 265  continue                  ! saut dessin D
c---
280   continue      !!   fin boucle ntl
            print *,'sortie boucle 280'
c******************               Boucle nw,nq pour creation fichiers F
c                                 -------------------------------------

        if(nrecad.eq.2)
     1  print *,' creation fichier F: minimum of average profile =',
     2   xminpro
        xnormf=10000.
        newminpro=xminpro

290   continue                      ! comes back if xnormf must be decreased
      do600 nq=nq1,nq2
      do500 nw=nw1,nw2
        print *,' cmf1:nrecad=',nrecad,' nq=',nq,' nw=',nw

c                                              calcul de FL(im,jm,nm)
c               Fl est calcule pour
c               reproduire le profil moyen profmm
c               quand la longueur d'onde du point i,j,n
c               est comptee par reference a ic,jc,nc

c     print *,' cmf: trjm,vna(nq,nw),kdecal(nw)',trjm,vna(nq,nw),kdecal(nw)

      iuc=26
      if(calfs.ne.2)then
      call openold22(vna(nq,nw),sundec,26)  !  flat v 
c          ---------
          print *,'      vna',vna(nq,nw)   !   oui 2013
      else
      call openold22(wna(nq,nw),sundec,26)  !  cont fs w
c          ---------
      endif
      rewind(iuc)
      read(iuc)kz

c------------------
      print *,'kdecal,ic,jc,xminpro,profmin',
     1     kdecal(1),ic,jc,xminpro,profmin
      write(7,*)'kdecal,ic,jc,xminpro,profmin',
     1     kdecal(1),ic,jc,xminpro,profmin    
c==================
      do400  n=1,nm 
          print *,'n',n
        do j=1,jm
        read(iuc)(tab(i,j,n),i=1,im)    !   y
c                 ---
        enddo

        yk=1000+(n-nc)*trk-kdecal(nw)   !  100 à 1800
c rel   --       -          --------- 
c==============================================

       do200 i=1,im                  ! boucle i
             if(trjm.eq.0.)then
             print *,' cmf1: trjm=0'
             stop
             endif
c        if(idy.eq.0)then
          yyk=yk+(ylinem(i)-ylinem(ic))*trk/trjm
c rel     ---     --------  ---------
c        else
c          yyk=yk+dyln(i,n,nw,7)*100./trjm
c         ---    --------------
c        endif 
            if(yyk.lt.1.)yyk=1.
            if(yyk.gt.1999.)yyk=1999.
c                                              boucle j
          do190 j=1,jm
          xj=j-jc  
          yyyk=yyk-xj*trk/trjm
c                   -
          k=yyyk  +0.5
      if(k.ge.kprof1.and.k.le.kprof2)then
c          alp=yyyk-k
          profj(j)=(profmm(k))   !*(1.-alp)+profmm(k+1)*alp)*
c         -----     ---------
c     1           *  xminpro/profmin

                        
c       write(3,*)'n,i,profj',n,i,(profj(j),j=1,jm,10)
c            if(istray.ne.0)then
c            call calstray(tab,iitab,jjtab,lltab,i,jm,n,nm,profj,
c                --------  in
c     1                  xminpro,xnormf,nw,nq,istray,fl,stray)
c                       in                   in     out
c            else

       if(profj(j).eq.0.)write(7,*)'warning: profj(j)=0'
              fl(i,j,n)=xnormf*tab(i,j,n)/profj(j) + 0.5 
c             --------- --------------------------
c      if(profj(j).eq.0.)then
c       write(3,*)'profj=0 cmf1  2003'
c       stop
c      endif

c            endif
       else
             fl(i,j,n)=0
      endif
 190      continue     !                           j

c si fl=0, fl=valeur point non nul proche
        do j=2,jm-1! (on est dans boucle i,n)
        if(fl(i,j,n).ne.0.)then
          jplus=j    ! point fl non nul
          goto262
        endif
        enddo
 262    continue
c           if(jplus.ne.2)      print *,'i,n,jplus',i,n,jplus
        do j=jplus,jm
         if(fl(i,j,n).eq.0.)fl(i,j,n)=fl(i,j-1,n)
         enddo
            
        do j=1,jplus
         jp=jplus-j+1
         if(fl(i,jp,n).eq.0.)fl(i,jp,n)=fl(i,jp+1,n)
        enddo
                  

          if(i.eq.400)
     1    print *,'n,tab(i,40,n),profj(40),fl(1-40-jm)',
     2        n,tab(i,40,n),profj(40),'  ',
     3        fl(i,1,n),fl(i,40,n),fl(i,jm,n)

c         if(piv.gt.32000.)then
c            xnormf=xnormf/2.
c         fl(i,j,n)=32000.      !                              2013

c          print *,  ' step cmf: fl exceeds 32000:'
c          print *,  ' newxnormf=',xnormf,'  new minpro=',newminpro
c          write(7,*)' step cmf: fl exceeds 32000:'
c          write(7,*)' newxnormf=',xnormf,'  new minpro=',newminpro
c          write(7,*)' minimum of new average profile =',xminpro

c            if(newminpro.gt.20000.or.xminpro.gt.20000.)then
c              print *,  ' new minpro too large'
c              write(7,*)' new minpro too large'
c            stop
c            endif


c                   print *,' nrecad,nq,nw,n,i ',nrecad,nq,nw,n,i
c                   goto290
c       enddo    

200   continue                  ! fin boucle i
c===================================================
c-----                    lissage i hors standard
c       if(smoothi.ne.0)then
c       do j=1,jm
c         do i=1,im
c         rec(i)=fl(i,j,n)
c         enddo

c       call zero3(9,im2,im2p,im8,0,0,0,rec,1,im,0,0,
c            -----
c    1             ar,br,cr,item,nrjm,nrej,y1,y2,ifin)
c           if(smoothi.eq.1)then
c           y1=0.5*(y1+y2)
c           y2=y1
c           endif
c         do i=1,im
c         piv=(i-1)/float(im-1)
c         rec(i)=y1*(1.-piv)+y2*piv+0.5
c         enddo

c         do i=1,im
c         fl(i,j,n)=rec(i)
c rel     ---------
c         enddo
c       enddo
c       endif

c-----                     lissage j hors standard
c     do300 i=1,im
c       if(smoothj.eq.0)goto300
c         do j=1,jm
c         rec(j)=fl(i,j,n)
c         ---
c         enddo

c       jc1=jc+1
c       jm8=jm-8
c       call zero3(9,jc,jc1,jm8,0,0,0,rec,1,jm,0,0,
c            -----                    ---
c    1             ar,br,cr,item,nrjm,nrej,y1,y2,ifin)
c       if(smoothj.eq.1)then
c       y1=0.5*(y1+y2)
c       y2=y1
c       endif
c
c           do j=1,jm
c           piv=(j-1)/float(jm-1)
c           fl(i,j,n)=y1*(1.-piv)+y2*piv
c           ---------
c           enddo

c00     continue
c-----             
 400  enddo                     !n
c==============
c     if(nrecad.eq.2)then

c     if(istray.ne.0)then
c     if(ides.ne.0)call pgadvance
c01    format(' i, stray(i,n)')
c02    format(i6, 18f8.0)
c     write(3,401)
c       do i=1,im,20
c       write(3,402)i,(stray(i,n),n=1,nm)
c       enddo

c     xa=0.
c     xb=nm
c     xl=im-1
c     yl=jm-1
c       call pgsch(2.)
c     call pgvport(0.1,0.9,0.1,0.7)
c       if(istray.eq.1)then
c         call pgwindow(xa,xb,-5.,5.)
c         call pgbox('bcgint',1.,0,'bcints',1.,0)
c       else
c         call pgwindow(xa,xb,-0.1,+0.1)
c         call pgbox('bcgint',1.,0,'bcints',0.1,10)
c       endif
c     call pgbox('bcgint',1.,0,'bcints',1.,0)
c     call pglabel('i,<j>,n','stray(i,n)/xminpro ','S2 '//vna(nq,nw))
c       do n=1,nm
c         do i=1,im
c         x(i)=n-1+(i-1)/xl
c         y(i)=stray(i,n)/xminpro
c         enddo
c       call pgline(im,x,y)
c            ------           
c       enddo
c     endif

c     endif

c                                    fin  boucle n
      close(unit=26)
c--------
c     if(nrecad.eq.1)call sminmax(fl,iitab,jjtab,nnfl,im,jm,nm,
c                         -------
c    1                            xmin,xmax,nrecad)
c       
c                           dessin de FL(i,jm2,n) 
c                                     Dessin E     
c      kflmoy=0
c      denfl=float(jm*nm)
c      do n=1,nm
c      do j=1,jm
c        kpiv=0.
c      do i=1,im
c      kpiv=fl(i,j,n)+kpiv
c      enddo
c        kflmoy=kflmoy+float(kpiv)/float(im)
c      enddo
c      enddo
c        flmoy=float(kflmoy)/denfl
c               print *,'flmoy=',flmoy

c      flmoy=0
c      denfl=float(nm)  !jm*nm)
c      do n=1,nm
c      j=jc   !do j=1,jm
c        piv=0.
c      do i=1,im
c      piv=fl(i,j,n)+piv
c      enddo
c        flmoy=flmoy+piv/float(im)
c      enddo
c      enddo
c        flmoy=flmoy/denfl
c               print *,'flmoy=',flmoy
      xa=n1-1
      xb=n2
      xl=im-1
      yl=jm-1
c-------
c     if(nrecad.eq.2)then
c     Dessin C
            idesc=0
c     if(ides.ne.0)then
      if(ides*idesc.ne.0)then
      call pgadvance
      call pgvport(0.1,0.9,0.3,0.7)   !0.1,0.7)
      call pgwindow(xa,xb,0.,2.) !          2013
c      call pgbox('bcgint',1.,0,'bcints',unit,2) !5000.,5)
c      call pglabel(' ',' ','B       mean profile')  !    'B      '//fna(nq,nw))
      call pgbox('bcgint',1.,0,'bcints',0.5,5) !0.5,5)    2013
      call pglabel('n,i',' ','C   F(i,jm/2,n)')
c                                       E
             ipiv=im/2        !                          2013
             jpiv=jm/2
      do n=1,nm
      print *,'n=',n,'  fl=',fl(ipiv,jpiv,n)
      enddo

      write(3,*)'Dessin C: xnormf,n1,n2',xnormf,n1,n2
        do n=n1,n2
          do i=1,im
          x(i)=n-n1+(i-1)/xl
          y(i)=fl(i,jm2,n)!  /flmoy
          y(i)=y(i)/xnormf
          enddo
        call pgline(im,x,y)
c            ------
        write(3,*)'im,jm,fl(1-10,jm2,n) ',im,jm,(fl(i,jm2,n),i=1,10)
        
c          do i=1,im
c          y(i)=0.001*fl(i,jm2,n)
c          enddo
c        call pgline(im,x,y)
c            ------
        enddo
      endif
c----------
c      dessin_court=1
c      if(dessin_court.eq.1)goto970

c                       dessin agrandi pour ideb (points 1-largpix)
c      do ncoupe=1,2
c       n=1
c       if(ncoupe.eq.2)n=nm

c      tit='                         '

c      if(nrecad.eq.2)then
c      miny=32000
c        do i=18,largmin
c        ifl=fl(i,jm2,n)
c        miny=min0(miny,ifl)
c        if(miny.eq.ifl)minpix=i-1
c        enddo
c      write(tit(1:7),'(a)')'minpix='
c      write(tit(8:11),'(i4)')minpix

c        if(mgrim.ne.0)then
c        ieps=itana-itgri/2
c        ieps=iabs(ieps)
c        ieps=ieps/2
c         ieps=0

c          if(ideb.eq.0)then
c          if(ideb.eq.0.and.itana.le.itgri/2)then
c            write(tit(12:19),'(a7)')'  idebf='
c            kpiv=milsec*minpix+2000             +ieps
c            idebf(nw,nq)=kpiv
c            write(tit(20:25),'(i6)')kpiv
c          endif

c          if(ideb.eq.-1.and.itana.gt.itgri/2)then
c            write(tit(12:19),'(a7)')'  idebf='
c            kpiv=milsec*minpix+2000             -ieps+itgri/2
c            idebf(nw,nq)=kpiv
c            write(tit(20:25),'(i6)')kpiv
c          endif
c        endif

c      endif

c      call pgadvance
c      call pgvport(0.1,0.9,0.1,0.7)
c       piv=float(largsec)
c      call pgwindow(0.,piv,0.,2.)
c      call pgbox('bcints',5.,5,'bcints',0.5,5)
c         if(ncoupe.eq.1)then
c         call pglabel('F(i-1,jm/2,1)',' ',tit)
c         else
c         call pglabel('F(i-1,jm/2,nm)',' ',tit)
c         endif

c          do i=1,largpix
c          x(i)=(i-1)*float(milsec)/1000.
c          y(i)=0.0001*fl(i,jm2,n)
c          enddo
c        call pgline(largpix,x,y)
c            ------
c        xpiv=x(minpix+1)
c        ypiv=y(minpix+1)
c        call pgpoint(1,xpiv,ypiv,23)
c            -------
c          do i=1,largpix
c          y(i)=0.001*fl(i,jm2,1)
c          enddo
c        call pgline(largpix,x,y)
c            ------
c        ypiv=y(minpix+1)
c        call pgpoint(1,xpiv,ypiv,23)
c            -------
c      enddo
970   continue
c                           Dessin F      coupe j
c      if(inclin.ne.3)then

c      call pgadvance
c      call pgvport(0.1,0.9,0.1,0.7) 
c      call pgwindow(xa,xb,0.,ffmax)
c      call pgbox('bcgint',1.,0,'bcints',0.2,2)
c      call pglabel(' j,n ',' ','F')
c      call pglabel('F(im/2+itpix4,j-1,n)',' ','F')

c      im2=im/2
c      im2t4=im2+itpix4
c        do n=n1,n2
c          do j=1,jm
c          x(j)=n-n1+(j-1)/yl
c          y(j)=0.0001*fl(im2t4,j,n)
c          enddo
c             print *,(y(j),j=1,jm)
c        call pgline(jm,x,y)
c            ------
c                                     modif 30 mars 2000
c          do j=1,jm
c          y(j)=0.001*fl(im2t4,j,n)
c          enddo
c             print *,(y(j),j=1,jm)
c        call pgline(jm,x,y)
c            ------

c        enddo
c      else       ! inclin=3

c      if(nrecad.eq.2)then
c      if(ides.ne.0)then
c            ibdm=3
      idesc=0
      if(idesc.eq.0)then
        call coupe_bandes(fl,iitab,jjtab,nnfl,im,jm,nm,x,y,flmoy,
     1                 nw,nq,sigbd,inclin,ides,jy1,jy2,nrecad)
c     -------------
      endif
c      endif
c      endif
C*******************
        if(nrecad.eq.2)then
c                                       ecriture de FL(im,jm,nnfl)  
c                                       --------------------------
      call system('rm '//fna(nq,nw))
      iuf=27
      call opennew22(fna(nq,nw),sundec,iuf)
c     -------
      do i=1,512
         kz(i)=0
      enddo
      
c     print *,'dans cmf.f avant write(iuf) day,time,lbda',day,time,lbda!!!!!
      write(3,*)'6,im,jm,nm,2000,1,1,10,day,time,iline,lbda,nc100,',
c                                                          canx finaux
     1        ' jt100,   ja100,jb100,  jz100,      nc100,kminm,',
c              initial       
     2         '(kz(i),i=20,30)' !!!!!!!!!!!!
c
      write(3,*)6,im,jm,nm,2000,1,1,10,day,time,iline,lbda,nc100,
c                                                          canx finaux
     1         jt100,   ja100,jb100,  jz100,      nc100,kminm,
c              initial       
     2         (kz(i),i=20,30)  !512) !!!!!!!!!!!!

 
      write(iuf)6,im,jm,nm,2000,1,1,10,day,time,iline,lbda,nc100,
c                                                          canx finaux
     1         jt100,   ja100,jb100,  jz100,      nc100,kminm,
c              initial       
     2         (kz(i),i=20,512) !!!!!!!!!!!!
c
c                     ja,jz,jb = moyennes des positions de raie
c                                dans les canaux nc(5 pour 9 canaux,
c                                                     4 pour 8 canaux)
c                                de toutes les fenetres

      write(3,*)' output cmf: jt100,ja100,jb100,jz100 ',
     1                        jt100,ja100,jb100,jz100

      call par1('     iff',1,iff)
      if(iff.eq.-1)then
        do i=1,im
        rec(i)=10000
        enddo
      endif

      do n=1,nm
      do j=1,jm
        if(iff.eq.-1)then
          write(iuf)(rec(i),i=1,im)
        else
          write(iuf)(fl(i,j,n),i=1,im) !!!!!!!!!!!!!!!!!!
c                    --
        endif
      enddo
      enddo
c
      if(istray.ne.0)then
      do n=1,nm
          write(iuf)(stray(i,n),i=1,im) !!!!!!!!!!!!!!!!!!
c                    -----
      enddo
      endif

        do i=1,2000
        pm(i)=profmm(i)+0.5
        if(pm(i).gt.32000.or.pm(i).lt.0)pm(i)=32000   !!!!!!!!!!!!!!!
        enddo
      write(iuf)pm !!!!!!!!!!!!!!!!!!!
980   format(10i8)
c      print 980,(pm(i),i=1,2000,10)
c      write(3,980)(pm(i),i=1,2000,10)
c
      close(unit=27)
      endif   !!  fin ecriture
      
c------               Moyenne des canaux en im2+itpix4
c     Calcul de yrecad(nw,nq)
c      fac=1./(flmoy*float(n2-n1+1))
      
c      do j=1,jm
c               y(j)=0.
c      do n=n1,n2
c               y(j)=y(j)+fac*fl(im2t4,j,n) 
c      write(3,*)'5206 im2t4,fl ',im2t4,fl(im2t4,j,n)
c      enddo
c      enddo
c                      
c      call recad(y,jm,reca,xrd,yrd,milrecad)
c          -----      out          in

c      if(ides.ne.0)then
c      call pgadvance
cc          ---------
c      call desreca(y,jm,reca,xrd,yrd,flmoy,jjtab)   ! dessin G
cc          -------      in

c      if(istray.eq.0)call pgadvance
c      endif
      
c      if(nrecad.eq.1)yrecad(nw,nq)=reca
 
500   continue  !!  fin boucle nw
600   continue  !!  fin boucle nq
      return
      end      
c***************************************************
      subroutine recad(y,jm,reca,xr,yr,milrecad)
c                -----
c                inutilise
      dimension y(1)

      if(milrecad.eq.0)goto10
      write(3,*)' recad: y=',(y(j),j=1,jm)

      seuil=float(milrecad)/1000.
c       seuil=0.5*(y(1)+y(jm))
        som=y(1)+y(jm)
        seuil=seuil*som
      reca=0
      jm2=jm/2
      jm4=jm/4
c        seuil=0.5*y(jm2)
c         write(3,*)' seuil,y ',seuil, y(1),y(jm2),y(jm)

c                     gauche
      do j=2,jm4
c        y1=y(j-1)/y(jm2)
c        y2=y(j)  /y(jm2)
         y1=y(j-1)
         y2=y(j)
      if(y1.le.seuil.and.y2.gt.seuil)then
        alp=(seuil-y1)/(y2-y1)
        reca=j-2+alp
        xr=j-1+alp
c        yr=y(jm2)*seuil
        yr=seuil
        return
      endif
      enddo
c                     droite
      do jp=2,jm4
      j=jm-jp+1
c        y1=y(j)/y(jm2)
c        y2=y(j+1)/y(jm2)
         y1=y(j)
         y2=y(j+1)
      if(y1.gt.seuil.and.y2.le.seuil)then
        alp=(seuil-y2)/(y1-y2)
        reca=j+1-jm-alp
        xr=j+1-alp
c        yr=y(jm2)*seuil
        yr=seuil
        return
      endif
      enddo

10    reca=0
      yr=0.
      return
      end
c**********************************************
      subroutine desreca(y,jm,reca,xr,yr,flmoy,jjtab)
c                -------                  inut       Dessin G
      dimension x(2000),y(1)
      character*11 titre

      write(3,*)' desreca: reca,xr,yr,ffmax ',reca,xr,yr,ffmax

        call pgsch(2.)
      call pgvport(0.1,0.9,0.1,0.7)
        xa=1
        xb=jm
      call pgwindow(xa,xb,0.,2.)
      call pgbox('bcints',50.,5,'bcints',0.5,5)
        titre=' Ecart=    '
        write(titre(8:11),'(f4.1)')reca
        call pgsch(2.)
      call pglabel(' ', ' ','G Moyenne Canaux '//titre)

      do j=1,jm
      x(j)=j
      enddo

      call pgline(jm,x,y)
c                                xr correspond a yrecad (indices j)
c                                yr est l'intensite correspondante
      if(yr.ne.0.)call pgpoint(1,xr,yr,23)
      call pgsch(2.)

      return
      end
c**********************************************
      subroutine sminmax(fl,iitab,jjtab,nnfl,im,jm,nm,xmin,xmax,nrecad)
c                -------
      integer*2 fl(iitab,jjtab,nnfl)
      integer*4 xmin(10),xmax(10),ymin(10),ymax(10),pixdiff

      call par1('  inveri',1,inveri)
      call par1('  milsec',1,milsec)

      seuil=0.10
      pixdiff=15

      smin=1.+seuil
      smax=1.-seuil
      do n=1,10
      xmin(n)=0
      ymin(n)=0
      xmax(n)=0
      ymax(n)=0
      enddo

10    format(/' grid gaps '/' xmin=',10i7)
11    format(' ymin=',10i7)
12    format(' xmax=',10i7)
13    format(' ymax=',10i7)
      jm2=jm/2

      nmin=0
      nmax=0
15    format(10i7)
c      write(7,15)im,jm2
c      write(7,15)(i,fl(i,jm2,1),i=1,im)
16    format(' inveri=',i2)
c      write(7,16)inveri

      do20 ip=1+pixdiff,im-pixdiff
          i=ip
c          if(inveri.ne.0)i=im-ip+1

        yaa=fl(i-pixdiff,jm2,1)
        ya=fl(i-1,jm2,1)
        yb=fl(i,jm2,1)
        yc=fl(i+1,jm2,1)
        ycc=fl(i+pixdiff,jm2,1)
      
        si=yb*smin
        sx=yb*smax
      if(ya.gt.yb.and.yc.ge.yb)then
        if(yaa.gt.si.and.ycc.gt.si)then
           nmin=nmin+1
          if(nmin.gt.10)goto20
          
c          xmin(nmin)=ip-1
          xmin(nmin)=milsec*(ip-1)
          ymin(nmin)=yb
          goto20
        endif
      endif
      if(ya.lt.yb.and.yc.le.yb)then
        if(yaa.lt.sx.and.ycc.lt.sx)then
          nmax=nmax+1
          if(nmax.gt.10)goto20
c           xmax(nmax)=ip-1
          xmax(nmax)=milsec*(ip-1)
          ymax(nmax)=yb
          goto20
        endif
      endif
20    continue

      if(nrecad.eq.2)then
      write(7,10)xmin
      write(7,11)ymin
      write(7,12)xmax
      write(7,13)ymax
      endif

      return
      end
c**********************************
      subroutine ncentre(tab,iitab,jjtab,lltab,im,jm,nm,nc)
c                -------                                out
      integer*2 tab(iitab,jjtab,lltab)
      jc=(1+jm)/2
      smin=10000000.

      do n=1,nm
      s=0.
        do i=1,im
        s=s+tab(i,jc,n)
        enddo
      write(3,*)' ncentre: n,s ',n,s

      smin=amin1(smin,s)
      if(smin.eq.s)nc=n
      enddo

      return
      end
c-----------------------------------
      subroutine comparatlas(profmm,imin,k1,k2)
c                -----------
      dimension profmm(2000)
      integer*4 dlbd,dlbda(18),intl(18),intr(18),intav(18)

      do l=1,18
      dlbda(l)=(l-1)*100     !  mA
      enddo

      call par1('    dlbd',1,dlbd)
      call par1('   nwinp',1,nwinp)

1      format(' dlambdas(mA)',18i6)
2      format(' mean profile')
3      format('    right    ',18i6)
4      format('    left     ',18i6)
5      format('   average   ',18i6)

      write(7,1)dlbda
      write(7,2)

c right
      do l=1,18
      xk=imin+float(dlbda(l))*100./float(dlbd*nwinp)
      k=xk
         intl(l)=0
      if(k.gt.k1.and.k.lt.k2)then
        x=xk-k
        intl(l)=profmm(k)*(1.-x)+profmm(k+1)*x+0.5
      endif
      enddo

      write(7,3)intl

c left
      do l=1,18
      xk=imin-float(dlbda(l))*100./float(dlbd*nwinp)
      k=xk
         intr(l)=0
      if(k.gt.k1.and.k.lt.k2)then
        x=xk-k
        intr(l)=profmm(k)*(1.-x)+profmm(k+1)*x+0.5
      endif
      enddo

      write(7,4)intr

c moyenne
      do l=1,18
      intav(l)=0.5*(intl(l)+intr(l))+0.5      
      enddo
      write(7,5)intav

      return
      end
c--------------------------------------------
      subroutine plot_line(tab,iitab,jjtab,lltab,im,jm,nm,
c                ---------
     1                              fna,x,y,
c                                       ecomonie        
     2              il1,il2,jl1,jl2,iil1,iil2,iliss,jmarge,jparli,
     3         tabp,tabq,nw,win,milsec, curv,cr,yln,ngood,ldes,
c                                            out
     4         dyln,nq,idy,trjm,nw1,nw2)
      integer*2 tab(iitab,jjtab,lltab)
      integer curv,win(2)
      character*22 fna
      character*22 titre   
c                         !32 titre
c      character*18 titre2
      dimension x(2000),y(2000),tabp(200),tabq(200),yln(2000),
     1          cr(3),xbord(16),xpte(16),dyln(iitab,18,2,7)
c                     dist      pte

      write(3,*)' entree dans plot_line'
      print *,'call plot_line'
      ic=(1+im)/2
      jc=(1+jm)/2
      xl=float(jm)-1.
      yl=float(im)-1.
      iilm=iil2-iil1+1
            do n=1,nm
            xbord(n)=10000.
            xpte(n)=10000.
            enddo
c---
c             npiv=nm/2-1
      call par1('  ncurv1',1,ncurv1)
      call par1('  ncurv2',1,ncurv2)
c                                                     boucle n
c      do n=ncurv1,ncurv2  !npiv,nm-npiv+1    !4,nm-3                       2013
      do n=ncurv1,ncurv2 
      print *,'n=',n,' xl,yl ',xl,yl
         if(ldes.eq.0)goto2
c      write(titre(1:22),'(a)')fna
c      write(titre(23:30),'(a)')'      n='
c      write(titre(31:32),'(i2)')n
       write(titre(1:20),'(a)')'A   Line center  n= '
       write(titre(21:22),'(i2)')n
       print *,' msdp2 5694    xl,yl ',xl,yl
      call pgslw(2)   ! 2
      call pgadvance
      call pgsch(2.5)  !2.5  
      call pgvport(0.2,0.9,0.2,0.8)   !0.2,0.8
      call pglabel('Y','X',titre)
      call pgwindow(0.,xl,0.,yl)
      call pgbox('bcnts',50.,5,'bcnts',200.,2)
c      call pglabel('J','I',//titre)
2     continue

      if(n.eq.ncurv1)then
 101  format(i4,2x20i4)
      write(3,*)' channel ncurv1  im,jm ',im,jm  
      do i=1,im,100
         write(3,101)i,(tab(i,j,n),j=1,jm,10)
      enddo
      endif
      print *,'call linecurv'
      call linecurv(tab,iitab,jjtab,lltab,im,jm,nm,
c          --------
     1          il1,il2,jl1,jl2,iil1,iil2,iliss,jmarge,jparli,n,
     2          tabp,tabq,nw,win,milsec, curv,cr,yln)
c                                             out

        if(ldes.eq.0)goto4
      do i=iil1,iil2
        iip=i-iil1+1
      x(iip)=yln(i)
c            *****
      y(iip)=i
      enddo
      call pgline(iilm,x,y)      !    raie
c          ------
4       continue

      do i=1,im
      x(i)=cr(1)+i*(cr(2)+i*cr(3))
      dyln(i,n,nw,nq)=x(i)     !  stockage valeurs yln
c     **************
      y(i)=i
      enddo
           write(3,*)'  '
           write(3,*)' n=',n,'  yl=',yl,'  xl=',xl
           write(3,*)' parab  x(i)=',(x(i),i=1,im,50)

      ja=(x(1)-x(ic))   !*milsec
      jb=(x(im)-x(ic))   !*milsec
      jz=x(ic)   !*milsec
             write(3,*)' ja,jb,jz ',ja,jb,jz
      pivc=abs(x(ic)-jc)
      xbord(n)=pivc    !          ecart pour point ic
      pivp=x(1)-x(im)
      xpte(n)=abs(pivp)
        call par1(' bordpte',1,kpiv)
      write(3,*)' bordpte,n,xbord(n),xpte(n) ',kpiv,n,xbord(n),xpte(n)
        if(kpiv.eq.0)then
      if(xbord(n).lt.xbord(n-1))ngood=n
        else
      if(xpte(n).lt.xpte(n-1))ngood=n
        endif

        if(ldes.eq.0)goto6
      call pgline(im,x,y)        !       parabole
c          ------

      x(1)=0.  !1.
      x(2)=xl
      y(1)=float(ic)
      y(2)=y(1)

      write(3,*)' ic,im,yl,y(1) ',
     1            ic,im,yl,y(1)

      call pgsls(2)
      call pgline(2,x,y)         !         axe milieu
c          ------
      call pgsls(1)

10    format(' cmf1: nw,n,ja1000,jb1000,jz1000 =',2i2,3i6)
      write(7,10)nw,n,ja,jb,jz
      write(3,10)nw,n,ja,jb,jz

c      write(titre2(1:6),'(i6)')ja
c      write(titre2(7:12),'(i6)')jb
c      write(titre2(13:18),'(i6)')jz

c      call pgvport(0.1,0.9,0.1,0.8)
c      call pglabel(' ',' ',titre2)
6     continue

      write(3,*)' nw1,nw2,nw,nq,trjm ',nw1,nw2,nw,nq,trjm

      enddo                              !  boucle n


c on centre les yln autour de jc
c    ---------------------------
        if(nw1.ne.nw2)then
c                             nw1           4       5
c                             nw2       4       5                         
c                                       *   *   *   *
c                                     |       |       |
c                                  jc-trjm    jc

            if(nw.eq.nw1)then
              piva=jc-trjm/4.
              pivb=jc+3.*trjm/4.
            else
              piva=jc-3.*trjm/4.
              pivb=jc+trjm/4.
            endif

      write(3,*)' piva,pivb,im ',piva,pivb,im

      do i=1,im
        dyln(i,4,nw,nq)=dyln(i,4,nw,nq)-piva
        dyln(i,5,nw,nq)=dyln(i,5,nw,nq)-pivb
      do n=1,3
        piv=0.5*(dyln(i,4,nw,nq)+dyln(i,5,nw,nq))
        dyln(i,n,nw,nq)  =piv
        dyln(i,n+5,nw,nq)=piv
      enddo
      enddo

        else                         ! 1 seul w
c                                       
c                              nw           4     5
c                                           *     *
c                                           |     |  

      do i=1,im
        dyln(i,5,nw,nq)=dyln(i,5,nw,nq)-jc
      do n=1,nm
        dyln(i,n,nw,nq)=dyln(i,5,nw,nq)        
      enddo
      enddo

        endif
      if(ldes.eq.0)return

20    format(' nw,nq,(dyln(ic,n,nw,nq),n=1,nm)')
21    format(2i3,18f7.1)
      write(3,20)
      if(nq.eq.1.and.nw.eq.1)write(7,20)
      write(3,21)nw,nq,(dyln(ic,n,nw,nq),n=1,nm)
      write(7,21)nw,nq,(dyln(ic,n,nw,nq),n=1,nm)
      return
      end
c---------------------------------------
      subroutine coupe_bandes(fl,iitab,jjtab,nnfl,im,jm,nm,x,y,flmoy,
c                                                               inut
     1                     nw,nq,sigbd,inclin,ides,jy1,jy2,nrecad)
c                -------------
      integer*2 fl(iitab,jjtab,nnfl)
      dimension x(2000),y(2000),sigbd(3,18,2,6)
      character*9 titre1,titre2
      character*28 titreC

      print *,'coupe_b.:iitab,jjtab,nnfl,im,jm,nm,ides,nrecad',
     1     iitab,jjtab,nnfl,im,jm,nm,ides,nrecad
      write(3,*)'coupe_b.:iitab,jjtab,nnfl,im,jm,nm,ides,nrecad',
     1     iitab,jjtab,nnfl,im,jm,nm,ides,nrecad

      xl=im-1
      yl=jm-1
      jm2=jm/2
      
c      ic=(1+im)/2

      j1=1   !jm/8+0.5
      j2=jm   !-j1+1
      denj=j2-j1+1
      
c limites pour cmd
      call par1('      li',1,li)
      call par1('      lj',1,lj)
      call par1('     jy1',1,jy1)
      call par1('     jy2',1,jy2)
c        li=465000
c        lj=60000
c        jy1=0
c        jy2=60000
      xxa=float(jy1)/float(lj)
      xxb=float(jy2)/float(lj)
      print *,'cmf: xxa,xxb',xxa,xxb
      write(3,*)'cmf: xxa,xxb',xxa,xxb

c---
c      idesc=0
c        if(idesc.ne.0)then
      do ndes=1,3,2   !2,2     !1,3     

        call pgsch(2.5)
        call pgslw(2)
      call pgadvance
      call pgvport(0.2,0.9,0.3,0.7)     !0.1,0.9,0.3,0.7)   !0.2,0.7)

        xa=0.
        xb=nm
        piva=xa
        pivb=xb
        titre1='    D    '                    !   Dessin F
c                   F
        titre2='   j,n   '
        call pgwindow(xa,xb,0.,2.)
        call pgbox('bcgint',1.,0,'bcints',0.5,5)
          call par1('   imarg',1,imarg)
      if(ndes.eq.1)then
         isect=1+imarg
c      call pgsch(2.)
       write(titreC(1:25),'(a)')'C   F(1+imarg,j,n) imarg='
       write(titreC(26:28),'(i3)')imarg
      call pglabel('n,j (full j-range)',' ',titreC)
      endif
      
      if(ndes.eq.2)then
      isect=im/2      ! (1+im)/2
      call pglabel('n,j')
      endif
      if(ndes.eq.3)then
         isect=im-imarg
      write(titreC(1:25),'(a)')'C  F(im-imarg,j,n) imarg='
      write(titreC(26:28),'(i3)')imarg
      call pglabel('n,j (full j-range)',' ',titreC)  
      endif
        write(3,*)' isect=',isect
c        im4=im/ibdm
        deni=im  !im4

c      do ibd=1,ibdm
        i1=1 !1+(ibd-1)*im4
        i2=im  !i1+im4-1
c       if(ides.ne.0) 
       call pgslw(2) !ibd)       
        n1=1
        n2=nm
c----
       do n=n1,n2
        do j=1,jm
        x(j)=n-1+(j-1)/yl
            y(j)=0.
          do i=i1,i2      
          y(j)=y(j)+fl(i,j,n)
          enddo
            y(j)=y(j)/deni
        enddo

        ymoy=0.
        do j=j1,j2
        ymoy=ymoy+y(j)
        enddo
        ymoy=ymoy/denj

          if(ymoy.eq.0.)then
          print *,' cmf1: ymoy=0 '
          stop
          endif

        do j=1,jm
        y(j)=fl(isect,j,n)   !y(j)/ymoy
        y(j)=y(j)/10000. !xnormf
        enddo
 9      format('n,fl(j)',i5,10i5)
 10     format('n,y    ',2x,10f5.2)
      print  9,n,(fl(isect,j,n),j=1,jm,10)
      print 10,(y(j),j=1,jm,10)
      call pgslw(2)
      call pgline(jm,x,y)
c          ------
      write(3,*)'im,jm,n,fl(isect,1-10,jm2,n) ',
     1           im,jm,n,(fl(isect,j,n),j=1,10)
      write(3,*)'im,jm,n,fl(isect,1-jm/10,n) ',
     1           im,jm,n,(fl(isect,j,n),j=1,jm,10)
      write(3,*)' '
      
      x(1)=n-1+xxa
      x(2)=x(1)
      y(1)=0.
      y(2)=2.
      call pgslw(1)
      call pgsls(2)
      call pgline(2,x,y)
      x(1)=n-1+xxb
      x(2)=x(1)
      call pgline(2,x,y)
      call pgsls(1)

      enddo   !             boucle n
      
c     1     format(' nq,nw,ibd,rms fluct fl= ',3i2,18f6.3)
c      if(nrecad.eq.2)write(7,1)nq,nw,ibd,(sigbd(ibd,n,nw,nq),n=1,nm)

c      enddo   !                 boucle ibd
c      if(ides.ne.0)call pgslw(1)
      enddo                     !                      boucle ndes
c      endif
c---
c      if(ides.ne.0)call pgsch(2.)
      return
      end
c----------------------------------------
      subroutine linecurv(tab,iitab,jjtab,lltab,im,jm,nm,
c                --------
     1          il1,il2,jl1,jl2, iil1,iil2,iliss,jmarge,jparli,nncw,
     2          tabp,tabq,nw,win,milsec, curv,cr,yln)
c                                             out
      integer*2 tab(iitab,jjtab,lltab)
      dimension tabp(200),tabq(200),yln(2000),cr(3),nrej(90)
      integer win(2),curv
      print *,'                    linecurv, nncw= ',nncw
      write(3,*)' linecurv: iil1,iil2,iliss ',iil1,iil2,iliss    ! m2403
      do i=iil1,iil2     ! m2403

      do j=jl1+jmarge,jl2-jmarge
                ipiv1=i-iliss
                ipiv2=i+iliss 
                piv=0.
            do ip=ipiv1,ipiv2
             piv=piv+tab(ip,j,nncw)
c                    ---      ----
            enddo

           iliss2=2*iliss+1          
           tabp(j)=piv/float(iliss2)
      enddo

        if(i.eq.il1)print *,'jm,jmarge,jparli',jm,jmarge,jparli
          call parafit(tabp,jl1+jmarge,jl2-jmarge,jparli,tabq)
c              -------
c        print *,'parafit i=',i

c      if(i.eq.im/2)write(3,*)'parafit: i, tabp,tabq',tabp(20),tabq(20)

              piv1=100000.
            do j=jl1+jmarge,jl2-jmarge
              piv2=tabq(j)
            if(piv2.lt.piv1)jmin=j
              piv1=amin1(piv1,piv2)
            enddo
c              if(jmin.eq.jmarge+1.or.jmin.eq.jm-jmarge)then
c       write(7,*)' warning flat profile n=',nncw,' jparli too large?'
c       print *,' warning flat profile n=',nncw,' jparli too large?'
c              endif
            piv1= tab(i,jmin-1,nncw)
            piv2= tab(i,jmin,nncw)
            piv3= tab(i,jmin+1,nncw)
c       print *,'avant calcul',piv1,piv2,piv3                    


       yln(i)=jmin
       den=piv1-2*piv2+piv3

          if(den.ne.0.)then
          yln(i)=jmin + 0.5*(piv1-piv3) / den
          endif

c       yline(i,nw,nq)= yln(i)
c     rel  ----------------
      enddo                     ! i
      print *,'linecurv, ylin(i),i=iil1,iil2,100  ',
     1        (yln(i),i=iil1,iil2,100)   ! m2403
c--------------                    lissage yln

        im8=im-8
        im16=im-16
        jm2=jm/2
      iic=0.5*(iil1+iil2)  ! m2403
      iic1=iic+1
c      print *,' iil1,iil2 ',iil1,iil2
      if(curv.eq.0)then
c     ---------
      print *,'call zerofloat'
      call zerofloat(iil1,iic,iic1,iil2,0,0,0,yln,1,im,0,0,
c          ---------
     1           ar,br,cr,item,nrjm,nrej,y1,y2,ifin)
      cr(1)=br
      cr(2)=ar
      cr(3)=0.
      else   !!   parabole
c
      call zerofloat(iil1,0,0,iil2,3000,5,3,yln,1,im,0,0,
c          ---------
     1           ar,br,cr,item,nrjm,nrej,y1,y2,ifin)     
      endif

c      do i=1,im
c      yline(i,nw,nq)=cr(1)+i*(cr(2)+i*cr(3))
c      enddo
c                            rec=raie    yln=raie lissee

c         yln est centree si inclin=1
      return
      end
c--------------------------------------------------------------
      subroutine centrage_dyln(dyln,im,nm,nw1,nw2,nq1,nq2,trjm,nrecad,
c                -------------
     1                         iitab)
      dimension dyln(iitab,18,2,7)
      double precision ppiv

c moyennes nq en 7
        den=nq2-nq1+1
      do n=1,nm
      do nw=nw1,nw2
      do i=1,im
        piv=0.
      do nq=nq1,nq2
        piv=piv+dyln(i,n,nw,nq)
      enddo
        dyln(i,n,nw,7)=piv/den
      enddo
      enddo
      enddo

c centrage
        den=(nw2-nw1+1)*im*nm
        ppiv=0.
      do i=1,im
      do n=1,nm
      do nw=nw1,nw2
        ppiv=ppiv+dyln(i,n,nw,7)
      enddo
      enddo
      enddo
        piv=ppiv/den

      do i=1,im
      do n=1,nm
      do nw=nw1,nw2
        dyln(i,n,nw,7)=dyln(i,n,nw,7)-piv
      enddo
      enddo
      enddo

        if(nrecad.ne.2)return
      ic=(im+1)/2
1     format(' nw    y-departures of lines (n) for i= 1')
2     format(' nw    y-departures of lines (n) for i=ic')
3     format(' nw    y-departures of lines (n) for i=im')
4     format(i3,3x,18f7.1)

      write(7,1)
      do nw=nw1,nw2
        write(7,4)nw,(dyln(1,n,nw,7),n=1,nm)
      enddo      
      write(7,2)
      do nw=nw1,nw2
        write(7,4)nw,(dyln(ic,n,nw,7),n=1,nm)
      enddo      
      write(7,3)
      do nw=nw1,nw2
        write(7,4)nw,(dyln(im,n,nw,7),n=1,nm)
      enddo      

      return
      end
c---------------------------------------------------------
      subroutine sstray(tab,iitab,jjtab,lltab,im,jm,nm,nw,nq,
c                ------
     1                 stray,nrecad,vna)

      integer*2 tab(iitab,jjtab,lltab)
      dimension t(2000,18),tm(2000),rap(18),stray(2000,18),
     1          x(2000),y(2000)
      character*22  vna

      ia=im/3
      ib=2*im/3

      do n=1,nm
      do i=1,im
        piv=0.
        do j=1,jm
        piv=piv+tab(i,j,n)
        enddo
      t(i,n)=piv/float(jm)
      enddo
      enddo

        aver=0.
      do i=1,im
      piv=0.
        do n=1,nm
        piv=piv+t(i,n)
        enddo
      tm(i)=piv/float(nm)
      aver=aver+tm(i)
      enddo
        aver=aver/float(im)

      do n=1,nm
        s1=0.
        s2=0.
        do i=ia,ib
          s1=s1+t(i,n)
          s2=s2+tm(i)
        enddo
          rap(n)=s1/s2
      
        do i=1,im
          rapi=t(i,n)/(rap(n)*tm(i))
          stray(i,n)=rapi

          if(rapi.gt.1.)then
            t(i,n)=t(i,n)*(rapi-1.)                        
            if(t(i,n).lt.0.)t(i,n)=0.
              do j=1,jm
              tab(i,j,n)=tab(i,j,n)-t(i,n)
              enddo
          endif
        enddo
      enddo

      if(nrecad.eq.2)then
      call pgadvance
      xa=0.
      xb=nm
      xl=im-1
      yl=jm-1
        call pgsch(2.)
      call pgvport(0.1,0.9,0.1,0.7)
      call pgwindow(xa,xb,0.,2.)
      call pgbox('bcgint',1.,0,'bcints',0.5,5)
      call pglabel('i,<j>,n','stray light ','S1 '//vna)
        do n=1,nm
          do i=1,im
          x(i)=n-1+(i-1)/xl
          y(i)=stray(i,n)
          enddo
        call pgline(im,x,y)
c            ------           
        enddo
      endif

      return
      end
c------------------------------------------
      subroutine calstray(tab,iitab,jjtab,lltab,i,jm,n,nm,profj,
c                --------  in
     2                  xminpro,xnormf,nw,nq,istray,fl,stray)
c                       in                   in     out
 
      integer*2 fl(iitab,jjtab,lltab),tab(iitab,jjtab,lltab)
      dimension profj(200),ecart(61),stray(2000,18),f(200)

c1 diffus
c1          tab(j)=fl(j)*(profj(j)+stray)/xnormf
c1    avec  S[(fl-flmoy)**2]**0.5/(jm*flmoy)  minimum

c2 cross-talk
c2          ndel=+1 si stray>0
c2               -1         <
c2          tab(j,n)-tab(j,n+ndel)*abs(stray)=fl(j)*profj(j)/xnormf
c2    avec  S[(fl-flmoy)**2]**0.5/(jm*flmoy)  minimum

      if(n.lt.3.or.n.gt.nm-2)then
      piv=0.
      goto160
      endif

        j1=5
        j2=jm-4
        nesm=61
        den=j2-j1+1

      if(istray.eq.1)then
          st1=-0.8*xminpro
          st2=10.*xminpro
      else                   ! istray=2
          st1=-0.05
          st2=0.05
      endif
          dst=(st2-st1)/float(nesm-1)
c---
      do150 nstep=1,2

        if(nstep.eq.2)then
        st2=piv+dst
        st1=st2-2.*dst
        dst=(st2-st1)/float(nesm-1)
        endif

      do100 nessai=1,nesm
        fmoy=0.
        st=st1+dst*(nessai-1)
           ndel=1
           if(istray.eq.2.and.st.lt.0.)ndel=-1

        do j=j1,j2

        if(istray.eq.1)then
          f(j)=tab(i,j,n)/(profj(j)+st)
        else          !  istray=2
                piva=tab(i,j,n)
                  nnn=n+ndel
                pivb=tab(i,j,nnn)
          f(j)=(piva-pivb*abs(st))/profj(j)
c          if(f(j).lt.0.)f(j)=0.
        endif
          fmoy=fmoy+f(j)
        enddo
        fmoy=fmoy/den

          eqm=0.
        do j=j1,j2
          eqm=eqm+(f(j)-fmoy)**2
        enddo
          if(fmoy.eq.0.)fmoy=1.
          ecart(nessai)=sqrt(eqm/den)/fmoy
c       if(i.eq.10) write(3,*)'nessai,eqm,fmoy',nessai,eqm,fmoy
100    continue

1     format(' calstray: nq,nw,n,i/xminpro,nesmin,stray(1,n)',
     1       'tab(i,jm/2,n),fl(i,jm/2,n)/ecart')
2     format(4i5,f8.0,i5,f6.2,2i8)
3     format(10f7.3)
c minimum
      piv=0.

        ecmin=10000.
      do nessai=1,nesm
       ecmin=amin1(ecmin,ecart(nessai))
       if(ecmin.eq.ecart(nessai))nesmin=nessai
c                                ------
      enddo

      piv=st1+dst*(nesmin-1)
c     ---

      nosol=0
         if(nstep.eq.1)then
      if(istray.eq.1)then
        if(ecart(1).lt.0.20.and.ecart(nesm).lt.0.20)nosol=1
      else
        if(ecart(1).lt.0.20.and.ecart(nesm).lt.0.20)nosol=1
      endif
      if(nesmin.eq.nesm)nosol=1
      if(nesmin.eq.1)nosol=1
         endif
      if(nosol.eq.1)piv=0.

      if(i.eq.20)then
           jm2=jm/2
      write(3,1)
      write(3,2)nq,nw,n,i,xminpro,nesmin,piv,
     1          tab(i,jm2,n),fl(i,jm2,n)
      write(3,3)ecart
      endif

      if(nosol.eq.1)goto160

150   continue

160   stray(i,n)=piv
c     -----

      doj=1,jm

      if(istray.eq.1)then
      fl(i,j,n)=xnormf*tab(i,j,n)/(profj(j)+piv)
c     ---------                             ---
      else    !  istray=2
        ndel=1
        if(piv.lt.0.)ndel=-1
      fl(i,j,n)=xnormf*(tab(i,j,n)-tab(i,j,n+ndel)*abs(piv))/profj(j)
c     ---------                            ------      ---

      endif

      enddo

      return
      end
c===========================================================
      subroutine angi(nm,xr,yr,milangi)
      dimension xr(18,3,2),yr(18,3,2)

      ax=0.
      ay=0.
      do n=1,nm
       ax=ax+xr(n,3,1)-xr(n,1,1)+xr(n,3,2)-xr(n,1,2)
       ay=ay+yr(n,3,1)-yr(n,1,1)+yr(n,3,2)-yr(n,1,2)
      enddo
       ang=ay/ax
      milangi=1000.*ang
      write(7,*)' milangi new = ',milangi
      write(7,*)' '
      return
      end
c---------------------------------------
      subroutine xyplot(nplot,ima,ijcam,i1,i2,j1,j2,iperm)
c        iperm sans inversion I/J
      integer*2 ima(ijcam,ijcam)
      integer*4 tabmin,tabmax
      dimension tr(6)
      real*4 imaperm(1536,1536)
        tr(1)=0.
        tr(2)=1.
        tr(3)=0.
        tr(4)=0.
        tr(5)=0.
        tr(6)=1.

        tabmin=32000
        tabmax=0
      do j=j1,j2
      do i=i1,i2
        kpiv=ima(i,j)
        tabmin=min0(kpiv,tabmin)
        tabmax=max0(kpiv,tabmax)
        if(iperm.eq.0)then
           imaperm(i,j)=ima(i,j)
        else
           imaperm(j,i)=ima(i,j)
        endif
      enddo
      enddo

      xa=0.
      ya=0.
      if(iperm.eq.0)then
      ii1=i1
      ii2=i2
      jj1=j1
      jj2=j2
      xb=i2-1
      yb=j2-1
      else
      ii1=j1
      ii2=j2
      jj1=i1
      jj2=i2
      xb=j2-1
      yb=i2-1
      endif

      write(3,*)' xyplot nplot iperm  ii1,ii2, jj1,jj2  tabmin,tabmax ',
     1                nplot,iperm,'  ', ii1,ii2,jj1,jj2,tabmin,tabmax
      print *,' xyplot nplot iperm  ii1,ii2, jj1,jj2  tabmin,tabmax ',
     1                nplot,iperm,'  ', ii1,ii2,jj1,jj2,tabmin,tabmax
      write(3,*)' xyplot liste ima'
      do j=jj1,jj2,100
      write(3,*)(imaperm(i,j),i=ii1,ii2,100)
      enddo

c      call pgbegin(0,'xyplot1.ps/ps',1,1)
c      call pgadvance
c      call pgvport(0.2,0.8,0.3,0.7)
c      call pgwindow(xa,xb,ya,yb)
c      call pgbox('bcnts',200.,4,'bcnts',200.,4)
c      call pggray(imaperm,ijcam,ijcam,ii1,ii2,jj1,jj2,tabmin,tabmax,tr)
c      call pgend
      return
      end
c-------------------------------------------


