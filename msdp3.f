
c msdp3.f
c========
c************************************************
c Sous-programme  cmd1.f  -  UNIX -   Mai 2016
c************************************************

      subroutine cmd1(idr,nw1,nw2,nwinp,milsec,npo,nq,
c     ----
     1            cna,dname,iud, iuj,
c                     noms       j pour corv=1
     2            corv,   tab,   tb, ides, mgrim,dlbd,idebfp,itgrig,
c                  in    economie   dessin                   inutile
c                        tabeco  tb
c                         iitab  iitab
c                         jjtab  jjtab
c                         nntab  lltab
     3            iitab,jjtab,nntab,lltab,moyw,lma,
c                                         inutilises
     4            dxana,dyana,kana,kanam,nfail,nscan,nx,icmd,mname,ium,
c                  in    in    in    in
     5            nqsign,jhair,iecri7,kind,icontv,
c                                          min cont pour v (sauf 0)
     6            minpro,mincont)
c                                 supprimer minpro

c                 protus

c  1536= max im
c   100= max jm
c    32= nbre max de canaux au total
c    18= nbre max de canaux par fenetre
c     2= nbre max de fenetres

c    32.ge. 1+2*(lmp(1)+lmp(2)+lmp(3)) nbre de tableaux en sortie
c    32.ge. nm (pour chaque polarisation)
c     arrays "3"    velocity unit = mps m/s
c   si ideb=0   --> idebfp fourni par flat (grille standard)   ?
c   si ideb=-1  --> idebfp fourni par flat (grille decalee)    ?
c   si ideb>0   --> ideb   fourni par ms.par                   ?

c si mgrim.ne.0              on ecrit dans iud, avec dim i1,i2 j1,j2
c si mgrim.ne.0  calana=0    on ecrit dans iud, avec dim i1,i2 j1,j2
c si mgrim.ne.0  calana=1   
c          kana.eq.1         on dessine
c                            on ecrit dans 60 (file t dsans header)
c                                                     avec dim i1,i2 j1,j2
c                            et dstok ecrit dans iud (file d), 
c                                                     avec dim reduites
c                                                     par itana,jtana
c                                le fichier d servira pour dme
c                                puis sera remplace a l'etape kana suivante
c          kana>1            on ne recalcule pas
c                            on lit dans 60 (file t sans header)
c                                                     avec dim i1,i2 j1,j2
c                            et dstok ecrit dans iud (file d),
c                                                     avec dim reduites
c                                                     par itana,jtana 
c                              (restera le dernier a kana=kanam)                

      character comm*80,jjname*22,dname*22,mname*22,
     1          cna*22,titre*30

      double precision apod(200),
     1      sa(60),sav(60),savv(60),
     2      sadc(60),sads(60),sacp(60),sacq(60),sasp(60),sasq(60),
     3                       savcp(60),savcq(60),savsp(60),savsq(60)
c      dimension tabjn(200,32)
      dimension cna(6,26),sinus(629),cosinus(629),cn(60),sn(60),
     1                                            cn1(60),sn1(60)
      dimension xlbd1(3),xlbdstep(3),dxana(6),dyana(6),ixscat(500)

      integer*4 lmp(3),lbd1(3),lbdstep(3),corv,
c modif stokes 1 mai 00                                     dlbb
     1          day,time,center,sum,dlbd,dlbcalc(60),dlb(60),dlbb(120),
c                                                       lm<=60
     2     nfail(200),ixlimb(400),kdispro(400),lold(60)
c                     0,  + disk left, - disk right      ---- cordisk

      integer*4 ivw(60),moyw(18,60),dlimb
      integer*4 kmoyv(60),scatter,iw(3),kind(60)
        
      dimension p(32),xp(129),u(60),v(60), ylinef(2000),kz(512)
c                 nm     4*nm-3  lm<=60
      dimension xdes(2),ydes(2),xxdes(200),yydes(200),
     1          xregres(1600),yregres(1600),poids(1600),coef(2)

      integer*2    tab(iitab,jjtab,nntab), jtab(2000),jjemabs(2000,200),
c     entree                                          
     1             ijtab(2000,200),tb(iitab,jjtab,lltab),jhair(200),
c                                     sortie  
     2             ijtab2(1500,150,3),kdisc(2000,200),ttab(90)
c                  pour promax ne 0, et tiroir pour mincent ne 0 
      character*30 labh(3)

      write(3,*)'cmd: nscan,nx,nq,nw1,nw2',nscan,nx,nq,nw1,nw2
      write(3,*)'     cna(nq,nw1)',cna(nq,nw1)
      write(3,*)'     cna(nq,nw2)',cna(nq,nw2)

      do j=1,200
         ixlimb(j)=0
      enddo
      call par1(' imaxpro',1,imaxpro)
      call par1('   scanc',1,kpiv)
        scanc=float(kpiv)/1000.
      call par1('  nscanc',1,nscanc)
      call par1('ispline3',1,ispline3)
      call par1('  icplot',1,icplot)
      call par1('  jcplot',1,jcplot)
      call par1('  icstep',1,icstep)
      call par1('  jcstep',1,jcstep)
      call par1('     njm',1,njm)

      call par1('  facpro',1,ifacpro)    !  * int protu
         facpro=float(ifacpro)
         if(ifacpro.eq.0)facpro=1.
      call par1(' cordisk',1,icordisk)   !     0/1   pour scordisk
      call par1(' mcorrec',1,mcorrec) !  correction vit m/s

      call par1(' iminpro',1,iminpro)
c            iminpro=iminpro*ifacpro     !   int protu

c      call par1(' imindis',1,imindis)
      call par1(' intlimb',1,intlimb)
      call par1(' lmaxpro',1,lmaxpro)
      call par1('  milsec',1,milsec)
      call par1('  intadd',1,intadd)
      call par1(' linvisu',1,linvisu)

c      call par1('  intmil',1,intmil)  !  interpolation milieux (cubiques)


      itor=0 !180*2    !  tornado
      jtor=0 !20*2


      write(3,*)'cmd: icontv=',icontv
      write(3,*)'cmd: minpro,maxcont',minpro,maxcont


      idessin_correc=0

      labh(1)='harmonic w1                   '
      labh(2)='harmonic w2                   '
      labh(3)='harmonic w3                   '
c absorption/emission
      call par1('   minab',1,minab)  ! min canaux extremes-line
      call par1('   minem',1,minem)  !     line-canaux extremes
      call par1('   maxem',1,maxem)  !     line-canaux extremes
c                                priment absord et absorr
c                                   *
c spicules themis
      call par1('  spimin',1,kpiv)
        spimin=kpiv
      call par1('  spimax',1,kpiv)
        spimax=kpiv
      call par1(' minsmax',1,minsmax)
        sminmax=float(minsmax)/1000.
      call par1(' discmin',1,kpiv)
        discmin=kpiv
      call par1('    fond',1,kpiv)
        fond=kpiv
      call par1('   vfond',1,kpiv)
        vfond=kpiv
c corrections disque avec ciel
      call par1(' mincent',1,mincent)

c correction protus
      call par1('   dlimb',1,dlimb)   !   0 = disque
c      call par1(' minprot',1,minprot) !   min protus tous les cas
c      call par1('minprotu',1,minprotu)
      call par1('  negpro',1,negpro)

      if(ides.ne.0)then     !   pas de dessins si boucles ...
      call par1(' iprofil',1,iprofil)
      call par1(' jprofil',1,jprofil)
      call par1(' plotpro',1,iplotpro)
c      call par1('     id1',1,id1)
c      call par1('     id2',1,id2)
            call system('rm cm'//dname//'.ps')
        kpiv1=njm
        kpiv2=njm
c        if(itor.ne.0)kpiv=1
      if(iplotpro.eq.0)then
      call pgbegin(0,'cm'//dname//'.ps/ps',kpiv1,kpiv2)
      else
      call pgbegin(0,'cm'//dname//'.ps/ps',1,1)
c          -------
        write(7,*)' d/r-name ',dname
      endif
      endif

        do n=1,629
        x=float(n-1)/100.
        sinus(n)=sin(x)
        cosinus(n)=cos(x)
        enddo

      sqpi=sqrt(3.1415926535)
c      cdat='cqp   '

c-------------                           vmoy,dxana,dyana
              vmoy=0.

      if(idr.eq.1)then
c                lecture du fichier j pour correction des lambdas
c          ce fichier, cree par dme.f, contient aussi les resultats
c          de correlations, que l'on doit sauter apres chaque lecture 
c          en cas de boucle Y
c          ainsi que dxana et dyana, qui remplacent les arguments d'entree
      
52    format(' Lecture fichier j ')
      write(3,52)
      rewind(iuj)
        read(iuj,1)jjname
c        print *,jjname
        write(3,1)jjname 
      read(iuj,1)comm

108       format(3f12.1)
        read(iuj,108)vmoy,pivx,pivy
          if(idr.eq.1)then
          dxana(kana)=pivx
          dyana(kana)=pivy
          endif

c          vmoy(lpo)=vmoy(lpo)+vvmoy(lpo,ly)/float(lymax)
  
110   format(' vmoy=',f12.1)
      write(3,110)vmoy
        if(corv.eq.0)vmoy=0.

      endif
c-------------------------------------

1     format(a80)
15    format(2x,10i8)
22    format(/)

      print*,' entree cmd: ides ',ides
c obs
      call par1('  inveri',1,inveri)
      call par1('  inverj',1,inverj)
      call par1('  inverl',1,inverl)
      call par1(' ijreduc',1,ijreduc)
c        ix1=ijreduc
c        iy1=ijreduc
c        ix2=li-ijreduc
c        iy2=lj-ijreduc
      call par1('     ix1',1,ix1)
      call par1('     ix2',1,ix2)
      call par1('     jy1',1,jy1)
      call par1('     jy2',1,jy2)
      call par1('   norma',1,norma)
c exe
      call par1('  ilisdr',1,ilis)
      call par1('  jlisdr',1,jlis)
c fix
      call par1('     mps',1,mps)

                        idelc=0
                pix=float(milsec)
            i1=1+float(ix1)/pix+0.5
            i2=1+float(ix2)/pix+0.5
            j1=1+float(jy1)/pix+0.5
            j2=1+float(jy2)/pix+0.5
                if(mps.eq.0)mps=5
      print *,' cmd: i1,i2,j1,j2 ',i1,i2,j1,j2
      write(3,*)' cmd: i1,i2,j1,j2 ',i1,i2,j1,j2
c     cqp
c      call par1('  milalp',1,milalpha)  !   non
        milzero=0
c      call par1(' milzero',1,milzero)
c        if(milalpha.eq.0)milalpha=1000

c           alpha=0.001*float(milalpha)
c           zer=0.001*float(milzero)   !  0

      if(mgrim.ne.0)then
      call par1('    ideb',1,ideb)
      call par1('    igri',1,igri)
      call par1('   itgri',1,itgri)
      call par1('   itana',1,itana)
      call par1('   jtana',1,jtana)
      call par1('   istep',1,istep)
                             istep=iabs(istep)
      call par1('  invers',1,invers)
          xdeb=float(ideb)/pix

          if(ideb.le.0)then
          xdeb=float(idebfp)/pix
c                    reglage automatique
          endif

c             xdeb  egaux pour nw1 et nw2 eventuels

        xgri=float(igri)/pix
            igri=xgri
        txgri=float(itgri)/pix
            itgri=txgri

        piv=itana+dxana(kana)
        txana=piv/pix
        piv=jtana+dyana(kana)
        tyana=piv/pix

        taux=0.
      endif

c                si corv=0, pas de correction de lambdas en entree
c                              (en particulier si longw et lat.ne.0)
c                           on cree un fichier l
c                       =1, on lit le fichier l existant 
c                               on corrige le zero des lambdas
c                           on ne cree pas de fichier l

c fix
      call par1('   longw',1,longw)
      call par1('     lat',1,lat)
c      call par1(' icentre',1,icentre)
      call par1(' mindisc',1,mindisc)     !  m2403
c      call par1(' milsigc',1,milsigc)
                icentre=0

      if(idr.eq.0)then
      call par1('  absord',1,iabsor)
      call par1('  cented',1,center)
      call par1('    sumd',1,sum)
      call par1('   crecd',1,lisond)
      call par1('   curvd',1,liscurv)
      call par1('   nlisd',1,nlis)
      call par1('  ratiod',1,itaux)

      else
      call par1('  absorr',1,iabsor)
      call par1('  center',1,center)
      call par1('    sumr',1,sum)
      call par1('   crecr',1,lisond)
      call par1('   curvr',1,liscurv)
      call par1('   nlisr',1,nlis)
      call par1('  ratior',1,itaux)

      endif
         lsum=0
         if(sum.ne.0)lsum=1

c modif 18/11/99              iabsor=0 (proche xc), 1(absor), 2(emiss)

      do m=1,3
      if(idr.eq.0)then
      call par1('    lmpd',m,lmp(m))
      call par1('   lbd1d',m,lbd1(m))
      call par1('  lbpasd',m,lbdstep(m))
      else
      call par1('    lmpr',m,lmp(m))
      call par1('   lbd1r',m,lbd1(m))
      call par1('  lbpasr',m,lbdstep(m))
      endif

      if(m.eq.2)then
        isigdif=0
        if(lbd1(2).lt.0)then
          isigdif=1
          lbd1(2)=-lbd1(2)
        endif
      endif

       xlbd1(m)=lbd1(m)*0.004         ! 300*0.004=1.200
        lbd1(m)=xlbd1(m)+0.0001
       xlbdstep(m)=lbdstep(m)*0.004
        lbdstep(m)=xlbdstep(m)+0.001

c        if(m.eq.3.and.lmp(3).ne.0)then
c          test1=xlbd1(m)-lbd1(m)
c          test2=xlbdstep(m)-lbdstep(m)
c          test1=abs(test1)
c          test2=abs(test2)
c            kpiv=iabs(iabsor)
c          if(kpiv.lt.2)then
c          if(test1.gt.0.001.or.test2.gt.0.001)then
c             print *,' Bisector: lbd1 and lpas must be multiple of 250'
c             stop
c          endif
c          endif
c        endif
      enddo

c                         corrections des harmoniques 1/2,1,2
      if(idr.eq.0)then
        call par1('     w1d',1,iw(1))      
        call par1('     w2d',1,iw(2))
        call par1('     w3d',1,iw(3))
      else
        call par1('     w1r',1,iw(1))      
        call par1('     w2r',1,iw(2))
        call par1('     w3r',1,iw(3))
      endif
        write(3,*)' cmd: iw ',iw

      icalc=0
      iterwm=1

c------
c type de tableaux   0=int    1=dint,v,B//,stokes
      do l=1,60
        kind(l)=0
      enddo

      if(mgrim.eq.0)then
        l1=center+lsum+2
        do l=l1,60,2
          kind(l)=1
        enddo

      else
        kind(2)=1
        l1=2*(center+lsum)+1
        if(lmp(1).ne.0)then
          do lp=1,lmp(1)
            l=l1+(lp-1)*4
            kind(l+1)=1
            kind(l+3)=1
          enddo
        endif
        l1=l1+4*lmp(1)
        do l=l1,60,4
          kind(l+1)=1
          kind(l+2)=1
          kind(l+3)=1
        enddo
      endif
 2    format(' kind ',30I2)
      if(iecri7.eq.1)write(7,2)(kind(l),l=1,30)
c------------------------
      lm=center+lsum+2*(lmp(1)+lmp(2)+lmp(3))
      call par1(' scatcor',1,kscatcor)   ! scattered lgth correction m2403
c     call par1('   ilin1',1,ilin1)
      if(iminpro.ne.0)lm=lm+1     !.and.ilin1.ne.0)lm=lm+1
      write(3,*)' cmd1: lm,ix1 ',lm,ix1
c modif0009
c modif suppression moy 1 mai 00
c      If (lmp(3).gt.1) lm = lm +2
c        lm=lm*npol !!!!!!!!!!!!
      if(center.ne.0)dlb(1)=0
      if(sum.eq.1)dlb(1+center)=10000
      kpiv=center+lsum
        do m=1,3
          if(lmp(m).ne.0)then
            do mm=1,lmp(m)
            kpiv=kpiv+1
            dlb(kpiv)=(xlbd1(m)+xlbdstep(m)*(mm-1))*float(dlbd)/4.
            kpiv=kpiv+1
            dlb(kpiv)=-dlb(kpiv-1)
            enddo
          endif
        enddo
          if(iminpro.ne.0.and.kscatcor.ne.0)dlb(lm)=dlb(lm-2)
        
        write(3,22)
        piv=3.1415927/180.
          vrot=0.
          if(longw.ne.0.and.lat.ne.0)then
        xlongw=float(longw)*piv
        xlat=float(lat)*piv
        slong=sin(xlongw)
        clat=cos(xlat)
        slat=sin(xlat)
        slat2=slat**2
        vrot=+(2.919-slat2*(0.312+0.212*slat2))*0.696*slong*clat
        vrot=10000.*vrot/float(mps)
          endif
c                         m/s,  + pour redshift

c-------------------------------------
c                                  ouvertures c 
c                            (d et eventuellement j deja ouverts)
c        write(3,*)' cna ',cna

c------------------------

       do nw=nw1,nw2

        iuc=30+nw
      
        call openold22(cna(nq,nw),sundec,iuc)  !sf
c            ---------
       enddo
                   
c -------------               Lecture header premier fichier c
c                                pour im,jm,nm,yline, kdec
c                                pour header d 
      iucw=30+nw1

      rewind(iucw)
      read(iucw)na3,im,jm,nmp,imm,nmm,jmm,nxx,nyy,
     1             nd,day,time,iline,lbda,ncm,
c                                        
     2             jt100,ja100,jb100,jz100,nc100,           kminm
c                  ----------------------- -----           k moyen
c                  centres nc100 final                     inutile
      write(3,*)'c im,jm,nmp,imm,nmm,jmm,nxx,nyy ',
     1             im,jm,nmp,imm,nmm,jmm,nxx,nyy

      print *,'na3:',na3,'im:',im,'jm:',jm,'nmp:',nmp
      print *,'imm:',imm,'jmm:',jmm,'nmm:',nmm
      print *,'nxx:',nxx,'nyy:',nyy,'nd:',nd
      print *,'day:',day,'time:',time,'iline:',iline
      print *,'apres read iucw lbda :',lbda

c     nmp= nombre de canaux initiaux
      nm=nmp*nwinp

      write(3,*)' input cmd: jt100,ja100,jb100,jz100,nc100 ',
     1                       jt100,ja100,jb100,jz100,nc100
c-----------
c      canal final de reference pour yline
      ync=0.01*nc100
      jc=(1+jm)/2
c-------
      nnm=4*nm-3  !!   nombre de canaux apres interpolation (sur 129)
      ltrj=jt100

c      nclim=1

c-----------------     debut du header fichier d
      do i=1,512
      kz(i)=0
      enddo

      nd1=4
c      iz1=11+lm+1+11 !!!!!!!!!
c       iz1=18+lm+1
      if(i1.eq.0)i1=1
      if(i2.eq.0)i2=im
      if(j1.eq.0)j1=1
      if(j2.eq.0)j2=jm
        iim=i2-i1+1
        jjm=j2-j1+1

c modif Stokes 29 avril 00
      if(mgrim.eq.0)then   
      ims=iim
      jms=jjm
      lpiv=lm
        do l=1,lpiv
        dlbb(l)=dlb(l)
        enddo

      else
      is=xgri+1
      ims=is*mgrim                !im sortie

        jtya=tyana+1
        if(tyana.lt.0)jtya=tyana-1
        if(tyana.eq.0)jtya=0
        jms=j2-j1-iabs(jtya)+1       !jm sortie

      lpiv=2*lm

        do l=1,lm
        l1=2*l-1
        l2=l1+1
        dlbb(l1)=dlb(l)
        dlbb(l2)=dlb(l)
        enddo
      endif

c  protus promax ne 0
c      call par1('  promax',1,kpromax) ! max int protus
c      call par1(' milwmax',1,milwmax) 
c      if(iminpro.ne.0)then
c      dlbb(lpiv-2)=dlbb(lpiv)
c      dlbb(lpiv-1)=-dlbb(lpiv)
c      dlbb(lpiv)=dlbb(lpiv)
c      lpiv=lpiv+3
c      endif
c        nd2=18+lpiv
c        izz1=nd1+nd2+3
c        write(3,*)' cmd1: nd1,nd2,lpiv,izz1 ',nd1,nd2,lpiv,izz1
c                                    largeurs, doppler ijtab2
c----------------------------------
30    format(/' Header fichier d')

c      write(iud)nd1,ims,jms,lpiv,npo,
c     1          nd2,day,time,iline,lbda,ncm,ltrj,(dlbb(l),l=1,lpiv),
c                                           ----
c     2          center,lsum,lmp,lbd1,lbdstep,mgrim,
c     3          (kz(i),i=izz1,512) !!!!!!!!!!!!!!!
c      write(3,30)
c      write(3,*)'d ims,jms,lpiv,npo ',ims,jms,lpiv,npo
c      write(3,15)nd1,ims,jms,lpiv,npo,
c     1           nd2,day,time,iline,lbda,ncm,ltrj,(dlbb(l),l=1,lpiv),
c     2           center,lsum,lmp,lbd1,lbdstep,mgrim !!!!!
c---                                                  header de m
c      if(icmd.gt.1.and.kana.eq.1)then
c      npiv=icmd+1
c      npiv2=6
c      npiv3=nd1+npiv2+3

cc      write(ium)nd1,im,jm,npiv,npo,
c     1          npiv2,day,time,iline,lbda,ncm,lbdstep,
c     2          (kz(i),i=npiv3,512)
c      endif
c---

      if(kana.ne.1)goto400
c------------------------------         calcul ylinef
c             average line-center = n=ncf and
c                                   j=(ja100+jz100)/100 in i=1
c                                            jz100/100     i=(1+imf)/2
c                                     (jb100+jz100)/100    i=imf
      imf=im
      jmf=jm

      trj=float(jt100)/100.
c     ---
      
      ic=(1+im)/2
c     ---
      xic=ic

      do ip=1,imf
         i=ip
         if(inveri.ne.0)i=imf-ip+1     !  inversion
c                         --------         
      xpiv=(i-xic)/(xic-1.)
      piv=jz100+xpiv*0.5*(jb100-ja100+
     1                   (ja100+jb100-2.*jz100)*xpiv)
      ylinef(ip)=piv/100.
c     ---------          pour le canal final nc100/100
c                                            
      enddo

      write(3,*)'cmd: xic,ja100,jb100,jz100,jt100 ',
     1            xic,ja100,jb100,jz100,jt100

48    format(' ylinef(i),i=1,imf,200 ',10f7.1)
      write(3,48)(ylinef(i),i=1,imf,200)
     
c-------------------------------------
c                                                 ylinef
c                                            (calcul necessitant ltrj)

c           decaly=-(float(ltrj)/float(ltrjconver))*(nc-ncc)/100. !!!!

c           ylinepo(i,npo)=ylinepo(i,npo)+(ylinef(i,nw)+decaly)/pivd
c
c         Avant de faire la moyenne des ylinef des diveses fenetres nw,
c         on ramene ylinef a sa position dans le canal central final.
c         L'ecart a rattraper en unite de canal final est
c             nc-ncc   ou ncc est le rang n du couple (nwl,ncan)
c         ex: deux fenetres entrelacees:
c         nwl     121212121212121212
c         ncan    112233445566778899
c                          |          ncc=10 pour le canal 5 de la fenetre 2
c                
c         ex: une fenetre:
c              111111111
c              123456789
c                  |               ncc=5 pour le canal 5 de la fenetre 1
c
c         Comme le dlbd initial ne figure pas dans les headers des fichiers
c         b ni f, on calcule le facteur de conversion des ltrj par nwl:
c                121212121212121212
c                | |                  ltrjconver=2
c
c                111111111
c                ||                   ltrjconver=1

c           enddo
c         enddo
c59       format(' npo,vmoy,nw1,nw2,ltrjpo,',
c     1       'ylinepo(1,npo),ylinepo(imf,npo),ltrjconver,decaly'/
c     2            i4,f6.2,2i4,i6,2f8.2,i4,f8.2)
c         write(3,59)npo,vmoy(npo),nw1,nw2,ltrjpo(npo),
c     1              ylinepo(1,npo),ylinepo(imf,npo),
c     2              ltrjconver,decaly


c--------------------                   chargement tab
c  saut des headers

      nw1p=nw2
      nw2p=nw1


      do nw=nw1,nw2
        write(3,*)' cmd: nw ',nw
       
      iucw=30+nw
        rewind(iucw)
      read(iucw)kz   !  *      
        write(3,*)' cmd: nw,kz ',nw,(kz(i),i=1,20)
      enddo
c--------------  boucle canaux/fenetres
        ksig=1
        if(inverl.ne.0)ksig=-1

        nt=0       !!  canal nt
c       print *,' nmp ',nmp
      do 200 n=1,nmp

c                         entrelac

      do 200 nw=nw1p,nw2p,-1                 !!!!!!!!!!!!   inverl.....
c        print *,' nw ',nw
        nt=nt+1

c      write(3,*)' n,nw,nt ',n,nw,nt
            ymoy=-(vrot-vmoy)*ksig*float(lbda)*mps/
c           ----
     1            (75000.*dlbd)
c                                       unite= 1/4 de canal final
c                                            (dlbd=resolution finale)
      write(3,*)' cmd: vmoy,ksig,lbda,ymoy ',vmoy,ksig,lbda,ymoy

      iucw=30+nw

        write(3,*)'nx,nscanc,scanc ',nx,nscanc,scanc
      do j=1,jm
   
      read(iucw)(tab(i,j,nt),i=1,im)              !            ********** 
c               -----------
c        write(3,*)' cmd: j,nt,tab(1,j,nt) ',j,nt,tab(1,j,nt)
      if(nx.eq.nscanc)then
         do i=1,im
            tab(i,j,nt)=tab(i,j,nt)*scanc
         enddo
      endif
      enddo
c        print *,'nw,n ',nw,n

c                  print *,' cmd: do 200: nw, tab(50,10) ',
c     1                                 nw, tab(50,10,nt)
200   continue
c------------------        protus:  calcul du bord ixlimb et soustraction diff.
      call par1('  lrdisk',1,lrdisk)
      if(iminpro.ne.0.and.lrdisk.ne.0)
     1call skyscat(tab,iitab,jjtab,nntab,im,jm,nm,lrdisk,ixlimb,kdispro)
c                                                    in    out
      write(3,*)'skyscat nw, im,   jm,    nt  ixlimb(1,jc,jm)'
 201  format(6x,7i5)
        jc=(1+jm)/2
      write(3,201)nw,im,jm,nt,ixlimb(1),ixlimb(jc),ixlimb(jm)
c---------------------------------------------
      call par1('  milsec',1,milsec)
      call par1(' subscat',1,isubscat)
c                            distance prom sky scat
        isubscat=isubscat*1000/milsec
      call par1(' maxscat',1,maxscat)
c                            max int last channel in sky
      if(isubscat.ne.0)call subscat(tab,iitab,jjtab,nntab,
     1                              im,jm,nm,isubscat,maxscat)
c                                                     to avoid disk
c----------------------------------------------
      call par1('ifixscat',1,ifixscat)                   !inutile, non .par
       ifixscat=ifixscat*1000/milsec
      call par1('  maxfix',1,maxfix)
      call par1('ivarscat',1,ivarscat)
      if(ifixscat.ne.0)call fixscat(tab,iitab,jjtab,nntab,
     1                              im,jm,nm,ifixscat,maxfix,ivarscat)
c-----------------------------------------------
      call linscat(tab,iitab,jjtab,nntab,im,jm,nm,nx,dname,ixscat)
c------------------         chargement de m
      if(icmd.gt.1.and.kana.eq.1)then
c                print *,'icmd,kana',icmd,kana    !080708
          demie=float(icmd-1)/2.
        do n=1,icmd          !   tableaux canaux
          npiv=n
          if(inverl.ne.0)npiv=icmd+1
        do j=1,jm           !   ligne j

        do i=1,im           !   point i
        channelc=ymoy/4.+ync+nwinp*(j-jc+ylinef(ic)-ylinef(i))/trj
        premier=channelc-demie

        nt=premier+0.5+npiv-1      !  le plus proche
        if(nt.ge.1.and.nt.le.nm)then
        jtab(i)=tab(i,j,nt)
        else
        jtab(i)=0
        endif
        enddo

        write(ium)(jtab(i),i=1,im)

        enddo
        enddo
c                        dernier tableau = ecart en intercanal/1000
        do j=1,jm           !   ligne j

        do i=1,im           !   point i
        channelc=ymoy/4.+ync+nwinp*(j-jc+ylinef(ic)-ylinef(i))/trj
c                                                   ---------           
        premier=channelc-demie
        nt=premier+0.5      !  le plus proche
        jtab(i)=1000.*(premier-nt)*ksig
        if(nt.lt.1.or.nt.gt.nm)jtab(i)=0
205   format(' cmd:j,nt,premier,jtab ',2i4,f6.3,i5)
          if(i.eq.50)write(3,205)j,nt,premier,jtab(i)
        enddo

        write(ium)(jtab(i),i=1,im)
        enddo

      endif   !    icmd > 1
c-----------------------------------
c      print *,' avant snorma'

      call snorma(tab,im,jm,nm,norma,iitab,jjtab,nntab,
c          ------
     1            kana,kanam,nx)
c      print *,' avant ijlis'
      call ijlis(tab,im,jm,nm,ilis,jlis,iitab,jjtab,nntab)
c          -----      
c        write(3,*)'  fin boucle fenetres'

c-------------------  calcul des profils ou bissecteurs  pour npo

c                                          boucles j,i
c      print *,'boucle 370 j1 :',j1,'j2 :',j2 
c      print *,'boucle 360 i1 :',i1,'i2 :',i2

        write(titre(1:22),'(a)')dname
c           write(3,*)' cmd: ',titre


c          xid=(i2-i1)/14.
c          yjd=(j2-j1)/14.
c      write(3,*)' i1,i2,xid, j1,j2,yjd ',i1,i2,xid, j1,j2,yjd
c---------------------------- initialisation   waves

      ccj=(float(j1)+float(j2))/2.
      do j=j1,j2
      piv=(float(j)-ccj)*2./(float(j2)-float(j1))
c      apod(j)=1.
      apod(j)=(1.-piv*piv)**2
            enddo
c        write(7,*)' apod(j),j=j1,j2,10 ',
c     1              (apod(j),j=j1,j2,10)


c        write(7,375)dname
c                                 lumiere diffusee
      call par1(' scatter',1,scatter)

      if(scatter.ne.0)then
        piv=float(scatter)/1000.
        call scattline(tab,iitab,jjtab,nntab,i1,i2,j1,j2,1,nm,
c            ---------
     1             ylinef,trj,piv)
c        call scatt    (tab,iitab,jjtab,nntab,i1,i2,j1,j2,1,nm,
c            -----
c     1             im,jm,nm,piv)
      endif

c                                 lissage profils
      if(nlis.ne.0)then
      call tablis(tab,iitab,jjtab,nntab,i1,i2,j1,j2,1,nm,nlis)
c          ------
      endif

      do380 iterw=1,iterwm    !                     boucle iterw
        do l=1,60
        cn1(l)=1.
        sn1(l)=0.
        cn(l)=1.
        sn(l)=0.
        kmoyv(l)=0
        enddo
               do l=1,60
               sa(l)=0.
               sav(l)=0.
               savv(l)=0.
               sadc(l)=0.
               sads(l)=0.
               sacp(l)=0.
               savcp(l)=0.
               sacq(l)=0.
               savcq(l)=0.
               sasp(l)=0.
               savsp(l)=0.
               sasq(l)=0.
               savsq(l)=0.
               enddo

                    write(3,*)' cmd: ync= ',ync

      call par1('  intery',1,intery)
      call par1('   disty',1,kpiv)
        disty=kpiv
        dypix=disty/pix
      call par1('  mincmd',1,mincmd)
      call par1('  maxcmd',1,maxcmd)
c---------------------------------------------
        ipp1=i1
        ipp2=i2
      if(iplotpro.ne.0.and.iprofil*jprofil.ne.0)then
        ipp1=iprofil
        ipp2=iprofil
      endif
c      if(id1*id2.ne.0)then
c         i1=id1
c         i2=id2
c      endif
      print *,'iplotpro,iprofil,jprofil ',iplotpro,iprofil,jprofil
c----
          xid=(i2-i1)/15.!+1.  !28.
          yjd=(j2-j1)/15.!+1.  !28.
      write(3,*)' i1,i2,xid,yjd ',i1,i2,xid,yjd
c=================================
            call par1('    irat',1,irat)
            call par1('nodisbis',1,nodisbis)   !   no plot disk biss
      do 370 jp=j1,j2  !      i=ipp1,ipp2   
       j=j1+j2-jp                      
c-------------------------------------
c      do j=j1,j2    
c                               boucle j1,j2
c--------------------------------------
        l1=1
c--
c      if(j.gt.j1+lg.and.j.lt.j2-ld)then
c      call spline8(i,j,nm,tab,1536,170,32,lg,ld,nlis,xp)
c          -------
c      else
c--

         nnm=4*nm-3
             if(nlis.eq.1)nnm=4*(nm-1)-3

c---

c                    transformations milalpha
c        if(milalpha.ne.1000)then   !---------------------------oui

         itest=0
c        if(intery.eq.0)then
c           call spline4(p,1,nm,xp,piv1,piv2,nlis)   !   oui
c               -------
c        else
c           call interpoly(tab,iitab,jjtab,nntab,i,j,j1,j2,1,nm,   !  non
c               ---------
c     1    intery,dypix,ltrj,xp,piv1,piv2,
c     2    iplotpro,iprofil,jprofil,kana,ides)
c        endif

c             if(piv1.lt.0.)piv1=0.
c           xp0(j)=piv1*zer
c                do n=1,nm
c                  piv3=p(n)-xp0(j)
c                  piv3=amax1(piv3,1.)
c                  tabjn(j,n)=piv3**alpha
c                enddo
c           if(i.eq.i1.and.j.eq.j1)write(3,*)' piv1,xp0',piv1,xp0(j)

c              p(n)=1000.*exp(alpha*p(n)/1000.)
c              if(i.eq.i1.and.j.eq.j1.and.n.eq.1)print *,' pexp ',p(n)

c         if(p(n).ge.32000.)then
c        if(p(n).eq.32000)then
c           if(itest.eq.1)call pgadvance 
c           goto344
c        endif
c        endif                    
c      enddo
c---           fin boucle j, p stocke dans tabj
c----------------------------------------------
c          jjc=(j1+j2)/2
c        do l=1,60
c        u(l)=0
c        enddo
c=============
      do 360 i=ipp1,ipp2  !jp=j1,j2       !-- boucle i (incluse boucle i -> 370)

        do n=1,nm
        p(n)=tab(i,j,n)
c       ----
c           if(p(n).ge.31000)p(n)=1
           p(n)=abs(p(n))
c           tabjn(j,n)=p(n)
c            ***
        enddo

c test dessin
c         kana     ides    itest    iplotpro
c            1        0                        aucun dessin
c         ne 1        0                        aucun dessin
c            1        1        1           0   plots multiples
c            1        1        0           1   dessin iabs(intery)=20 ou 1
c            1        1        0       2,3,..   dessin points voisins
       itest=0

c          xid=(i2-i1)/12.+1.  !28.
c          yjd=(j2-j1)/12.+1.  !28.
c      write(3,*)' i1,i2,xid,yjd ',i1,i2,xid,yjd

c ---  autour icplot,jcplot
      if(icplot.ne.0.and.njm.ne.0)then
              nic=njm/2+1
              njc=njm/2+1
           do njd=1,njm !3
           do nid=1,njm !3
           idd=icplot+(nid-nic)*icstep
           jdd=jcplot+(njd-njc)*jcstep
           if(i.eq.idd.and.j.eq.jdd)goto 211
           enddo
           enddo
        itest=0
        goto 215
 211  itest=1
c        write(3,*)'iplot,jplot  ',i,j
      goto 215
      endif
c  ---

       if(ides.ne.0.and.kana.eq.1.and.iplotpro.eq.0)then
c          if(i.eq.i1.and.j.eq.j1)goto210
c          if(i.eq.i2.and.j.eq.j2)goto210
            do nid=1,100  !56   !28
            idd=i1+xid*nid
            jdd=j1+yjd*njd
            if(i.eq.idd.and.j.eq.jdd)goto210
            enddo
          itest=0             !   profil pour ce point
          goto215
210     itest=1
        endif
215     continue

      if(itor.ne.0)itest=0    ! tornado
      if(i.eq.itor.and.j.eq.jtor)itest=1


      if(itest.eq.1)write(3,*)'  i,j,iplotpro,ides,itest ',
     1                           i,j,iplotpro,ides,itest

        if(itest.eq.1)then
          write(titre(23:26),'(i4)')i
          write(titre(27:30),'(i4)')j

        call pgsch(3.)
        endif

c                                corrections liscurv
c      if(liscurv.gt.0.and.liscurv.ne.3)then     !  non
c      call curv(tabjn,liscurv,ltrj,nwinp,
c          ----
c     1           j,j1,j2,nm,
c     2           xp,nnm,piv1,piv2,nlis,jjtab)
c      goto220
c      endif

c---           
c      if(liscurv.eq.3)then   !  non
c      call diff(tabjn,liscurv,ltrj,nwinp,
c          ----
c     1            j,j1,j2,nm,
c     2            xp,nnm,piv1,piv2,nlis,jjtab)
c      goto220
c      endif
c---              cas sans corrections curv ou diff


c        if(intery.eq.0)then
        if(ispline3.eq.0)then
        call spline4(p,1,nm,xp,piv1,piv2,nlis)   !  oui
c            -------
        else
        call spline3(p,nm,xp,nnm)
        endif
c        else
c        if(intery.ne.0)then
c        call interpoly(tab,iitab,jjtab,nntab,i,j,j1,j2,1,nm,
c            ---------
c     1         intery,dypix,ltrj,xp,piv1,piv2,
c     2         iplotpro,iprofil,jprofil,kana,ides)
c        endif
c          write(3,*)'apres spline4' !!!!!!!!!!!!!!!!
c-----
c                  le profil est dans xp
c                  on effectue la transformation inverse milalpha 
220       continue   !      piv=1./alpha

c          do n=1,nm
c          p(n)=tabjn(j,n)
c          enddo

        do nn=1,nnm
         if(xp(nn).lt.0.)xp(nn)=abs(xp(nn))

c        if(milalpha.ne.1000)then
c              xp(nn)=piv*1000.*alog(xp(nn)/1000.)
c        xp(nn)=xp(nn)**piv+xp0(j)        
c        endif

        enddo
c--
      if(itest.eq.1.and.kana.eq.1)then
      if(i.eq.iprofil.and.j.eq.jprofil)print *,' xp ',(xp(k),k=1,nnm)
       write(3,*)'itest,i,j,itor,inverl',itest,i,j,itor,inverl
      
                 call protest(xp,nnm,mincmd,maxcmd,itor,inverl)
c                     -------                      tornado
      call pgsch(2.5)
      if(itor.eq.0)call pglabel(' ',' ',titre)  


        write(3,*)'                                       ',dname
        write(3,*)' protest: ylinef(i),trj,i,j ',
     1                       ylinef(i),trj,i,j
        write(3,*)'          profil p ',(p(nn),nn=1,nm)
        write(3,*)'          profil xp ',(xp(nn),nn=1,nnm)
      endif                !                    itest
230   continue 
        ksig=1
        if(inverl.ne.0)ksig=-1

c            centre raie calcule par kminm avec ancien ncm
c      yyc=4.*(ncm+float(kminm-1000)*nwinp/100.
c     1            +nwinp*(j-0.5*(1.+jm)-ylinef(i))/trj)  -3.       
c        nc100=xnc*100.
c rel   -----          ne sert que pour preciser le centre raie dans cmd 
c     yyc=4.*(ync+nwinp*(j
c     ---     ---        -
c     1             -ylinef(i))/trj) -3.       
c                   -------------------
c          centre raie calcule par centrage ync, et variation yline en i
        ic=0.5*(1+im)         
      yyc=4.*(ync+nwinp*(j-jc+ylinef(ic)-ylinef(i))/trj)-3.   
c     ---                                ========
      ksig=1
      if(inverl.ne.0)ksig=-1
      corryc=-mcorrec*lbda*ksig/(75000.*float(dlbd))
      yyc=yyc+corryc           !  correction yyc*********

        if((i.eq.i1.or.i.eq.i2).and.(j.eq.j1.or.j.eq.j2))write(3,*)
     1      ' cmd: ync,j,ylinei,ylineic,trj,yyc ',
     2             ync,j,ylinef(i),ylinef(ic),trj,yyc
             do l=1,60
             v(l)=0.
             enddo
c                                             center,sum
      if(center.eq.1)then
         dlbcalc(1)=0
c        ------------
        ivw(1)=1
c      if(icub9.eq.0)then
      call pinter(xp,nnm,yyc,ymoy,piv,cosinus,sinus,1,
c          ------       ---  ----     in  
     1            iph,cn,sn,nosol)   
c                 out
c      else
c      call cub9(p,nm,yyc,ymoy,piv,nosol)
c      endif
         if(nosol.eq.1)then
            nfail(j)=nfail(j)+1
            v(1)=u(1)
            v(1+lm)=u(1+lm)
         else
            v(1)=piv   !+0.5  !10.*sqrt(piv) 
            v(1+lm)=iph
         endif

c prominences
c      if(minpro.ne.0.and.maxcont.ne.0)then
c        contin=0.5*(xp(1)+xp(nnm))
c      if(contin.gt.float(maxcont))then 
c         v(1)=0.
c      endif
c      endif
c------

      if(itest.eq.1)then
c      write(3,*)' xp  ',(xp(nn),nn=1,nnm)
      write(3,*)' nnm,yyc,ymoy,piv ',nnm,yyc,ymoy,piv 
      call pgsch(2.)
       pivx=yyc+ymoy
      call pgpoint(1,pivx,piv,24)
c          -------
      endif
      endif
c----                         somme canaux
           lsum=0
      if(sum.eq.1)then
      lsum=1
      kpiv=0
        do n=1,nm
        kpiv=kpiv+p(n)+0.5
        enddo
      l=center+l1
        ivw(l)=1
      v(l)=0.5+float(kpiv)/float(nm)
      v(l+lm)=0
      endif
c----                         canal kn
      if(sum.lt.0)then
      lsum=1
      kn=-sum
        if(kn.gt.nm)then
        print *,' iabs(sumd) or iabs(sumr) > number of channels'
        stop
        endif
      l=center+l1
        ivw(l)=1
      v(l)=p(kn)
      endif
c----
c                                              profiles
c      print *,'etape profil' !!!!!!!!!!!
      ksig=1
      if(inverl.ne.0)ksig=-1

      if(lmp(1).ne.0)then !!!!!!lmo -> lmp
        do lb=1,lmp(1)                      ! boucle profils
        xlbd=xlbd1(1)+(lb-1)*xlbdstep(1)

c                                     point +
        l=l1+center+lsum+2*(lb-1)
        dlbcalc(l)=xlbd+0.001
c       ---------------------
          ivw(l)=1
        yy=yyc+xlbd*ksig
c          ---
             xdes(2)=yy+ymoy
      call pinter(xp,nnm,yy,ymoy,piv,cosinus,sinus,l,
c          ------       ---  ----     in  
     1            iph,cn,sn,nosol)   
c                 out

c         if(i.gt.130.and.j.eq.8)write(7,*)' i,j,cn(1),sn(1)',
c     1                                      i,j,cn(1),sn(1)

        if(nosol.eq.1)then
             nfail(j)=nfail(j)+1
             v(l)=u(l)
             v(l+lm)=u(l+lm)
        else
             v(l)=piv
             ydes(2)=piv
             v(l+lm)=iph
        endif
c                                     point -
        yy=yyc-xlbd*ksig
c     ---
        dlbcalc(l+1)=-dlbcalc(l)
c       ------------------------
        xdes(1)=yy+ymoy
      call pinter(xp,nnm,yy,ymoy,piv,cosinus,sinus,l+1,
c          ------       ---  ----     in  
     1            iph,cn,sn,nosol)   
c                 out
           ivw(l+1)=1
        if(nosol.eq.1)then
          nfail(j)=nfail(j)+1
          v(l)=u(l)
          v(l+lm)=u(l+lm)
        else
             v(l+1)=piv
             ydes(1)=piv   
             v(l+1+lm)=iph
        endif
          if(itest.eq.1)then
          call pgline(2,xdes,ydes)
c              ------
          endif

        enddo                        ! fin boucle profils
      endif

c                                    sum and diff +/- xlbd
      if(lmp(2).ne.0)then
        do lb=1,lmp(2)
        xlbd=(xlbd1(2)+(lb-1)*xlbdstep(2))
        l=l1+center+lsum+2*(lb-1+lmp(1))
          ivw(l)=1
        yy=yyc+xlbd*ksig
c          ---
            xdes(2)=yy+ymoy
      call pinter(xp,nnm,yy,ymoy,piv1,cosinus,sinus,l,
c          ------       ---  ----     in                 droite
     1            iph,cn,sn,nosol)   
c                 out

        if(nosol.eq.1)then
            nfail(j)=nfail(j)+1
            v(l)=u(l)
            v(l+lm)=u(l+lm)
        else
            v(l)=piv1
            v(l+lm)=iph
            ydes(2)=piv1
        endif
        yy=yyc-xlbd*ksig
c          ---
             xdes(1)=yy+ymoy
      call pinter(xp,nnm,yy,ymoy,piv2,cosinus,sinus,l+1,
c          ------       ---  ----     in                 gauche
     1            iph,cn,sn,nosol)   
c                 out    
        if(nosol.eq.1)then 
            nfail(j)=nfail(j)+1
            v(l)=u(l)
            v(l+lm)=u(l+lm)
         else
            ivw(l+1)=1
            v(l+1)=piv2
            v(l+1+lm)=iph
            ydes(1)=piv2
        endif

        if(piv1.ne.32000..and.piv2.ne.32000.)then
          v(l)=(piv1+piv2)/2.
          v(l+1)=(piv2-piv1)/2.                 !1/2  droite-gauche
        endif

        if(isigdif.eq.1)then
          yy=yyc
        call pinter(xp,nnm,yy,ymoy,piv3,cosinus,sinus,l,
c            ------
     1              iph,cn,sn,nosol)
        if(v(l).lt.piv3)v(l+1)=-v(l+1)
        endif

          if(itest.eq.1)then
          call pgline(2,xdes,ydes)
c              ------
          endif

        enddo
      endif
c                                 bissecteur
c                                 ----------
c      print *,'etape bissecteur' !!!!!!!!!!!!!

      if(lmp(3).ne.0)then

      lint=l1+center+lsum+2*(lmp(1)+lmp(2))          !   premiere intensite/vit
c     ****
      if(i.eq.ipp1.and.jp.eq.j1)write(3,*)' msdp3(1343) lint= ',lint 
       
        do lb=1,lmp(3) !                            lb
        xlbd=xlbd1(3)+(lb-1)*xlbdstep(3)
c         l=l1+center+lsum+2*lmp(1)+2*lmp(2)+3*(lb-1)
c      lint=l1+center+lsum+2*(lmp(1)+lmp(2))          !   premiere intensite/vit
c     ****

      l=lint+2*(lb-1)           !l1+center+lsum+2*(lb-1+lmp(1)+lmp(2))
      dlbcalc(l)=xlbd+0.001
c     ---------------------
      dlbcalc(l+1)=-dlbcalc(l)
c     ------------------------

        lp=l+1
          ivw(l)=1
          ivw(lp)=2
c              if(i.eq.i1)print*,' l1,center,lsum,lb,lmp(1),lmp(2),lp ',
c     1                           l1,center,lsum,lb,lmp(1),lmp(2),lp 

c modif 18/11/99                        iabsor
c          if(minab.ne.0)then
c          call ab_em(xp,nnm,minab,minem,maxem,iabsor)
c                                             out

      if(dlimb.ne.0)then   !              protus   2013
            if(xp(1).eq.0)then
            nosol=1
            goto 240
            endif
               if(tab(i,j,1).gt.0)then  !  disque
            iabsor=1
            endif
             if(tab(i,j,1).lt.0)then    !  protu
            iabsor=-1
            endif
      endif
c          do n=1,nnm
c          xp(n)=abs(xp(n))
c          enddo
      if(i.eq.i1.and.jp.eq.j1)write(3,*)'call IV xlbd',xlbd
      
          call iv(xp,nnm,xlbd,v(l),v(lp),iabsor,yyc,spimin,spimax,
c              --                        in
     1           sminmax,discmin,fond,vfond,
     2           cosinus,sinus,l,iph,cn,sn,nosol,itest,   0,
c                                           out  dessin,icontv,
     3              0   ,  0      ,idr,jemabs)
c                minpro,mincont   fichiers D ou R (0/1)
          jjemabs(i,j)=jemabs   !  em/abs
          if(jemabs.eq.-1.and.facpro.ne.0.)v(l)=v(l)*facpro  !int protu*facpro
          if(v(l).gt.32000.)v(l)=0.

c          if(iminpro.ne.0.and.v(1).lt.iminpro)then
c             v(l)=0.
c             v(lp)=0.
c          endif
          
 240   if(nosol.eq.1)then    !.and.spimax.eq.0.)then
                 nfail(j)=nfail(j)+1
         v(l)=0.
         v(lp)=0.
         v(lp+1)=0.
        else
c-
         if(itest.eq.1)then
         write(3,*)' protest iv: lbd,i,v(mps) ',lbd,v(l),v(lp)
         endif
        if(itest.eq.1.and.iabs(iabsor).lt.2)then
          ydes(1)=v(l)
          ydes(2)=v(l)
          xdes(1)=v(lp)-xlbd
          xdes(2)=v(lp)+xlbd                
c      write(3,*)' iv: xdes,ydes ',xdes,ydes
        call pgslw(2)
        call pgsls(2)
        call pgline(2,xdes,ydes)
        call pgsls(1)
        call pgpoint(2,xdes,ydes,2)
          xdes(1)=(xdes(1)+xdes(2))/2.
          xdes(2)=xdes(1)
          ydes(2)=0.
        call pgpoint(1,xdes,ydes,5)
c          call pgsls(2)
        call pgline(2,xdes,ydes)
c          call pgsls(1)
          pivx=xdes(1)+1.
          pivy=ydes(1)+5.
c        call pgpoint(1,pivx,pivy,2233)
          pivx=pivx+0.5
          pivy=pivy-3.
        call pgsch(1.)
        if(l.eq.1)call pgtext(pivx,pivy,'0.3 A')
        endif   !  itest

        if(jemabs.eq.-1.and.negpro.ne.0)v(l)=-v(l)   !    int < 0 pour emission
c                             0 general
c        if(iabsor.eq.0.and.dlimb.ne.0)v(l)=-v(l)   !    int < 0 pour emission
        v(lp)=v(lp)-yyc
c     ----
                    piv=lbda
            v(lp)=v(lp)*ksig*75000.*dlbd/(float(mps)*piv)
c                                               ---
        if(idr.eq.1) v(lp)=v(lp)-vmoy
c     ----
               if(v(l).lt.0.)then
         v(l)=0.
         v(lp)=0.
         v(lp+1)=0.
      endif                
c--------------------------------
        if(iminpro.ne.0)then    !.and.l.eq.lint+2)then    !  protus    
         piv1=float(iminpro)     !/2.

           npiv=1+2*lmp(1)                      ! 1+2*8=17
        piv2=v(1)-(v(npiv-1)+v(npiv))/2.       !   centre-(i16+i17)/2

        if(j.eq.jm/2.and.v(1).gt.piv1) write(3,*)
     1      'npiv,v(1),v(npiv),v(npiv-1) ',npiv,v(1),v(npiv),v(npiv-1)
c----
        if(piv2.gt.piv1)then             !   protu acceptable
           v(lp+1)=v(l)+intadd
c     if(i.eq.ixscat(j)) v(lp+1)=linvisu  !tb(i,j,lm)= linvisu !  linvisu
        else         
          v(l)=0.
          v(lp)=0.
          v(lp+1)=0.
c     if(irat.eq.1)then
c             v(lint)=0.
c             v(lint+1)=0.
c        endif
       endif                               !  acceptable
c----
      endif            ! iminpro
c        if(i.eq.500.and.j.eq.50)write(3,*)
c     1        'lint,l,v(lint),v(l)',lint,l,v(lint),v(l)

c        if(irat.eq.1.and.v(lint).ne.0.)then    !non
c           v(lint)=1000.*v(l)/v(lint)
c           v(lint+1)=v(lint+1)-v(lp)
c        endif

        if(nodisbis.eq.1)then
          if(jemabs.eq.1)then 
c                disque
          v(l)=0.
          v(lp)=0.
          v(lp+1)=0.
c          v(lint)=0.
c          v(lint+1)=0.
          endif
        endif
       
c      endif
c--------------------------------------------
        endif   !   nosol

c prominences
c      if(minpro.ne.0.and.maxcont.ne.0)then
c      if(minpro.ne.0.and.idr.eq.1)then   !  fichiers R
c                      contvd  pris en compte dans IV pour disque
c        contin=0.5*(xp(1)+xp(nnm))
c          pro=0.
c        do n=1,nnm
c        pro=amax1(xp(n),pro)
c        enddo
c          protu=pro-contin

c      if(i.eq.jp.and.protu.gt.200.)
c     1 print *,'    maxcont,minpro,contin,protu',
c     2                   maxcont,minpro,contin,protu

c      if(protu.lt.float(minpro))then
c         v(l)=0.
c         v(lp)=0.
c      endif
c      endif
c-------
      enddo           !  lb      
      endif
c-----
c     Intensity increase intadd for prominences in the last I/V intensity map
c     ========================================================================
c        llm=lm
c        if(iminpro.ne.0)llm=lm-1
      do l=1,lm   !llm
        kpiv=v(l)
        if(iabs(kpiv).gt.32000)kpiv=0    !32000
        tb(i,j,l)=kpiv
      enddo

c      if(iminpro.ne.0)then
c        if(tb(i,j,lm-2).gt.iminpro) tb(i,j,lm)=tb(i,j,lm-2)+intadd
c        if(i.eq.ixscat(j)) tb(i,j,lm)= linvisu !  linvisu
c      endif
c  -----------
c      tb(i,j,l+lm)=v(l+lm)
c                  phases
c      enddo                     !   l

c                        valeurs en j stockees pour j+1
        if(j.ne.j2)then

        do l=1,60
        u(l)=v(l)
        cn1(l)=cn(l)
        sn1(l)=sn(l)
        enddo

      endif

      ipiv=(ipp1+ipp2)/2
      jpiv=(j1+j2)/2
      if(i.eq.ipiv.and.j.eq.jpiv)then
         write(3,*) 'i,j,lm,tb(i,j,lm-2),tb(..lm-1),tb(..lm)',
     1        'iminpro,nodisbis,jemabs,intadd,linvisu'
         write(3,*)i,j,lm,tb(i,j,lm-2),tb(i,j,lm-1),tb(i,j,lm),
     1 iminpro,nodisbis,jemabs,intadd,linvisu
      endif   
      
360   continue

c                    end of loop l1,i2   ----------------------
370   continue
c                        end of loop j1,j2  ----------------------
c                        =================

c     Wavelength departures at first calculations  dlbcalc(l)
c     ========================================================
              call par1('    dlbd',1,dlbd)   ! 300
              call par1('   lbd1d',1,llbd) !  250
        
           if(iminpro.ne.0)then
              dlbcalc(lm)=dlbcalc(lm-2)
           endif
 
           kpiv=float(dlbd)*float(llbd)/1000.+0.001   !75
        do l=1,lm
           dlbcalc(l)=dlbcalc(l)*kpiv
        enddo
         write(3,*)' cmd: llm, dlbcalc ',llm,(dlbcalc(l),l=1,lm)

c-----------------------------------------------------------
374   format('     l   rw1   rw2   rw3   aw1',
     1        '   aw2   aw3   bw1   bw2   bw3   moy sigma ')
375   format(a24)
376   format(13i6)
c                                   Si 1ere iteration iterw              
c      if(lwref(1).ne.0.and.iterw.eq.1)then

c       write(7,374)

380   continue    !        fin boucle iterw

      write(3,*)' cmd1: l, phases iph=tb(50,j,l+lm)'
379   format(20i5)
      do l=1,5
      write(3,379)l
c      write(3,379)(tb(50,j,l+lm),j=j1,j2)
      enddo

c------------------------------ chargement d 

      write(3,*)' Fin de boucle i,j '

      if(idessin_correc.ne.0)then
c dessin coupes en j
      if(ides.ne.0)then
               ic=(i1+i2)/2
               jmax=j2-j1+1
               xj1=1.
               xj2=jmax
       do l=1,5

        cmin=100000.
        cmax=-100000.
      do j=j1,j2
        jp=j-j1+1
        xxdes(jp)=jp
      yydes(jp)=tb(ic,j,l)
      cmin=amin1(yydes(jp),cmin)
      cmax=amax1(yydes(jp),cmax)
      enddo

      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)
      call pgwindow(xj1,xj2,cmin,cmax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
      call pglabel('pixels j','output d',' ')

      call pgline(jmax,xxdes,  yydes)
c          ------      j       coupe sortie l de d
      if(l.eq.5)call pgadvance
      enddo
      endif
      endif

c------------                   corrections crec=lisond
c                               -----------------------
      if(kana.eq.1)then

c     crec=lisond     = 0 rien
c     crec = lisond   <-1 corr. moy. sur 2*(-lisond/pix)+1 points en i
c                      -1 corr jd,jg
c                      >0 calcul fourier et lissage 2*lisond/pix+1
c                            pour harmoniques determines par lwref(nh)
c                         lwref  2 correc fourier fct 1 param (4eme degre)
c                                1 correc directe calculee plus haut
c                                3 regression 3 param et correc Fourier
c                               >3 regression 3 param et lwref bandes phases
c                                   (lwref<36)

c         if(iabs(lisond).gt.1)lisond=float(lisond)/pix

       xjgd=float(ltrj)/(100.*nwinp)   ! periode

      if(lisond.eq.-1)then    !     calcul et correc jd,jg   non
c        -----------
       jd=xjgd/4.+1
       jg=jd
       ud=jd-xjgd/4.
       vd=1.-ud
         jp1=max0(j1,1+jg)
         jp2=min0(j2,jm-jd)

      do l=1,lm
      do i=i1,i2

        do j=1,jm
        jtab(j)=tb(i,j,l)
        enddo

        do j=jp1,jp2
c        tb(i,j,l)=0.5*(jtab(j-jg)+jtab(j+jd))
       tb(i,j,l)=0.5*(vd*(jtab(j-jg)  +jtab(j+jd))
     1               +ud*(jtab(j-jg+1)+jtab(j+jd-1)))
        enddo

      enddo
      enddo      
      endif

      if(lisond.lt.-1)then   ! calc et corr. moy. sur 2*lisond/pix+1 points en i
c        -----------
        ig=-float(lisond)/pix
        id=ig
        jd=xjgd/4.+1.
        jg=jd

       ud=jd-xjgd/4.
       vd=1.-ud
         jp1=max0(j1,1+jg)
         jp2=min0(j2,jm-jd)

      do l=1,lm

      do j=jp1,jp2
      do i=i1,i2
        ip1=max0(i-ig,i1)
        ip2=min0(i+id,i2)
        piv=0.
        spiv=0.
          do ip=ip1,ip2
c modif 010508
          xpiv=float(ip-i)/float(ig+1)
          xpiv=1.-xpiv*xpiv
          xpiv=xpiv*xpiv
          piv=piv+xpiv
          spiv=spiv+xpiv*
     1         (0.5*(vd*(tb(ip,j-jg,l)  +tb(ip,j+jd,l))
     2              +ud*(tb(ip,j-jg+1,l)+tb(ip,j+jd-1,l)))
     3         -tb(ip,j,l))
          enddo
        spiv=spiv/piv
        ijtab(i,j)=tb(i,j,l)+spiv
      enddo
      enddo

      do j=jp1,jp2
      do i=i1,i2
        tb(i,j,l)=ijtab(i,j)
      enddo
      enddo

      enddo
      
      endif

      if(lisond.gt.0)then
c        ----------------
          if(lm.gt.60)then
          print *,' correction crec impossible avec lm>60'
          stop
          endif

        lpar2=l1+center+lsum+2*(lmp(1)+lmp(2))   !  pour paramfou

      ifour=float(lisond)/pix
        if(ifour.gt.499)ifour=499

      do390 nh=1,3      !           boucle harmoniques  1=periode 2*trj
c                                                       2=          trj
c                                                       3=          trj/2
      if(iw(nh).eq.0)goto390

      harm=2./2.**(nh-1)
      per=xjgd*harm
      jfour=per+0.5
        write(3,*)' cmd avant four: nh,harm,per ',nh,harm,per
      call four(tb,iitab,jjtab,lltab,i1,ia,ib,i2,j1,ja,jb,j2,lm,
c     ---------                      in out   in    out   in
     1          nh,per,ifour,jfour,iw(nh),            nx)
c                                      1=correc appliquees  write

c                         Dans tb(i,j,l) se trouvent
c                            la valeur v                dans le tableau l
c                            la phase iph (1/1000 canal)               l+lm
c                            l'amplitude de correction amp             l+2*lm

      write(3,*)' four: ia,ja,(tb(ia,ja,l),l=1,20) ',
     1                  ia,ja,(tb(ia,ja,l),l=1,20)
c-------
c    les corrections sont appliquees pour l'harmonique nh
c    si iw(nh).ne.0:       =1  application directe (=filtrage spatial)
c                          =2  correc fonction du param 1
c                          =3  regression 3 et Fourier
c                          >3  regression 3 et bandes de phases

      if(iw(nh).eq.2)then
          if(lm.lt.lpar2+2)then
      print *,
     1' crec>0 and iw>1 imply line center and 2 bisector calculations'  
          stop
          endif

      call corfou(tb,iitab,jjtab,lltab,i1,ia,ib,i2,j1,ja,jb,j2,lm,
c          ------
     1            ides,nh,lpar2,labh(nh))
      endif

      if(iw(nh).ge.3)then
          if(lm.lt.lpar2+2)then
      print *,
     1' crec>0 and iw>1 imply line center and 2 bisector calculations'
          stop  
          endif

      call corfou3(tb,iitab,jjtab,lltab,i1,ia,ib,i2,j1,ja,jb,j2,lm,
c          -------
     1       ides,iw(nh),nscan,mgrim,xdeb,xgri,txgri,txana,
c            eco
     2       nh,lpar2,labh(nh))
c            1,2 ou 3
      endif
 
390   continue
      endif    !    fin lisond>0


      endif    !    fin test kana
400   continue
c                   ------------

      if(kana.eq.1.and.kanam.ne.1)
     1  call writet(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm)
c            ------

      if(kana.ne.1)
     1  call readt(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm)
c            -----
c correction cheveu
      call cheveu(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm,jhair,milsec)
c          ------

c modif stokes avril 00
      if(mgrim.eq.0)then                            !  mgrim

c corrections disque si fond de ciel
      if(mincent.ne.0)
     1 call regresdisc(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm,mincent,
     2                 ijtab2,iecri7,kind,xregres,yregres,poids)
c                                         economie.............
c param protus (meudon)   tableaux 1,2,3 (+/- blue-red), lm+1,lm+2
      call par1('  promax',1,kpromax) ! max int protus
      call par1('  disque',1,kdisque) ! coef pour image int disque
        disque=float(kdisque)/1000.
      call par1('  skymax',1,kskymax)    ! max fond ciel
      call par1('  discat',1,kpiv)    ! coef lumiere diffusee disque 
        discat=float(kpiv)/1000.
      call par1('  promin',1,kpromin)
      call par1(' milvmax',1,milvmax)
c---------------------------------------------
      do l=1,lm                     ! boucle l /mgrim
381   format(' l=',i2)
      write(3,381)l

        do j=j1,j2                  ! boucle j / l / mgrim

          do i=i1,i2
          ijtab(i,j)=tb(i,j,l)
          enddo

c corrections protus
c / j l mgrim
c int centre raie
      if(l.eq.1.and.kpromax.ne.0)then
          nd=0
          sy=0.
        do i=i1,i2
          ijtab(i,j)=0.
        kpiv=tb(i,j,1)
        sy=sy+kpiv
        enddo
          sy=sy/float(i2-i1+1)

        do i=i1,i2
          kpiv=tb(i,j,1)
          kskym=kskymax+discat*sy
          if(kpiv.lt.kskym)then
          nd=nd+1
          poids(nd)=1.
          xregres(nd)=i
          yregres(nd)=tb(i,j,2)    !  int 2
          endif
        enddo
 
             coef(1)=0.
             coef(2)=0.
         if(nd.gt.1)then
c         if(nd.gt.(i2-i1)/3)then
c          call DPMCAR(xregres,yregres,poids,nd,2,coef)
c         else
          call DPMCAR(xregres,yregres,poids,nd,1,coef)
c         endif
         endif
 385     format('prominences: l,j,sy,nd,coef ',2i4,f6.0,i6,2f8.3)
             write(3,385) l,j,sy,nd,coef
          do i=i1,i2
          ktab=tb(i,j,2)
          correct=coef(1)+coef(2)*i
            if(ktab.gt.kpromax)then
            ijtab(i,j)=tb(i,j,1)*disque
            else
            ijtab(i,j)=tb(i,j,1)-correct
            endif
              ijtab2(i,j,1)=ijtab(i,j)
          enddo        
      endif            ! fin centre raie

c int +/-blue red
      if(l.eq.2.and.kpromax.ne.0)then    !  non
          nd=0
          sy=0.
        do i=i1,i2
          ijtab(i,j)=0.
        kpiv=tb(i,j,2)
        sy=sy+kpiv
        enddo
          sy=sy/float(i2-i1+1)

        do i=i1,i2
          kpiv=tb(i,j,2)
          kskym=kskymax+discat*sy
          if(kpiv.lt.kskym)then
          nd=nd+1
          poids(nd)=1.
          xregres(nd)=i
          yregres(nd)=tb(i,j,2)    !  int 2
          endif
        enddo
 
             coef(1)=0.
             coef(2)=0.
         if(nd.gt.5)then
c         if(nd.gt.(i2-i1)/3)then
          call DPMCAR(xregres,yregres,poids,nd,2,coef)
c         else
c          call DPMCAR(xregres,yregres,poids,nd,1,coef)
c         endif
         endif
             write(3,385) l,j,sy,nd,coef
          do i=i1,i2
          ktab=tb(i,j,l)
          correct=coef(1)+coef(2)*i
            if(ktab.gt.kpromax)then
            ijtab(i,j)=0
c            ijtab(i,j)=(tb(i,j,2)-correct)*disque
            else
            ijtab(i,j)=tb(i,j,2)-correct
            endif
              ijtab2(i,j,2)=ijtab(i,j)
          enddo        
      endif             ! fin int +/-
c diff +/- blue-red
      if(l.eq.3.and.kpromax.ne.0)then

          sy=0.
        do i=i1,i2
          ijtab(i,j)=0.
        kpiv=tb(i,j,2)
        sy=sy+kpiv
        enddo
          sy=sy/float(i2-i1+1)

          nd=0
        do i=i1,i2
          ijtab(i,j)=0.
          kpiv=tb(i,j,2)
          kskym=kskymax+discat*sy

          if(kpiv.lt.kskym)then
          nd=nd+1
          poids(nd)=1.
          xregres(nd)=i
          yregres(nd)=tb(i,j,3)     !  dif 3
          endif
        enddo
 
         if(nd.gt.5)then
c        if(nd.gt.(i2-i1)/3)then
          call DPMCAR(xregres,yregres,poids,nd,2,coef)
c         else
c         call DPMCAR(xregres,yregres,poids,nd,1,coef)
c         endif
         endif
             write(3,385)l,j,sy,nd,coef

            do i=i1,i2
            kpiv=tb(i,j,2)
            if(kpiv.lt.kpromax)then
              piv=tb(i,j,3)-(coef(1)+coef(2)*i)
              ijtab(i,j)=-piv                    
c                        changement de signe pour les protus
            endif
              ijtab2(i,j,3)=-piv
            enddo
      endif           !  fin diff +/-   / protus  j l mgrim

c    modif 20/11/99    
      enddo            !   fin j / l mgrim
c------------------------------------------------centrage sur moyennes X
      if(icentre.eq.1)then
       do j=j1,j2
       do i=i1,i2
         kdisc(i,j)=1
       if(mindisc.ne.0.and.l.eq.1.and.ijtab(i,j).lt.mindisc)kdisc(i,j)=0
c           (0)
       enddo
       enddo
c      call xxcentre(ijtab,i1,i2,j1,j2,kdisc)
       call centrej(ijtab,i1,i2,j1,j2,kdisc,ylinef,trj,milsigc)
      endif
c-----------------------------------------------correction profils disque
      if(iminpro.eq.0)then   ! disk alone
         do j=1,200
            ixlimb(j)=i2
         enddo
      endif
      if(icordisk.eq.1)then
      call scordisk(ijtab,ixlimb,jjemabs,i1,i2,j1,j2,trj,kdispro)
      endif  

      do j=j1,j2
      do i=i1,i2
        tb(i,j,l)=ijtab(i,j)
      enddo
      enddo

      enddo          !   boucle l  jusqu'a lm   / mgrim
c--------------------------------------------------------
c     computation of lold(l) for increasing wavelength departures
c     ===========================================================
c                  pour I/V
      do l=1,lm
      lold(l)=l
      enddo
c                  pour profils
      lmpa=lmp(1)
c  ex                        lmpa=4          *
c                            l       1 2 3 4 5 6 7 8 9
c                            lold    9 7 5 3 1 2 4 6 8
c                            lmpa=1  2 1 3      
c                                      *
c                            lmpa=2  4 2 1 3 5     
c                                        *
c      if(lmpa.ne.1)then
          do l=1,lmpa
          lold(l)=2*(lmpa-l)+3
          enddo
              lold(lmpa+1)=1
          do l=lmpa+2,2*lmpa+1
          lold(l)=2*(l-lmpa)-2
          enddo
c      endif
      print *,'lold ',(lold(l),l=1,lm)
      write(3,*)' lm,lold ',lm,(lold(l),l=1,lm)
c-------------------------------------------
      do j=j1,j2
      do i=i1,i2
        do l=1,lm
          lo=lold(l)
        ttab(l)=tb(i,j,lo)
        enddo
        do l=1,lm
           tb(i,j,l)=ttab(l)
c           tb in new order
        enddo
      enddo
      enddo
c-----------------------------------------------
c     Headers of d-files
c     =======
      do i=1,512
      kz(i)=0
      enddo

      kz(1)=4
      kz(2)=i2-i1+1
      kz(3)=j2-j1+1
      kz(4)=lm

      kzm=kz(1)+lm
      do l=1,lm
         lo=lold(l)
c         dlb(l)=dlbcalc(lo)
         kz(l+4)=dlb(lo)
      enddo

      write(iud)(kz(i),i=1,512)
      write(3,*)' header d ',(kz(i),i=1,lm+4)
c-----------------------------------------------------------
c     tables for d-files
c                =======
c ecriture tous les cas non magnetiques(protu ou non)
c                on ecrit seulement les lignes entre j1 et j2 
      write(3,*)'write(iud) lm,i1,i2,j1,j2 ',lm,i1,i2,j1,j2

      if(iminpro.ne.0)then
      do j=j1,j2                           !    ixscat linvisu
            ipiv=ixscat(j)
            tb(ipiv,j,lm)=linvisu
            write(3,*)'j,ixscat,tb  ',j,ipiv,tb(ipiv,j,lm)
      enddo
      endif
     
      do l=1,lm
         do j=j1,j2
           write(iud)(tb(i,j,l),i=i1,i2)
          if(j.eq.(j1+j2)/2)then
          write(3,*)' iud: l,tab(i...,jc,l) ',l,(tb(i,j,l),i=i1,i2,100)
          write(7,*)' iud: l,tab(i...,jc,l) ',l,(tb(i,j,l),i=i1,i2,200)
          endif   
       enddo
c 389   format('file d ',20i6)
c        if(l.eq.lm)write(3,389)(tb(i,j2,lm),i=i1,i2,100)
         if(l.eq.lm)then
           write(3,*)' '
           write(7,*)' '
         endif
      enddo                     !   boucle l  jusqu'a lm   / mgrim
      lcc=lm/2
      do j=j1,j2,20
         write(3,*)' iud(lm/2): j,tb ',j,(tb(i,j,lcc),i=i1,i2,50)
         write(7,*)' iud(lm/2): j,tb ',j,(tb(i,j,lcc),i=i1,i2,200)
      enddo
c===========================================
c promax ne 0, tableaux supplementaires   int centre,largeurs, vitesses
c lm+1,2,3
      call par1(' milwmax',1,milwmax)
      write(3,*)' msdp3.f,cmd1 2130,tab supp: milwmax:',milwmax !m2403
c---------
      if(kpromax.ne.0.and.milwmax.ne.0)then   !  gaussienne 3 tableaux suppl
c   int,largeur,vit pour gaussienne
            pivw=float(milwmax)/1000.
            pivv=float(milvmax)/1000.
            wmin=1./sqrt(pivw)
            coefv=300000.*dlbd/float(lbda)
      do j=j1,j2
        do 395 i=i1,i2
        piv1=ijtab2(i,j,2)+ijtab2(i,j,3)
        piv2=ijtab2(i,j,1)
        piv3=ijtab2(i,j,2)-ijtab2(i,j,3)
        kpiv2=piv2
          z1=alog(piv1)
          z2=alog(piv2)
          z3=alog(piv3)

          do k=1,3
          ijtab2(i,j,k)=-32000
          enddo
       if(kpiv2.le.kpromin)goto 395
       ktab=tb(i,j,2)
       if(ktab.ge.kpromax)goto 395   ! elimine disque 
       wpiv=2.*z2-z1-z3
       if(wpiv.lt.0.)goto 395   ! elimine absorption     
       if(wpiv.lt.wmin)goto 395
       dpiv=1./sqrt(wpiv)
       vpiv=0.5*(z3-z1)/(z1-2.*z2+z3)
       if(abs(vpiv).gt.pivv)goto 395
       apiv=exp(z2-vpiv*vpiv*wpiv)

       ijtab2(i,j,1)=apiv
       ijtab2(i,j,2)=dpiv*float(dlbd)
       ijtab2(i,j,3)=vpiv*coefv
 395  continue
      enddo

c     write d-files
c     =============
      do k=1,3
       do j=j1,j2
       jp=j
        if(inverj.ne.0)jp=j2-j+j1

c          if(inveri.eq.0)then
          write(iud)(ijtab2(i,jp,k),i=i1,i2)
c                    ----------
c          else
c          write(iud)(ijtab2(i,jp,k),i=i2,i1,-1)
c          endif
        enddo        !   boucle j 
      enddo          ! boucle k

      endif          ! promax ne 0, milwmax ne 0,  3 tableaux suppl gaussienne
c--------------
c modif Stokes 29 avril 00
c                              Stokes apres lisond
      else       ! mgrim

      write(3,*)' cmd,dstok : kana,txana,tyana ',kana,txana,tyana

      call dstok(tb,inveri,inverj,invers,j1,j2,
c          -----                               
     1           mgrim,xdeb,xgri,txgri,txana,tyana,
     2           itaux,lm,iud,iitab,jjtab,lltab,nqsign)
      endif      ! mgrim

c-----------------------------  close
        close(unit=iud)
      write(3,*)'   unit=',iud
      do nw=nw1,nw2
        iuc=30+nw
        close(unit=iuc)
           if(idelc.eq.1)then
           call system('rm '//cna(nq,nw))
           endif
      enddo
      write(3,*)'             ides,itest ',ides,itest
      call pgend
c          -----
      return
      end
c********************************************************
      subroutine protest(yy,nmp,mincmd,maxcmd,itor,inverl)
c                -------                      tornado
      dimension x(257),y(257),yy(129)
c      character*23 psname

1     format(' Profil centre du champ premiere image')
c      write(3,1)
2     format(10f6.0)
c      write(3,2)(y(n),n=1,nm)

      nm=2*nmp-1
        a=-1./16.
        b=9./16.
      do np=1,nmp
      n=2*np-1
      x(n)=float(np)/2.
      y(n)=yy(np)
      enddo
      do np=1,nmp-1
      n=2*np
      x(n)=0.5*(x(n-1)+x(n+1))
        if(n.eq.2.or.n.eq.nm-1)then
        y(n)=0.5*(y(n-1)+y(n+1))
        else
        y(n)=a*y(n-3)+b*y(n-1)+b*y(n+1)+a*y(n+3)
        endif
      enddo
      call pgadvance
      call pgvport(0.1,0.9,0.2,0.8)

      if(maxcmd.ne.0)then
      ymin=mincmd
      ymax=maxcmd
        do n=1,nm
        x(n)=float(n)/2.+0.5
        enddo
c        do n=1,nm
c        x(n)=n
c        enddo

      else
          ymax=-100000.
c                          ymin=+100000.
        do n=1,nm
        x(n)=float(n)/2.+0.5
        ymax=amax1(ymax,y(n))
c                          ymin=amin1(ymin,y(n))
        enddo
        ymin=0.
      ymax=1.2*ymax    !                     2013

      endif
      xnmp=float(nmp)
      xcan=(xnmp+3.)/4.
      if(ymax.le.0.)ymax=1.
        write(3,*)'inverl,xcan,xnmp',inverl,xcan,xnmp
c                         9    35
      
      call pgsch(2.5) !1.8)
      call pgslw(2)

        if(itor.eq.0)then

      if(inverl.eq.1)then
      call pgwindow(xcan,1.,ymin,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)

      call pgwindow(xnmp,1.,ymin,ymax)
      call pglabel('wavelength ->','intensity',' ')
      call pgline(nm,x,y)
       endif   !    inverl

        else  !itor
      call pgwindow(2.,8.,ymin,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
             piv=xnmp-4.
      call pgwindow(5.,piv,ymin,ymax)           ! tornado..............
      call pglabel('Lambda (channels)','Intensity',' ')
      call pgline(nm,x,y)
        endif    !itor

c      print *,'nm,xcan,xnmp ',nm,xcan,xnmp
c      print *,'x  ',(x(n),n=1,nm)

      n2=(nm-1)/8+1
      do n=1,n2
         nn=1+(n-1)*8
      x(n)=x(nn)
      y(n)=y(nn)
      enddo
c        nn2=1+(n2-1)*8
c        x(n2)= x(nn2)-0.05
      call pgsch(3.)
      call pgpoint(n2,x,y,17)  !   2013
      call pgsch(1.8)
      return
      end
c---------------------------------------------
      subroutine pinter(xp,nnm,yy,yrot,p,cosinus,sinus,  l,
c                ------                  inutil  
     1                  iph, cn,sn, nosol)
c                       out  inutil
      dimension xp(129),cosinus(629),sinus(629),cn(60),sn(60)

      cn(l)=0.
      sn(l)=0.

      y=yy+yrot
      k=y

      nosol=0
      if(k.lt.1.or.k.gt.nnm-1)nosol=1

c        if(k.lt.1.or.y.gt.float(nnm))then
c        p=32000.
c          cn(l)=1.
c          sn(l)=0.
c        return
c        endif
c          if(y.eq.float(nnm))then
c          p=xp(nnm)
c            cn(l)=1.
c            sn(l)=0.
c          return
c          endif

      if(k.lt.1)k=1
      if(k.gt.nnm-1)k=nnm-1

      alp=y-k
          dk=k-1-4*((k-1)/4)
          dabscisse=(dk+alp)/4.
c          alpha=dabscisse*628.
c          kpiv=alpha+1.5
            icanal=(k-1)/4
            iph=(dabscisse+icanal)*1000.

c         n      1...  2...    3...   4
c         k      1234  5678
c         dk     0123  0123
c        iph     0     1000... 2000   3000...


c              if(kpiv.lt.1.or.kpiv.gt.629)then
c              print *,' depassement cos/sin   k=',kpiv
c              stop
c              endif
c          cn(l)=cosinus(kpiv)
c          sn(l)=sinus(kpiv)

c        if(kpiv.ge.1.and.kpiv.le.629)then
c        cn(l)=cosinus(kpiv)
c        sn(l)=sinus(kpiv)
c        endif

        if(k.eq.1.or.k.eq.nnm-1)then
        p=xp(k)*(1.-alp)+xp(k+1)*alp
        else
c                    cubique par 4 points 
          a=xp(k-1)
          b=xp(k)
          c=xp(k+1)
          d=xp(k+2)
        x=2.*alp-1.
        p=      (-a+9.*b+9.*c-d)/16.+
     1        x*((a-27.*b+27.*c-d)/48.+
     2        x*((a-b-c+d)/16.+
     3        x*((-a+3.*b-3.*c+d)/48.)))

          if(p.gt.32000.)then
       print *,'xp',(xp(n),n=1,nnm,4)
       print *,'pinter: intensity',k,p,
     1         ' larger than 32000: reduce minprof'
       write(3,*)'pinter: intensity',k,p,
     1         ' larger than 32000: reduce minprof'
          p=32000.
c          stop
          endif

        endif          

      return
      end
c---------------------------------------------------
      subroutine spline4(p,n1,n2,xp,a,b,nlis)
c                -------
c                                      (nlis deja traite par tablis)
      dimension p(32),q(32),xp(129),c1(5),c2(5)

      n2p=n2
c      if(nlis.eq.1)n2p=n2-1 

      km=4*(n2p-n1)+1

      do n=n1,n2
      q(n)=p(n)
      enddo

      call cpoly4(q,n1,c2)
c          ------
      do k=1,9
      x=k-9
      xp(k)=c2(1)+x*(c2(2)+x*(c2(3)+x*(c2(4)+x*c2(5))))
      enddo
c
      do n=n1,n2p-5
          do i=1,5
          c1(i)=c2(i)
          enddo
c  intervalle deja traite    -------                      aile gauche

c   premier cas: n=1        1   2   3   4   5   6
c   coef c2                  ---------------
c      abscisses x2                  x2                   (1,2,3)
c   coef c1                      ---------------
c      abscisses x1                  x1                  (-3,-2,-1)
c intervalle traite                  ---
c             k=                                          10,11,12       

          call cpoly4(q,n+1,c2)
c              ------
        do kp=1,3
        x1=kp
        x2=kp-4
        y1=c1(1)+x1*(c1(2)+x1*(c1(3)+x1*(c1(4)+x1*c1(5))))         
        y2=c2(1)+x2*(c2(2)+x2*(c2(3)+x2*(c2(4)+x2*c2(5))))
        k=kp+4*(n+2)-3
        xp(k)=(x1*y2-x2*y1)/4.

        enddo
        k=4*n+9
        xp(k)=q(n+3)
      enddo
c
c      call cpoly4(q,n2-4,c2)
c          inutile, car deja charge en c2

c aile droite
c                                            nm-2  nm-1  nm
c                                               ---------
      do kp=1,8
      k=km+kp-8
      x=kp
      xp(k)=c2(1)+x*(c2(2)+x*(c2(3)+x*(c2(4)+x*c2(5))))
      enddo
c
      a=100000.
      b=-100000.
        do k=1,km
        a=amin1(a,xp(k))
        b=amax1(b,xp(k))
        enddo
      return
      end
c---------------------------------------
      subroutine cpoly4(p,n,cc)
c                ------
c        coefficients polynome 4eme degre pour points n, n+1, .. n+4

c                 X...X...X...X...X
c                         0
      dimension p(32),cc(5)
      a=p(n)
      b=p(n+1)
      c=p(n+2)
      d=p(n+3)
      e=p(n+4)
c
      cc(1)=c
      cc(2)=(a-8.*b+8.*d-e)/48.
      cc(3)=(-a+16.*b-30.*c+16.*d-e)/384.
      cc(4)=(-a+2.*b-2.*d+e)/768.
      cc(5)=(a-4.*b+6.*c-4.*d+e)/6144.
      return
      end
c------------------------------------------------
      subroutine ijlis(tab,im,jm,nm,ilis,jlis,iitab,jjtab,nntab)
c                -----
      integer*2 tab(iitab,jjtab,nntab)
      integer*4 mem(2000)
      if(ilis.gt.1)then
        do n=1,nm
        do j=1,jm
          do i=1,im
          mem(i)=tab(i,j,n)
          enddo
c               print *,(mem(i),i=1,100)
        call klis(mem,im,ilis,iitab)
c            ----
          do i=1,im
          tab(i,j,n)=mem(i)
          enddo
c               print *,(mem(i),i=1,100)
c               stop
        enddo
        enddo
      endif

      if(jlis.gt.1)then
        do n=1,nm
        do i=1,im
          do j=1,jm
          mem(j)=tab(i,j,n)
          enddo
        call klis(mem,jm,jlis,iitab)
c            ----
          do j=1,jm
          tab(i,j,n)=mem(j)
          enddo
        enddo
        enddo
      endif

      return
      end
c--------------------------------------
      subroutine klis(mem,km,lis,iitab)
c                ----
      dimension mem(2000),memp(2000),p(25)
      write(3,*)' klis calc  msdp3.f 2511 '    !  m2403
      if(lis.le.1)return
      do kd=1,50
      memp(kd)=mem(1)
      memp(km+kd+50)=mem(km)
      enddo

      do k=51,km+50
      memp(k)=mem(k-50)
      enddo

      if(lis.eq.2)then
        do k=1,km
        piv=.25*memp(k+49)+.5*memp(k+50)+.25*memp(k+51)

        if(piv.ge.0.)then
        mem(k)=piv+0.5
        else
        mem(k)=piv-0.5
        endif

        enddo
      endif

      if(lis.eq.3)then
        do k=1,km
        piv=.3333*memp(k+49)+.3333*memp(k+50)+.3333*memp(k+51)

        if(piv.ge.0.)then
        mem(k)=piv+0.5
        else
        mem(k)=piv-0.5
        endif
        enddo
      endif        

      if(lis.eq.4)then
        do k=1,km
        piv   =.125*memp(k+48)+.250*memp(k+49)+
     1         .250*memp(k+50)+
     2         .250*memp(k+51)+.125*memp(k+52)

        if(piv.ge.0.)then
        mem(k)=piv+0.5
        else
        mem(k)=piv-0.5
        endif

        enddo
      endif
c--------------
      if(lis.gt.4)then
        milis=lis/2
c--
      if(lis.eq.(2*milis))then      !  2403
           q=0
        do l=1,milis-1
        x=float(l)/float(milis)
c        p(l)=(1-x*x)**2

        xmilis=float(milis)
        piv=x*3.14159
        p(l)=0.5*(1.+cos(piv))

        q=q+p(l)
        enddo
           q=2.*q+1.

c      write(3,*)' cmd/klis: milis,q, p(l) ',
c     1                      milis,q,(p(l),l=1,milis-1) 

        do k=1,km
          piv=memp(k+50)
            do l=1,milis-1
            piv=piv+(memp(k+l+50)+memp(k-l+50))*p(l)
c            piv=piv+(memp(k+l+50)+memp(k-l+50))
            enddo
          piv=piv/q
c          piv=piv/float(2*milis-1)

        if(piv.ge.0.)then
        mem(k)=piv+0.5
        else
        mem(k)=piv-0.5
        endif

        enddo
c--
      else
        do k=1,km
          piv=memp(k+50)
          do l=1,milis-1
          piv=piv+memp(k+l+50)+memp(k-l+50)
          enddo
            piv=piv/float(lis)
        if(piv.ge.0.)then
        mem(k)=piv+0.5
        else
        mem(k)=piv-0.5
        endif
        enddo
      endif
c--
      endif
c--------------------
      return
      end
c--------------------------------------------
c modif stokes 29 avril 00 (dstok + intertab)

      subroutine dstok(tab,inveri,inverj,invers,j1,j2,
c                -----                                
     1           mgrim,xdeb,xgri,txgri,txana,tyana,
     2           itaux,lm,iud,iitab,jjtab,lltab,nqsign)

c modif stokes 1 mai 00                     2
      integer*2 tab(iitab,jjtab,lltab),t(2000,200,2)

      call par1('   cstok',1,kstok)
                 if(kstok.eq.0)kstok=1
      call par1(' spiscat',1,kpiv)
          spiscat=kpiv
      call par1('  spimax',1,kpiv)
          spimax=kpiv

      is=xgri+1
      im=mgrim*is
        jtya=tyana+1
        if(tyana.lt.0.)jtya=tyana-1
        if(tyana.eq.0.)jtya=0
      jm=j2-j1-iabs(jtya)+1
      isigne=1-2*invers

      isigne=isigne*nqsign

      print *,'lm dans dstok',lm
      do l=1,lm
        write(3,*)' l ',l
      do j=1,jm,10
        write(3,*)' j ',j, ' tab(i,j,l),i=1,im,50)'
      write(3,*)(tab(i,j,l),i=1,im,50)
      enddo
      enddo

c boucle l
      do20 l=1,lm
       write(3,*)' dstok: l ',l
c boucle mgri
      do10 mgri=1,mgrim
        itgri=txgri
      xideb1=xdeb+itgri*(mgri-1)   ! intervalle a partir premier i
      xideb2=xideb1+txana

        do j=1,jm
c                  dans tab
        if(tyana.ge.0)then
        ytab1=j+j1-1
        ytab2=j+j1-1+tyana
        else 
        ytab1=j+j1-1-tyana
        ytab2=j+j1-1
        endif

c      if(j.eq.jm)print *,'jm,ytab1,ytab2',jm,ytab1,ytab2

          do i=1,is
          xtab1=xideb1+i
          xtab2=xideb2+i
      
          call intertab(tab,xtab1,ytab1,l,tt1,iitab,jjtab,lltab,
c              --------
     1                  spimax,kt3)
          call intertab(tab,xtab2,ytab2,l,tt2,iitab,jjtab,lltab,
c                                         out
     1                  spimax,kt3)
      
c          if(i.eq.1.and.j.eq.1)print *,'mgri,x1,x2,y1,y2',
c     1                        mgri,xtab1,xtab2,ytab1,ytab2  
c                   dans t        
c modif stokes 1 mai 00
        it=i+(mgri-1)*is
        jt=j

      if(spimax.ne.0..and.l.ge.2)then
      if(kt3.eq.0)then
        tt1=0.
        tt2=0.
      endif
      endif

          kt1=0
          kt2=0
          if(tt1.ne.32000.and.tt2.ne.32000)then
          kt1=0.5*(tt1+tt2)
          endif

          if(itaux.eq.0)then
          kt2=(tt1-tt2)*isigne*kstok*0.5
          else
            if(tt1+tt2.gt.(2.*itaux))then
            kt2=isigne*10000.*(tt1-tt2)*kstok/(tt1+tt2)
            else
            kt2=0
            endif
          endif

        t(it,jt,1)=kt1
        t(it,jt,2)=kt2

          enddo  !  i
        enddo    !  j

10    continue   !  mgri
c              fin boucle mgri
c     ecriture sur iud
c          modif stokes 1 mai 00

                  kseuil=2000
      do isd=1,2

c        do jp=1,jm        
c          if(jp.gt.5.and.j.lt.jm)then
c            do i=5,im-4
c            kpiv1=t(i-1,jp,isd)
c            kpiv2=t(i,jp,isd)
c            kpiv3=t(i+4,jp,isd)
c            kpiv4=t(i,jp-4,isd)
c            kpiv5=t(i,jp+4,isd)
c            if(iabs(kpiv2-kpiv1).gt.kseuil)then
c     1         iabs(kpiv2-kpiv3).gt.kseuil.and.
c     2         iabs(kpiv2-kpiv4).gt.kseuil.and.
c     3         iabs(kpiv2-kpiv5).gt.kseuil)then
c            t(i,jp,isd)=t(i-1,jp-1,isd)
c            endif
c            enddo
c          endif
c        enddo

c  spicules scattering
      if(l.eq.1.and.spiscat.ne.0.)then
      do i=1,im
        piv=0.
        do j=1,jm
          piv=piv+t(i,j,isd)
        enddo
          piv=(spiscat/1000.)*piv/float(jm)

        do j=1,jm
          kpiv=t(i,j,isd)-piv
          if(kpiv.lt.0)kpiv=0
          t(i,j,isd)=kpiv
        enddo
      enddo
      endif

c chargement  fichier d
        do j=1,jm        
        jp=j
        if(inverj.ne.0)jp=jm-j+1
          if(inveri.eq.0)then
          write(iud)(t(i,jp,isd),i=1,im)
c                    -----------
          else
          write(iud)(t(i,jp,isd),i=im,1,-1)
          endif
        enddo

      do j=1,jm,10
      write(3,*)' isd,j ',isd,j,' t(i,j,isd),i=1,im,50)'
      write(3,*)(t(i,j,isd),i=1,im,50) 
      enddo

      enddo   ! boucle isd
20    continue
c           fin cboucle l

      return
      end
c-------------------------------------------------------
      subroutine intertab(tab,xtab,ytab,l,tt,iitab,jjtab,lltab,
c                --------  tb
     1                    spimax,kt3)
c     appele par dstok pour le tableau tb (et non tab) du principal
      integer*2 tab(iitab,jjtab,lltab)

      i=xtab
      di=xtab-i
      j=ytab
      dj=ytab-j

      a=tab(i,j,l)
      b=tab(i+1,j,l)
      c1=a*(1.-di)+b*di

      a=tab(i,j+1,l)
      b=tab(i+1,j+1,l)
      c2=a*(1.-di)+b*di

      tt=c1*(1.-dj)+c2*dj

c spicules quand 1 des 2 images (2) est < spimax
c       -->      les 2 valeurs  (3) =0

           kt3=1
      if(spimax.ne.0..and.l.ge.2)then
      if(tab(i,j,2).eq.0.or.tab(i,j,2).eq.32000)kt3=0
      if(tab(i+1,j,2).eq.0.or.tab(i+1,j,2).eq.32000)kt3=0
      if(tab(i,j+1,2).eq.0.or.tab(i,j+1,2).eq.32000)kt3=0
      if(tab(i+1,j+1,2).eq.0.or.tab(i+1,j+1,2).eq.32000)kt3=0
      endif

      return
      end
c------------------------------------------------------      
c      subroutine lisp(p,nm,nlis)
c                ----
c      dimension p(32),q(32)

c      if(nlis.eq.2)then
c      q(1)=p(1)
c      q(nm)=p(nm)

c      do n=2,nm-1
c      q(n)=0.25*p(n-1)+0.5*p(n)+0.25*p(n+1)
c      enddo

c      do n=1,nm
c      p(n)=q(n)
c      enddo
c      endif

c      return
c      end
c-------------------------------------------------------
      subroutine snorma(tab,im,jm,nm,norma,iitab,jjtab,nntab,
c                ------
     1                  kana,kanam,nx)

      integer*2 tab(iitab,jjtab,nntab)
      double precision moy1,moy2,poids1,poids2,coef(2000)

      if(norma.eq.0)return

      nd=nm-1
c      if(norma.eq.1)then
c        ia=1
c        ib=1
c        ic=1
c        id=im
c      goto5
c      endif

      if(norma.gt.8999)return

      ka= norma/1000
      kb=(norma-1000*ka)/100
      kc=(norma-1000*ka-100*kb)/10
      kd= norma-1000*ka-100*kb-10*kc
      if(kd.eq.0)kd=10

      ia=1+(im-1)*ka*0.1
      ib=1+(im-1)*kb*0.1
      ic=1+(im-1)*kc*0.1
      id=1+(im-1)*kd*0.1
      if(ka.gt.kb)return
      if(kc.gt.kd)return

c          effet de invi pour norma
      call par1('    invi',1,invi)
      call par1('  inveri',1,inveri)
      if(invi.eq.1.or.inveri.eq.1)then
        i1=ia
        i2=ib
        i3=ic
        i4=id
       ia=im-i4+1
       ib=im-i3+1
       ic=im-i2+1
       id=im-i1+1
      endif

      poids1=0.
      poids2=0.
      moy1=0.
      moy2=0.
c          le coeff est deduit des ailes de la raie
5     do n=1,nm,nd     ! 1 puis nm
      do j=1,jm

        if(ia.eq.ib)goto10
        do i=ia,ib
          poids1=poids1+1.
          x=tab(i,j,n)
          moy1=moy1+x
        enddo 
     
10      do i=ic,id
        poids2=poids2+1.
        x=tab(i,j,n)
        moy2=moy2+x
        enddo
  
      enddo
      enddo

c         les ailes sont ajustees a 1000
      coef1=1000.*poids1/moy1
      if(ia.ne.ib)coef2=1000.*poids2/moy2
      if(kana.eq.kanam.and.nx.eq.1)write(7,*)' coef norma ',coef1,coef2

      x1=float(ia+ib)/2.
      x2=float(ic+id)/2.
      if(ia.eq.ib)then
      do i=1,im
        coef(i)=coef2
      enddo
      else
      do i=1,im
        x=float(i)
      alp=(x-x1)/(x2-x1)
      coef(i)=coef1*(1.-alp)+coef2*alp
      enddo
      endif


      do n=1,nm
      do j=1,jm
      do i=1,im
      tab(i,j,n)=tab(i,j,n)*coef(i)+0.5
      enddo
      enddo
      enddo

      return
      end
c--------------------------------------
      subroutine curv(tabjn,liscurv,ltrj,nwinp,
c                ----
     1                 j,j1,j2,nm,
     2                 xp,nnm,piv1,piv2,nlis,jjtab)

c la condition de courbure est prise aux points X
c situes a +/- 1/2 periode ltrj en j


c         ud  vd                             vd  ud
c    ----+--X----+------------0------------+----X--+-----
c      j-jg    j-jg+1         j        j+jd-1     j+jd

c canaux    en j-jg      1   2   3   4            nm
c              j           1   2   3   4   ......... nm
c              j+jg          1   2   3   4             nm

       dimension p(32),q(32),q1(32),q3(32),xp(129),y(4),t(5) ! a(5)
       dimension tabjn(200,32)

      xjgd=float(ltrj)/(100.*nwinp)   !   periode
       jd=xjgd/2.+1
       jg=jd
       ud=jd-xjgd/2.
       vd=1.-ud
      nnm=4*nm-3

      if(liscurv.ge.5)then
c                             .  .  .  *  .  .  .  *  .  .  .
c                                        q1  q q3
c      val prises a                       d d+g g
c                     r1      .           .           .
c                     r          .           .           .         
c                     r3            .           .           .

        jd1=xjgd/4.+1
        jg1=jd1
        ud1=jd1-xjgd/4.
        vd1=1.-ud1
      endif

c  intervalles comptes de 2 a  nm-2
c                debuts   2    nm-2
c  interpol paraboliques pour  1 et nm-1 (wing2)

      if(j.ge.j1+jg+1.and.j.le.j2-jd)then
        do n=1,nm-1
        q(n)=0.5*(vd*(tabjn(j-jg,n)  +tabjn(j+jd,n+1))
     1           +ud*(tabjn(j-jg+1,n)+tabjn(j+jd-1,n+1)))
          if(liscurv.ge.5)then
          q1(n)=vd1*tabjn(j-jg1,n)+ud1*tabjn(j-jg1+1,n)
          q3(n)=vd1*tabjn(j+jd1,n+1)+ud1*tabjn(j+jd1-1,n+1)
          endif
        enddo

      else
        if(j.lt.j1+jg+1)then
           do n=1,nm-1
           q(n)=vd*tabjn(j+jd,n+1)
     1         +ud*tabjn(j+jd-1,n+1)
              q1(n)=q(n)
              q3(n)=q(n) 
           enddo
        else
           do n=1,nm-1
           q(n)=vd*tabjn(j-jg,n) 
     1         +ud*tabjn(j-jg+1,n)
              q1(n)=q(n)
              q3(n)=q(n)
           enddo
        endif
      endif
c                        1   2   3   4        nm
c                          0   1   2         0

c intervalles
        
        do n=1,nm
        p(n)=tabjn(j,n)
        enddo

        do n=2,nm-2
                    ngd=0
          do k=1,4
          y(k)=p(n+k+ngd-2)
          enddo

          r=q(n+ngd-1)+q(n+ngd+1)-2.*q(n+ngd)
        if(liscurv.ge.5)then
          r1=q1(n+ngd-1)+q1(n+ngd+1)-2.*q1(n+ngd)
          r3=q3(n+ngd-1)+q3(n+ngd+1)-2.*q3(n+ngd)
        endif

      if(liscurv.eq.4) call quart(y,r,t)        
c                           -----
      if(liscurv.eq.2) call para(y,r,t)
c                           ----
      if(liscurv.eq.1) call linear(y,t)
c                           ------
      if(liscurv.eq.5)call quart11(y,r1,r,r3,t)
c                          -------           
      if(liscurv.eq.6)call six13(y,r1,r,r3,t,ngd)
c                          -----
c            n=2          1         2           3
c            nn=                    5  6  7  8  9

c           n=nm-2               nm-2          nm-1          nm  
c           nn=                 4nm-3 -2 -1  0  +1 
c ex nm=9         n                 7           8             9
c                 nn               25          29            33        

          do m=1,5
            nn=4*n-4+m
            xp(nn)=t(m)
          enddo

        enddo    ! fin intervalle  nm-2,nm-1

      call wings2(p,1,nm,xp)
c          ------

c lin
c      xp(1)=p(1)
c      xp(5)=p(2)
c      do k=2,4
c        alp=float(k-1)/4.
c      xp(k)=alp*p(2)+(1.-alp)*p(1)
c      enddo

c      xp(nnm)=p(nm)
c      xp(nnm-4)=p(nm-1)
c      do k=2,4
c        alp=float(k-1)/4.
c      xp(nnm-5+k)=alp*p(nm)+(1.-alp)*p(nm-1)      
c      enddo

c      if(i.eq.(i1+20).and.j.eq.(j1+20))then
c      write(3,*)'    p',(p(n),n=1,nm)
c      write(3,*)'    q',(q(n),n=1,nm-1)
c      write(3,*)'    y',(y(k),k=1,4)
c      write(3,*)'    r',r
c      write(3,*)'    t',(t(k),k=1,5)
c      write(3,*)'   xp',(xp(nn),nn=1,nnm)
c      endif

      return
      end
c--------------------------------------
      subroutine quart(y,r,t)
c                -----
c                quartique par 4 points + condition courbure
      dimension y(4),t(5)
c           points          y1       y2          y3         y4
c                                z1        z2         z3
c           resultats                t1 t2 t3 t4 t5
c           abscisses       -3   -2  -1     0     1    2     3  

      s1=(y(1)+y(4))/2.
      s2=(y(2)+y(3))/2.
      d1=(y(4)-y(1))/2.
      d2=(y(3)-y(2))/2.
c                          courbure imposee    
c     r=z(1)+z(3)-2.*z(2)

      d=(d1-3.*d2)/24.
      c=(5.*r-2.*s1+2.*s2)/24.
      b=(27.*d2-d1)/24.
      e=(s1-s2-r)/48.
      a=(15.*s2+s1-3.*r)/16.

      t(1)=y(2)
      t(2)=a-b/2.+c/4.-d/8.+e/64.
      t(3)=a
      t(4)=a+b/2.+c/4.+d/8.+e/64.
      t(5)=y(3)
  
      return
      end
c------------------------------------------

      subroutine para(y,r,t)
c                ----
c                parabole par 2 points + condition courbure
      dimension y(4),t(5)
c           points                   y2          y3           
c                                z1        z2         z3
c           resultats                t1 t2 t3 t4 t5
c           abscisses            -2  -1     0     1    2    

      s1=y(2)+y(3)
      d1=y(3)-y(2)
c                          courbure imposee    
c     r=z(1)+z(3)-2.*z(2)

      a=s1/2.-r/8.
      b=d1/2.
      c=r/8.

      t(1)=y(2)
      t(2)=a-b/2.+c/4.
      t(3)=a
      t(4)=a+b/2.+c/4.
      t(5)=y(3)

      return
      end
c------------------------------------------
      subroutine diff(tabjn,liscurv,ltrj,nwinp,
c                ----
     1                 j,j1,j2,nm,
     2                 xp,nnm,piv1,piv2,nlis,jjtab)

c les conditions de differences sont prises aux points X
c situes a +/- 1/2 periode ltrj en j


c         ud  vd                             vd  ud
c    ----+--X----+------------0------------+----X--+-----
c      j-jg    j-jg+1         j        j+jd-1     j+jd

c canaux    en j-jg      1   2   3   4            nm
c              j           1   2   3   4   ......... nm
c              j+jg          1   2   3   4             nm

       dimension p(32),q(32),xp(129),y(4),t(5) ! a(5)
       dimension tabjn(200,32)

      xjgd=float(ltrj)/(100.*nwinp)
       jd=xjgd/2.+1
       jg=jd
       ud=jd-xjgd/2.
       vd=1.-ud
      nnm=4*nm-3

c  intervalles comptes de 2 a  nm-2
c                debuts   2    nm-2
c  interpol paraboliques pour  1 et nm-1

      if(j.ge.j1+jg+1.and.j.le.j2-jd)then
        do n=1,nm-1
        q(n)=0.5*(vd*(tabjn(j-jg,n)  +tabjn(j+jd,n+1))
     1           +ud*(tabjn(j-jg+1,n)+tabjn(j+jd-1,n+1)))
        enddo

      else
        if(j.lt.j1+jg+1)then
           do n=1,nm
           q(n)=vd*tabjn(j+jd,n+1)
     1         +ud*tabjn(j+jd-1,n+1)
           enddo
        else
           do n=1,nm-1
           q(n)=vd*tabjn(j-jg,n) 
     1         +ud*tabjn(j-jg+1,n)
           enddo
        endif
      endif

c                        1   2   3   4        nm
c                          0   1   2         0

c intervalles
        
        do n=1,nm
        p(n)=tabjn(j,n)
        enddo

c cubique
        do n=2,nm-2
          do k=1,4
          y(k)=p(n+k-2)
          enddo

          dif1=q(n)-q(n-1)
          dif2=q(n+1)-q(n)

          call cub(y,dif1,dif2,t)        
c              ---

          do m=1,5
          nn=4*n+m-4
            if(abs(t(m)).lt.31000.)then
            xp(nn)=t(m)
            else
            xp(nn)=0.5*(t(1)+t(5))
            endif
          enddo
        enddo

c ailes
      call wings2(p,1,nm,xp)
c          ------
c lin
c      xp(1)=p(1)
c      xp(5)=p(2)
c      do k=2,4
c        alp=float(k-1)/4.
c      xp(k)=alp*p(2)+(1.-alp)*p(1)
c      enddo

c      xp(nnm)=p(nm)
c      xp(nnm-4)=p(nm-1)
c      do k=2,4
c        alp=float(k-1)/4.
c      xp(nnm-5+k)=alp*p(nm)+(1.-alp)*p(nm-1)      
c      enddo

c      if(i.eq.(i1+20).and.j.eq.(j1+20))then
c      write(3,*)'    p',(p(n),n=1,nm)
c      write(3,*)'    q',(q(n),n=1,nm-1)
c      write(3,*)'    y',(y(k),k=1,4)
c      write(3,*)'dif1,dif2',dif1,dif2
c      write(3,*)'    t',(t(k),k=1,5)
c      write(3,*)'   xp',(xp(nn),nn=1,nnm)
c      endif

      return
      end
c--------------------------------------
      subroutine cub(y,t1,t2,t)
c                ---
c                cubique par 2 points + 2 conditions tangentes
      dimension y(4),t(5)
c           points                   y2          y3           
c                                z1        z2         z3
c           resultats                t1 t2 t3 t4 t5
c           abscisses            -2  -1     0     1    2   

      s1=y(2)+y(3)
      d1=y(3)-y(2)
c                          differences imposees    
c     t1=z2-z1
c     t2=z3-z2

      d=(t1+t2)/12.-d1/6.
      c=(t2-t1)/8.
      b=2.*d1/3.-(t1+t2)/12.
      a=s1/2.+(t1-t2)/8.

      t(1)=y(2)
      t(2)=a-b/2.+c/4.-d/8.
      t(3)=a
      t(4)=a+b/2.+c/4.+d/8.
      t(5)=y(3)
  
      return
      end
c------------------------------------------
      subroutine linear(y,t)
c                ------
      dimension y(4),t(5)
      t(1)=y(2)
      t(2)=0.75*y(2)+0.25*y(3)
      t(3)=0.50*y(2)+0.50*y(3)
      t(4)=0.25*y(2)+0.75*y(3)
      t(5)=y(3)
      return
      end
c--------------------------------------------
      subroutine quart11(y,r1,r2,r3,t)
c                -------
c                quartique par 2 points + 3 conditions courbure
      dimension y(4),t(5)
c           points                   y2          y3         
c           courbures                   r1 r2 r3
c           resultats                t1 t2 t3 t4 t5
c           abscisses                -2 -1  0  1  2   

      s=(y(2)+y(3))/2.
      dd=(y(3)-y(2))/2.
c     r1,r2,r3                     courbures imposees   
      rr1=r1/32.
      rr2=r2/32.
      rr3=r3/32.      

      d=(rr3-rr1)/6.
      c=(11.*rr2-4.*rr1-4.*rr3)/12.
      b=dd/2.-2.*(rr3-rr2)/3.
      e=(rr1+rr3-2.*rr2)/12.
      a=s+4.*(rr1+rr3-3.*rr2)

      t(1)=y(2)
      t(2)=a-b+c-d+e
      t(3)=a
      t(4)=a+b+c+d+e
      t(5)=y(3)
  
      return
      end
c------------------------------------------
      subroutine six13(y,r1,r2,r3,t,ngd)
c                -----
c                pol degre 6 par 4 points + 3 conditions courbure
      dimension y(4),t(5)
c           points           y1      y2          y3      y4    
c           courbures                   r1 r2 r3
c           resultats                t1 t2 t3 t4 t5
c           abscisses                -2 -1  0  1  2   

      sp1=(y(1)+y(4))/2.
      sp2=(y(2)+y(3))/2.
      dp1=(y(4)-y(1))/2.
      dp2=(y(3)-y(2))/2.

      s=(sp1-sp2)/32.
      dd=(dp1-3.*dp2)/32.

c     r1,r2,r3                     courbures imposees   
      rr1=r1/32.
      rr2=r2/32.
      rr3=r3/32.      

      f=(dd+rr1-rr3)/60.
      d=dd/6.-40.*f
      g=(s-2.*rr1+3.*rr2-2.*rr3)/180.
      e=(s-rr2-1200.*g)/24.
      c=rr2-16.*e-256.*g
      b=144.*f+(27.*dp2-dp1)/48.
      a=sp2-4.*c-16.*e-64.*g

      if(ngd.eq.0)then
      t(1)=y(2)
      t(2)=a-b+c-d+e-f+g
      t(3)=a
      t(4)=a+b+c+d+e+f+g
      t(5)=y(3)
      endif

      if(ngd.eq.1)then
      t(1)=y(1)
      t(2)=a-5.*b+25.*c-125.*d+625.*e-3125.*f+15625.*g
      t(3)=a-4.*b+16.*c -64.*d+256.*e-1024.*f +4096.*g
      t(4)=a-3.*b +9.*c -27.*d +81.*e -243.*f  +729.*g
      t(5)=y(2)
      endif

      if(ngd.eq.-1)then
      t(1)=y(3)
      t(2)=a+3.*b +9.*c +27.*d +81.*e +243.*f  +729.*g
      t(3)=a+4.*b+16.*c +64.*d+256.*e+1024.*f +4096.*g
      t(4)=a+5.*b+25.*c+125.*d+625.*e+3125.*f+15625.*g
      t(5)=y(4)
      endif
      
        return
      end
c------------------------------------------
      subroutine tablis(tab,iitab,jjtab,nntab,i1,i2,j1,j2,n1,n2,nlis)
c                ------
      dimension p(32),q(32)
      integer*2 tab(iitab,jjtab,nntab)

      do i=i1,i2
      do j=j1,j2
      
      do n=n1,n2
      p(n)=tab(i,j,n)
      enddo

      n2p=n2
      if(nlis.eq.1)n2p=n2-1

      do n=n1,n2p
      q(n)=p(n)
      enddo

      if(nlis.eq.-1)then
        s1=0.
        s2=0.
      do n=n1,n2-1,2
      s1=s1+p(n)
      s2=s2+p(n+1)
      enddo
        s=(s1+s2)/2.
        cor1=s/s1
        cor2=s/s2
      do n=n1,n2-1,2
      q(n)=p(n)*cor1
      q(n+1)=p(n+1)*cor2
      enddo
      endif

      if(nlis.eq.1)then
        do n=n1,n2p
        q(n)=0.5*(p(n)+p(n+1))
        enddo
      q(n2)=2.*q(n2p)-q(n2p-1)
      endif

      if(nlis.eq.2)then
      q(n1)=p(n1)
      q(n2)=p(n2)
        do n=n1+1,n2-1
        q(n)=.25*p(n-1)+.5*p(n)+0.25*p(n+1)
        enddo
      endif

      if(nlis.eq.3)then
      q(n1)=p(n1)
      q(n2)=p(n2)
        do n=n1+1,n2-1
        q(n)=.33333*p(n-1)+.33333*p(n)+.33333*p(n+1)
        enddo
      endif

      if(nlis.eq.4)then
      q(n1)=p(n1)
      q(n2)=p(n2)
      q(n1+1)=.25*p(n1)+.5*p(n1+1)+.25*p(n1+2)
      q(n2-1)=.25*p(n2-2)+.5*p(n2-1)+.25*p(n2)
        do n=n1+2,n2-2
        q(n)=.125*p(n-2)+.25*p(n-1)+.25*p(n)+
     1       .25*p(n+1)+.125*p(n+2)
        enddo
      endif

      do n=n1,n2
      tab(i,j,n)=q(n)
      enddo

      enddo
      enddo
      return
      end
c--------------------------------------------
c      subroutine wings4(p,n1,n2,xp)
c                ------
c   calcule les interpol des intervalles extremes par pol deg 5
c      dimension p(32),c(5),xp(129)

c      call cpoly4(p,n1,c,xp)
cc          ------
c      do k=1,9
c      x=k-9
c      xp(k)=c(1)+x*(c(2)+x*(c(3)+x*(c(4)+x*c(5))))
c      enddo

c      km=4*(n2-n1)+1

c      call cpoly4(p,n2-4,c,xp)
cc          ------
c      do kp=1,8
c      k=km+kp-8
c      x=kp
c      xp(k)=c(1)+x*(c(2)+x*(c(3)+x*(c(4)+x*c(5))))
c      enddo      

c      return
c      end
c----------------------------------------------
      subroutine wings2(p,n1,n2,xp)
c                ------
c  calcule les interpolations paraboliques pour intervalles
c  extremes  n1 et n2-1 (en association avec curv ou diff)

      dimension p(32),xp(129)

c aile gauche
c                                  n=  1        2        3
c                                  k=  X . . .  X 6    
c 3 points definissant parabole        y3      y2 y1
c   abscisses                          4 3 2 1  0 -1

      y1=xp(6)
      y2=p(2)
      y3=p(1)

      a=y2
      b=(-16.*y1+15.*y2+y3)/20.
      c=(4.*y1-5.*y2+y3)/20.

      xp(1)=p(1)
      xp(2)=a+3.*b+9.*c
      xp(3)=a+2.*b+4.*c
      xp(4)=a+b+c
      xp(5)=p(2)

c aile droite
c                             n= n2-2                  n2-1       n2
c                             k=              4(n2-n1)   X  . . . X 
c 3 points parabole                              y1      y2       y3
c   abscisses                                    -1      0  1 2 3 4

        k=4*(n2-n1)-4
      y1=xp(k)
      y2=p(n2-1)
      y3=p(n2)

      a=y2
      b=(-16.*y1+15.*y2+y3)/20.
      c=(4.*y1-5.*y2+y3)/20.

      xp(k+5)=p(n2)
      xp(k+4)=a+3.*b+9.*c
      xp(k+3)=a+2.*b+4.*c
      xp(k+2)=a+b+c
      xp(k+1)=p(n2-1)      

      return
      end
c------------------------------------------------
      subroutine writet(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm)
c                ------
      integer*2 tb(iitab,jjtab,lltab)

      do l=1,lm
      do j=j1,j2
        write(60)(tb(i,j,l),i=i1,i2)
      enddo
      enddo

      return
      end
c------------------------------------------------
      subroutine readt(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm)
c                -----
      integer*2 tb(iitab,jjtab,lltab)

      do l=1,lm
      do j=j1,j2
        read(60)(tb(i,j,l),i=i1,i2)
      enddo
      enddo

      return
      end
c------------------------------------------------
      subroutine four(tb,iitab,jjtab,lltab,i1,ia,ib,i2,j1,ja,jb,j2,lm,
c                ----
     1        nh, per,ifour,  jfour,  iw,nx)


c si iw>1 les corrections seront recalculees et appliquees
c               dans corfou  (pas de perte de filtrage spatial)


      integer*2 tb(iitab,jjtab,lltab),ijtab(2000,200)
c                                    stock lissage i
      dimension  cs(200),  sn(200), apj(200),  api(1000)
c                cos       sin      apod j     apod i
      dimension amp(60)


c intervalle Fourier km     ex jfour=3   kc=4   km=7
      kc=1+jfour
      km=2*kc-1
      sapj=0.

      do k=1,km
      angle=2.*3.14159*(k-kc)/per
      cs(k)=cos(angle)
      sn(k)=sin(angle)

      alpod=3.14159*(k-kc)/float(kc)
      if(nh.ne.1)then
        apj(k)=1.+cos(alpod)
      else
        apj(k)=1.
      endif

c      apj(k)=(1.-((k-kc)/float(kc-1))**2)**2

      sapj=sapj+apj(k)
      enddo

      write(3,*)' apj, cs, sn'
      write(3,*)(apj(k),k=kc,km)
      write(3,*)(cs(k),k=kc,km)
      write(3,*)(sn(k),k=kc,km)

c apod i
      mc=1+ifour
      mm=2*mc-1
      sapi=0.

      do m=1,mm
      api(m)=(1.-((m-mc)/float(mc-1))**2)**2
      sapi=sapi+api(m)
      enddo

      write(3,*)' api'
      write(3,*)(api(m),m=mc,mm)

      ja=j1+kc-1
      jb=j2-kc+1
      ia=i1+mc-1
      ib=i2-mc+1

1     format('nh,j1,ja,jb,j2,kc=jfour+1,per ',6i4,f6.2)      
      write(3,1)nh,j1,ja,jb,j2,kc,per
      print 1,  nh,j1,ja,jb,j2,kc,per

      if(ja.gt.jb)then
      print *,'four:ja>jb: correc Fourier impossible a cette frequence'
      stop
      endif

c ex  j1=1  j2=12  jfour=2  kc=3  km=5

c                 j=      1   2   3   4   5   6   7   8   9  10  11  12  
c                        j1                                          j2
c                                             j
c                                   jp1   .   .   .  jp2
c                                ja
c                       jp1   .   .   .   jp2
c                                                            jb
c                                                    jp1  .   .   .  jp2
c              jq=                                    1   2   3   4   5

c boucle l
      do l=1,lm

c lissage i et stockage ijtab
      do j=j1,j2
        do i=ia,ib
        ip1=i-mc+1
        ip2=i+mc-1
        s=0.
          do ip=ip1,ip2
          m=ip-ip1+1
          s=s+tb(ip,j,l)*api(m)
c             ---------
          enddo
        ijtab(i,j)=s/sapi
        enddo

        do i=i1,ia-1
        ijtab(i,j)=ijtab(ia,j)
        enddo

        do i=ib+1,i2
        ijtab(i,j)=ijtab(ib,j)
        enddo
      enddo

c corrections j

      do i=i1,i2

c entre ja et jb  inutile de calculer les sinus
        ss=0.
        ampi=0.
      do j=ja,jb
        aa=0.
        cc=0.
        jp1=j-kc+1
        jp2=j+kc-1
          do jp=jp1,jp2
          aa=aa+ijtab(i,jp)
          enddo
          aa=aa/float(jp2-jp1+1)
        do jp=jp1,jp2
           k=jp-jp1+1
           cc=cc+(ijtab(i,jp)-aa)*cs(k)*apj(k)
        enddo
        ampj=-2.*cc/sapj

        if(iw.eq.1)then
                      tb(i,j,l)=tb(i,j,l)+ampj
c                     ---------           cos(0)=1.
c            print *,' corrections four'
        endif

        tb(i,j,l+2*lm)=ampj
c       --------------
        ampj=ampj*ampj
        ampi=ampi+ampj
5     format('tb(60,j,1),tb(60,j,l+2*lm),j,cc,ss,sapj ',2i5,i4,3f8.1)
          if(l.eq.1.and.i.eq.60)write(3,5)
     1    tb(i,j,l),tb(i,j,l+2*lm),j,cc,ss,sapj
      enddo

        ampi=ampi/float(jb-ja+1)
        amp(l)=amp(l)+ampi

c de j1 a ja-1 
      aa=0.
      cc=0.
      ss=0.
      jp1=j1
      jp2=j1+km-1
          do jp=jp1,jp2
          aa=aa+ijtab(i,jp)
          enddo
          aa=aa/float(jp2-jp1+1)
        do jp=jp1,jp2
          k=jp-jp1+1
          cc=cc+(ijtab(i,jp)-aa)*cs(k)*apj(k)
          ss=ss+(ijtab(i,jp)-aa)*sn(k)*apj(k)
        enddo

        do j=j1,ja-1
        jq=j-jp1+1
          ampj=-2.*(cc*cs(jq)+ss*sn(jq))/sapj
        if(iw.eq.1)tb(i,j,l)=tb(i,j,l)+ampj
c                     ---------
        tb(i,j,l+2*lm)=ampj
c       --------------

          if(l.eq.1.and.i.eq.i1)write(3,5)
     1    tb(i,j,l),tb(i,j,l+2*lm),j,cc,ss,sapj
        enddo

c de jb+1 a j2
      aa=0.
      cc=0.
      ss=0.
      jp1=j2-km+1
      jp2=j2
          do jp=jp1,jp2
          aa=aa+ijtab(i,jp)
          enddo
          aa=aa/float(jp2-jp1+1)
        do jp=jp1,jp2
          k=jp-jp1+1
          cc=cc+(ijtab(i,jp)-aa)*cs(k)*apj(k)
          ss=ss+(ijtab(i,jp)-aa)*sn(k)*apj(k)
        enddo

        do j=jb+1,j2
        jq=j-jp1+1
          ampj=-2.*(cc*cs(jq)+ss*sn(jq))/sapj
        if(iw.eq.1)tb(i,j,l)=tb(i,j,l)+ampj
c                     ---------
        tb(i,j,l+2*lm)=ampj
c       --------------
          if(l.eq.1.and.i.eq.i1)write(3,5)
     1    tb(i,j,l),tb(i,j,l+2*lm),j,cc,ss,sapj
        enddo

      enddo
          amp(l)=amp(l)/float(i2-i1+1)
          amp(l)=sqrt(amp(l))
      enddo
10    format(' Fourier correc: period,rms cr',f5.1,15f4.0/
     1       35x,15f4.0)
      if(nx.eq.1)write(7,10)per,(amp(l),l=1,lm)
      return
      end
c------------------------------------
      subroutine scatt(tab,iitab,jjtab,nntab,i1,i2,j1,j2,n1,n2,
c                ----- inutilise
     1                                       im,jm,nm,taux)
c lumiere diffusee - moyennes a j constant
      integer*2 tab(iitab,jjtab,nntab)
      double precision s

      do n=n1,n2
      do j=j1,j2

      s=0.
      k=0
      kp=0
        do i=i1,i2
        kpiv=tab(i,j,n)
        s=s+kpiv
        k=k+1
        enddo
      d=taux*s/float(k)
      if(d.ge.0.) kp=d+0.5

      do i=1,im
        tab(i,j,n)=tab(i,j,n)-kp
      enddo

      enddo
      enddo
      return
      end
c--------------------------------------------
      subroutine scattline(tab,iitab,jjtab,nntab,i1,i2,j1,j2,n1,n2,
c                ---------
     1      ylinef,trj,taux)
c lumiere diffusee - moyennes a lambda constant (ylinef(i))

      integer*2 tab(iitab,jjtab,nntab)
      dimension pmoy(370,32),ylinef(2000)

      print *,'i1,i2,j1,j2,n1,n2,ylinef(i1),ylinef(i2),trj,taux',
     1         i1,i2,j1,j2,n1,n2,ylinef(i1),ylinef(i2),trj,taux

      ic=(i1+i2)/2

      ymin=1000
      ymax=1
      do i=i1,i2
      ypiv=ylinef(i)
      ymin=amin1(ymin,ypiv)
      ymax=amax1(ymax,ypiv)
      enddo
      yic=ylinef(ic)

      jp1=100+j1-(ymax-yic)
      jp2=100+j2+yic-ymin+1
1     format(' i1,i2,j1,j2,jp1,jp2,ymin,yic,ymax')
2     format(6i4,3f6.1)
      write(3,1)
      print 1
      write(3,2)i1,i2,j1,j2,jp1,jp2,ymin,yic,ymax
      print 2,  i1,i2,j1,j2,jp1,jp2,ymin,yic,ymax

      do n=n1,n2
      do jp=jp1,jp2
      call moyline(tab,iitab,jjtab,nntab,i1,i2,j1,j2,n1,n2,
c          -------
     1             ylinef,trj,ic,jp,n,pmoy(jp,n))
      enddo
3     format(' n,jp1,jp2 ',3i4,' pmoy=')
4     format(10f6.0)
      write(3,3)n,jp1,jp2
      write(3,4)(pmoy(jp,n),jp=jp1,jp2)
      enddo

      do n=n1,n2
      do j=j1,j2
        do i=i1,i2
        yjp=j+100+ylinef(i)-ylinef(ic)
        jp=yjp
        x=yjp-jp
        p=0.
          if(pmoy(jp,n).ne.0..and.pmoy(jp+1,n).ne.0.)then
          p=pmoy(jp,n)*(1.-x)+x*pmoy(jp+1,n)
          tab(i,j,n)=tab(i,j,n)-taux*p
          endif
        enddo
      enddo

      enddo
      return
      end
c--------------------------------
      subroutine moyline(tab,iitab,jjtab,nntab,i1,i2,j1,j2,n1,n2,
c                -------
     1     ylinef,trj,ic,jp,n,pmoy)

      integer*2 tab(iitab,jjtab,nntab)
      dimension ylinef(2000)
      double precision s

      s=0.

      do i=i1,i2
      j=jp-100+ylinef(i)-ylinef(ic)+0.5

      if(j.lt.j1)then
        jj=j+trj+0.5
        if(n.lt.n2)then
          s=s+tab(i,jj,n+1)
        else
          s=s+2*tab(i,jj,n)-tab(i,jj,n-1)
        endif
      endif

      if(j.ge.j1.and.j.le.j2) s=s+tab(i,j,n)

      if(j.gt.j2)then
        jj=j-trj+0.5
        if(n.gt.n1)then
          s=s+tab(i,jj,n-1)
        else
        s=s+2*tab(i,jj,n)-tab(i,jj,n+1)
        endif
      endif

      enddo
      pmoy=s/float(i2-i1+1)
      return
      end
c---------------------------
      subroutine corfou(tb,iitab,jjtab,lltab,i1,ia,ib,i2,
c                ------
     1                     j1,ja,jb,j2,lm,ides,nh,lpar2,labh)
c
c                         dans tb(i,j,l) se trouvent
c                            la valeur v           dans le tableau l
c                            la phase iph                          l+lm
c                            l'amplitude crec                      l+2*lm

c if iw.ne.0 les corrections sont recalculees apres statistique
c               et appliquees a tb (15 premiers tableaux max) 


      integer*2 tb(iitab,jjtab,lltab)
c                               45      = 3*15
      dimension x(360),y(360),p(360),kmax(60),xline(2),yline(2),! z(360)
c                                                 l
     1          amp(36,5,60),pamp(36,5,60),ampmin(60),ampmax(60)
c                  nph nb l
      dimension cn36(72),sn36(72)
      dimension af( 5,60,3),  bf(5,60,3),cf(5,60,3) 
c                  nb  l nharm            ct term
      dimension cndeg(720),sndeg(720)
      dimension afc(5,60,3),bfc(5,60,3),cfc(5,60,3),pp(32),cc(5)
c                  nc  l nharm
      character*30 labh

      nharm=nh
      lharm=2

        piv=3.14159/180.
      do nd=1,720
        ang=piv*(float(nd)-0.5)
        cndeg(nd)=cos(ang)
        sndeg(nd)=sin(ang)
      enddo
c        write(3,*)' cndeg ',cndeg
c        write(3,*)' sndeg ',sndeg

      if(lm.gt.15)then
      print *,' Number of wavelengths too large for Fourier corrections'
      stop
      endif
      
      nbm=5
      nphm=36   ! obligatoire
             piv=3.14159/18.
      do n=1,72
      ang=piv*(float(n)-0.5)
      cn36(n)=cos(ang)
      sn36(n)=sin(ang)
      enddo

c min et max de i0, bandes y0
      y0min=20000.
      y0max=-20000.
        do i=ia,ib
c        do j=ja,jb
        do j=j1,j2
            call paramfou(tb,iitab,jjtab,lltab,i,j,piv0,lpar2)
c                --------
        y0min=amin1(y0min,piv0)
        y0max=amax1(y0max,piv0)
        enddo
        enddo

        dy0=(y0max-y0min)/float(nbm)
         write(3,*)' y0min,y0max,dy0 ',y0min,y0max,dy0

c amp par bandes/phase
           dx=360./float(nphm)
        do i=1,nphm
        x(i)=dx/2.+dx*float(i-1)      
        enddo

      do l=1,lm
        ampmin(l)=20000.
        ampmax(l)=-20000.

        do nb=1,nbm
        do nph=1,36
        amp(nph,nb,l)=0.
        pamp(nph,nb,l)=0.
        enddo
        enddo

      do i=ia,ib
c      do j=ja,jb
      do j=j1,j2
        call paramfou(tb,iitab,jjtab,lltab,i,j,piv0,lpar2)
c            --------
          imil=tb(i,j,l+lm)
        call phasedeg(imil,nh,hdeg)
c            --------
        piv=tb(i,j,l+lm*lharm)             !   amplitude four

        ampmin(l)=amin1(ampmin(l),piv)
        ampmax(l)=amax1(ampmax(l),piv)

        nb=1+(piv0-y0min)/dy0
          if(nb.gt.nbm)nb=nbm
        nph=1+hdeg/dx
        amp(nph,nb,l)=amp(nph,nb,l)+piv   !  amplitude
        pamp(nph,nb,l)=pamp(nph,nb,l)+1.  !  poids
      if(i.eq.ib.and.j.eq.jb)
     1 write(3,*)' ampmin(l),ampmax(l),piv0,hdeg,piv,nb,nph ',
     2             ampmin(l),ampmax(l),piv0,hdeg,piv,nb,nph

      enddo
      enddo

      do nb=1,nbm
      do nph=1,nphm
            piv=pamp(nph,nb,l)
            if(piv.ne.0.)then
              amp(nph,nb,l)=amp(nph,nb,l)/piv
            else
              amp(nph,nb,l)=0.
            endif
      enddo
        write(3,*)' amp(nph,nb,l),pamp(nph,nb,l),nph=1,36 ',
     1             (amp(nph,nb,l),pamp(nph,nb,l),nph=1,36)
      enddo

      enddo    ! fin boucle l

c dessin
      if(ides.ne.0)then
        do i=1,6
        call pgadvance
        enddo

      do nb=1,nbm
      do l=1,lm

      if(ides.ne.0.and.l.le.5)then

          call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)
      call pgwindow(0.,360.,ampmin(l),ampmax(l))
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
      call pglabel('phase',' ',labh)
c          call pgsls(nb)
c          call pgslw(nb)
          do nph=1,nphm
              y(nph)=amp(nph,nb,l)
          enddo

         call pgline(nphm,x,    y)
c             ------      phase amplitude moyenne four par bande de phase
      endif

c calcul coefficients Fourier

        piva=0
        pivb=0
        pivc=0
        pivmoy=0.

      do nph=1,nphm
        pivmoy=pivmoy+amp(nph,nb,l)
      enddo
        aa=pivmoy/float(nphm)

        scc=0.
        sss=0.
      do nph=1,nphm
        piva=piva+(amp(nph,nb,l)-aa)*cn36(nph)
        pivb=pivb+(amp(nph,nb,l)-aa)*sn36(nph)
        scc=scc+cn36(nph)**2
        sss=sss+sn36(nph)**2
      enddo

        af(nb,l,nharm)=piva/scc
        bf(nb,l,nharm)=pivb/sss
        cf(nb,l,nharm)=aa

      if(ides.ne.0.and.l.le.5)then
      do nph=1,nphm
        y(nph)=af(nb,l,nharm)*cn36(nph)+
     1         bf(nb,l,nharm)*sn36(nph)+
     2         cf(nb,l,nharm)
      enddo
        call pgsls(2)
      call pgline(nphm,x,     y)
c          ------      phase  Fourier ajuste
        call pgsls(1)
        if(l.eq.5)call pgadvance
      endif

      enddo   ! fin boucle l
      enddo   ! fin boucle nb
      endif

c amp moy/phase

        do k=1,360
        x(k)=float(k)-0.5 
        enddo

c                   on va dessiner 5 plots (l=1,5) pour
c                       harm 1   amp moy/phase
c                                amp    /I0 centre raie   a amp max
c    ou                 harm 2   amp moy/phases
c                                amp    /I0 centre raie   a amp max
      if(ides.ne.0)then      
      do l=1,5
        do k=1,360
        p(k)=0.
        y(k)=0.
        enddo

        do i=ia,ib
c        do j=ja,jb
        do j=j1,j2
          imil=tb(i,j,l+lm)
          call phasedeg(imil,nh,hdeg)
c              --------
          k=hdeg+0.5
          y(k)=y(k)+tb(i,j,l+lharm*lm)
          p(k)=p(k)+1.
        enddo
        enddo

        do k=1,360
        if(p(k).ne.0.)y(k)=y(k)/p(k)
        enddo

      call pgadvance
        ymin=0.
        ymax=0.
        kmax(l)=1
      do k=1,360
c        ymin=amin1(y(k),ymin)
        ymax=amax1(y(k),ymax)
        if(ymax.eq.y(k))kmax(l)=k      !    phase de correction max
c        ymax=amax1(ymax,1.)
      enddo
c        ampmin(l)=ymin
c        ampmax(l)=ymax
      call pgvport(0.1,0.9,0.1,0.8)
      call pgwindow(0.,360.,ampmin(l),ampmax(l))
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
      call pglabel('phase',' ',labh)
      call pgline(360,x,y)
c          ------
         xline(1)=x(kmax(l))
         xline(2)=xline(1)
         yline(1)=ampmin(l)
         yline(2)=ampmax(l)
      call pgline(2,xline,yline)
c          ------
      write(3,*)' corfou:l, y(i),i=1,360,20 ',
     1                   l, (y(i),i=1,360,20)

        if(l.eq.5)call pgadvance
      enddo
      endif
c             fin des dessins amplitude moy/phase

      if(ides.ne.0)then
      do l=1,5
c          xmin=20000.
c          xmax=-20000.
c          ymin=10000.
c          ymax=-10000.
          npoints=0
        do i=ia,ib
c        do j=ja,jb
        do j=j1,j2
          imil=tb(i,j,l+lm)
            call phasedeg(imil,nh,hdeg)
c                --------
          kphase=hdeg+0.5
          ktest=iabs(kphase-kmax(l))
          if(ktest.le.5)then
             npoints=npoints+1
c             xi0=tb(i,j,1)
c             xmin=amin1(xi0,xmin)
c             xmax=amax1(xi0,xmax)
             yamp=tb(i,j,l+lharm*lm)
c             ymin=amin1(yamp,ymin)
c             ymax=amax1(yamp,ymax)
          endif
        enddo
        enddo

          call pgadvance
          if(npoints.ne.0)then
            call pgvport(0.1,0.9,0.1,0.8)
            call pgwindow(y0min,y0max,ampmin(l),ampmax(l))
            call pgbox('bcnts',0.,0,'bcnts',0.,0)
            call pglabel('I2-I0',' ',labh)
          do i=ia,ib
c          do j=ja,jb
          do j=j1,j2
            imil=tb(i,j,l+lm)
            call phasedeg(imil,nh,hdeg)
c                --------
            kphase=hdeg+0.5
            ktest=iabs(kphase-kmax(l))
            if(ktest.le.5)then

               call paramfou(tb,iitab,jjtab,lltab,i,j,xx,lpar2)
c                   --------
               yy=tb(i,j,l+lharm*lm)
               call pgpoint(1,xx,    yy,2)
c                   -------   param  correction Fourier pour phase du max
            endif
          enddo
          enddo
 
          do nb=2,nbm
            xline(1)=(nb-1)*dy0+y0min
             write(3,*)' y0min,xline(1) ',y0min,xline(1)
            xline(2)=xline(1)
            yline(1)=ampmin(l)
            yline(2)=ampmax(l)
            call pgline(2,xline,yline)
          enddo

          endif
        if(l.eq.5)call pgadvance
      enddo   ! fin boucle l
      endif
c             fin des dessins amplitude/I0 a phase amp max

c interpol af,bf,cf par pol 4eme degre
      do l=1,lm

      do nb=1,5
      pp(nb)=af(nb,l,nharm)
      enddo
        call cpoly4(pp,1,cc)
      do nc=1,5
      afc(nc,l,nharm)=cc(nc)
      enddo

      do nb=1,5
      pp(nb)=bf(nb,l,nharm)
      enddo
        call cpoly4(pp,1,cc)
      do nc=1,5
      bfc(nc,l,nharm)=cc(nc)
      enddo

      do nb=1,5
      pp(nb)=cf(nb,l,nharm)
      enddo
        call cpoly4(pp,1,cc)
      do nc=1,5
      cfc(nc,l,nharm)=cc(nc)
      enddo

c      print *,' afc ',(afc(nc,l,nharm),nc=1,5)

c dessin
      if(ides.ne.0)then
      if(l.le.5)then
        call pgadvance
        call pgvport(0.1,0.9,0.1,0.8)
        call pgwindow(y0min,y0max,ampmin(l),ampmax(l))
        call pgbox('bcnts',0.,0,'bcnts',0.,0)
        call pgwindow(-2.5,2.5,ampmin(l),ampmax(l))
        call pglabel('I2-I0',' ',labh)

        do i=1,51
        x(i)=-2.6+float(i)*0.1
        xx=4.*x(i)
        y(i)=afc(1,l,nharm)+
     1       xx*(afc(2,l,nharm)+
     2       xx*(afc(3,l,nharm)+
     3       xx*(afc(4,l,nharm)+
     4       xx*(afc(5,l,nharm)))))
        enddo

        call pgsls(1)
        call pgline(51,x,y)

        do i=1,51
        xx=4.*x(i)
        y(i)=bfc(1,l,nharm)+
     1       xx*(bfc(2,l,nharm)+
     2       xx*(bfc(3,l,nharm)+
     3       xx*(bfc(4,l,nharm)+
     4       xx*(bfc(5,l,nharm)))))
        enddo

        call pgsls(2)
        call pgline(51,x,y)

        do i=1,51
        xx=4.*x(i)
        y(i)=cfc(1,l,nharm)+
     1       xx*(cfc(2,l,nharm)+
     2       xx*(cfc(3,l,nharm)+
     3       xx*(cfc(4,l,nharm)+
     4       xx*(cfc(5,l,nharm)))))
        enddo

        call pgsls(3)
        call pgline(51,x,y)
        call pgsls(1)

      do i=1,5
      xx=i-3
      yy=af(i,l,nharm)
      call pgpoint(1,xx,yy,2)
c          -------
      enddo

      endif
        if(l.eq.5)call pgadvance
      endif

c corrections
        do i=i1,i2
        do j=j1,j2
          imil=tb(i,j,l+lm)
          call phasedeg(imil,nh,hdeg)
c              --------
          ideg=hdeg+0.5
          call paramfou(tb,iitab,jjtab,lltab,i,j,piv0,lpar2)
c              --------
          xx=4.*(5.*(piv0-y0min)/(y0max-y0min)-2.5)
          afx=afc(1,l,nharm)+
     1       xx*(afc(2,l,nharm)+
     2       xx*(afc(3,l,nharm)+
     3       xx*(afc(4,l,nharm)+
     4       xx*(afc(5,l,nharm)))))
          bfx=bfc(1,l,nharm)+
     1       xx*(bfc(2,l,nharm)+
     2       xx*(bfc(3,l,nharm)+
     3       xx*(bfc(4,l,nharm)+
     4       xx*(bfc(5,l,nharm)))))
          cfx=cfc(1,l,nharm)+
     1       xx*(cfc(2,l,nharm)+
     2       xx*(cfc(3,l,nharm)+
     3       xx*(cfc(4,l,nharm)+
     4       xx*(cfc(5,l,nharm)))))
          correc=afx*cndeg(ideg)+bfx*sndeg(ideg)+cfx
          tb(i,j,l)=tb(i,j,l)+correc
c                             ------
        enddo
        enddo

      enddo  !  fin boucle l

c dessin moyennes vers phase 0 en fonction de i
      if(ides.ne.0)then
      do l=1,5
        
        xi1=i1
        xi2=i2
        call pgadvance
        call pgvport(0.1,0.9,0.1,0.8)
          pivmin=ampmin(l)
          pivmax=ampmax(l)
        call pgwindow(xi1,xi2,pivmin,pivmax)
        call pgbox('bcnts',0.,0,'bcnts',0.,0)
        call pglabel('pixels i',' ',labh)
        
          imax=(ib-ia+1)/10
          y1=0.
          z1=0.
        do ip=1,imax
        iaa=ia+(ip-1)*10
        ibb=iaa+9
        x(ip)=(iaa+ibb)/2
          piv1=0.
          piv2=0.
          pivz=0.
          do i=iaa,ibb
c          do j=ja,jb
          do j=j1,j2
              imil=tb(i,j,l+lm)
               call phasedeg(imil,nh,hdeg)
c                  --------
              kpiv=hdeg+0.5
            if(kpiv.lt.10)then
            piv1=piv1+tb(i,j,l+lm*lharm)
c            call paramfou(tb,iitab,jjtab,lltab,i,j,pivz,lpar2)
c                --------
            piv2=piv2+1.
            endif
          enddo
          enddo
        y(ip)=y1
c        z(ip)=z1
        if(piv2.ne.0.)then
          y(ip)=piv1/piv2
c          z(ip)=pivz/piv2
        endif
        y1=y(ip)
c        z1=z(ip)
        enddo

        call pgline(imax,x,y)
c            ------
c        call pgwindow(xi1,xi2,y0min,y0max)
c        call pgsls(2)
c        call pgline(imax,x,z)
c            ------
        call pgsls(1)
        if(l.eq.5)call pgadvance
      enddo  !  fin boucle l
      endif

      return
      end
c----------------------------------------------
      subroutine paramfou(tb,iitab,jjtab,lltab,i,j,piv,lpar2)
c                --------
      integer*2 tb(iitab,jjtab,lltab)

      piv=tb(i,j,4)-tb(i,j,1)
      return
      end
c-----------------------------------------------
      subroutine corfou3(tb,iitab,jjtab,lltab,
c                -------
     1          i1,ia,ib,i2,j1,ja,jb,j2,lm,idesp,iw,nscan,
     2          mgrim,xdeb,xgri,txgri,txana,nh,lpar2,labh)

      integer*2 tb(iitab,jjtab,lltab),ijtab(2000,200),inutil(2000)
      dimension cn36(36),sn36(36),!  amp(36),  x(1000),y(1000),
     1          cndeg(360),sndeg(360),r(4,36,60),
c                                     coef regres
     2          af(4,60,3),bf(4,60,3),cf(4,60,3),
c               coef fourier 
     3          pmin(3),pmax(3)
      character*7 tit(3)
      character*30 labh
c                         dans tb(i,j,l) se trouvent
c                            la valeur v           dans le tableau l
c                            la phase imil                         l+lm
c                            l'amplitude correc ampj               l+2*lm

c  les corrections sont recalculees apres statistique
c  avec regression lineaire sur 3 parametres
c  et appliquees a tb (15 premiers tableaux max) 

      nharm=nh
      lharm=2
      nphm=36

      do i=1,i2
      inutil(i)=0
      enddo

      tit(1)='param 1'
      tit(2)='param 2'
      tit(3)='param 3'

      ides=idesp
      if(ides.ne.0)then
         do i=1,6
         call pgadvance
         enddo
      endif

      piv=3.14159/18.
      do n=1,36
      ang=piv*(float(n)-0.5)
      cn36(n)=cos(ang)
      sn36(n)=sin(ang)
      enddo

      piv=3.14159/180.
      do n=1,360
      ang=piv*(float(n)-0.5)
      cndeg(n)=cos(ang)
      sndeg(n)=sin(ang)
      enddo

c                  3  pour regression + Fourier
c                <36  pour regression + nphm bandes de phases

c    intervalles de variation de p1,p2,p3
        p1min=100000.
        p2min=100000.
        p3min=100000.
        p1max=-100000.
        p2max=-100000.
        p3max=-100000.

        write(3,*)' corfou3: ia,ib,j1,ja,jb,j2 ',ia,ib,j1,ja,jb,j2
        if(ja.gt.jb)then
           print *,' corfou3: ja>jb'
           stop
        endif
   
      do2 i=ia,ib

      call iutil(i,mgrim,xdeb,xgri,txgri,txana,no)
c          -----
      if(no.eq.1)goto2
c      do j=ja,jb
      do j=j1,j2
      call paramfou3(tb,iitab,jjtab,lltab,i,j,p1,p2,p3,lpar2)
c          ---------

      p1min=amin1(p1min,p1)
      p2min=amin1(p2min,p2)
      p3min=amin1(p3min,p3)
      p1max=amax1(p1max,p1)
      p2max=amax1(p2max,p2)
      p3max=amax1(p3max,p3)

      enddo
2     continue

      pmin(1)=p1min
      pmin(2)=p2min
      pmin(3)=p3min
      pmax(1)=p1max
      pmax(2)=p2max
      pmax(3)=p3max

      write(3,*)' corfou3: pmin(3),pmax(3) ',pmin,pmax

       do10 l=1,lm

c pour chaque bande de phase, on calcule la regression de amp/p1,p2,p3
      do nph=1,nphm

      call regres3(tb,iitab,jjtab,lltab,ia,ib,j1,ja,jb,j2,l,lm,lharm,
c          -------
     1          nph,nphm,r(1,nph,l),r(2,nph,l),r(3,nph,l),r(4,nph,l),
     2          mgrim,xdeb,xgri,txgri,txana,nh,lpar2)

1     format(' nharm,l,nph,  r1,r2,r3,r4 ',3i3,4f9.3)
      if(nph.eq.1.or.nph.eq.10)write(3,1)nharm,l,nph,
     1          r(1,nph,l),r(2,nph,l),r(3,nph,l),r(4,nph,l)
      enddo

         if(ides.eq.1.and.l.le.5)
     1call desreg(tb,iitab,jjtab,lltab,ia,ib,j1,ja,jb,j2,l,lm,nh,
c          ------
     1     r,pmin,pmax,mgrim,xdeb,xgri,txgri,txana,tit,lpar2,labh)

      if(iw.eq.3)then
c on calcule les coefficients Fourier af,bf,cf(np,   l)
c                                             param  tableau
          
      do5 np=1,4  !  boucle parametres p1,p2,p3,terme constant  
        piva=0
        pivb=0
        pivc=0
        pivmoy=0.

      do nph=1,nphm
        pivmoy=pivmoy+r(np,nph,l)
      enddo
        aa=pivmoy/float(nphm)

      do nph=1,nphm
        piva=piva+(r(np,nph,l)-aa)*cn36(nph)
        pivb=pivb+(r(np,nph,l)-aa)*sn36(nph)
      enddo

        af(np,l,nharm)=piva*2./float(nphm)
        bf(np,l,nharm)=pivb*2./float(nphm)
        cf(np,l,nharm)=aa
3     format(' np,l,nharm,  af,bf,cf ',3i3,3f7.3)
      write(3,3)np,l,nharm,
     1          af(np,l,nharm),bf(np,l,nharm),cf(np,l,nharm)  

5     continue    !    fin boucle param
6     format('nh,l,af(4),bf(4)',2i3,8f7.4)
      write(7,*)' nh,l,af(4),bf(4) ',
     1            nh,l,(af(nf,l,nharm),nf=1,4),(bf(nf,l,nharm),nf=1,4)
      endif

c correction de tb

      do9 i=i1,i2

      call iutil(i,mgrim,xdeb,xgri,txgri,txana,no)
c          -----
         if(no.eq.1)then
          inutil(i)=1
        goto9
        endif 
      do j=j1,j2

      call paramfou3(tb,iitab,jjtab,lltab,i,j,p1,p2,p3,lpar2)
c          ---------
        imil=tb(i,j,l+lm)
      call phasedeg(imil,nh,ph)
c          --------
      idegh=ph+0.5 

      if(iw.eq.3)then    !    Fourier

      correc=(af(1,l,nharm)*p1+af(2,l,nharm)*p2+af(3,l,nharm)*p3
     1       +af(4,l,nharm))*cndeg(idegh)+
     2       (bf(1,l,nharm)*p1+bf(2,l,nharm)*p2+bf(3,l,nharm)*p3
     3       +bf(4,l,nharm))*sndeg(idegh)+
     4        cf(1,l,nharm)*p1+cf(2,l,nharm)*p2+cf(3,l,nharm)*p3
     5       +cf(4,l,nharm)

      else    !                bandes de phases
         nbm=360/nphm
         nph=1+idegh/nbm
      correc=r(1,nph,l)*p1+r(2,nph,l)*p2+r(3,nph,l)*p3+r(4,nph,l)
        if(i.eq.50.and.j.eq.10)write(3,*)' correc bandes phases '
      endif

      tb(i,j,l)=tb(i,j,l)+correc
c     --------------------------
      piv=tb(i,j,l+lm*lharm)
      ijtab(i,j)=0
      if(piv.ne.0.)
     1 ijtab(i,j)=piv-correc

      if(i.eq.50.and.j.eq.10)then
c8     format('l,nharm,  i,j, ideg - af1,af2,af3,af4 '/
c     1        5i10/4f10.4)
c      write(3,8)
c     1   l,nharm,i,j,ideg,
c     2   af(1,l,nharm),af(2,l,nharm),af(3,l,nharm),af(4,l,nharm)

      write(3,*)' p1,p2,p3,correc,amp ',
     1            p1,p2,p3,correc,piv
      endif
      enddo
9     continue

      if(ides.eq.1)then
95    format(' corfou3: tableau des indices elimines')
      write(3,95)
96    format(50i1)
      write(3,96)(inutil(i),i=1,i2)
      endif

c dessin
      if(ides.eq.1)
     1   call desfou(tb,iitab,jjtab,lltab,
c             ------
     2   pmin,pmax,ijtab,ia,ib,j1,ja,jb,j2,l,
     3   mgrim,xdeb,xgri,txgri,txana,tit,lpar2,labh)


10    continue  ! fin  boucle l

      return
      end
c------------------------------------------------------------
      subroutine paramfou3(tb,iitab,jjtab,lltab,i,j,p1,p2,p3,lpar2)
c                ---------
      integer*2 tb(iitab,jjtab,lltab)

      p1=tb(i,j,lpar2+2)-tb(i,j,1)
      p2=tb(i,j,lpar2)-tb(i,j,1)
      p3=tb(i,j,lpar2+3)-tb(i,j,lpar2+1)

      return
      end
c-------------------------------------------------------------
      subroutine regres3(tb,iitab,jjtab,lltab,
c                -------
     1     ia,ib,j1,ja,jb,j2,l,lm,lharm,nph,nphm,ar,br,cr,dr,
     2     mgrim,xdeb,xgri,txgri,txana,nh,lpar2)

c        t                 =ar*p1    +br*p2          +cr*p3         +d
c    amp fourier a,b ou c        param1    param2          param3
c                                I2-I0     I1-I0           V2-V1
c                             tache/fac  largeur noyau   dysymetrie

      integer*2 tb(iitab,jjtab,lltab)
      double precision a(3,3),c(3),xx(3)  ! pour cramer3
c                                 ar,br,cr
      double precision  sx2,sy2,sz2,sx,sy,sz,sxy,syz,szx,st,
     1                  stx,sty,stz     
c      dimension x(100),y(100)

      sx=0.
      sy=0.
      sz=0.
      sx2=0.
      sy2=0.
      sz2=0.
      sxy=0.
      syz=0.
        szx=0.
      st=0.
      stx=0.
      sty=0.
      stz=0.
      p=0.
      tmin=100000.
      tmax=-100000.
      nbm=360/nphm

      do 11 i=ia,ib
      call iutil(i,mgrim,xdeb,xgri,txgri,txana,no)
c          -----
        if(no.eq.1)goto 11
c      do10 j=ja,jb
      do10 j=j1,j2
        imil=tb(i,j,l+lm)
        call phasedeg(imil,nh,hdeg)
c            --------
         ideg=hdeg+0.5
         nph1=1+ideg/nbm
        if(i.eq.60.and.j.eq.ja)
     1  write(3,*)' regres3: imil,nh,hdeg,nph1,nph ',
     2                       imil,nh,hdeg,nph1,nph

      if(nph1.ne.nph)goto10

      call paramfou3(tb,iitab,jjtab,lltab,i,j,p1,p2,p3,lpar2)
c          ---------

      t=tb(i,j,l+lm*lharm)
        tmin=amin1(tmin,t)
        tmax=amax1(tmax,t)
      sx=sx+p1
      sy=sy+p2
      sz=sz+p3
      sx2=sx2+p1*p1
      sy2=sy2+p2*p2
      sz2=sz2+p3*p3
      sxy=sxy+p1*p2
      syz=syz+p2*p3
      szx=szx+p3*p1
      st=st+t
      stx=stx+t*p1
      sty=sty+t*p2
      stz=stz+t*p3
      p=p+1.
  
10    continue
11    continue

      if(p.eq.0.)then
      print *,' poids = 0 dans cramer3, statistique trop faible'
      endif

      a(1,1)=sx2-sx*sx/p
      a(2,1)=sxy-sx*sy/p
      a(3,1)=szx-sz*sx/p

      a(1,2)=a(2,1)
      a(2,2)=sy2-sy*sy/p
      a(3,2)=syz-sy*sz/p

      a(1,3)=a(3,1)
      a(2,3)=a(3,2)
      a(3,3)=sz2-sz*sz/p

      c(1)=stx-st*sx/p
      c(2)=sty-st*sy/p
      c(3)=stz-st*sz/p

      call cramer3(a,c,xx)
c          ------- 
      ar=xx(1)
      br=xx(2)
      cr=xx(3)

      dr=(st-ar*sx-br*sy-cr*sz)/p

      return
      end
c------------------------------------------------------------
      subroutine cramer3(a,c,x)
c                -------
c Resolution d'un systeme de 3 eq lineaires a 3 inconnues

      double precision a(3,3),c(3),x(3),dd,cc

c                   a(1,1)*x(1)+a(2,1)*x(2)+a(3,1)*x(3)=c(1)
c                       2           2           2         2
c                       3           3           3         3

      call det3(a,dd)

      if(dd.eq.0.)then
         print *,' determinant nul dans cramer3 '
         stop
      endif

      do n=1,3
      call det3c(a,c,n,cc)

      x(n)=cc/dd

      enddo

      return
      end
c----------------------------------------
      subroutine det3c(a,c,n,cc)
c                -----
      double precision a(3,3),c(3),p(3,3),cc

c  cc = valeurs de a avec rempolacement de la colonne n par c


        do i=1,3
        do j=1,3
        p(i,j)=a(i,j)
        enddo
        enddo

        do j=1,3
        p(n,j)=c(j)
        enddo

        call det3(p,cc)

      return
      end
c----------------------------------------
      subroutine det3(a,dd)
c                ----
      double precision a(3,3),dd

      dd =a(1,1)*(a(2,2)*a(3,3)-a(3,2)*a(2,3))+
     1    a(2,1)*(a(3,2)*a(1,3)-a(1,2)*a(3,3))+
     2    a(3,1)*(a(1,2)*a(2,3)-a(2,2)*a(1,3))

      return
      end
c----------------------------------------
      subroutine desfou(tb,iitab,jjtab,lltab,
c                ------
     1                 pmin,pmax,ijtab,ia,ib,j1,ja,jb,j2,l,
     2                 mgrim,xdeb,xgri,txgri,txana,tit,lpar2,labh)
      integer*2 tb(iitab,jjtab,lltab),ijtab(2000,200)
      dimension pmin(3),pmax(3)
      character*7 tit(3)
      character*30 labh

      write(3,*)' desfou: pmin,pmax,ia,ib,j1,ja,jb,j2',
     1                    pmin,pmax,ia,ib,j1,ja,jb,j2

      do np=1,3
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)
      call pgwindow(pmin(np),pmax(np),-100.,100.)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
      call pglabel(tit(np),'correc locale-globale',labh)

      do 10 i=ia,ib,10
      call iutil(i,mgrim,xdeb,xgri,txgri,txana,no)
c          -----
        if(no.eq.1)goto 10
      do j=j1,j2
      call paramfou3(tb,iitab,jjtab,lltab,i,j,p1,p2,p3,lpar2)
c          ---------
      
      if(np.eq.1)x=p1
      if(np.eq.2)x=p2
      if(np.eq.3)x=p3

      y=ijtab(i,j)

      if(i.eq.ia.and.j.eq.ja)
     1write(3,*)' dessin cmd1/desfou: np,x,y pour ia,ja ',np,x,y
      call pgpoint(1,x,y,2)
c          -------
      enddo
10    continue

      enddo   !  fin boucle np
      return
      end
c------------------------------------
      subroutine desreg(tb,iitab,jjtab,lltab,ia,ib,j1,ja,jb,j2,l,lm,nh,
c                ------
     1             rr,ppmin,ppmax,mgrim,xdeb,xgri,txgri,txana,tit,
     2             lpar2,labh)
      integer*2 tb(iitab,jjtab,lltab)
      dimension rr(4,36,60),ppmin(3),ppmax(3),pmin(6),pmax(6),
     1          p(3),p0(6),r(6),x(101),y(101)
      character*7 tit(3)
      character*30 labh
      lharm=2

      do np=1,3
      pmin(np)=ppmin(np)
      pmin(np+3)=ppmin(np)
      pmax(np)=ppmax(np)
      pmax(np+3)=ppmax(np)

      r(np)=rr(np,1,l)
      r(np+3)=r(np)
      enddo

      do np=1,6
      p0(np)=0.5*(pmin(np)+pmax(np))
      enddo

      do10 n=1,3   !   boucle des 3 parametres
c   pour le dessin, on ne considere que les les crec entre 0 et 10 degres
c   (nph=1) correpondant a cosinus voisin de 1

c terme constant
      ct=r(n+1)*p0(n+1)+r(n+2)*p0(n+2)+rr(4,1,l)

c calcul ymin,ymax
        ymin=100000.
        ymax=-100000.

      do 6 i=ia,ib
      call iutil(i,mgrim,xdeb,xgri,txgri,txana,no)
c          -----
        if(no.eq.1)goto 6
      do 5 j=j1,j2
c  on ne garde que les points a phase < 10 degres
        imil=tb(i,j,l+lm)
        call phasedeg(imil,nh,hdeg)
c            --------
      if(hdeg.gt.10.)goto5

      call paramfou3(tb,iitab,jjtab,lltab,i,j,p(1),p(2),p(3),lpar2)
c          ---------

      yq=r(1)*p(1)+r(2)*p(2)+r(3)*p(3)
      yp=r(n)*p(n)+r(n+1)*p0(n+1)+r(n+2)*p0(n+2)
      yn=tb(i,j,l+lm*lharm)
      ym=yp+yn-yq
        ymin=amin1(ymin,ym)
        ymax=amax1(ymax,ym)
        if(ymin.eq.ymax)ymax=ymin+1.
5     continue
6     continue
        write(3,*)' desreg: nparam,pmin(n),pmax(n),ymin,ymax ',
     1                      n,     pmin(n),pmax(n),ymin,ymax

c dessin points
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)
      call pgwindow(pmin(n),pmax(n),ymin,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
      call pglabel(tit(n),'regression',labh)
      
      do 9 i=ia,ib
      call iutil(i,mgrim,xdeb,xgri,txgri,txana,no)
c          -----
        if(no.eq.1)goto 9
c      do8 j=ja,jb
      do 8 j=j1,j2
c  on ne garde que les points a phase < 10 degres
        imil=tb(i,j,l+lm)
        call phasedeg(imil,nh,hdeg)
c            --------
      if(hdeg.gt.10.)goto 8

      call paramfou3(tb,iitab,jjtab,lltab,i,j,p(1),p(2),p(3),lpar2)
c          ---------

      xx=p(n)

      yq=r(1)*p(1)+r(2)*p(2)+r(3)*p(3)
      yp=r(n)*p(n)+r(n+1)*p0(n+1)+r(n+2)*p0(n+2)
      yn=tb(i,j,l+lm*lharm)
      ym=yp+yn-yq

      call pgpoint(1,xx,ym,2)
c          -------
8     continue
9     continue

c dessin lignes regression
      do i=1,101
      x(i)=pmin(n)+(pmax(n)-pmin(n))*(i-1)/100.
      y(i)=r(n)*x(i)+ct
      enddo

      call pgline(101,x,y)
c          ------

10    continue   !   fin boucle l

      return
      end
c-----------------------------------------
      subroutine iutil(i,mgrim,xdeb,xgri,txgri,txana,no)
c                -----
         no=0
      if(mgrim.eq.0)return

        no=1
      do mgri=1,mgrim
         
      ideb1=xdeb+txgri*(mgri-1)+1
      ifin1=ideb1+xgri
      itest1=(i-ideb1)*(i-ifin1)
      if(itest1.le.0)goto10

      ideb2=ideb1+txana
      ifin2=ideb2+xgri
      itest2=(i-ideb2)*(i-ifin2)
      if(itest2.le.0)goto10

      enddo
      return
10    no=0
      return
      end
c------------------------------------------------------------
      subroutine phasedeg (imil,   nh,    hdeg)
c                -------- 1/1000  1,2,3  phase en degres de l'harmonique

      per=2000./2.**(nh-1)
      iper=per

      i=imil/iper
      hdeg=360.*(imil-per*i)/per

      return
      end
c-------------------------------------------------------------
      subroutine interpoly(tab,iitab,jjtab,nntab,i,j,j1,j2,n1,n2,
c                ---------
     1   intery,dypix,ltrj,xp,piv1,piv2,
c                          out inutiles
     2   iplotpro,iprofil,jprofil,kana,ides)

      integer*2 tab(iitab,jjtab,nntab)
      dimension xp(129),c(4,5),xq(129),pa(16),pb(16),
     1          xdes(129),ydes(32),pp(32),xxdes(129),yydes(129)

c      print *,' i,j,iplotpro,kana,intery ',
c     1          i,j,iplotpro,kana,intery

      trj=float(ltrj)/200.  !  en pixels
      km=4*(n2-n1)+1
      kpm=2*km-1
      nm=n2-n1+1
      lm=2*nm
c------------                      compromise +/-dypix pixels
      if(intery.eq.1)then
c        -----------
          alp=1.-2*dypix/trj
          den=2.*(1.-alp*alp)
        ca=-alp*alp/den
        cb=1./den
        cc=cb
        cd=ca
      if(i.eq.iprofil.and.j.eq.jprofil)
     1    print *,' dypix,trj,ca,cb ',dypix,trj,ca,cb

      xa=j-dypix
      xb=j+dypix
        ja=xa
        jb=xb
        dja=xa-ja
        djb=xb-jb
c      print *,' j1,ja,jb,j2 ',j1,ja,jb,j2
      if(ja.lt.j1)return
      if(jb.gt.j2)return

c    n    1   2   3               nm 
c          a b
c    k    1 3 5 7 9 .            4nm-3
c    k    12345...9...           4nm-3

      do n=1,nm-1
        nt=n+n1-1
        t1=tab(i,j,nt)
        t2=tab(i,j,nt+1)
        tb=tab(i,jb,nt+1)*(1.-djb)+tab(i,jb+1,nt+1)*djb
        ta=tab(i,ja,nt)*(1.-dja)+tab(i,ja+1,nt)*dja
          pb(n)=tb
          pa(n)=ta 
        k=4*n-3             !  1   5   9   ..    cotes rondes
        xp(k)=t1

        k=k+2                !    3   7   ..
        xp(k)=ca*t1+cb*ta+cc*tb+cd*t2   !  cubique voisinages
      enddo
      xp(km)=tab(i,j,n2)   !                4nm-3  derniere cote ronde

c interpolations cubiques pour      4,6,8,...  km-3
c                                                       (-3-1 [0] 1 3)
c                paraboles         1              km-1
      call intercub(xp,km)

c---  dessin
      if(ides.eq.1.and.kana.eq.1.and.iplotpro.eq.1.and.
     1   i.eq.iprofil.and.j.eq.jprofil)then

c         print *,' i,j ',i,j
         print *,' tab ',(tab(i,j,n),n=1,nm)
         write(3,*)' xp  ',(xp(k),k=1,km)
c interpolations cubiques pour     yydes
        do kp=1,kpm,2
           k=(kp+1)/2
        yydes(kp)=xp(k)
        enddo
      call intercub(yydes,kpm)
c         print *,' yydes',(yydes(kp),kp=1,kpm)
c--
      call pgslw(2)
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)

      call pgscr(2,1.,0.,0.)
      call pgscr(3,0.,1.,0.)
      call pgscr(4,0.,0.,1.)

          ymax=-100000.
c                          ymin=+100000.
        do kp=1,kpm
        ymax=amax1(ymax,yydes(kp))
        enddo
          ymax=1.1*ymax
          print *,' ymax=',ymax
          write(7,*)' interpoly: i,j,ymax ',i,j,ymax 
        ymin=0.

        if(ymax.le.0.)ymax=1.
      call pgsch(1.8)
         nm=n2-n1+1

         xcan=nm-1
      call pgwindow(0.,xcan,ymin,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
c      call pglabel('lambda','intensity',' ')

         xcan=kpm-1
      call pgwindow(0.,xcan,ymin,ymax)

      do kp=1,kpm
      xxdes(kp)=float(kp-1)    !  abscisses de yydes
      enddo

      call pgline(kpm,xxdes,yydes)
c---                                 


      call pgsci(2)
      call pgsfs(1)
         do n=1,nm      
         piv=8*(n-1)
         k=4*(n-1)+1                  !  1,9,...kpm
      call pgpoint(1,piv,xp(k),18)    !   stars
         enddo
c      print *,'xp(1)', xp(1)

         do n=1,nm      
         piv=8*(n-1+dypix/trj)
      call pgpoint(1,piv,pa(n),17)    !   cercles pleins
         piv=8*(n-dypix/trj)
      call pgpoint(1,piv,pb(n),17)
         enddo
      call pgsci(1)
      call pgsfs(2)

         do n=1,nm-1
         piv=4+8*(n-1)
         k=4*(n-1)+3                !  3,11....
      call pgpoint(1,piv,xp(k),6)   !   carres
        enddo


      endif        ! fin dessin
c        print *,' i,j,xp ',i,j,(xp(k),k=1,km)


      endif        !  fin intery=1
c---------------------------------------------------
      if(intery.eq.2.or.intery.eq.20)then

         ca=-1./16.
         cb= 9./16.
         cc=cb
         cd=ca
               da= 5./16
               db=15./16.
               dc=-5./16.
               dd= 1./16.
        if(intery.lt.0)then
         ca=0.
         cb=0.5
         cc=0.5
         cd=0.
        endif
c---
      if(intery.eq.2)then

      xa=j-trj/4.
      xb=j+trj/4.
        ja=xa
        jb=xb   
        dja=xa-ja
        djb=xb-jb
      if(ja.lt.j1)return
      if(jb.gt.j2)return

c    n    1   2   3               nm 
c          a b 
c    k    1 3 5 7 9 .            4nm-3
c    k    1...5...9...           4nm-3

      do n=1,nm
        k=4*n-3
        xp(k)=tab(i,j,n)
      enddo
      do n=1,nm-1
        pa(n)=tab(i,ja,nt)*(1.-dja)+tab(i,ja+1,nt)*dja
        pb(n)=tab(i,jb,nt+1)*(1.-djb)+tab(i,jb+1,nt+1)*djb
        xp(k+2)=-tab(i,j,n)+4.*pa(n)+4.*pb(n)-tab(i,j,n+1)
      enddo

      endif      ! fin 2

c---------                                   intery= 20 ou -20
      if(iabs(intery).eq.20)then

      xa=j-trj/4.
      xb=j+trj/4.
        ja=xa
        jb=xb
        dja=xa-ja
        djb=xb-jb
      if(ja.lt.j1)return
      if(jb.gt.j2)return

c    n    1   2   3               nm 
c            b a
c    k    1 3 5 7 9 .            4nm-3
c    k    1...5...9...           4nm-3

      do n=1,nm
        nt=n+n1-1
        t1=tab(i,jb,nt)*(1.-djb)+tab(i,jb+1,nt)*djb
        t2=tab(i,ja,nt)*(1.-dja)+tab(i,ja+1,nt)*dja
 
        pb(n)=t1
        pa(n)=t2
        pp(2*n-1)=t1
        pp(2*n)  =t2
      enddo

c                                              1,  3,7,11...  km =2lm-3
      do l=1,lm-3
        k=2*l+1
        xp(k)=ca*pp(l)+cb*pp(l+1)+cc*pp(l+2)+cd*pp(l+3)
      enddo
        xp(1)=da*pp(1)+db*pp(2)+dc*pp(3)+dd*pp(4)
        xp(km)=dd*pp(lm-3)+dc*pp(lm-2)+db*pp(lm-1)+da*pp(lm)

      endif
c---

c interpolations cubiques pour                 4,6,8,...  km-1
        k1=4*n1-3
        k2=4*n2-9
      do k=k1,k2,2
        ka=k        ! 1
        kb=k+2      ! 3
        kc=k+4      ! 5
        kd=k+6      ! 7
      xp(k+3)=ca*xp(ka)+cb*xp(kb)+cc*xp(kc)+cd*xp(kd)
      enddo
c                                              2
      xp(2)     =da*xp(1)+db*xp(3)+dc*xp(5)+dd*xp(7)

c                                              4nm-4                     
        k=4*n2-9
        ka=k
        kb=k+2
        kc=k+4
        kd=k+6
      xp(k+5  )=dd*xp(ka)+dc*xp(kb)+db*xp(kc)+da*xp(kd)

c dessin intery
      if(ides.eq.1.and.kana.eq.1.and.iplotpro.eq.1.and.
     1   i.eq.iprofil.and.j.eq.jprofil)then
c---
      call pgslw(2)
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)

          ymax=-100000.
c                          ymin=+100000.
        do k=1,km
        ymax=amax1(ymax,xp(k))
c                          ymin=amin1(ymin,xp(k))
        enddo
        ymin=0.

        if(ymax.le.0.)ymax=1.
      call pgsch(1.8)
         nm=n2-n1+1

         xcan=nm+0.25
      call pgwindow(0.75,xcan,ymin,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
c      call pglabel('lambda','intensity',' ')

         xcan=km+1
      call pgwindow(0.,xcan,ymin,ymax)

      kpm=2*km-1
      do k=1,km
      kp=2*k-1
      xxdes(kp)=k
      yydes(kp)=xp(k)
      enddo
      do k=1,km-1
      kp=2*k
      xxdes(kp)=k+0.5
      if(kp.eq.2.or.kp.eq.kpm-1)then
      yydes(kp)=0.5*(yydes(kp-1)+yydes(kp+1))
      else
      yydes(kp)=(-yydes(kp-3)+9.*yydes(kp-1)+9.*yydes(kp+1)-yydes(kp+3))
     1          /16.
      endif
      enddo

      call pgline(kpm,xxdes,yydes)

         do n=2,nm      
         xdes(n)=4*n-4
         enddo
      call pgpoint(nm,xdes,pb,6)

         do n=1,nm      
         xdes(n)=4*n-2
         enddo
      call pgpoint(nm,xdes,pa,7)

         do n=1,nm      
         xdes(n)=4*n-3
         ydes(n)=xp(4*n-3)
         enddo
      call pgpoint(nm,xdes,ydes,12)

         do n=1,nm      
         xdes(n)=4*n-1
         ydes(n)=xp(4*n-1)
         enddo
      call pgpoint(nm,xdes,ydes,2)

c           klm=2*nm-1
c         do kl=1,klm
c         xdes(kl)=2*kl-1
c         ydes(kl)=xp(2*kl-1)
c         enddo
c      call pgpoint(klm,xdes,ydes,5)

      endif        ! fin dessin

      endif        ! fin 2, 20
c----------------------------------------------------------------------------
      if(intery.eq.40)then

      xa=j-trj/4.
      xb=j+trj/4.
        ja=xa
        jb=xb
        dja=xa-ja
        djb=xb-jb
      xaa=j-trj/2.
      xbb=j+trj/2.
        jaa=xaa
        jbb=xbb
        djaa=xaa-jaa
        djbb=xbb-jbb
      if(jaa.lt.j1)return
      if(jbb.gt.j2)return

c           n               n+1
c               a  aa/bb  b 
c                                 (interpol-corrections->)
c    k    4n-3              4n+1


      do np=n1,n2
      n=np-n1+1
      xp(4*n-3)=tab(i,j,np)
      xp(4*n+1)=tab(i,j,np+1)

      ta=tab(i,ja,np)*(1.-dja)+tab(i,ja+1,np)*dja
      taa=tab(i,jaa,np)*(1.-djaa)+tab(i,jaa+1,np)*djaa
      tbb=tab(i,jbb,np)*(1.-djbb)+tab(i,jbb+1,np)*djbb
      tb=tab(i,jb,np)*(1.-djb)+tab(i,jb+1,np)*djb

      xp(4*n-2)=ta+(tbb-taa)/4.
      xp(4*n-1)=(taa+tbb)/2.
      xp(4*n)=tb-(tbb-taa)/4.
      enddo
      endif
c----------------------------------------------------------------------------
      if(iabs(intery).eq.4)then
c Dans chaque intervalle n,            n+1, le code calcule 5 points de profil
c en                   k=4n-3 -2 -1 0  +1
c par interpolation entre les points de meme longueur d'onde 
c des canaux                         n-1    a  n+2

c        k=
c      4n-3        /___/*___/   /   poids    4   8   4       /16
c      4n-2       /___/_*__/___/             3   7   5   1   /16
c      4n-1      /___/__*_/___/              2   6   6   2   /16
c      4n       /___/___*/___/               1   5   7   3   /16
c      4n+1    /   /___/*___/                    4   8   4   /16
c
c  canx     n-1   n   n+1   n+2
c              trj  trj  trj

      do iii=1,5
        c(1,iii)=(5.-iii)/16.
        c(2,iii)=(9.-iii)/16.
        c(3,iii)=(3.+iii)/16.
        c(4,iii)=(-1.+iii)/16.
      enddo

c      data c/4.,8.,4.,0.,
c     1       3.,7.,5.,1., 
c     2       2.,6.,6.,2.,
c     3       1.,5.,7.,3.,
c     4       0.,4.,8.,4./
      endif
c----
      if(iabs(intery).eq.3)then
c        k=
c      4n-3        poids    2   4   2   0       /8
c      4n-2                 1   4   3   0       /8
c      4n-1                 0   4   4   0       /8
c      4n                   0   3   4   1       /8
c      4n+1                 0   2   4   2       /8
c             canaux       n-1  n   n+1 n+2
      do iii=1,3
        c(1,iii)=(3.-iii)/8.
        c(2,iii)=0.5
        c(3,iii)=(1.+iii)/8.
        c(4,iii)=0.
      enddo
      do iii=4,5
        c(1,iii)=0.
        c(2,iii)=(7.-iii)/8.
        c(3,iii)=0.5
        c(4,iii)=(-3.+iii)/8.
      enddo
      endif
c-----------------------------------------------------------
      if(intery.ne.1)then
      do n=n1,n2-1
      do kp=1,5
      k=4*(n-n1)+kp
      
      xa=j-float(kp-1)*trj/4.-trj
      xb=xa+trj
      xc=xb+trj
      xd=xc+trj

      ja=xa
      jb=xb
      jc=xc
      jd=xd

      dja=xa-ja
      djb=xb-jb
      djc=xc-jc
      djd=xd-jd

      if(ja.lt.j1)then
          ja=j
          dja=0
                  endif
      if(jb.lt.j1)then
          jb=j
          djb=0
                  endif
      if(jc+1.gt.j2)then
          jc=j
          djc=0
                  endif
      if(jd+1.gt.j2)then
          jd=j
          djd=0
                  endif

      na=n-1
      nb=n
      nc=n+1
      nd=n+2

      if(na.lt.n1)then
         na=n
         ja=j
         dja=0
                  endif
      if(nd.gt.n2)then
         nd=n
         jd=j
         djd=0
                  endif

      xp(k)=c(1,kp)*(tab(i,ja,na)*(1.-dja)+tab(i,ja+1,na)*dja)
     1     +c(2,kp)*(tab(i,jb,nb)*(1.-djb)+tab(i,jb+1,nb)*djb)
     2     +c(3,kp)*(tab(i,jc,nc)*(1.-djc)+tab(i,jc+1,nc)*djc)
     3     +c(4,kp)*(tab(i,jd,nd)*(1.-djd)+tab(i,jd+1,nd)*djd)

      enddo
      enddo
c----
      do k=2,km-1
      xq(k)=0.5*(xp(k-1)+xp(k+1))
      enddo
      do k=2,km-1
      xp(k)=xq(k)
      enddo

      piv1=100000.
      piv2=-100000.
      do k=1,km
        piv1=amin1(piv1,xp(k))
        piv2=amax1(piv2,xp(k))
      enddo

        if(intery.lt.0)then
        do k=1,km
        xp(k)=xp(k)-piv1
        enddo
        endif
             endif   ! fin cas intery.ne.1
c------                             calculation by neighbouring points
c         print *,' intery ',intery
      if(intery.gt.100)then
c        if(i.eq.iprofil.and.j.eq.jprofil)then
      call calcjn(tab,iitab,jjtab,nntab,i,j,intery,trj,n1,n2,xp,km)
c          intery=1(nn)(nd)
c                            nn  neighbouring points
c                            nd = degree of least square polynom
c        endif
      endif
c---------------------------
      intervalj=iplotpro
      if(ides.eq.1.and.
     1   kana.eq.1.and.iplotpro.ge.2.and.i.eq.iprofil.and.j.eq.jprofil)
     2 call plotjn(tab,iitab,jjtab,nntab,i,j,intervalj,intery,trj,n1,n2,
     3             xp,km)

      return
      end
c-----------------------------------------------
      subroutine cheveu(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm,
c                ------
     1                  jhair,milsec)
      dimension hair(21)
      integer*2 tb(iitab,jjtab,lltab),jhair(200)

      call par1('   haira',1,kpiv)
        ihaira=float(kpiv)/float(milsec)
      if(ihaira.ne.0)then                     ! if cheveu

      call par1('   hairb',1,kpiv)
        ihairb=float(kpiv)/float(milsec)
      call par1('  hairhw',1,kpiv)          ! demi-largeur cheveu
        ihairhw=float(kpiv)/float(milsec)
c      if(ihairhw.lt.10.and.ihairhw.ne.0)then


      do l=1,lm                     ! boucle l
      do j=j1,j2                    ! boucle j
        ih=jhair(j)
        iha=ih-ihairhw
        ihb=ih+ihairhw

c interpolation    1  ih1  iha  ih  ihb  ih2  is
c                           ..........
      
        pas=(tb(ihb,j,l)-tb(iha,j,l))/float(ihb-iha)
      do i=iha,ihb
        ip=i-iha
        hair(ip)=tb(iha,j,l)+ip*pas
      enddo

          jj=(j1+j2)/2
        if(j.eq.jj)then
        do i=iha,ihb
        write(3,*)'cmd: l,i,tb(i,jj,l),hair(i-iha) ',
     1              l,i,tb(i,jj,l),hair(i-iha)
        enddo
        endif

      do i=iha+1,ihb-1
        ip=i-iha
        tb(i,j,l)=hair(ip)+0.5
      enddo


      enddo     ! fin boucle j
      enddo     ! fin boucle l

      endif
c           fin cheveu

      return
      end
c------------------------------------------------
      subroutine regresdisc(tb,iitab,jjtab,lltab,i1,i2,j1,j2,lm,mincent,
     1                      tab,iecri7,kind,xregres,yregres,poids)
c                                           economie
      integer*2 tb(iitab,jjtab,lltab),tab(1500,150,3)
c                                                  1 reference
c                                                  2 droites moindres carres
      dimension  xregres(1),yregres(1),poids(1),coef(2),
     1           kind(60)

        lref=2
        id=(i2-i1)/10+1
        jd=(j2-j1)/5+1

      do j=j1,j2
      do i=i1,i2
      tab(i,j,1)=tb(i,j,lref)
      enddo
      enddo
        if(iecri7.eq.1)then   
 1    format(' Reference array of d-file for disk-corrections',
     1       ' by least squares when >',i6)
 2    format(10i8)
      write(7,1)mincent
      do j=j1,j2,jd
      write(7,2)(tab(i,j,1),i=i1,i2,id)
      enddo
        endif

      do 100 l=1,lm    !  boucle l
      if(kind(l).eq.0)goto100
c moyenne
      sij=0.
      syij=0.
      do j=j1,j2
        si=0.
        syi=0.
        do i=i1,i2
        if(tab(i,j,1).gt.mincent)then
          si=si+1.
          syi=syi+tb(i,j,l)
        endif
        enddo
      sij=sij+si
      syij=syij+syi
      enddo

      if(sij.ne.0.)then
      sy=syij/sij
      else
      sy=0.
      endif

c regression par ligne
      do j=j1,j2

          nd=0
        do 5 i=i1,i2
        tab(i,j,2)=0
            if(tab(i,j,1).le.mincent)then
            tb(i,j,l)=0
            else
            nd=nd+1
            xregres(nd)=i
            yregres(nd)=tb(i,j,l)
            poids(nd)=1.
            endif
 5      continue
          if(nd.gt.1)then
          call DPMCAR(xregres,yregres,poids,nd,2,coef)
c      print*,' l,j,coef ',l,j,coef
            do i=i1,i2
              if(tab(i,j,1).gt.mincent)then
              ky=coef(1)+i*coef(2)
              tab(i,j,2)=ky
              tb(i,j,l)=tb(i,j,l)-ky
              else
              tb(i,j,l)=0
              endif
            enddo
          endif
      enddo     ! boucle j

        if(iecri7.ge.1)then
 10   format(' departures from least squares linear fctions, l=',i2)
 11   format(10i8)
      write(7,10)l
      do j=j1,j2,jd
      write(7,11)(tab(i,j,2),i=i1,i2,id)
      enddo
        endif

 100  continue                        !      boucle l
      return
      end
c-----------------------------------------------------------------
      subroutine plotjn(tab,iitab,jjtab,nntab,i,j,intervalj,intery,trj,
c                ------
     1                  n1,n2,xp,km)
      integer*2 tab(iitab,jjtab,nntab)
      dimension xp(119),xdes(119),ydes(119)

      call pgslw(2)
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.8)

          ymax=-100000.
c                          ymin=+100000.
        do k=1,km
        ymax=amax1(ymax,xp(k))
c                          ymin=amin1(ymin,xp(k))
        enddo
        ymax=ymax*1.1
        ymin=0.

        if(ymax.le.0.)ymax=1.
      call pgsch(1.8)
         nm=n2-n1+1

         xcan=nm-1.
      call pgwindow(0.,xcan,ymin,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
c      call pglabel('lambda','intensity',' ')

c         xcan=km+1
c      call pgwindow(0.,xcan,ymin,ymax)

      do n=1,nm
      xdes(n)=n-1.
      ydes(n)=tab(i,j,n-n1+1)
      enddo
      call pgpoint(nm,xdes,ydes,4)

c points voisins dessines
         jc=intervalj/2+1 
      call pgsls(1)
      do n=1,nm
        do jp=1,intervalj
          jj=j-jc+jp
          xdes(jp)=n-1-float(jp-jc)/trj
          ydes(jp)=tab(i,jj,n-n1+1)
        enddo
      call pgline(intervalj,xdes,ydes)
      enddo

c points utilises dans le calcul intery>100
      if(intery.gt.100)then
      nd=intery/10-10+1          ! 6  nbre de donnees pour leastsquares =
c                                     nbre pts chaque canal +1
      intj=nd-1
         jc=intj/2+1
      call pgslw(6)
      do n=1,nm
        do jp=1,intj
          jj=j-jc+jp
          xdes(jp)=n-1-float(jp-jc)/trj
          ydes(jp)=tab(i,jj,n-n1+1)
        enddo
      call pgline(intj,xdes,ydes)
      enddo
      call pgslw(2)
      endif

c profils calcules
        xn1=nm-1.
        call pgwindow(0.,xn1,ymin,ymax)
      do k=1,km
      xdes(k)=0.25*(k-1)
      enddo
        call pgpoint(km,xdes,xp,2)

      return
      end
c-----------------------------------------------------------------          
      subroutine calcjn(tab,iitab,jjtab,nntab,
c                ------
     1                      i,j,intery,trj,n1,n2,xp,km)
c          intery=1(nn)(nd)
c                            nd-1 neighbouring points for each channel
c                                (nd for least squares)
c                            ndeg = degree of least square polynom
      integer*2 tab(iitab,jjtab,nntab)
      dimension xp(119),p(10),x(10),y(10),coef(10),z(5,30)

      nd=intery/10-10+1              ! 6  nbre de donnees =
c                                         nbre pts chaque canal +1
      ndeg=intery-10*(intery/10)     ! 4
      nt=ndeg+1                      ! 5

      nm=n2-n1+1
      intervalj=nd-1    ! 6  ..+..    ..+..
c                              A        B
c                 jp=          123    456
      jc=intervalj/2+1  ! 3 

      do jp=1,10
      p(jp)=1.
      enddo
c-------------
      do 100 n=2,nm
c             autour A
        do jp=1,jc       ! 1,2,3
          jj=j-jp+1      ! j,j-1,j-2 
          x(jp)=float(jp-1)/trj   ! unite lbda = intercanal, origine A
          y(jp)=tab(i,jj,n-n1)   
        enddo   
c             autour B
        do jp=nd,jc+1,-1  ! 6,5,4
          jj=j-jp+nd      ! j,j+1,j+2
          x(jp)=1.+float(jp-nd)/trj
          y(jp)=tab(i,jj,n-n1+1)
        enddo 
      
c        print *,' '
c        print *,' n,nd,nt,i,j ',n,nd,nt,i,j
c        print *,(x(jp),jp=1,nd)
c        print *,(y(jp),jp=1,nd)

      call DPMCAR(X,Y,P,ND,NT,COEF)
c Ce sous-programme [ Schneider 75 ] calcule par une 
c methode de moindres carres en double precision le 
c polynome de NT termes (10 max) Y=F(X) associe a ND mesures.
c
c P(ND)=poids

c      DOUBLE PRECISION C,F,PIVOT
c      DIMENSION X(ND),Y(ND),P(ND),COEF(10),F(10),C(11,11)
c      print *,' coef ',(coef(k),k=1,nt)

       do kp=1,5
       x(kp)=0.25*(kp-1)
       z(kp,n)=coef(1)
            if(kp.gt.1.and.nt.gt.1)then
        do ntt=2,nt
        z(kp,n)=z(kp,n)+coef(ntt)*x(kp)**(ntt-1)
        enddo
            endif
       enddo
c        if(n.eq.1)then
c      print *,' x ',(x(k),k=1,5)
c      print *,' z ',(z(k,n),k=1,5)
c        endif
 100  continue
c---------------------
c  bornes
      do n=1,nm
      k=1+4*(n-1)
      xp(k)=tab(i,j,n)
      enddo
c polynomes
      do n=2,nm
      do kp=2,4
        k=kp+(n-2)*4
        xp(k)=z(kp,n)
      enddo
      enddo

c      print *,' '
c      print *,' xp ',(xp(k),k=1,km)

      return
      end
c----------------------------------------------------
      subroutine intercub(y,km)
c  calcule      y(  2  4  6  ..km-1)
c a partir de   y(1  3   5   ..   km)

      dimension y(129)

      y(2)=(3.*y(1)+6.*y(3)-y(5))/8.      !  parabole

      do k=4,km-3,2
      y(k)=(-y(k-3)+9.*y(k-1)+9.*y(k+1)-y(k+3))/16. ! cubique
      enddo

      y(km-1)=(-y(km-4)+6.*y(km-2)+3.*y(km))/8.  !  parabole
      return
      end
c-----------------------------------------------------
      subroutine ab_em(xp,nnm,minab,minem,maxem,iabsor)
      dimension xp(129)
        xmin=32000.
        xmax=0.
        cont=0.5*(xp(1)+xp(nnm))
      do n=1,nnm
      xmin=amin1(xmin,xp(n))
      xmax=amax1(xmax,xp(n))
      enddo

        pivab=cont-xmin
      if(pivab.gt.float(minab))then
              iabsor=1
              return
      endif
        pivem=xmax-cont
      if(pivem.gt.float(minem).and.pivem.lt.float(maxem))then
              iabsor=-1
c        print *,'nnm,xmax,cont,pivem,iabsor',
c     1           nnm,xmax,cont,pivem,iabsor
              return
      endif
        iabsor=0
      return
      end
c-----------------------------------------------
      subroutine xxcentre(ijtab,i1,i2,j1,j2,kdisc)
      integer*2 ijtab(2000,200),kdisc(2000,200)
      dimension xmoy(200)

        deni=i2-i1+1
        denj=j2-j1+1
        xymoy=0.
c  calcul xmoy(j)
      do j=j1,j2
         xmoy(j)=0.
      do i=i1,i2
         if(kdisc(i,j).ne.0)xmoy(j)=xmoy(j)+ijtab(i,j)
      enddo
         xmoy(j)=xmoy(j)/deni
      enddo
c calcul xymoy
      do j=j1,j2
      xymoy=xymoy+xmoy(j)
      enddo
        xymoy=xymoy/denj
c centrage
      do j=j1,j2
        dy=xmoy(j)-xymoy
      do i=i1,i2
        ijtab(i,j)=ijtab(i,j)-dy
      enddo
      enddo

      return
      end
c----------------------------------
      subroutine centrej(ijtab,i1,i2,j1,j2,kdisc,ylinef,trj,milsigc)
      integer*2 ijtab(2000,200),kdisc(2000,200)
      dimension xmoy(200,2),sig(2000),ylinef(2000)
c           jk(i) = courbe lambda constant partant du point i1,j
c                   si j<j1  jk=jk+trj
c                      j>j2  jk=jk-trj
        ktrj=trj+0.5

      do iter=1,2
        xymoy=0.
c  calcul xmoy(j)
      do j=j1,j2

         if(iter.eq.2)seuil=sig(j)*float(milsigc)/1000.

         deni=0.
         xmoy(j,iter)=0.
      do i=i1,i2
c          jk(i)=j+ylinef(i)-ylinef(i1)+0.5
c        if(jk(i).lt.j1)jk(i)=jk(i)+ktrj
c        if(jk(i).gt.j2)jk(i)=jk(i)-ktrj
c           jj=jk(i)
            jj=j     
      if(iter.eq.2)then
        ecart=ijtab(i,jj)-xmoy(j,1)
c        sig(j)=sig(j)+ecart**2
        if(abs(ecart).gt.seuil)goto 10
      endif

        if(kdisc(i,jj).eq.0)goto 10
           deni=deni+1.
           xmoy(j,iter)=xmoy(j,iter)+ijtab(i,jj)
 10     continue
      enddo
         xmoy(j,iter)=xmoy(j,iter)/deni
      enddo
      print *,' '
      print *,'xmoy  ',(xmoy(j,iter),j=j1,j2,20)
c calcul xymoy
        denj=0.
      do j=j1,j2
      if(xmoy(j,iter).ne.0.)then
      denj=denj+1.
      xymoy=xymoy+xmoy(j,iter)
      endif
      enddo
        xymoy=xymoy/denj
      print *,'xymoy  ',xymoy
c calcul des sigmas
      if(iter.eq.1)then
      do j=j1,j2
        sig(j)=0.
        deni=0.
      do i=i1,i2
c          jk(i)=j+ylinef(i)-ylinef(i1)+0.5
c        if(jk(i).lt.j1)jk(i)=jk(i)+ktrj
c        if(jk(i).gt.j2)jk(i)=jk(i)-ktrj
c           jj=jk(i)
            jj=j
        if(kdisc(i,jj).eq.0)goto 20
           deni=deni+1.
           sig(j)=sig(j)+(ijtab(i,jj)-xmoy(j,iter))**2
 20     continue
      enddo
c        if(j.eq.j1+20)print *,'trj  ,jk  ',trj,(jk(i),i=i1,i2,200)
      if(deni.ne.0.)then
            sig(j)=sig(j)/deni
            sig(j)=sqrt(sig(j))
      endif
      enddo
        print *,'sig  ',(sig(j),j=j1,j2,20)
      endif   !    iter
      enddo   !   iter

c centrage
      do j=j1,j2
        dy=xmoy(j,2)-xymoy
      do i=i1,i2
c          jk(i)=j+ylinef(i)-ylinef(i1)+0.5
c        if(jk(i).lt.j1)jk(i)=jk(i)+ktrj
c        if(jk(i).gt.j2)jk(i)=jk(i)-ktrj
c           jj=jk(i)
            jj=j
        ijtab(i,jj)=ijtab(i,jj)-dy
      enddo
      enddo

      return
      end
c------------------------------------------------
      subroutine skyscat(tab,iitab,jjtab,nntab,im,jm,nm,lrdisk,ixlimb,
c                                                         in     out
     1                   kdispro)         
c                        out   0,1,2
      integer*2 tab(iitab,jjtab,nntab)  !,t0(2000)
      dimension ixlimb(400),ixsky(400),ixlimb2(400),i4(400),kdispro(400)
c                                                           0  1  2
c                                                         pp  dp  dd    
      dimension nosubtract(200)
      call par1(' intlimb',1,intlimb)
      call par1('   hlimb',1,khlimb)
      call par1(' lmaxpro',1,lmaxpro)
      call par1('  milsec',1,milsec)
      call par1(' nochrom',1,nochrom)
          imaxpro=float(lmaxpro)*1000./float(milsec)
          ihlimb=float(khlimb)*1000./float(milsec)
        jc=(jm+1)/2
        nc=(nm+1)/2
c      do i=1,im
c      t0(i)=tab(i,jc,nc)
c      enddo
c calcul bord ixlimb(j)
      jc=(jm+1)/2
c                      left right disk  1=gauche 2=droite
c                        1     2
c                           2*lrdisk-3=      -1       1
c                                 id  =       1      -1

c        coef=float(intlimb)/100.
c max disque
        maxd=0.
      do j=1,jm
      do i=1,im
        kpiv=tab(i,j,nm)
        maxd=max0(maxd,kpiv)
      enddo
      enddo
        a=float(maxd)
        write(7,*) ' Max int disk = ',a
        print *,' Max int disk  = ',a
c boucle j  --------------------------------------------
      do 12 j=1,jm
         kpiv1=tab(1,j,nm)
         kpivm=tab(im,j,nm)
         kdispro(j)=1   !   dis et pro
         if(kpiv1.gt.intlimb.and.kpivm.gt.intlimb)kdispro(j)=2 ! dis et dis
         if(kpiv1.lt.intlimb.and.kpivm.lt.intlimb)kdispro(j)=0 ! pro et pro
            write(3,*)'j,kpiv1,kpivm,kdispro  ',j,kpiv1,kpivm,kdispro(j)
         if(kdispro(j).ne.1)goto 12
c  moyenne disque
c      if(lrdisk.eq.1)then
c        a=0.
c        do i=1,10
c        a=a+tab(i,j,nm)
c        enddo
c        a=a/10.
c      else                          !  lrdisk 2
c        a=0.
c        do i=im-10,im
c        a=a+tab(i,j,nm)
c        enddo
c        a=a/10.
c      endif
c calcul ixlimb
          nodisk=1
        do 9 ip=1,im
            i=ip
            if(lrdisk.eq.1)i=im-ip+1
          b=float(tab(i,j,nm))
c          bsa=b/a
c            print *,'i,b,bsa ',i,b,bsa
          if(b.gt.float(intlimb))then   !sa.gt.coef)then
            ixlimb(j)=i                                ! i1
               ixlimb2(j)=ixlimb(j)-(2*lrdisk-3)*ihlimb    ! i2
               ixsky(j)=ixlimb(j)-(2*lrdisk-3)*imaxpro ! i3
               i4(j)=ixsky(j)-(2*lrdisk-3)*10
               nodisk=0
           goto 10
          endif
 9       continue  !ip

 10      continue
         if(nodisk.eq.1)then   !   sky alone
            if(lrdisk.eq.1)then
               ixlimb(j)=1
               ixlimb2(j)=1+ihlimb
               ixsky(j)=1+imaxpro
               i4(j)=ixsky(j)+10
            endif
            if(lrdisk.eq.2)then
               ixlimb(j)=im
               ixlimb2(j)=im-ihlimb
               ixsky(j)=im-imaxpro
               i4(j)=ixsky(j)-10
            endif
         endif
              nosubtract(j)=0
         if(i4(j).gt.im.or.i4(j).lt.1)then
              nosubtract(j)=1
         print *,'im,jm,nm,j,lrdisk,intlimb,khlimb,lmaxpro,b,a,bsa,coef'
         print *,im,jm,nm,j,lrdisk,intlimb,khlimb,lmaxpro,b,a,bsa,coef
         print *,'ixlimb,ixlimb2,ixsky '
         print *, ixlimb(j),ixlimb2(j),ixsky(j)
        i1=ixlimb(j)
        i2=ixlimb2(j)
        i3=ixsky(j)      
         print *,'tab(i1,1,1),tab(i2,1,1),tab(i3,1,1)'
         print *,tab(i1,1,1),tab(i2,1,1),tab(i3,1,1)
                 print *,'lmaxpro too large'
              write(3,*)'lmaxpro too large'
c               istop=1
c               if(istop.eq.1)stop    
c              return
           endif 
 12     continue                ! j
        write(3,*)'intlimb,kdispro',intlimb,(kdispro(j),j=1,jm,10)
        
       write(3,*)'    im,jm,nm ',im,jm,nm
      write(3,*)'    ixlimb ',(ixlimb(j),j=1,jm,10)
c------------------------   calcul  ixlimb2, ixsky
c     do j=1,jm
c        ixlimb2(j)=ixlimb(j)-(2*lrdisk-3)*ihlimb
c          ixsky(j)=ixlimb(j)-(2*lrdisk-3)*imaxpro
c      enddo
      write(3,*)'    ixlimb2',(ixlimb2(j),j=1,jm,10)
      write(3,*)'    ixsky  ',(ixsky(j),j=1,jm,10)
c boucles n,j ------------------------------------==============
      do 20 n=1,nm
         do 19 j=1,jm
            if(kdispro(j).ne.1.or.nosubtract(j).eq.1)goto 19
        i1=ixlimb(j)
        i2=ixlimb2(j)
        i3=ixsky(j)
c----------------------------- annulation tab entre ixlimb et ixlimb2
c                                                      i1        i2    
      if(nochrom.eq.1)then
      if(i1.lt.i2)then
        do i=i1+1,i2
        tab(i,j,n)=0
        enddo
      else
        do i=i2,i1-1
        tab(i,j,n)=0
        enddo
      endif
      endif
c-----------------------------   soustraction entre ixlimb2 et ixsky  +10
c                                                     i2        i3    i4
c                                + annulation
c                                si 1<i4<im
      if(i4(j).ge.1.and.i4(j).le.im)then
          piv=0.
       if(i3.gt.i2)then
c          i4=i3+10
          do i=i3,i4(j)                     ! moy i3,i4
             piv=piv+tab(i,j,n)
          enddo
             piv=piv/10.  
         do i=i2,i3
            tab(i,j,n)=tab(i,j,n)-piv
         enddo
         do i=i3,im                             ! annulation i3,im
            tab(i,j,n)=0
         enddo
       else             !  i4,i3   i2  i1         id=-1
c         i4=i3-10
         do i=i4(j),i3
            piv=piv+tab(i,j,n)
         enddo
            piv=piv/10.
         do i=i3,i2
            tab(i,j,n)=tab(i,j,n)-piv
         enddo
         do i=1,i3                              !   annulation  1,i3
            tab(i,j,n)=0
         enddo
       endif
      endif
      
c      if(j*n.eq.1)then
c      if(i1.lt.i4(j))then
c        ipas=5
c        iia=i1-20
c        ii1=i1
c        ii2=i2
c        ii3=i3
c        ii4=i4(j)
c        iib=i4(j)+20
c      else
c        ipas=-5
c        iia=i4(j)-20
c        ii1=i4(j)
c        ii2=i3
c        ii3=i2
c        ii4=i1
c        iib=i1+20
c      endif
c      endif

c       if(j.eq.50)write(3,*)'lrdisk,ixlimb(j=50)  ',lrdisk,ixlimb(j)
 19   continue                         !  j
 20   continue                          !   n
      do j=1,jm
       if(lrdisk.eq.2)ixlimb(j)=-ixlimb(j)
      enddo
c-----------------------------------------
c      write(3,*)'skyscat:'
c        write(3,3)(i,i=iia,iib,5)
c      write(3,1)(t0(i),i=iia,iib,5)
c 1    format(' tab(i,jc,nc) ',10i5)
c 3    format(14x10i5)
c      write(3,1)(tab(i,jc,nc),i=iia,iib,5)

      return
      end
c-------------------------------------
      subroutine scordisk(ijtab, ixlimb,jjemabs,i1,i2,j1,j2,trj,kdispro)
c                         in-out    in       in                  0  1  2
c                                                              pp  dp  dd
      integer*2 ijtab(2000,200),jjemabs(2000,200)
      dimension ixlimb(400),ipolm(200),pol(2000,200),kdispro(400)
c     ajustement regressions disque + annulation absor hors disque
      
c     paraboles  y=a(j)*i**2+b(j)*i+c(j)
c      DIMENSION X(ND),Y(ND),P(ND),COEF(10),F(10),C(11,11)
c      subroutine DPMCAR(X,Y,P,ND,    NT,   COEF)                  (msdp4.f)
c                ------       donnees,termes
c Ce sous-programme [ Schneider 75 ] calcule par une 
c methode de moindres carres en double precision le 
c polynome de NT termes (10 max) Y=F(X) associe a ND mesures.
C
C P=poids
      dimension a(200),b(200),c(200),x(2000),y(2000),p(2000),coef(10),
     1    alis(200),blis(200),clis(200),polis(2000,200)      
c      nd=3
c      im=i2-i1+1
c      jm=j2-j1+1
      idpara=0 !10

      do i=1,2000
         p(i)=1.
      enddo
      
         piv=trj/2.+0.5   !  demi decalage en j
         kdj=piv
      write(3,*)'scordisk  i1,i2,,j1,j2,trj,kdj ',i1,i2,j1,j2,trj,kdj
         jc=(j1+j2)/2
        write(3,*)'scordisk: ixlimb   j1,jc,j2  ',
     1                       ixlimb(j1),ixlimb(jc),ixlimb(j2)
      do 10 j=j1,j2
c        print *,'ixlimb(j)',ixlimb(j)
      kpiv=iabs(ixlimb(j))
      if(kpiv.lt.i1+idpara.or.kpiv.gt.i2-idpara)goto 10
      
      a(j)=0.
      b(j)=0. 
      c(j)=0.
      
      if(ixlimb(j).gt.0)then   !   annulation absor hors disk
         do i=i1,i2
            if(i.gt.ixlimb(j).and.jjemabs(i,j).gt.0)ijtab(i,j)=0
c                                            annulation absor hors disk
         enddo
c     ip    1... i1..........ixlimb(j)-idpara ........................i2
c                                >0
c                i1           ipdeb
c     i          imax.......... 1      
              ipdeb=ixlimb(j)-idpara
              imax=ipdeb-i1+1
          if(imax.lt.1)goto 10
              ipolm(j)=imax
         do i=1,imax
           ip=ipdeb-i+1        
           x(i)=i-1
           y(i)=ijtab(ip,j)
        enddo
         call DPMCAR(x,y,p,imax,3,coef)
         a(j)=coef(3)
         b(j)=coef(2)
         c(j)=coef(1)

      else
         do i=i1,i2
            jpiv=-ixlimb(j)
            if(i.lt.jpiv.and.jjemabs(i,j).gt.0)ijtab(i,j)=0
c                                            annulation absor hors disk
         enddo
c     ip    1... i1.....................-ixlimb(j)+idpara .............i2
c                                         <0
c                                        ipdeb                         i2
c      i                                  1                           imax
                ipdeb=-ixlimb(j)+idpara
                imax=i2-ipdeb+1
             if(imax.lt.1)goto10
                ipolm(j)=imax
         do i=1,imax
           ip=ipdeb+i-1        
        if(ip.gt.i2)write(3,*)'ip grand',ip
           x(i)=i-1
           y(i)=ijtab(ip,j)
         enddo            
         call DPMCAR(x,y,p,imax,3,coef)
         a(j)=coef(3)
         b(j)=coef(2)
         c(j)=coef(1)
      endif    !  ixlimb
      
c     calcul pol(i,j)   polynomes, i a partir du bord
      do i=1,2000 !imax+100   !   pour decalages entre coupes voisines
       x(i)=i-1
       pol(i,j)=c(j)+x(i)*(b(j)+x(i)*a(j))
      enddo
              jc=(j1+j2)/2
      if(j.eq.j1.or.j.eq.jc.or.j.eq.j2)then
      write(3,*)'  j,ixlim(j) ',j,ixlimb(j)
      write(3,*)'  ipolm,a,b,c =',ipolm(j),a(j),b(j),c(j)
      write(3,*)'  x,y,pol='
 8    format(10f6.0)
      write(3,8)(x(i),i=1,imax,200)
      write(3,8)(y(i),i=1,imax,200)
      write(3,8)(pol(i,j),i=1,imax,200)
      endif  ! j

 10   continue                  ! j
c     lissage polynome
      call abclis(a,j1,j2,kdj,alis)
      call abclis(b,j1,j2,kdj,blis)
      call abclis(c,j1,j2,kdj,clis)
c                               corrections
      do 20 j=j1,j2
            
      if(ixlimb(j).gt.0)then
              ipdeb=ixlimb(j)-idpara
              imax=ipdeb-i1+1
          if(imax.lt.1)goto 20
              ipolm(j)=imax
          do i=1,imax
             polis(i,j)=clis(j)+x(i)*(blis(j)+x(i)*alis(j))
             eps=pol(i,j)-polis(i,j)
           ip=ipdeb-i+1        
           ijtab(ip,j)=ijtab(ip,j)-eps
        enddo

      else
                ipdeb=-ixlimb(j)+idpara
                imax=i2-ipdeb+1
             if(imax.lt.1)goto 20
                ipolm(j)=imax
             do i=1,imax
                polis(i,j)=clis(j)+x(i)*(blis(j)+x(i)*alis(j))
                eps=pol(i,j)-polis(i,j)
           ip=ipdeb+i-1
           ijtab(ip,j)=ijtab(ip,j)-eps
         enddo
      endif            
 20   continue  !  j
 30   continue
      return
      end
c-------------------------------------------------------------
      subroutine abclis(a,j1,j2,kdj,alis)
      dimension a(200),alis(200)
      ja=j1+kdj
      jb=j2-kdj
      do j=j1,j2
         if(j.lt.ja)then
           jja=j1
           jjb=j1+2*kdj+1
         endif
         if(j.ge.ja.and.j.le.jb)then
           jja=j-kdj
           jjb=j+kdj
         endif
         if(j.gt.jb)then
           jja=j2-2*kdj-1
           jjb=j2
        endif

        piv=0.
        den=float(jjb-jja+1)
          do jp=jja,jjb
             piv=piv+a(jp)
          enddo
          alis(j)=piv/den
       enddo

       return
       end
c-----------------------------------------------
      subroutine subscat(tab,iitab,jjtab,nntab,
     1                       im,jm,nm,isubscat,maxscat)
c                                              to avoid disk
      integer*2 tab(iitab,jjtab,nntab)
        i2=im
        if(isubscat.gt.0)i2=im-isubscat  
        i1=1
        if(isubscat.lt.0)i1=1-isubscat

      isign=1
      ia=i1
      ib=i2
      if(isubscat.lt.0)then
        isign=-1
        ia=i2
        ib=i1
      do i=ia,ib,isign
        isky=i+isubscat
      do j=1,jm
         kwing=tab(i,j,nm)
       if(kwing.lt.maxscat)then            
          do n=1,nm
            tab(i,j,n)=tab(i,j,n)-tab(isky,j,n)
          enddo
       endif
      enddo
      enddo
      endif

      return
      end
c------------------------------------------------
      subroutine fixscat(tab,iitab,jjtab,nntab,     ! inutile
     1                       im,jm,nm,ifixscat,maxfix,ivarscat)
      integer*2 tab(iitab,jjtab,nntab)
      dimension wingf(500),scatf(500,18),coef(1000)
      write(3,*)'ifixscat,maxfix,ivarscat  ',ifixscat,maxfix,ivarscat

c        i2=im
c        if(isubscat.gt.0)i2=im-isubscat  
c        i1=1
c        if(isubscat.lt.0)i1=1-isubscat

c      isign=1
c      ia=i1
c      ib=i2
c      if(isubscat.lt.0)then
c        isign=-1
c        ia=i2
c        ib=i1
        isky=1+ifixscat
      do j=1,jm
      do n=1,nm
      scatf(j,n)=tab(isky,j,n)
      enddo
      wingf(j)=(scatf(j,1)+scatf(j,nm))/2.
      enddo

      do j=1,jm
      do i=1,im
        scati1=tab(i,j,1)
        scati2=tab(i,j,nm)
        wingi=(scati1+scati2)/2.
       coef(i)=0.
       if(wingf(j).gt.0.and.wingf(j).lt.float(maxfix))then
       if(wingi.lt.float(maxfix))then
         coef(i)=1. 
         if(ivarscat.eq.1)then
           piv=wingi/wingf(j)
           if(piv.lt.10..and.piv.gt.0.5)coef(i)=piv
         endif
       endif
       endif
      do n=1,nm
c         scat=tab(isky,j,n)
         scat=scatf(j,n)*coef(i)         
            tab(i,j,n)=tab(i,j,n)-scat
            if(tab(i,j,n).lt.0)tab(i,j,n)=0 
      enddo  ! n
      enddo  ! i
               jtest=j-10*(j/10)
            if(jtest.eq.0)then
 1000          format('coef var scat',10f5.2)
            write(3,1000)(coef(i),i=1,im,50)
            endif            
      enddo  ! j

      return
      end
c------------------------------------------------
      subroutine linscat(tab,iitab,jjtab,nntab,
     1                       im,jm,nm,nx,dname,ixscat)
c                                     in
      integer*2 tab(iitab,jjtab,nntab),kscat(500)
      dimension ixscat(500)
      character dname*22
      ilis=5                                ! lissge surr +/-5 points
c  subtraction of intensity along a linear line 
        nc=(nm+1)/2
      print *,'start linscat'
      call par1(' scatcor',1,kscatcor) !  scattered light correction
        if(kscatcor.eq.0)return
            call par1('  milsec',1,milsec)
      call par1('   ilin1',nx,ilin1)
      call par1('   ilinm',nx,ilinm)
c       if(ilinm.eq.0)return
      call par1('     ix1',1,ix1)
            kix1=ix1/1000          ! 160
        ilin1=ilin1+kix1
        ilinm=ilinm+kix1                  ! 210
        write(3,*)' im,jm,ilin1,ilinm,kix1 ',im,jm,ilin1,ilinm,kix1
c      call par1(' linvisu',1,linvisu)
      call par1(' maxwing',1,maxwing)
        kpix=1000.5/float(milsec)
      ilin1=ilin1*kpix+1
      ilinm=ilinm*kpix+1

 1    format('nx,ilin1,ilinm,dname',3i4,2x,a22)
        write(3,1)nx,ilin1,ilinm,dname
        write(7,1)nx,ilin1,ilinm,dname
       
 4      format(' linscat: j, tab(nc) avt ',20i5)
 6      format('                          i=  ',20i5)
        write(3,6)(i,i=1,im,50)
        write(3,*)' '
        do j=1,jm,20
           write(3,4)j,(tab(i,j,nc),i=1,im,50)
        enddo
        
        do 100 j=1,jm
           x0=float(ilin1)+float(ilinm-ilin1)*float(j-1)/float(jm-1)
           ix0=x0+0.5
           if(ix0.lt.0)ix0=1
        ixscat(j)=ix0
c       ******   
          jtest=j-1-20*((j-1)/20)
      if(jtest.eq.0)then
          kscat(j)=tab(ix0,j,nc)
 2       format(' nc: j,ix0,kscat',2i4,i5)
         write(7,2)j,ix0,kscat(j)  
         write(3,2)j,ix0,kscat(j)  
      endif
        wing0=float(tab(ix0,j,1)+tab(ix0,j,nm))/2.   
c     moyenne ailes au point ix0(j)
                 piv=float(maxwing)
      if(jtest.eq.0)write(3,*)'wing0, maxwing',wing0,piv
      if(wing0.gt.piv)goto 100       ! ----->
c        pas de correction si x0 sur disque

      do 90 n=1,nm
      kscat(j)=tab(ix0,j,n)
         i1=ix0-ilis
         i2=ix0+ilis
         if(i1.ge.1.and.i2.le.im)then
            piv=0.
          do i=i1,i2
             piv=piv+tab(i,j,n)
          enddo
          kscat(j)=piv/float(i2-i1+1)
       endif
c      kscat(j)=tab(ix0,j,n)
      if(kscat(j).lt.0)kscat(j)=0.
      
      do i=1,im
      tab(i,j,n)=tab(i,j,n)-kscat(j)
c                          **********
      enddo
 90   continue
 100  continue
 5    format(' linscat: j, tab(nc) apr ',20i5)
      write(3,6)(i,i=1,im,50)
      write(3,*)' '
        do j=1,jm,20
           write(3,5)j,(tab(i,j,nc),i=1,im,50)
        enddo
c        do j=1,jm
c        write(3,*)'j, ixscat ',j,ixscat(j)
c        enddo
      return
      end
c------------------------------------------------
      subroutine spline3(p,nm,pp,nnm)
      dimension p(32),pp(129)
c                  9      33
      DIMENSION X(2000),Y(2000),PDS(2000),COEF(10)
      nnm=4*nm-3
      do n=1,nm
        nn=4*n-3
        pp(nn)=p(n)
      enddo

      do nn=2,4
       alp=(5.-nn)/4.
      pp(nn)=alp*p(1)+(1.-alp)*p(2)
      enddo

      do nn=nnm-3,nnm-1
c                           alp=1 pour nn=nnm-4
       alp=float(nnm-nn)/4.
      pp(nn)=alp*p(nm-1)+(1.-alp)*p(nm)
      enddo

c                    cubique par 4 points 
      do n=2,nm-2
c                 n    n-1      n                  n+1   n+2
c                 k    1        2                   3    4         
c           xx=  nn    4n-7     4n-3               4n+1  4n+5  
c calcul                             4n-2 4n-1 4n
      do k=1,4
      x(k)=4*n-7+4*(k-1)   !  
        ny=n+k-2     !    -1 0 1 2
      y(k)=p(ny)
      pds(k)=1.

c      print *,'k,x,y',k,x(k),y(k)
      enddo

      call DPMCAR(x,y,pds,4,4,coef)
 
c      subroutine DPMCAR(X,Y,P,ND,NT,COEF)
c                ------
c Ce sous-programme [ Schneider 75 ] calcule par une 
c methode de moindres carres en double precision le 
c polynome de NT termes (10 max) Y=F(X) associe a ND mesures.
C
C P=poids
c
      do l=1,3
       npp=4*n-2+l-1
       xx=npp
       pp(npp)=coef(1)+xx*(coef(2)+xx*(coef(3)+xx*coef(4)))       
c        print *,' '
c        print *,'n,xx,coef',n,xx,(coef(m),m=1,4)
c        print *,'npp,pp',npp,(pp(npp),npp=1,33)
      enddo
      enddo   !   n

c        stop
      return      
      end
c------------------------------------------------



