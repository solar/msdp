c  msdp1.f
c  =======
c   ms1.f
c   =====
      equivalence (tb,etab)
      equivalence (ttb,grtab)
      equivalence (ttb,xtab)
      equivalence (tabeco,fl)
      equivalence (tab,etab1)
      equivalence (dc,dyln)
      equivalence (tb,stok)
c      equivalence (grtab,amem)
c      equivalence (xtab,imem)

      double precision amem(9,200,200)                     ! nfit0,nfit1,nfit2
      real*4 grtab(1600,5000)                        ! iitab*jje
      dimension dyln(1600,18,2,7)  !  iitab,18,2,7
      dimension fcont(1600,150,18) !  iitab,jjtab,nntab
c               image cont pour calib
      integer*4 xtab(1536,1536),imem(200,200)   ! ijcam*ijcam nfit1,nfit2
      integer*4 ttb(21000000) !kstok,istok,jstok,lbdmax!iitab*jje*raf*burst..

      integer*2 dc(1536,1536),tab(1536,1536)
      integer*2 tabeco(1600,150,18),fl(1600,150,18),     !  iitab,jjtab
     1   tabxy(1650,150),etab1(1600,150),etab2(1600,150)
c     1   tabxy(2000,200),etab1(1650,150),etab2(1650,150)
      integer*2 tb(1600,150,90)                      ! iitab*jjtab*lltab
      integer*2 etab(1600,5000)
c      integer*2 stok(6,400,600,29) ! 83 520 k
      integer*2 stok(6,400,29) ! kstok,istok,lbdstok
      integer*2 kdisc(1600,5000)
      
      call system('rm msdp.lis')
      open(unit=3,file='msdp.lis',status='new')
      
      print *,'call readpar'
      call readpar

c                                                                       max
      ijcam=1536           ! 2000                                      1536 
      nnfl=18              !                                             11
      maxttb=21000000      !  soft0602: 8000000  (frame selection) 10000000
      iitab=1600  !1600     !  1600         1024                        2000
      jjtab=150          !   150 ?         60  Themis?                200
      nntab=18             !                 18                          18
      lltab=90             !                 90  (for 60 arrays)         90
      jje=5000             !               5000                        5000

      kstok=6
      istok=400
      lbdstok=29

      nfit0=9
      nfit1=200             !  600
      nfit2=200            !  400

      call system('rm correl.lis')
      open(unit=2,file='correl.lis',status='new')

      write(3,*)' ijcam,nnfl,iitab,jjtab,nntab,lltab,jje,',
     1       'kstok,istok,jstok,lbdstok '
      write(3,*)
     1 ijcam,nnfl,iitab,jjtab,nntab,lltab,jje,kstok,istok,lbdstok

      call msdp1b(ttb,maxttb,tb,ijcam,nnfl,iitab,jjtab,nntab,lltab,jje,
c          ------
     1    grtab,etab,xtab,tabeco,fl,tab,tabxy,etab1,etab2,dc,dyln,
     2    stok,kstok,istok,lbdstok,amem,imem,nfit0,nfit1,nfit2,
     3    fcont,kdisc)

      close(unit=3)
      close(unit=2)
      stop
      end
c-----------------------------------------------------
c Dimensions
c ----------
c   ss/pgr    tableaux.....................................................

c  moyenne    xtab 
c            int*4  
c  bmg1                  tab  
c  bmc1                  tab           dc  fl 
c                                       |
c  cmf1                              dyln  fl         tb    
c  cmd1                                        tabeco tb
c  i3scan1                                            tb
c  dmd1                        ttb
c                             int4
c  echan                                                         etab1 etab2
c  dme1                                                     etab etab1 etab2
c  gray1          grtab                                      
c                real*4

c equival      ^________________^       
c                   ^___________^
c                                                      ^_____^

c                                          ^______^

c                         ^_________________________________________^

c                 2000      10000000       2000
c            1536 1024  1536 8000000  1536 1024  1024  1024 1024  1024  1024
c                           (6000000)   
c                 5000                      100             5000   100
c            1536 4000  1536          1536  170   170   170 4000   170   170
c                                           11    18    45
c                                                      (90)
c-------------------------------------------------------------------------
c                     tabeco             tb              tab
c                  2000,200,18      2000,200,90       1536,1536
c               iitab jjtab nntab  iitab,jjtab,lltab  ijcam,ijcam

c cmd1                   tab              tb
c       snorma           tab
c       ijlis            tab
c       scattline        tab
c       tablis           tab
c       interpoly        tab
c       four                              tb
c       corfou                            tb
c              paramfou                   tb
c       corfou3                           tb
c              paramfou3
c              regres3
c              desfou
c       writet,readt                      tb
c       dstok                            tab
c             intertab                   tab
c             moyline                    tab
c===========================================================================
      subroutine msdp1b(ttb,maxttb,tb,ijcam,nnfl,iitab,jjtab,nntab,
c                ------
     1 lltab,jje, grtab,etab,xtab,tabeco,fl,tab,tabxy,etab1,etab2,dc,
     2 dyln,stok,kstok,istok,lbdstok,amem,imem,nfit0,nfit1,nfit2,fcont,
     3 kdisc)

c********************************************
c   Dimensions maximales:
c                        camera           1536 * 1536
c                        cartes en sortie 1024 * 4000  (2000*5000)
c*****************************************************
c
c   ATTENTION   le texte qui suit n'est pas totalement a jour
c*****************************************************
c
c  Ce programme traite les donnees d'une observation DPSM THEMIS 
c          pour 1 region solaire
c                        (caracterisee par les heures d'observation)
c               1 raie (donc 1 angle de reseaux)
c                        (caracterisee par les fenetres nw)
c
c Les fenetres d'une meme raie sont entrelacees en longueur d'onde 
c     (   kdecal=0,50  
c     pour nwinp=2   )
c-------------------------
c  Les structures des fichiers sont les suivantes:
c      -----------------------
c           1 fichier par   fenetre w / temps
c
c       Boucles d'observation
c       -------
c         nbre scans             nscan=1,nscanmax
c         nbre images scan ppd     nx =1,nxm
c         nbre positions //        ny =1,ipos   (3 si polar)
c         nbre images rafales(pic) nr =1,raf
c         nbre polar               nq =1,nquv   (1 a 3)
c         nbre images rafales(th.) nb =1,burst
c
c
c        pour un scan commencant en nf1,
c        le numero du fichier nsx,nsy,nq,nb   est

c            nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nsy-1+ipos*(nsx-1))))

c      Noms de fichiers
c      ----
c      caracteres   \   x,y,s    c  d,r   d,r      q,p
c                                        (i3scan)
c           1           x,y,s    c  d,r   d,r      q,p
c         2:7           date     .....................
c           8             _      .....................            
c        9:16           heure    ..............  heure*
c          17             _      .....................
c       18:19             0      nb...................
c          20           raie     .....................
c          21            nw      nw  0      1       0
c          22        0 ou polar  nq  .................

c        nb = burst
c        nw = window               
c        heure* signifie "heure de la premiere image du scan"
c                (avec les bonnes valeurs de nb et nq)
c
c        les pointilles signifient "meme valeur qu'a gauche"         
c
c    
c
c   dimensions:  i,j     pixels   images brutes               simultane
c                I,J     disque   image soleil simultanees    simultane       
c                n       canaux                               simultane
c                N       ~ lambdas                            simultane
c                nq      eventuellement Q,U,V  (pol= I+/-*)   balayage
c                X,Y     disque   image soleil grand champ    balayage
c
c
c carac.       print   format  dimensions        contenu                unite 
c       obs.par        txt                                                91    
c       exe.par        txt                                                92
c       fix.par        txt                                                93
c
c41     w0r0l   b      fts    i,j,sto=1,X,Y    observation                20
c41     w000l   b      fts    i,j,sto=1,X,Y    dark current               21
c41     w-00l   b      fts    i,j,sto=1,X,Y    flat field                 22
c   22  mwrls     o,x  bin    i,j,sto=1        moyenne dc  sur X,Y        23
c   22  wwrls     y    bin    i,j,sto=1        moyenne ff  sur X,Y        24
c   22  mwrls     g    ascii                   geometrie                  25
c   22  mwrls     z    bin    I,J,n,sto=1      images canaux pour ff      26
c                                                    avec correc dc
c                             +I,2,n           et backgrounds 
c                                                   (lumiere diffusee)
c  
c   22  mwrls     f    bin    I,J,n,sto=1                         |cmf    27 
c                             + 1024,sto=1     flat field + profil moy  
c
c   22  mwrls  c  bin  I,J,n,STO=1,X,Y  images des canaux obs     |bmc 31,32,..
c                                        avec correc dc
c                       +I,2,n           et backgrounds
c                                        (lumiere diffusee)
c
c   22  mwrls  f  bin  (comme fichiers f plus haut)               |cmd 41,42,..
c
c   22  m0rls d q  bin  I,J,N,npol,
c                          sto=1,X,Y  images separees
c                                                     quick-look          51
c
c   22  m0rls  j  ascii                j pour lambdas de reference        52
c                              
c
c   22  q0r01 e   fts  U,V,N,npol       composites quick-look     |dme    53
c                       sto=1,Y
c
c   22  m0rls  j  ascii              j pour lambdas (+geo. jonctions)     54
c
c   22  m0rls d p  bin   I,J,N,npol
c                           sto=1,X,Y    images separees
c                                                             profils     55
c
c   22  p0r01 e   fts  U,V,N,npol,      composites profils        |dme    56
c                        sto=1,Y
c*****************************************************************************

c noms des fichiers en entree:
c filtre:
c            b000000_000_000_000000_m0000_00000000.fts
c*****************************************************************************
c noms des fichiers intemediaires a 22 caracteres (exemple fichier c):
c     c980910_14590100_m2011   
c                     fenetre(i1), rafale(i2), polar(i1)
c
c*****************************************************************************
c transport des infos de calibration, et recentrage vmoy en longueur d'onde
c
c  etape code  info                                  1 par    localisation 
c  iflat cmf   ncm,jt100m,ja100m,jb100m,jz100m,kdec  nf,npo,nw     header f
c  ibmc  bmc   ncm,jt100m,ja100m,jb100m,jz100m,kdec  nf,npo,nw     header c
c  id    cmd                                                       header d
c  iquick
c  ir
c  iprof

c*****************************************************************************
c Dimensions
c ----------
c   ss/pgr    tableaux.....................................................

c  moyenne    xtab 
c            int*4  
c  bmg1                  tab  
c  bmc1                  tab           dc  fl 
c  cmf1                              dyln  fl         tb    
c  cmd1                                        tabeco tb
c  i3scan1                                            tb
c  dmd1                        ttb
c                             int4
c  echan                                                         etab1 etab2
c  dme1                                                     etab etab1 etab2
c  gray1          grtab                                      
c                real*4

c equival      ^________________^       
c                   ^___________^
c                               ^______________________^
c                               ^_____________________________^

c                                          ^______^

c                         ^_________________________________________^

c                 2000      12000000       2000
c            1536 1024  1536 8000000  1536 1024  1024  1024 1024  1024  1024
c                           (6000000)   
c                 5000                      100             5000   100
c            1536 4000  1536          1536  170   170   170 4000   170   170
c                                           11    18    45
c                                                      (90)

c           ijcam iitab ijcam             iitab  iitab iitab iitab iitab
c           ijcam   jje ijcam             jjtab  ijtab jjtab jje   jjtab jjtab
c                                          nnfl  nntab lltab
c************************
      double precision sana(6),amem(nfit0,nfit1,nfit2)
      dimension imem(nfit1,nfit2)
      dimension xrecad(2,6),yrecad(2,6),idebf(2,6),
     1          dxana(6),dyana(6),x(2000),y(2000),stray(2000,18),
     2            sigbd(3,18,2,6),fluc(10,3),    dyln(iitab,18,2,7)
c                      ibd  n nw nq    iterfluc,ibd      i,n,nw,nq
      dimension fcont(iitab,jjtab,nntab)  ! cont calib
      integer dob,tob1,tob2,ddc,tdc1,tdc2,dfs,tfs1,tfs2,
     1        calfs,calana,dff,tff1(10,6),tff2(10,6),
     2        dcl,tcl1(10,6),tcl2(10,6),nfail(200),
     3        sundec,reg,dlbd,burst,raf,rafburst,select,
     4        polord,nqord(9,2),nqsign(9,2),raf1,raf2,burst1,burst2,
c                    1..6 789
     5        dreject(60),rreject(60),kind(60)
c                                   
      integer tete(512),ftime(10000),sto,corv,kdecal(2),moyw(18,60)
      integer win(2),nbcln(2),nblgn(2),ibs(2),jbs(2),
c                nw
     1        nfdeb(2000)
      integer pagex,pagey,kz(512)
c---
      real*4 grtab(iitab,jje)
      integer*4 ttb(maxttb),xtab(ijcam,ijcam)

      integer*2 tab(ijcam,ijcam),dc(ijcam,ijcam), 
     1  tabeco(iitab,jjtab,nntab),fl(iitab,jjtab,nnfl),
c                                                  18
     2  etab(iitab,jje),etab1(iitab,jjtab),etab2(iitab,jjtab),   
     3  tabxy(iitab,jjtab),tb(iitab,jjtab,lltab),jhair(200),
c     4  stok(6,400,29)                             90
     4  stok(kstok,istok,lbdstok),kdisc(iitab,jje)
      logical*4 present

c---
      character*41 fnom(5000,2),fic(8), dcn(5000,2),ffn(5000,2),
     1             bna(26),Filter
      character*22
     1  oname,xname,yname,zname,fname,gname,cname,dname,rname,yzname,
     2  xna(26),yna(6,26),zna(26),vna(6,26),wna(6,26),fna(6,26),gna(26),
c         dc     ff        fs       cff      cfs                
     3  ona(26),        cna(6,26),dnanfp(10000,2),rnanfp(10000,2),
c         dc(ff et fs)
     4  jname,qname,pname,tname,
     5  kname,dnanfsp(500,2),rnanfsp(500,2),mname,nname,vname,wname
      character rl*2,hfin*8,blancs*2
c      character*23 stokname
      character*26 grayname
      character*2 cam
c      data lettre/'0','1','2','3','4','5','6','7','8','9',
c     1            'k','l','m','n','o','p','q','r','s','t',
c     2            'u','v','w','x','y','z'/

c
c       prevu pour une raie utilisant 8 fenetres maximum parmi 26 maximum
c
c Constantes provisoires pour traitement des donnees THEMIS
      integer*4 pos_nomcam
c      Character*1 Icam3/'0'/,Icam4/'0'/,Icam8/'0'/
      Character*80 repert
c---
c                                                              2013
c      dimension darkc(ijcam,ijcam),
c                     1536         
c     1          xpix(iitab,jjtab,nntab),ypix(iitab,jjtab,nntab),
c               abs camera              ord camera
c     2          phot(iitab,jjtab,nntab),spec(4)
c                    2000  200   18     trans,yla,ylb,ylc
c     1      geom(nntab,3,  2)
c                18    ihb,kg

c      call system('rm correl.lis')
c      open(unit=2,file='correl.lis',status='new')
      print *,'msdp1b:iitab,jjtab',iitab,jjtab
      kstok=6
      istok=600
      jstok=600       !  m2403
      lbdstok=29
c------
      ibdm=3
c                    certains petits tableaux sont restes a ancien 32 
      xnormf=10000

      do j=1,200
      jhair(j)=0
      enddo
c              nxfirst=0
      do n=1,9
        nqsign(n,1)=+1
        nqsign(n,2)=-1
      enddo
c---
      pos_nomcam = 27      !position du numero de la camera dans le nom FITS

      sto=0
      pagex=2
      pagey=1
      blancs='  '
c  
      stepx=1000.
      stepy=500.

      dxana(1)=0.
      dxana(2)=-stepx
      dxana(3)=stepx
      dxana(4)=0.
      dxana(5)=0.

      dyana(1)=0.
      dyana(2)=0.
      dyana(3)=0.
      dyana(4)=-stepy
      dyana(5)=stepy      

1     format(18x,a)

      call system('rm test.lis')
      open(unit=9,file='test.lis',status='new')
      write(9,*)' test.lis'
      write(9,*)' --------'

      open(unit=96,file='msdp.par',status='old')
      rewind(96)
      call par1('ndprofps',1,ndprof) ! plot of profiles
      ndend=0
      
      call system('rm vel.lis')
      open(unit=8,file='vel.lis',status='new')
      call par1(' mcorrec',1,mcorrec)     
      write(8,*)'Sequence computed with mcorrec =',mcorrec,'(m/s)'
      write(8,*)' '
      write(8,*)
     1 '   n  qname                    npts  vel(m/s) mcorrec  average'
      write(8,*)
     1 '                                                new    1 to n'
           scorrec=0.
           nvel=0
      call system('rm scan.lis')
      open(unit=7,file='scan.lis',status='new')
2     format('  scan.lis'/'  --------')
      write(7,2)

      write(3,*)' ms.lis'
      write(3,*)' ------'
c---------------------
c      call sname(96,'     dir',ier)
c      IF (Ier.EQ.-1)THEN
         Repert=' '
c      ELSE 
c         read(96,1)repert
c         write(3,1)repert
         Filter = 'b??????_???_???_??????_m????_????????.fts'
         write(3,1)Filter
c      ENDIF
      rewind(96)
c---------------------      
c      call par1('  minpro',1,minpro) !   cmr protus
      call par1(' maxcont',1,maxcont) !  cmr       
      call par1('   lcorq',1,kpiv)
        if(kpiv.gt.100)then
        icutcor=1
        else
        icutcor=0
        endif
      call par1('    nseq',1,nseq)      
      call par1('   calfs',1,calfs)

      call par1('     dob',1,dob)
c      call par1('    tob1',1,tob1)
c      call par1('    tob2',1,tob2)

      ddc=dob
      call par1('    tdc1',1,tdc1)
      call par1('    tdc2',1,tdc2)

      dfs=dob
      call par1('    tfs1',1,tfs1)
      call par1('    tfs2',1,tfs2)

      call par1('    nqff',1,nqff)

      call par1('     reg',1,reg)
      call par1('     lin',1,lin)
      call par1('  linref',1,linref)
      call par1('  milsec',1,milsec)
      call par1('  intert',1,intert)
      call par1('   ntmax',1,ntmax)
      call par1('    lbda',1,lbda)
      call par1('    dlbd',1,dlbd)  
      call par1('      nm',1,nm)
        write(rl(1:1),'(i1)')reg
        write(rl(2:2),'(i1)')lin

      call par1('     ixy',1,ixy)
      call par1('    igeo',1,igeo)
      call par1('   iflat',1,iflat)
      call par1('    ibmc',1,ibmc)
      call par1('    icmd',1,icmd)
      call par1('  iquick',1,iquick)
      call par1('    icmr',1,icmr)
      call par1('   iprof',1,iprof)
      call par1('  igrayq',1,igrayq)
      call par1('  igrayp',1,igrayp)
      call par1('   iunno',1,iunno)
      call par1(' istokes',1,istokes)
        if(iunno.ne.0)istokes=1

c iflat fournit recad pour bmc, et idebg (grid) pour cmd1
c il faut le relancer de toute facon, sauf si ixy et igeo sont calcules seuls:

      if(ibmc+icmd+icmr.ne.0.and.iflat.eq.0)then
      iflat=1
      endif

      call par1('  sundec',1,sundec)
      call par1('   iswap',1,iswap)
c      call par1('  iplotg',1,iplotg)
c      call par1('  iplotf',1,iplotf)
            iplotf=4
      call par1('  ipermu',1,ipermu)
      call par1('    igrq',1,igrq)
      call par1('    jgrq',1,jgrq)
      call par1('    igrp',1,igrp)
      call par1('    jgrp',1,jgrp)

         if(igrq.eq.0)igrq=1
         if(jgrq.eq.0)jgrq=1
         if(igrp.eq.0)igrp=1
         if(jgrp.eq.0)jgrp=1

      call par1('    npol',1,npol)
        npol=1
      call par1('   nwinp',1,nwinp)
        nwinp=1
      call par1('   mgrim',1,mgrim)

      call par1('    ipos',1,ipos)
c         if(ipos.lt.0)then
c         nxfirst=1
c         ipos=-ipos
c         endif
      call par1('  xfirst',1,nxfirst)


c polarimetry
c -----------
      call par1('    nquv',1,nquv)
      call par1('  nqseul',1,nqs)
      call par1('  polord',1,polord)
        if(nquv.eq.0)nquv=1
c        if(nqs.eq.0)nqs=1
        if(polord.eq.0)polord=123456

        nqord(7,1)=polord/100000
        nqord(7,2)=polord/10000 -10*(polord/100000)
        nqord(8,1)=polord/1000  -10*(polord/10000)
        nqord(8,2)=polord/100   -10*(polord/1000)
        nqord(9,1)=polord/10    -10*(polord/100)
        nqord(9,2)=polord       -10*(polord/10)

        nqord(1,1)=nqord(7,1)
        nqord(2,1)=nqord(7,2)
        nqord(3,1)=nqord(8,1)
        nqord(4,1)=nqord(8,2)
        nqord(5,1)=nqord(9,1)
        nqord(6,1)=nqord(9,2)

        if(nqs.le.6)then
          if(nqs.eq.0)then
            nq1=1
            nq2=nquv
          else
            nq1=nqs
            nq2=nqs
          endif

        else
          if(nqs.eq.10)then
            nq1=7
            nq2=9
          else
            nq1=nqs
            nq2=nqs
          endif
        endif

        nqexm=1
        if(nqs.gt.6)nqexm=2

      write(3,*)' nquv,nqs,polord ',nquv,nqs,polord
      write(3,*)' nq1,nq2,nqexm,nqord ',nq1,nq2,nqexm,nqord

c Bursts    if >0 loops order =  bursts  /polar          (slow/fast)
c-------                         1 to 100
c           if <0                        /polar/bursts
c                                               1 to 99 

c      do k=1,60
c      call par1(' dreject',k,dreject(k))
c      call par1(' rreject',k,rreject(k))
c      enddo
c      call par1('  reject',1,kpiv)
      if(kpiv.eq.0)then
         do k=1,60
         dreject(k)=0
         rreject(k)=0
         enddo
      endif

      call par1('   burst',1,burst)
c                            -----
      if(burst.eq.0)then
        burst=1
        raf=1
      endif
      if(burst.gt.0)then
        raf=burst    
        burst=1
      endif
      if(burst.lt.0)then
        raf=1
        burst=-burst
      endif
              rafburst=raf*burst

      call par1('  select',1,select)
c                            ------
c                               0 to 99 = computations with raf   (1 to 100)
c                               0 to 98                     burst (1 to 99)
c                               -1      = average of all burst (or raf)
         
      raf1=1
      raf2=1
      burst1=1
      burst2=1

      if(select.ge.0.and.rafburst.gt.1)then
         if(raf.gt.1)then
            raf1=select+1
            raf2=select+1
         else
            burst1=select+1
            burst2=select+1
         endif
      endif

      if(select.lt.0)then
        raf1=1
        raf2=raf
        burst1=1
        burst2=burst
      endif

      write(3,*)' raf,burst,select ',raf,burst,select
      write(3,*)' raf1,raf2,burst1,burst2 ',raf1,raf2,burst1,burst2

c-----------

        if(nqff.eq.0)nqff=1

c        if(nquv.eq.0)then           ! pas de polar
c           nquv=1
c           nq1=1
c           nq2=1
c        else
c          if(nqs.le.6)then          ! pas de beam exchange
c            if(nqs.eq.0)then
c              nq1=1
c              nq2=nquv
c            else
c              nq1=nqs
c              nq2=nqs
c            endif
c          else                      ! beam exchange
c            if(nqs.eq.10)then
c              nqe1=7              ! premier final
c              nqe2=6+nquv/2       ! dernier final
c              nq1=1               ! premier pour calcul
c              nq2=nquv            ! dernier pour calcul
c            else
c              nqe1=nqs            
c              nqe2=nqs
c              nq1=(nqs-6)*2-1
c              nq2=nq1+1
c              nq1=nqord(nqe1,1)
c              nq2=nqord(nqe1,2)
c            endif
c          endif
c        endif
         
c      if(nqs.le.6)then
c      nqp1=nq1
c      nqp2=nq2
c      else
c      nqp1=nqe1
c      nqp2=nqe2
c      endif

c        endif           
        if(ipos.eq.0)ipos=1
c        if(burst.eq.0)burst=1
      write(3,*)' burst,nq1,nq2,nquv,raf,ipos ',
     1            burst,nq1,nq2,nquv,raf,ipos

      dff=dob      
      call par1('     nff',1,nff)

c      nl=0
      do nq=1,nquv
c                      toujours boucle nq la plus rapide pour ff
      nl=0
        do nf=1,nff
        nl=nl+1
        call par1('    tff1',nl,tff1(nf,nq))
        call par1('    tff2',nl,tff2(nf,nq))
        enddo
      enddo

      dcl=dob
         do nq=1,nquv
      call par1('    tcl1',1,tcl1(1,nq))
      call par1('    tcl2',1,tcl2(1,nq))
         enddo
c-------

      ncycle=ipos*nquv*rafburst
c      ncycleqb=nquv*rafburst
c                              npol=nbre polars simultanees petites cameras
c                              nquv=nbre de stokes successifs
c                              nqs=nq si 1 dessin gray au lieu de 3
c                              ipos=nbre de positions en i (1 ou 3)
c                                         si mgrim.ne.0 
         nwin=npol*nwinp       

        do nw=1,nwin
      call par1('     win',nw,win(nw))
          win(nw)=1     
      call par1('  kdecal',nw,kdecal(nw))
          kdecal(nw)=0    
      call par1('   nbcln',nw,nbcln(nw))
      call par1('   nblgn',nw,nblgn(nw))
     
                       if(ipermu.eq.0)then
                       ibs(nw)=nbcln(nw)
                       jbs(nw)=nblgn(nw)
                       else
                       ibs(nw)=nblgn(nw)
                       jbs(nw)=nbcln(nw)
                       endif
         enddo
c---                    si ncam1=0        1 ou 2       en pos_nomcam=27
c                       si ncam1.ne.0   ncam1 ou ncam2 en 26,27
      call par1('   ncam1',1,ncam1)
      call par1('   ncam2',1,ncam2)

      if(ncam1+ncam2.eq.0)then  
         do nw=1,nwin
            fic(nw)= Filter
            write(fic(nw)(pos_nomcam:pos_nomcam),'(i1)')win(nw)
           write(3,*)' nw, fic(nw) ',nw,fic(nw)
         enddo
      else
            fic(1)=Filter
            write(fic(1)(26:27),'(i2)')ncam1
            if(fic(1)(26:26).eq.' ')write(fic(1)(26:26),'(i1)')0

            fic(2)=Filter
            write(fic(2)(26:27),'(i2)')ncam2
            if(fic(2)(26:26).eq.' ')write(fic(2)(26:26),'(i1)')0
      endif
c--------------------------------
c                                     Noms des fichiers xna a 22 caract.

      call par1('   nline',1,nline)

      do nw=1,nwin
          ipiv=win(nw)   
        write(cam(1:2),'(i2)')ipiv
c
        xname='x000000_00000000_00'//cam//'0'
c        write(xname(22:22),'(i1)')sto
        ipiv=ddc-1000000*(ddc/1000000) !!!!!!!!!!!!!!!!!!!!!!!!
        write(xname(2:7),'(i6)') ipiv
c        write(chartdc1,'(i8)')tdc1
c        xname(9:16)=chartdc1(1:8)

        if(tdc1.ne.0)then
        write(xname(9:16),'(i8)')tdc1
        else
        write(xname(9:16),'(i8)')nseq
        endif

        write(xname(20:20),'(i1)')nline

c Afin d'eviter les ' ' dans le nom du fichier
        do ii=1,22
           if(xname(ii:ii).eq.' ') then
              xname(ii:ii)='0'
           endif
        enddo

        xna(nw)=xname
        write(3,*)' xna(nw)  ',xna(nw)
        oname=xname
        write(oname(1:1),'(a1)')'o'
        ona(nw)=oname
        write(3,*)' ona(nw)  ',ona(nw)
      enddo

        is=nbcln(nw)     !attention nbcol devient nbcln
        js=nblgn(nw)      !attention nblig devient nblgn

c**************************************************************************
c     IXY
c**************************************************************************
c
      print *,'ixy : ',ixy
      if(ixy.ge.1)then
c        ***

      call par1('     idc',1,idc)
        if(idc.lt.0)goto52    

      print*,'creation des fichiers o et x'
c                         Moyenne des courants d'obscurite: fichiers o et x
c                                     --------------------  ---------------
c                         (dependent de la raie (par l'intermediaire du
c                                                temps de pose)
c                          mais pas de la region)
c


c  boucle sur les fenetres
c  -----------------------
      write(3,*)' ------  fichiers o et x -----'
      do50 nw=1,nwin

      write(3,*)' nw,date,ideb,ifin ',nw,ddc,tdc1,tdc2

        call fnomw2('x',fic,nw,nw, ddc,tdc1,tdc2,
     1             nfm,dcn,ftime,istop,repert)
c            -----

42    format(i6,' fichier(s) a partir de ',a41)
        write(3,42)nfm,dcn(1,nw)

c                        modif 19 avril 00
        is=nbcln(nw)      !attention nbcol devient nbcln
        js=nblgn(nw)      !attention nblig devient nblgn

c ouverture du fichier dc de la fenetre nw
c      do n=1,nfm
c        write(dcn(n,nw)(pos_nomcam:pos_nomcam),'(i1)')win(nw)
c      enddo
    
      if(ixy.eq.1)then
c        --------
      no1=1
      no2=nfm
      nx1=1
      nx2=nfm
      endif

      if(ixy.eq.2)then    ! limb (protus) dc disque avant dc bord
c        --------
      no1=1
      no2=nfm/2
      nx1=no2+1
      nx2=nfm
      endif

      if(ixy.eq.3)then    ! limb (protus) dc bord avant dc disque
c        --------
      nx1=1
      nx2=nfm/2
      no1=nx2+1
      no2=nfm
      endif 

      call dark(no1,no2,ijcam,is,js,tab,xtab,dcn,sundec,repert,iswap,
c          ----
     1          ona(nw),tete,ipermu,nw)
      call dark(nx1,nx2,ijcam,is,js,tab,xtab,dcn,sundec,repert,iswap,
c          ----
     1          xna(nw),tete,ipermu,nw)

 50   continue                       !FIN DE BOUCLE FENETRE
52    continue    ! cas idc<0
      endif
c                                  nom des fichiers yna (moyennes des y)
c                                  --------------------
c                                                et zna (moyennes des z)
c                                                ------
      do nq=1,nquv  !! reecrire mieux en sortant les zna de la boucle
      do nw=1,nwin

        yname=xna(nw)
        write(yname(1:1),'(a1)')'y'
        zname=xna(nw)
        write(zname(1:1),'(a1)')'z'
        ipiv=dff-1000000*(dff/1000000)
                !!!!(dob -> dff) et 10000 -> 1000000
        write(yname(2:7),'(i6)')ipiv
        ipiv=dfs-1000000*(dfs/1000000) !!!!(dob -> dfs) et 10000 -> 1000000
        write(zname(2:7),'(i6)')ipiv
c Modif du 09/11/99 noms de fichiers y + declaration de chartff1
c        write(chartff1,'(i8)')tff1(1,nq)
c        yname(9:16)=chartff1(1:8)
c fin de modif
c        write(chartfs1,'(i8)')tfs1
c        zname(9:16)=chartfs1(1:8)

        if(tff1(1,nq).ne.0)then
        write(yname(9:16),'(i8)')tff1(1,nq)
        else
        write(yname(9:16),'(i8)')nseq
        endif

        if(tfs1.ne.0)then
        write(zname(9:16),'(i8)')tfs1
        else
        write(zname(9:16),'(i8)')nseq
        endif

        write(yname(22:22),'(i1)')nq

c Afin d'eviter les ' ' dans le nom du fichier
        do ii=1,22
           if(yname(ii:ii).eq.' ') then
              yname(ii:ii)='0'
           endif
           if(zname(ii:ii).eq.' ') then
              zname(ii:ii)='0'
           endif
        enddo
        yna(nq,nw)=yname
        write(3,*)' nq,nw,yna ',nq,nw,yna(nq,nw)
        zna(nw)=zname
        write(3,*)' nq,nw,zna ',nq,nw,zna(nw)
      enddo
      enddo    !! fin boucle nq

c-------------------------                     calcul fichiers y
      if(ixy.ne.0)then
c        ---
      print *,' etape creation fichiers y'
      write(3,*)' ------  fichiers y  ------'

      do60 nq=1,nquv           !!  boucle polars
c        if(nqs.ne.0.and.nq.ne.nqs)goto60

      print *,' nq=',nq

      do nw=1,nwin         !!  boucle fenetres   
        is=nbcln(nw)  !!!!!!!!!!!!!!!!!!!!!!
        js=nblgn(nw)     !!!!!!!!!!!!!!!!!!!!

      piv=0.           !!   compteur d'images

        do i=1,ijcam
           do j=1,ijcam
              xtab(i,j)=0
           enddo
        enddo 
 
      do nscanf=1,nff   !!  boucle scans separes de ff

        kdff=dff
        ktff1=tff1(nscanf,nq)
        ktff2=tff2(nscanf,nq)

c      write(3,*) ' fic ',fic

      call fnomw2('y',fic,nw,nw,kdff,ktff1,ktff2,
     1           nfm,ffn,ftime,istop, repert)
c          -----
c                 y n'est pris en compte que si ktff1.ne.0 
        write(3,42)nfm,ffn(1,nw)

      write(3,*)' nq,nw,nscanf,nfm,ffn(1,nw) ',
     1            nq,nw,nscanf,nfm,ffn(1,nw)
 
        nn1=1
        nn2=nfm
c                            6 series de flat fields Q,U,V
c                            avec heures 0, 24000000
c        if(nqff.eq.1.and.ktff1.eq.0)then
c              nfelem=nfm/nquv
c              nn1=1+(nq-1)*nfelem
c              nn2=nn1+nfelem-1
c        endif

c        write(3,*)' nn1,nn2 ',nn1,nn2

c           if(nqff.eq.1)then
c           npiv=1
c           else
c           npiv=burst*nquv
c           endif
c                          choix des y a moyenner
      call par1('     iff',1,iff)
      if(iff.eq.0)then
         nn1=nq
         nn2=nfm
         npiv=nquv
      endif
      if(iff.eq.1)then
         nn1=1
         nn2=nfm
         npiv=1
      endif

       do 56 n=nn1,nn2,npiv    !!   boucle pour un scan ff
           np=n
c           if(nqff.ne.1)then
c             nnpiv=n+burst*(nq-1)
c             np=min0(nn2,nnpiv)
c           endif

           piv=piv+1.   !!    compteur

c           write(ffn(np,nw)(pos_nomcam:pos_nomcam),'(i1)')win(nw)
              write(3,*)' np,ffn(np,nw) ',np,ffn(np,nw)

          call openold41(ffn(np,nw),sundec,22,repert)
c              ---------
          call comptehead(22,inbhead)          !ffn en 22
          if(n.eq.nn1)print *,'call moyenne  is,js ',is,js
          if(ipermu.eq.0)then
             call moyenne(22,inbhead,iswap,is,js,xtab,ijcam)                       
          else
             call moyenne(22,inbhead,iswap,js,is,xtab,ijcam)         
          endif

      close(unit=22)

 56   continue  !!  fin boucle pour un scan
      enddo     !!   fin boucle nscanf

      if(ipermu.eq.0)then
         do i=1,is
            do j=1,js
              kpiv=xtab(i,j) 
              tab(i,j)=0.5+float(kpiv)/piv
            enddo
         enddo
      else
         do i=1,is
            ip=is+1-i
            do j=1,js
               kpiv=xtab(j,i)
               tab(ip,j)=0.5+float(kpiv)/piv
            enddo
         enddo
      endif

c ouverture du fichier yname                                24

      yname=yna(nq,nw)
c               **
        print *,'yname : ',yname
         call system('rm '//yname)
      call opennew22(yname,sundec,24)       
c          ---------                                        24
      tete(1)=3
      tete(2)=is
      tete(3)=js
      tete(4)=1
c
      write(3,*)'y: tete ',(tete(i),i=1,10)
      write(24)tete   !!!! 31 -> 24

      do j=1,js
         write(24)(tab(i,j),i=1,is) !!!!!!!!!!!!!  31-> 24
         if(j.eq.js/2)print*,' y: tab(1,is,10 , js/2)',
     1        (tab(i,j),i=1,is,10)
      enddo
        is2=is/2
      write(3,*)' y: tab(is2,j),j=1,js,10 ',(tab(is2,j),j=1,js,10)

      
c
      close(unit=24)

      enddo       !! fin boucle nw
60    continue    !! fin boucle nq
      endif

c-------------------  
c-------------------------                     calcul fichiers z (field stops)
      if(ixy.ne.0)then
c        ---
      print *,' etape creation fichiers z'

      write(3,*)' ------  fichiers z ------'

c                           boucle sur les fenetres
      do61 nw=1,nwin

605   continue
c      write(3,*)'fic(nw1),fic(nw2),nwin,dfs,tfs1,tfs2,istop,repert',
c     1           fic(nw1),fic(nw2),nwin,dfs,tfs1,tfs2,istop,repert

c                     si calfs=-1  le flat field sert de field stop
      if(calfs.ne.-1)then
      call fnomw2('z',fic,nw,nw,dfs,tfs1,tfs2,
     1           nfm,ffn,ftime,istop, repert)
c          -----
      else
      call fnomw2('y',fic,nw,nw,dfs,tfs1,tfs2,
     1           nfm,ffn,ftime,istop, repert)
c          -----
      endif
      piv=float(nfm)

      write(3,42)nfm,ffn(1,nw)

        is=nbcln(nw)  !!!!!!!!!!!!!!!!!!!!!!
        js=nblgn(nw)     !!!!!!!!!!!!!!!!!!!!

c        do n=1,nfm
c           write(ffn(n,nw)(pos_nomcam:pos_nomcam),'(i1)')win(nw)
c        enddo
        do i=1,ijcam
           do j=1,ijcam
              xtab(i,j)=0
           enddo
        enddo 
 
        do 55 n=1,nfm
           call openold41(ffn(n,nw),sundec,26, repert)
           call comptehead(26,inbhead) !sfn en 26  
        if(n.eq.1)print *,'call moyenne  is,js ',is,js

           if(ipermu.eq.0)then
              call moyenne(26,inbhead,iswap,is,js,xtab,ijcam)
           else
              call moyenne(26,inbhead,iswap,js,is,xtab,ijcam)
           endif
55    continue

      if(ipermu.eq.0)then

         do i=1,is
            do j=1,js
               kpiv=xtab(i,j)
               tab(i,j)=0.5+float(kpiv)/piv
            enddo
         enddo
      else

         do i=1,is
            ip=is+1-i
            do j=1,js
               kpiv=xtab(j,i)
               tab(ip,j)=0.5+float(kpiv)/piv
            enddo
         enddo
      endif
c                test de la valeur moyenne de z
      pivmoy=0.
      ipiv=is/2
        do j=1,js
        pivmoy=pivmoy+tab(ipiv,j)
        enddo
      pivmoy=pivmoy/float(js)
      
      if(pivmoy.lt.0.)then
c                  ----
        if(calfs.eq.-1)then
          print *,' averages ff and fs < 0, calib stopped'
          stop
        endif

        calfs=-1
        write(7,*)' files fs replace files ff for geo'
        goto 605
c       --------
      endif

c ouverture du fichier zname                               27

      zname=zna(nw)
c           -------
        print *,'zname : ',zname
         call system('rm '//zname)
      call opennew22(zname,sundec,27)       
c          ---------                                        27

      tete(1)=3
      tete(2)=is
      tete(3)=js
      tete(4)=1
c
      write(3,*)'z: tete ',(tete(i),i=1,10)
      write(27)tete   !!!! 31 -> 27
        jja=50
        jjb=js-50
 59   format(18i5)
      do j=1,js
         write(27)(tab(i,j),i=1,is) !!!!!!!!!!!!!  31-> 27
         if(j.eq.jja)print *, ' z: tab(1,is,100) j=50,js/2,js-50   '
         if(j.eq.jja)print 59, (tab(i,j),i=1,is,100)
         if(j.eq.js/2)print 59,(tab(i,j),i=1,is,100)
         if(j.eq.jjb)print 59, (tab(i,j),i=1,is,100)
      enddo
c
      close(unit=26)
      close(unit=27)
       
 61   continue                     !Fin de boucle fenetres
c
      endif                     !FIN DE FICHIER Z
c***********************************************************************
c     GEO
c***********************************************************************
c-------------------  
c                                  noms fichiers g
      do  nw=1,nwin
      gname=zna(nw)
      write(gname(1:1),'(a)')'g'
      write(gname(9:12),'(i4)')lbda
      gna(nw)=gname   
      enddo

      if(igeo.ne.0)then
c        ****
c                                        geometrie: fichiers "g"
c                                        ---------  ------------
       print*,'creation fichiers g'
c boucle fenetres
c----------------

c      if(iplotg.eq.0)then
c      call pgbegin(0,'/xwin',1,1)
c      else
c      call pgbegin(0,'geo.ps/ps',1,1)
c      endif

      do70 nw=1,nwin
        is=nbcln(nw)   
        js=nblgn(nw)  

      gname=gna(nw)
      call system('rm '//gname)
      write(3,*)'gname  ',gname
        call opennew22sf(gname,sundec,25)
c            -----------                                 (sf = avec format??)
c                                           gname en 25
c
c        if(tdc1.ne.0)
        call openold22(ona(nw),sundec,23)
c            ---------                         o  en 23
        call openold22(zna(nw),sundec,24)
c            ---------                                     z  en 24

       gna(nw)=gname

       print *,'avant bmg, gname=',gname
       write(3,*)'gname = ',gname
        call pgadvance
        call bmg1(nw,win,nm,23,24,25,gname,istop,tab,ijcam)!23 utilise
c            ---            x  y  g in-out
       print *,'apres bmg'

      if(istop.eq.0)call system('cp '//gname//' ../.')
 
      close(unit=23)
      close(unit=24)
      close(unit=25)       

        if(istop.ne.0)then
        call par1('    nseq',1,nseq)
           do nnseq=1,100
           do63 kpiv=1,3,2

             mp=kpiv-2
            nseq1=nseq+nnseq*mp
             if(nseq1.eq.0)goto63
             write(gname(14:16),'(i3)')nseq1
               if(gname(14:14).eq.' ')write(gname(14:14),'(a)')'0'
               if(gname(15:15).eq.' ')write(gname(15:15),'(a)')'0'
               write(3,*)' nseq1,gname ',nseq1,gname
  
               inquire(file='../'//gname,exist=present)
                 if(present) then
                   call system('cp ../'//gname//'  .')
                   gna(nw)=gname
                     write(7,*)' g-file replaced by: ',gname

                   goto65
                 endif

63          continue
            enddo
               call pgend
               stop
c                  Arret si ecart > milgeo/1000 et nseq1 impossible
65      continue
           endif

70    continue
      call pgend
      call par1('  igeops',1,igeops)
      if(igeops.eq.1)call system('gv geo.ps &')
      endif
c***********************************************************************
c     IFLAT
c***********************************************************************
c                                        noms fichiers f
      do701 nq=1,nquv
c        if(nqs.ne.0.and.nq.ne.nqs)goto701

      do nw=1,nwin
      fname=yna(nq,nw)   
      write(fname(1:1),'(a)')'f'
      fna(nq,nw)=fname
c          write(3,*)' Avant iflat: nq,nw,fna ',nq,nw,fna
      enddo

701   continue
c---------------------------------
c                                           Etape iflat
c                                           ***********

      if(iflat.ne.0.or.ibmc.ne.0)           then
c                      pour recharger recad
        print*,' etape y->v'

c      if(iplotf.eq.0)then
c         call pgbegin(0,'/xwin',1,1)
c      else
       call pgbegin(0,'cal.ps/ps',2,3)   !3)   !3,3) 
c      endif

c                                       flat field: fichiers "f"
c                                       ----------  ------------
c  1 fichier f par fenetre
c      (tous canaux et polarisations + normalisation)
c                                                 etape y->v
c                                                 ----------
      do nq=1,nquv
      do nw=1,2
      xrecad(nw,nq)=0.
      yrecad(nw,nq)=0.
      enddo
      enddo

        call par1('  milrec',1,milrec)
        nrecad1=1
        if(milrec.eq.0)nrecad1=2    
c=============
c      do810 nrecad=nrecad1,2     !!                     boucle recad

c                                                                 2012 Halpha
      do 810 nrecad=nrecad1,2   !  2,2
      do75 nq=1,nquv              !                  boucle nq

c    Boucle fenetres pour calcul fichiers vna et wna (si calfs.eq.1) 
c    ---------------             ------------    -------------------
c                                pour     cmf
c                                         ---

      do72 nw=1,nwin              !                 boucle nw
c--------------------------------------------------------
      izyb=1   !                               z -> wna, fcont
c     ****
c wna           zna --> wna          extension possible aux fs polarises...
c                                    avant vna pour ne pas modifier 
c                                             xrecad,irecad
c      if(calfs.ge.1)then
      wname=zna(nw)             !!!!!!!!!!!!!!!!!
c    
      write(wname(1:1),'(a)')'w'
      call system('rm '//wname)
      wna(nq,nw)=wname
         
        ipermuty=0    !!   le fichier y est deja retourne si necessaire
        icalib=0

       write(3,*)' nrecad,yrecad(1,1) ',nrecad,yrecad(1,1)

         message=0
         yzname=zna(nw)
c               ***
         istray=0   !                                 istray=0

      print *,'avant bmc1:iitab,jjtab',iitab,jjtab

c                                                   z ->      cree wna (cont)
c                                                                  ***
      call bmc1(nw,win,nm,milsec,ona(nw),izyb,yzname,bna(nw),
c          ---                           1   zna(nw)   bidon si iyb=0
c               etiq, fenetre,       x   0/1 y,z ou b   
     1        gna(nw),wna(nq,nw),  
c                g     out  

     2      sundec,iswap,ipermuty,icalib,fna(nq,nw), fl, repert,lbda,
c                                        calib     economie
     3    nq,xrecad,yrecad,istop,ibs(nw),jbs(nw),iitab,jjtab,nntab,nnfl,
c         in in     in           dim fich b
     4      ijcam,tab,tabxy,dc,0.,0.,message,istray,stray,xnormf,jhair,
c               economie                     in     eco   inutil
     5      fcont)
c           out si icalcont=1

      print *,'apres bmc1:iitab,jjtab',iitab,jjtab

c      if(istop.eq.1)stop
c      endif
c------------------------------------------------------
      izyb=2     !                     y -> v utilise fcont si icalcont=1
c     ****
c vna       yna --> vna                                   flat-field
      vname=yna(nq,nw)             !!!!!!!!!!!!!!!!!
c               **
      write(vname(1:1),'(a)')'v'
      call system('rm '//vname)
      vna(nq,nw)=vname
c         **
        ipermuty=0    !!   le fichier y est deja retourne si necessaire
        icalib=0

       write(3,*)' nrecad,yrecad(1,1) ',nrecad,yrecad(1,1)

               message=0
               yzname=yna(nq,nw)
               istray=0

      print *,'avant bmc1:iitab,jjtab',iitab,jjtab

c                                               y ->       cree vna  (raie)
c                                                               ***
      call bmc1(nw,win,nm,milsec,ona(nw),izyb,yzname,bna(nw),
c          ---                            2  yna(nq,nw)  bidon iyb=0
c               etiq, fenetre,       x   0/1  y,z ou b   
     1        gna(nw),vna(nq,nw),  
c                g     out  

     2      sundec,iswap,ipermuty,icalib,fna(nq,nw), fl, repert,lbda,
c                                        calib     economie
     3    nq,xrecad,yrecad,istop,ibs(nw),jbs(nw),iitab,jjtab,nntab,nnfl,
c         in in     in           dim fich b
     4      ijcam,tab,tabxy,dc,0.,0.,message,istray,stray,xnormf,jhair,
c               economie                     in     eco   inutil
     5      fcont)
c           in

      print *,'apres bmc1',iitab,jjtab


72    continue  !! nw
75    continue  !! nq
c       print *,'apres yzmvw'
c--------------------------------------- 

        nw1=1
        nw2=nw1+nwinp-1

      idy=0   !                                                   idy=0
      istr=0
      if(iflat.eq.4)istray=1
      if(iflat.eq.5)istray=2
      if(iflat.eq.6)idy=1    !                     idy=1
      iterflucm=1
      if((iflat-3).eq.0.and.nrecad.eq.2)iterflucm=10
        deljt=0.
        delja=0.
        deljb=0.
c--
      do iterfluc=1,iterflucm

      ides=0
      if(nrecad.eq.2.or.iflat.eq.2)ides=1
      if(iterfluc.ne.iterflucm)ides=0

         write(7,*)' nrecad,iterfluc ',nrecad,iterfluc

      print *,'avant cmf1,iitab,jjtab',iitab,jjtab

c      deljt=0.   !2012
      
      call cmf1(nw1,nw2,win,nm,1,nquv,kdecal,
c          ---
     1        gna,vna,wna,fna,                 fl,tb,
c             g   v   w   f et pour nom pgplot, economie
     2        nrecad,xrecad,yrecad,idebf,iitab,jjtab,nnfl,lltab,nntab,
c                    out (modifies si nrecad.eq.1)
     3        im,jm,     x,y,    jt100, ibdm, sigbd,ides,
c             out     economie   out   in   out  in   
     4        iterfluc,deljt,delja,deljb,idy,dyln,trjm,
c             in                         in  out  out
     5        istray,stray,xnormf,iplotf)
c             in     eco   in/out    in
      print *,'apres cmf1'      
      print *,'nrecad ',nrecad      
      if(nrecad.eq.2)then
         do ibd=1,ibdm
            piv=0.
         do nq=1,nquv
         do nw=1,nwin
         do n=3,nm-2
           piv=piv+sigbd(ibd,n,nw,nq)
         enddo
         enddo
         enddo
           piv=piv/float((nm-4)*nwin*nquv)
           fluc(iterfluc,ibd)=piv
c          ----
         enddo
       endif
      
803   format(' iterfluc,deljt,delja,deljb, fluc(iterfluc,ibd): ',
     1        /i3,2x,3f8.0,4x,3f8.4)
      write(7,803)iterfluc,deljt,delja,deljb,
     1            (fluc(iterfluc,ibd),ibd=1,ibdm)

      call interflu(iterfluc,fluc,jt100,deljt,delja,deljb)
c          -------- in                   out
      enddo     !    iterfluc
c--
      if(nrecad.eq.2)goto810

805   format(/'    nq    nw  xrec  yrec')
      write(3,805)
806   format(2i6,2f6.2)

      xrec=0.
      yrec=0.        
      idebfp=0

      do808 nq=1,nquv
      do nw=1,nwin
        write(3,806)nq,nw,xrecad(nw,nq),yrecad(nw,nq)
        xrec=xrec+xrecad(nw,nq)
        yrec=yrec+yrecad(nw,nq)
c        idebfp=idebfp+idebf(nw,nq)
      enddo
808   continue
 
      piv=nquv*nwin

      xrec=xrec/piv
      yrec=yrec/piv
c      idebfp=float(idebfp)/piv+0.5
c                                egalisation sur nq et nw
      do 809 nq=1,nquv
      do nw=1,nwin
        xrecad(nw,nq)=xrec
        yrecad(nw,nq)=yrec
        idebf(nw,nq)=idebfp            !  zero
c       -----
      enddo
809   continue
      write(3,*)' yrecad(nw,nq)',yrecad
      write(3,*)' idebf(nw,nq)',idebf

810   continue  !!   fin boucle recad
      
      call pgend                !   fin flat.ps
      print *,'before gv cal.ps '
      call par1('  icalps',1,icalps)
      if(icalps.eq.1)call system('gv cal.ps &')
c-----------------------------   dessin coupe tb
      call coupe_y(tb,iitab,jjtab,lltab,im,jm,nm,jt100)
c--------------------------------
c bplot vw.ps
      call pgbegin(0,'vw.ps/ps',4,4)
c                     --------
      do nw=1,nwin
        do nq=1,nquv
      write(3,*)' plot_c: nw,nq ',nw,nq
        call plot_c(vna(nq,nw),sundec,tete,
c            ------                   
     1                          x,y,tabeco,iitab,jjtab,nntab)
c                               economie

      if(calfs.ge.1)call plot_c(wna(nq,nw),sundec,tete,
c                        ------
     1                          x,y,tabeco,iitab,jjtab,nntab)
c                               economie
        enddo
      enddo

      call pgend
c---
      if(mgrim.ne.0)
     1 call grid(fl,iitab,jjtab,nnfl,im,jm,nm,idebg,itgrig,x,y)
c           ----                              out          eco
        print *,' Fin etape FLAT'

      endif
c     fin etape iflat
c***********************************************************************
c                                            Etapes ibmc, iquick,ipro
c                                                   ****  ****** ****
c**********************************************************************
c                     noms fichiers sequence necessaires dans les 3 cas  

      if(ibmc+icmd+iquick+icmr+iprof+igrayq+igrayp+istokes+iunno.eq.0)
     1   goto900

c                                                fnom(nf,nw)
c        ipiv=win(1)+1
c        let1=lettre(ipiv)
        print *,'fic(nw) : ',fic(nw)
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      call par1('noblines',1,noblm)
        if(noblm.eq.0)noblm=1

      do 850 nobl=1,noblm
        write(3,*)'nobl=',nobl
        call par1('    tob1',nobl,tob1)
        call par1('    tob2',nobl,tob2)
        call par1('    nob1',nobl,nob1)
        call par1('    nob2',nobl,nob2)
      nfm=0  !!                 pour rechercher tous les fichiers
c                                               toutes polars confondues
        print *,'dob : ',dob

      do nw=nw1,nw2
      call fnomw2('b',fic,nw,nw,   dob,tob1,tob2,
     1           nfm,fnom,ftime,istop, repert)
c          ----- out  out  out  out     in
        write(3,*)' reg ',reg,'  nfm',nfm,' nw',nw

c      write(3,*)' bmc:fnom(nnnf,nw),nnnf=1,nfm) ',
c     1               (fnom(nnnf,nw),nnnf=1,nfm)

      if(nfm.eq.0)then
      print *,' ms1: nfm=0 : no data-files found'
      stop
      endif
c        print *,fnom(1,nw)
c        print *,fnom(nfm,nw) !!!!!!!!!!!!!!!!!!
c      write(3,*)' apres fnomw2:nw,fnom(1,nw)',nw,fnom(1,nw)
c      write(3,*)' apres fnomw2:nw,fnom(nfm,nw),nfm',nw,fnom(nfm,nw),nfm  
      enddo
c---------                                        nscanmax,nfdeb(nscan)
      call scan1(nob1,nob2,intert,ntmax,ftime,nfm,nscanmax,nfdeb)
c          ----             in    in    in    in   out      out

        if(icutcor.eq.1)nscanmax=1
        write(3,*)' intert,ntmax,nfm,nscanmax ',intert,ntmax,nfm,
     1                                                 nscanmax
        write(3,*)' nfdeb ',(nfdeb(i),i=1,nscanmax)

c      write(3,*)' bmc:fnom(nnnf,nw),nnnf=1,nfm) ',
c     1               (fnom(nnnf,nw),nnnf=1,nfm)

c*********************************BOUCLE SCAN*****************************
c                                 inclut  bmc,cmd,(i3scan,)quick,grayq
c                                             cmr,(i3scan,)prof,grayp
        write(3,*)' nscanmax ',nscanmax
        call par1('   ntmax',1,ntmax)
      do 800 nscan=1,nscanmax
          nf1=nfdeb(nscan)
          nf2=nf1+ntmax-1
                                 call par1('    nob2',1,nob2)
                              if(nscan.eq.nscanmax)then
                                if(nob2.ne.0)then
                                nf2=min0(nfm,nob2)
                                else
                                nf2=nfm
                                endif
C                              else
C                                nf2=nfdeb(nscan+1)-1
                              endif

      nxm=ntmax   !(nf2-nf1+1)/(ipos*nquv*rafburst)
      if(nxm.eq.0)goto800
c      nf2=nf1-1+nxm*(ipos*nquv*rafburst)
      write(3,*)' nscan,nscanmax, nf1,nf2,nxm,nfm,ipos,nquv,rafburst'
      write(3,*)  nscan,nscanmax, nf1,nf2,nxm,nfm,ipos,nquv,rafburst

c-----------------------  noms des fichiers du scan 
c                               dnanfp(nf,1)  
c                               rnanfp(nf,1) 
c
      do nx=1,nxm      !   12345
      do ny=1,ipos
      do nr=1,raf
      do nq=1,nquv
      do nb=1,burst
        if(nxfirst.eq.0)then
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
        else
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
        endif

c      write(3,*)' nscan,nx,ny,nq,nb,nf ',nscan,nx,ny,nq,nb,nf
            nw1=1
            dname=xna(nw1)

c                     premiere fenetre
            dname(2:7)=fnom(nf,1)(2:7)
            dname(8:8)='_'
            dname(9:16)=fnom(nf,1)(30:37)
            write(dname(1:1),'(a)')'d'
            write(dname(21:21),'(i1)')0
            write(dname(22:22),'(i1)')nq            

            if(rafburst.ne.1)then
                  nrb=nr*nb-1
            write(dname(18:19),'(i2)')nrb
                 if(nrb.le.9)write(dname(18:18),'(i1)')0
            endif

            dnanfp(nf,1)=dname
c           ------
            rnanfp(nf,1)=dname
c           ------
            write(rnanfp(nf,1)(1:1),'(a)')'r'
            write(3,*)' pour icmd: dname ',dname
            write(3,*)' pour icmr: rname ',rnanfp(nf,1)


      enddo
      enddo
      enddo
      enddo
      enddo
c                            les dnanfp contiennent
c                                  les heures reelles qqsoit nq
c                                  les indices burst ou raf 
c                                  si burst(ou raf).ne.0  en 18,19
c                                  le numero de la raie en 20
c                                  0 en 21
c                                  les valeurs de nq en 22

81    format(2x,a)
82    format(40x,i8)
        write(7,81)dnanfp(nf1,1)
        write(7,81)dnanfp(nf2,1)

        write(7,82)nxm

c                             ncycle=3 pos. pour 1 stokes et 1 burst
c                                    9           3           1

c---------------------- fin noms fichiers
c                                              Etape bmc
c                                              ---------  

      write(3,*)' nf1,nf2 ',nf1,nf2

      nxnx=1
      nyny=1
      nrnr=1
      nnqnnq=1
      nqexnqex=1
      nbnb=1

      if(ibmc.eq.2)then  !2 calculs seulement pour test glissement coulisses
        nxnx=nxm-1
        nyny=ipos
        nrnr=raf2
        nnqnnq=nq2
        nqexnqex=nqexm
        nbnb=burst2
      endif

           message=0
      do nx=1,nxm,nxnx
      do ny=1,ipos,nyny
      do nr=raf1,raf2,nrnr
      do nnq=nq1,nq2,nnqnnq
      do nqex=1,nqexm,nqexnqex
         nq=nqord(nnq,nqex)

       write(3,*)' bmc:nx,ny,nr,nnq,nqex,nq ',
     1                 nx,ny,nr,nnq,nqex,nq

      do nb=burst1,burst2,nbnb

        if(nxfirst.eq.0)then
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
        else
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
        endif

c      write(3,*)' bmc:fnom(nnnf,1),nnnf=1,nfm) ',
c     1               (fnom(nnnf,1),nnnf=1,nfm)

      write(3,*)' bmc:nf,fnom(nf,1) ',
     1                nf,fnom(nf,1)

        call par1('    dxr0',1,kdxr0)
        call par1('    dyr0',1,kdyr0)
        call par1('  dxrmms',1,kdxrmms)  !  micro arcsec par fichier
        call par1('  dyrmms',1,kdyrmms)
      dxr=kdxr0+(nf-1)*float(kdxrmms)/1000.
      dyr=kdyr0+(nf-1)*float(kdyrmms)/1000.

        nw1=1
        nw2=nw1+nwinp-1
c                       noms des fichiers b et c (a chaque nf)
      call par1('   ncam1',1,ncam1)
      call par1('   ncam2',1,ncam2)

      if(ncam1*ncam2.eq.0)then  
         do nw=1,nwin
            bna(nw)=fnom(nf,nw)
            write(bna(nw)(pos_nomcam:pos_nomcam),'(i1)')win(nw)
         enddo
      else
            bna(1)=fnom(nf,1)
            write(bna(1)(26:27),'(i2)')ncam1
            if(bna(1)(26:26).eq.' ')write(bna(1)(26:26),'(i1)')0

            bna(2)=fnom(nf,2)
            write(bna(2)(26:27),'(i2)')ncam2
            if(bna(2)(26:26).eq.' ')write(bna(2)(26:26),'(i1)')0
      endif

c      do nw=nw1,nw2
c         obname=fnom(nf,nw)
c         write(obname(pos_nomcam:pos_nomcam),'(i1)')win(nw)
c         bna(nw)=obname          
c      enddo
          write(3,*)' bna(nw1), bna(nw2) ',bna(nw1),bna(nw2)

      do nw=nw1,nw2
        is=nbcln(nw)
        js=nblgn(nw)
        cname=xna(nw)
        cname(2:7)=bna(nw)(2:7)
        cname(8:8)='_'
        cname(9:16)=bna(1)(30:37)
        write(cname(1:1),'(a)')'c'

               if(rafburst.ne.1)then
                   nrb=nr*nb-1
                 write(cname(18:19),'(i2)')nrb
                 if(nrb.le.9)write(cname(18:18),'(i1)')0
               endif

        write(cname(22:22),'(i1)')nq
        if(ibmc.ne.0)call system('rm '//cname)
        cna(nq,nw)=cname

c        write(3,*)' ibmc: nw, bna,cna: ',nw,bna(nw),cna(nq,nw)
      enddo   
c                   les heures des fichiers b,c,d,r dependent de nq
c                   les heures des fichiers i,j,q,p 
c                        sont celles des premiers b du scan
c                        pour la premiere des 3 positions ipos
c                        et dependent de nq


c------------                                creation fichiers c
c                                            -------------------
      if(ibmc.ne.0)then
c     -------------

        print *,' etape ibmc: dxr,dyr ',dxr,dyr
        do nw=nw1,nw2

205   format(' bmc:   nx   ny   nr  nnq nqex   nb   nf c'/5x,7i5,1x,a22)
      write(3,205)nx,ny,nr,nnq,nqex,nb,nf ,cna(nq,nw)
      print   205,nx,ny,nr,nnq,nqex,nb,nf ,cna(nq,nw)

c                write(3,*)' call bmc1: nq,nw,fna ',nq,nw,fna(nq,nw)
c                 write(3,*)' nf,nw,c :'nf,nw,cna(nq,nw)

         izyb=3  !         ***************************************************
c        ****
           icalib=1
           yzname=yna(nq,nw)

           write(3,*)'avant bmc: nw,bna(nw), ipermu,ibs,jbs ',
     1                           nw,bna(nw),ipermu,ibs(nw),jbs(nw)
c      call pgbegin(1,'limb'//cna//'.ps/ps',3,2)
      call bmc1(nw,win,nm,milsec,xna(nw),izyb,yzname, bna(nw),
c          ---                           1   bidon iyb=1
     1          gna(nw),cna(nq,nw),  
c                  g       
     2     sundec,iswap,ipermu,icalib,fna(nq,nw), fl, repert,lbda,
c                                       calib     economie
     1    nq,xrecad,yrecad,istop,ibs(nw),jbs(nw),iitab,jjtab,nntab,nnfl,
c             in     in
     4    ijcam,tab,tabxy,dc,dxr,dyr,message,istray,stray,xnormf,jhair,
c             economie  mi      lliarcsec           eco   in
     5    fcont)
c         in si icalcont=1
c     call pgend
      
      if(istop.eq.1)stop
        enddo
c                                  fin boucle fenetres fichiers c
      endif

      enddo
      enddo
      enddo
      enddo
      enddo
      enddo
c--------------------------------------------------------  
c                                          Test calana
      call par1('  calana',1,calana)
      kanam=1
      if(mgrim.ne.0.and.calana.ne.0)kanam=6
      if(kanam.gt.1)then
        if(icmd.eq.0.or.iquick.eq.0)then
        print *,' calana=1 implies icmd.ge.1 and iquick=1 '
        stop
        endif
      endif         

c                          boucle kanam (cospatialite)
      do315 kana=1,kanam
c           ------------
        sana(kana)=0.
      print*,' kana ',kana
      write(3,*)' kana ',kana
      write(3,*)' dxana ',dxana
      write(3,*)' dyana ',dyana
      write(3,*)' sana ',sana
        write(3,*)'nobl= ',nobl

      if(icmd.ne.0)then
c------------------------------------    Etape icmd
c                                              ----
      ides=0
      if(kana.eq.1)then
      ides=1
c                     dessins de profils pour 1er Stokes seulement
c                  les calculs d (avant dstok) ne sont faits
c                  que pour kana=1
      endif

        denom=0.

      do n=1,jjtab
      nfail(n)=0
      enddo

      do ny=1,ipos
      do nr=raf1,raf2
c      do nr=1,raf
      do nnq=nq1,nq2    !     nnq
      do nqex=1,nqexm
         nq=nqord(nnq,nqex)
      do nb=burst1,burst2
c      do nb=1,burst
      do nx=1,nxm
        
        if(nxfirst.eq.0)then
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
        else
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
        endif
      nw1=1
      nw2=nw1+nwinp-1
c              write(3,*)'nw1,nw2',nw1,nw2

               piv=0.
      do nw=nw1,nw2
c              piv=piv+idebf(nw,nq)
c               piv=piv+idebg
         cna(nq,nw)=xna(nw)
         cna(nq,nw)(2:7)=fnom(nf,1)(2:7)
         cna(nq,nw)(8:8)='_'
         cna(nq,nw)(9:16)=fnom(nf,1)(30:37)
         write(cna(nq,nw)(1:1),'(a)')'c'

               if(rafburst.ne.1)then
                    nrb=nr*nb-1
                 write(cna(nq,nw)(18:19),'(i2)')nrb
                 if(nrb.le.9)write(cna(nq,nw)(18:18),'(i1)')0
               endif

         write(cna(nq,nw)(22:22),'(i1)')nq
c            print *,' ms: nw,nb,nq,fnom(nf,1) ',nw,nb,nq,fnom(nf,1)
c            print*,' ms: cna ',cna(nq,nw)
      enddo

c                                             ouverture d
            iud=51
            dname=dnanfp(nf,1)
            call system('rm '//dname)
            call opennew22(dname,sundec,51)
c                ---------                 
            print *,' etape icmd  nf=',nf,' dname:',dname

c                                             ouverture m, t
          if(icmd.gt.1.and.kana.eq.1)then
            ium=61
            mname=dname
            write(mname(1:1),'(a1)')'m'
            call system('rm '//mname)
            call opennew22(mname,sundec,61)
c                ---------
          endif            

            tname=dname
            write(tname(1:1),'(a)')'t'
        if(kana.eq.1)then
            call system('rm '//tname)
            call opennew22(tname,sundec,60)
c                ---------
        else
            call openold22(tname,sundec,60)
c                ---------
        endif            

            corv=0
c             pas de correction de lambdas (Vitesses) dans l'etape iquick
c             donc pas d'ouverture de 53
      iuj=53
         npo=1

c signes pour echange de voies grille
      nqsig=1

      if(nqs.gt.6.and.mgrim.ne.0)then
           itest=0
        do npiv1=7,9
        do npiv2=1,2
          if(nqord(npiv1,npiv2).eq.nq)then
            nqsig=nqsign(npiv1,npiv2)
            itest=1
          endif
        enddo
        enddo

        if(itest.eq.0)then
         print *,' polord must not be zero for the polar under study'
         print *,' if nqs>6 and mgrim.ne.0'
         stop
        endif
      endif

      write(3,*)' call cmd1: nq,nqsig ',nq,nqsig
c      write(7,*)' npolar,nburst,nframe ',nq,nb,nx
        iecri=0
        if(nx.eq.1.or.nx.eq.nxm)iecri=1

c prominences: min cont for velocities
      call par1('   contv',1,icontvd)
        icontv=icontvd
      call par1(' mincont',1,mincont)
      call par1('  minpro',1,minpro)
        karg1=icontv
        karg2=minpro
        karg3=mincont
        
      call cmd1(0,nw1,nw2,nwinp,milsec,npo,    nq, cna, dname,   
c          ---                               head d  in   in  pour noms
     1                  iud,iuj,       
     2          corv,   tabeco,tb, ides, mgrim,dlbd,idebg,itgrig,
c                in     economie  dessin
     3          iitab,jjtab,nntab,lltab,moyw,lma,
c                                       inutilises
     4          dxana,dyana,kana,kanam,nfail,nscan,nx,icmd,mname,ium,
     5       nqsig,jhair,iecri,kind,karg1,karg2,karg3)

      ndend=ndend+1
      if(ndend.eq.ndprof)then
         print *,' ndend  dname',ndend,dname
         call system('gv cm'//dname//'.ps &')
      endif
c                                 pour dme1   minpro,mincont

c si mgrim.ne.0              on ecrit dans iud, avec dim i1,i2 j1,j2
c si mgrim.ne.0  calana=0    on ecrit dans iud, avec dim i1,i2 j1,j2
c si mgrim.ne.0  calana=1   
c          kana.eq.1         on ecrit dans 60 (file t dsans header)
c                                                     avec dim i1,i2 j1,j2
c                            et dstok ecrit dans iud (file d), 
c                                                     avec dim reduites
c                                                     par itana,jtana
c                                le fichier d servira pour dme
c                                puis sera remplace a l'etape kana suivante
c          kana>1            on ne recalcule pas
c                            on lit dans 60 (file t sans header)
c                                                     avec dim i1,i2 j1,j2
c                            et dstok ecrit dans iud (file d),
c                                                     avec dim reduites
c                                                     par itana,jtana 
c                              (restera le dernier a kana=kanam)            


      if(icmd.gt.1.and.kana.eq.1)close(unit=ium)

      close(unit=iud)
      close(unit=60)

      if(kana.eq.kanam)call system('rm '//tname)
        denom=denom+1.

      enddo
        ides=0
      enddo
      enddo
      enddo
      enddo
      enddo

      if(kana.eq.kanam)call fail(nfail,jjtab,milsec)
c                           ----

210   format(' Moyennes d'/'     l   rw1   rw2   rw3   aw1',
     1        '   aw2   aw3   bw1   bw2   bw3   moy sigma ')
c      if(kana.eq.kanam)write(7,210)
211   format(12i6)
c      do l=1,lma
c        do iw=1,11
c        moyw(iw,l)=moyw(iw,l)/denom
c        enddo
c      if(kana.eq.kanam)write(7,211)l,(moyw(iw,l),iw=1,11)
c      enddo

      print*,' fin etape icmd'
      endif

c---------------------------------------------------------------------
c  Ouvertures pour dme de iquick (utilise les fichiers d de cmd)
 
        if(iquick.ne.0)then
c          -----------
       print *,' iquick'  

      if(kanam-kana.eq.0)then
      call system('rm quick.ps')
      call pgbegin(0,'quick.ps/ps',2,1)
c          -------
      endif

c      raf1=1
c      raf2=raf
c      burst1=1
c      burst2=burst
c      nqp1=nq1
c      nqp2=nq2
c--------
c moyenne/selection/destretching des fichiers d 

      if(select.lt.0)call dmd1(0,select,nxm,ipos,raf,nq1,nq2,
c        -----------      ---- 
     1                nquv,burst,dnanfp,nf1,nxfirst,
     2                nqexm,nqord,
     3                ttb,kz,sundec,dreject,maxttb,iitab)
c                     eco

      print *,' fin moyennes/selection dmd1'

        nr1=raf1
        nr2=raf2
        if(select.eq.-1)nr2=nr1

        nb1=burst1
        nb2=burst2
        if(select.eq.-1)nb2=nb1
c--------------------
c  Beam-exchange for modulator (Pic du Midi)
      if(nqs.gt.6              .and.mgrim.eq.0)then
c        --------                   ----------

      write(3,*)' select,nqord',select,nqord
      write(3,*)' nr1,nr2,nq1,nq2,nb1,nb2 ',
     1            nr1,nr2,nq1,nq2,nb1,nb2
      write(3,*)' nxm,ipos,raf,nquv,burst,nf1,nxfirst ',
     1            nxm,ipos,raf,nquv,burst,nf1,nxfirst

      call echan_modul(0,select,nqord,nqexm,
c          -----------
     1        nq1,nq2,nr1,nr2,nb1,nb2,
c             in  in  
     2        nxm,ipos,raf,nquv,burst,
     3        dnanfp,nf1,nxfirst,etab1,etab2,kz,sundec,iitab,jjtab)
c                                eco
      endif
c----------
c  Beam-exchange for grid observations (Themis)
      if(nqs.gt.6              .and.mgrim.ne.0)then
c        --------                   ----------

      write(3,*)' select,nqord',select,nqord
      write(3,*)' nr1,nr2,nq1,nq2,nb1,nb2 ',
     1            nr1,nr2,nq1,nq2,nb1,nb2
      write(3,*)' nxm,ipos,raf,nquv,burst,nf1,nxfirst ',
     1            nxm,ipos,raf,nquv,burst,nf1,nxfirst

      call echan_grille(0,select,nqord,
c          ------------
     1        nq1,nq2,nr1,nr2,nb1,nb2,
c             in  in  
     2        nxm,ipos,raf,nquv,burst,
     3        dnanfp,nf1,nxfirst,etab1,etab2,kz,sundec,iitab,jjtab)
c                                eco
      endif

c-------------------------------------boucles 310 (tous sauf nx)
        call par1('   ntmax',1,ntmax)
      do310 nr=nr1,nr2
      do310 nnq=nq1,nq2         !  inclut beam-exch. si necessaire
           nq=nqord(nnq,1)
      do310 nb=nb1,nb2

c premier fichier du scan pour nq,nb,nr

             nfaa=nf1+nb-1+burst*(nq-1+nquv*(nr-1))
c            ----
      write(3,*)' ms1, boucle 310:nr,nnq,nq,nb,nfaa ',
     1                            nr,nnq,nq,nb,nfaa

c             qname=dnanfp(nfaa,1)
             qname=dnanfp(nf1,1)

             write(qname(18:19),'(i2)')ntmax ! select !  select
c                                      -----
             if(qname(18:18).eq.' ')write(qname(18:18),'(a1)')'0'
c          if(select.ge.0.and.select.le.9) write(qname(18:18),'(a1)')'0'
          write(qname(22:22),'(i1)')nnq  !  beam-exchge

                       write(qname(1:1),'(a)')'q'
                       call system('rm '//qname)
                       call opennew22(qname,sundec,52)
c                           ---------
                       jname=qname      
c                        jname=dnanfp(nf1,1)
                       write(jname(1:1),'(a)')'j'
c             if(select.lt.0)write(jname(18:19),'(i2)')select  !  select
c             if(nqs.gt.6)write(jname(22:22),'(i1)')nnq !  beam-exchge

c             if(linref.eq.0)then
c               write(jname(18:22),'(a5)')'00101'
c               write(jname(20:20),'(i1)')nline
c             else
c               write(jname(18:22),'(i5)')linref
c               if(jname(18:18).eq.' ')write(jname(18:18),'(a1)')'0'
c               if(jname(19:19).eq.' ')write(jname(19:19),'(a1)')'0'
c             endif

                       call system('rm '//jname)
                       call opennew22sf(jname,sundec,53)
c                           -----------

                       print *,'avant i3scan : '
c------------------
      if(mgrim.ne.0)then                             !  traitement de ny
c                       iname=qname
c                       write(iname(1:1),'(a)')'i'
c                       call system('rm '//iname)
c                       call opennew22sf(iname,sundec,50)
c                           -----------

220   format(a)
c      write(50,220)iname
222   format('   di1   dj1  cor1   di2   dj2  cor2   di3   dj3  cor3')
c      write(50,222)

                     
c  Pour chaque bande Q (ou U, ou V), 3 positions ny:
 
c                        nq      ny   --> bande complete 

c    nquv=1               1       1           V           nfa  (nfaa debut scan)
c                         2       1                       nfa    "
c                         3       1                       nfa    "

c    nquv=3                1       1           Q          nfa    "
c                          2       1           U          nfa    "
c                          3       1           V          nfa    "
c                           1       2            
c                           2       2
c                           3       2
c                            1       3
c                            2       3
c                            3       3  

c    calcul d regroupe pour chaque scan i de 3 (ou 4) positions
c    correlations sur i/j pour les 3 positions si nq=1 et lcor>0 

      do nx=1,nxm

c premier fichier pour nx
      if(nxfirst.eq.0)then
         nfa=nfaa+(nx-1)*ipos*nquv*rafburst
      else
         nfa=nfaa+(nx-1)*     nquv*rafburst
      endif

        icorrel=1
        npo=1
      write(3,*)' avant i3scan: nfa,dnanfp ',nfa,dnanfp(nfa,1)
      call i3scan(mgrim,nquv,nnq,ipos,nfa,rafburst,
c     -----
     1        dnanfp,51,50,sundec,tb,icorrel,milsec,
c                   iud iui
     2        iitab,jjtab,lltab,nxfirst,nxm,select)   ! select

      enddo
      endif  !!  fin test mgrim
c-------------------------------   creation fichiers q
c                                          dnanfsp

                                 write(3,*)' nq,nxm ',nq, nxm
                                
        do nx=1,nxm
           ny=1
                             
        if(nxfirst.eq.0)then
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
        else
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
        endif
                                   dname=dnanfp(nf,1)

          write(dname(18:19),'(i2)')select  !  select
          if(select.ge.0.and.select.le.9) write(dname(18:18),'(a1)')'0'
          write(dname(22:22),'(i1)')nnq  !  beam-exchge

c             if(select.lt.0)write(dname(18:19),'(i2)')select  !  select
c             if(nqs.gt.6)write(dname(22:22),'(i1)')nnq  !  beam-exchge

        if(mgrim.eq.0)then
                                      nns=0
                                      else
                                      nns=1
                                   endif
                                write(dname(21:21),'(i1)')nns

                                dnanfsp(nx,1)=dname
c                               -------                               
         enddo    !   nx               
c                                          dnanfp   pour tous d
c                                          dnanfsp  pour d composites i3scan                                

c nxm = nbre de cycles complets
85    format(' nq,nx,nb,dnanfsp ',3i4,a)
      do nx=1,nxm
      write(3,85)nq,nx,nb,dnanfsp(nx,1)
      enddo

      call dme1(0,nxm,1,dnanfsp,52,53,jname,jnameref,qname,nvel,
c          ----                 q  j  out    inutile
c                             out out
     1          linref,sundec, lbda,dlbd,milsec,etab,etab1,etab2,
c                   out    in              
     2          iitab,jjtab,jje,dxana,dyana,sana,kana,kanam,nqs,
     3          nscanmax,icutcor,kind,kdisc,lmh,jt100)   ! m2403
c                                in
      close(50)
c      close(52)
      rewind(52)
      close(53)

c        if(ideld.ne.0)then
c          do nx=1,nxm
c             dname=dnanfsp(nx,1)
c             call system('rm '//dname)
c          enddo
c        endif

310   continue   !!  fin boucle nr,nq,nb
c---------------------------=========================

      if(kana.ge.5)call ssana(dxana,dyana,sana,kana)
c                       -----  in   in                  pour 1,2,3,4,5
c                              out  out                      6


      if(kanam-kana.eq.0)call pgend
      print *,'fin etape iquick'!!!!!!!!!!!!!!!!!

      endif

315   continue
      write(3,*)' kanam ',kanam
      write(3,*)' dxana ',dxana
      write(3,*)' dyana ',dyana
      write(3,*)' sana  ',sana
        write(3,*)'apres 315 nobl= ',nobl
c                                   Fin boucle kana

c--------------------------------------------------- Etape  icmr
c                                                           ----
      print *,'icmr'!!!!!!!!!!!!!!!!!

      kana=1
      kanam=1

      if(icmr.ne.0)then

        ides=1
        denom=0.

      do n=1,jjtab
      nfail(n)=0
      enddo

      print *,' ipos,raf1,raf2,nq1,nq2,nqexm,burst1,burst2,nxm ',
     1          ipos,raf1,raf2,nq1,nq2,nqexm,burst1,burst2,nxm

      do ny=1,ipos
      do nr=raf1,raf2
      do nnq=nq1,nq2
      do nqex=1,nqexm
         nq=nqord(nnq,nqex)
      do nb=burst1,burst2
      do nx=1,nxm
        
        if(nxfirst.eq.0)then
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
        else
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
        endif
      nw1=1
      nw2=nw1+nwinp-1
c              write(3,*)'nw1,nw2',nw1,nw2

            piv=0.

      print *,' nf,nw1,nw2 ',nf,nw1,nw2

      do nw=nw1,nw2
c            piv=piv+idebf(nw,nq)
c             piv=piv+idebg
         cna(nq,nw)=xna(nw)
         cna(nq,nw)(2:7)=fnom(nf,1)(2:7)
         cna(nq,nw)(8:8)='_'
         cna(nq,nw)(9:16)=fnom(nf,1)(30:37)
         write(cna(nq,nw)(1:1),'(a)')'c'

               if(rafburst.ne.1)then
                     nrb=nr*nb-1
                 write(cna(nq,nw)(18:19),'(i2)')nrb
                 if(nrb.le.9)write(cna(nq,nw)(18:18),'(i1)')0
               endif

         write(cna(nq,nw)(22:22),'(i1)')nq

c            print*,' ms: cna ',cna(nq,nw)
      enddo
c                                                    ouverture r          
            iud=51
            rname=rnanfp(nf,1)
            call system('rm '//rname)
            call opennew22(rname,sundec,51)
c                ---------                 
            print *,' etape icmr  nf=',nf,' rname:',rname
            corv=1
c             correction de lambdas (Vitesses) dans l'etape icmr
c             ouverture de 53 sur fichier j de nq

c             nfaa=nf1+nb-1+burst*(nq-1+nquv*(nr-1))
c            ----
             jname=dnanfp(nf1,1)     

c             if(linref.eq.0)then
c               write(jname(18:22),'(a5)')'00101'
c             else
c               write(jname(18:22),'(i5)')linref
c               if(jname(18:18).eq.' ')write(jname(18:18),'(a1)')'0'
c               if(jname(19:19).eq.' ')write(jname(19:19),'(a1)')'0'
c             endif


                       write(jname(1:1),'(a)')'j'
          write(jname(18:19),'(i2)') select  !  select
          if(select.ge.0.and.select.le.9) write(jname(18:18),'(a1)')'0'
          write(jname(22:22),'(i1)')nnq  !  beam-exchge

                       call openold22sf(jname,sundec,53)
c                           -----------

c                                                     ouverture n
          if(icmr.gt.1)then
            iun=61
            nname=rname
            write(nname(1:1),'(a1)')'n'
            call system('rm '//nname)
            call opennew22(nname,sundec,61)
c                ---------
          endif

         iur=51
         iuj=53
         npo=1

c signes pour echange de voies   grille
      nqsig=1

      if(nqs.gt.6.and.mgrim.ne.0)then
           itest=0
        do npiv1=7,9
        do npiv2=1,2
          if(nqord(npiv1,npiv2).eq.nq)then
            nqsig=nqsign(npiv1,npiv2)
            itest=1
          endif
        enddo
        enddo

        if(itest.eq.0)then
         print *,' polord must not be zero for the polar under study'
         print *,' if nqs>6 and mgrim.ne.0'
         stop
        endif
      endif

c le zero des vitesses est calcule avec l'option select courante
c   (fichier j)

c      write(7,*)' npolar,nburst,nframe ',nq,nb,nx
        iecri=0
        if(nx.eq.1)iecri=1
        if(nx.eq.nxm)iecri=2

        karg1=icontv
        karg2=minpro
        karg3=mincont

      call cmd1(1,nw1,nw2,nwinp,milsec,npo,    nq, cna, rname,   
c          --- idr                          head d  in   in  pour noms
     1                  iur,iuj,       
c                           jname
     2          corv,   tabeco, tb, ides, mgrim,dlbd,idebg,itgrig,
c                in     economie   dessin
     3          iitab,jjtab,nntab,lltab,moyw,lma,
c                                       inutilises
     4          dxana,dyana,kana,kanam,nfail,nscan,nx,icmr,nname,iun,
     5          nqsig,jhair,iecri,kind,     karg1,  karg2,karg3)
c               lus dans j        pour dme1 icontv protus
c                     etape icmr

      if(icmr.gt.1)close(unit=iun)
      close(unit=iur)
        denom=denom+1.

      close(unit=iuj)
      enddo
        ides=0
      enddo
      enddo
      enddo
      enddo
      enddo

      call fail(nfail,jjtab,milsec)
c          ----

      print*,' fin etape icmr'
      endif

c--------------------------------------------    
c  Ouvertures pour dme de iprof (utilise les fichiers r de cmd)
 
        if(iprof.ne.0)then
c          ----------

      write(3,*)' iprof: nscanmax ',nscanmax
       print *,' iprof'  

      call system('rm prof.ps')
      call pgbegin(0,'prof.ps/ps',2,1)
c          -------
c-----------------------------   

c moyenne/selection/destretching des fichiers r 
      if(select.lt.0)call dmd1(1,select,nxm,ipos,raf,nq1,nq2,
c        -----------      ---- 
     1                nquv,burst,rnanfp,nf1,nxfirst,
     2                nqexm,nqord,
     3                ttb,kz,sundec,rreject,maxttb,iitab)
c                     eco
c   les nqp sont inchanges

      print *,' fin moyennes/selection dmd1 pour icmr'

        nr1=raf1
        nr2=raf2
        if(select.eq.-1)nr2=nr1

        nb1=burst1
        nb2=burst2
        if(select.eq.-1)nb2=nb1

c---------

c  Beam-exchange for modulator (Pic du Midi)
      if(nqs.gt.6              .and.mgrim.eq.0)then
c        --------                   ----------

      write(3,*)' select,nqord',select,nqord
      write(3,*)' nr1,nr2,nq1,nq2,nb1,nb2 ',
     1            nr1,nr2,nq1,nq2,nb1,nb2
      write(3,*)' nxm,ipos,raf,nquv,burst,nf1,nxfirst ',
     1            nxm,ipos,raf,nquv,burst,nf1,nxfirst

      call echan_modul(1,select,nqord,nqexm,
c          -----------
     1        nq1,nq2,nr1,nr2,nb1,nb2,
c              in  in  
     2        nxm,ipos,raf,nquv,burst,
     3        rnanfp,nf1,nxfirst,etab1,etab2,kz,sundec,iitab,jjtab)
c                                eco
      endif
c----------

c  Beam-exchange for grid observations (Themis)
      if(nqs.gt.6              .and.mgrim.ne.0)then
c        --------                   ----------

      write(3,*)' select,nqord',select,nqord
      write(3,*)' nr1,nr2,nq1,nq2,nb1,nb2 ',
     1            nr1,nr2,nq1,nq2,nb1,nb2
      write(3,*)' nxm,ipos,raf,nquv,burst,nf1,nxfirst ',
     1            nxm,ipos,raf,nquv,burst,nf1,nxfirst

      call echan_grille(1,select,nqord,
c          ------------
     1        nq1,nq2,nr1,nr2,nb1,nb2,
c             in  in   
     2        nxm,ipos,raf,nquv,burst,
     3        rnanfp,nf1,nxfirst,etab1,etab2,kz,sundec,iitab,jjtab)
c                                eco
      endif

c      write(3,*)' avant bcle 510: nscanmax ',nscanmax

c-----------------------------  boucles 510  (tous sauf nx)

      do510 nr=nr1,nr2
      do510 nnq=nq1,nq2
                nq=nqord(nnq,1)
      do510 nb=nb1,nb2

      write(3,*)' 510: nr,nnq,nb,nscanmax ',nr,nnq,nb,nscanmax 

             nfaa=nf1+nb-1+burst*(nq-1+nquv*(nr-1))
c            ----
             pname=dnanfp(nf1,1)

          write(pname(18:19),'(i2)') select  !  select
          if(select.ge.0.and.select.le.9) write(pname(18:18),'(a1)')'0'
          write(pname(22:22),'(i1)')nnq  !  beam-exchge

                       write(pname(1:1),'(a)')'p'
                       call system('rm '//pname)
                       call opennew22(pname,sundec,54)
c                           ---------
                       kname=pname      
                       write(kname(1:1),'(a)')'k'
                       call system('rm '//kname)
                       call opennew22sf(kname,sundec,55)
c                           -----------
c------------------
      if(mgrim.ne.0)then

      do nx=1,nxm

c premier fichier pour nx
      if(nxfirst.eq.0)then
         nfa=nfaa+(nx-1)*ipos*nquv*rafburst
      else
         nfa=nfaa+(nx-1)*     nquv*rafburst
      endif

        icorrel=1
        npo=1
                       print *,'avant i3scan : '
      call i3scan(mgrim,nquv,nnq,ipos,nfa,burst,
c           -----
     1        rnanfp,51,50,sundec,tb,icorrel,milsec,
c                   iur iui
     3        iitab,jjtab,lltab,nxfirst,nxm,select)   !  select
      enddo
      endif  !!  fin test mgrim
c-------------------------------   creation fichiers p
c                                          rnanfsp

                                 write(3,*)' nq,nxm ',nq, nxm
                                
                              do nx=1,nxm
                                 ny=1
                             
        if(nxfirst.eq.0)then
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
        else
        nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
        endif
                                   rname=rnanfp(nf,1)
c on choisit les fichiers r de l'option select
          write(rname(18:19),'(i2)')select  !  select
          if(select.ge.0.and.select.le.9) write(rname(18:18),'(a1)')'0'
          write(rname(22:22),'(i1)')nnq  !  beam-exchge

                                   if(mgrim.eq.0)then
                                      nns=0
                                      else
                                      nns=1
                                   endif
                                write(rname(21:21),'(i1)')nns
                                rnanfsp(nx,1)=rname
c                               -------                               
                              enddo               
c                                    rnanfp   pour tous r
c                                    rnanfsp  pour r composites i3scan                                
c nxm = nbre de cycles complets

      do nx=1,nxm
      write(3,85)nq,nx,nb,rnanfsp(nx,1)
      enddo

c      write(3,*)' avant dme: nscanmax ',nscanmax

      call dme1(1,nxm,1,rnanfsp,54,55,kname,jnameref,qname,nvel,
c          ----                 q  k         inutile
c                                            remplace dans le calcul par
c                                            kname -> jname
c                             out out
     1          linref,sundec, lbda,dlbd,milsec,etab,etab1,etab2,
c                 e? out    in 
     2          iitab,jjtab,jje,dxana,dyana,sana,kana,kanam,nqs,
     3          nscanmax,icutcor,kind,kdisc,lmh)
c                                in
c      write(3,*)' apres dme: nscanmax ',nscanmax

      close(50)
      close(54)
      close(55)

c        if(ideld.ne.0)then
c          do nx=1,nxm
c             dname=dnanfsp(nx,1)
c             call system('rm '//dname)
c          enddo
c        endif

510   continue   !!  fin boucle nq,nb

c      write(3,*)' apres 510: nscanmax ',nscanmax
      call pgend
      print *,'fin etape iprof'!!!!!!!!!!!!!!!!!

      endif
c--------------------------------------------------------  igrayq
      if(igrayq.ne.0)then

        nr1=raf1
        nr2=raf2
        if(select.eq.-1)nr2=nr1

        nb1=burst1
        nb2=burst2
        if(select.eq.-1)nb2=nb1


        call par1('   ntmax',1,ntmax)
      do nr=nr1,nr2
      do nnq=nq1,nq2
c        nq=nqord(nnq,1)
         nq=nnq
        write(3,*)' grayq: nr,nnq ',nr,nnq

      do nb=nb1,nb2
                 
cc             nfaa=nf1+nb-1+burst*(nq-1+nquv*(nr-1))
c              ----
         write(3,*)' qname 2771 msdp1.f  ',qname
c               qname=dnanfp(nf1,1)
c               write(qname(1:1),'(a)')'q'
c               write(qname(18:19),'(i2)')ntmax !select  !  select
c                                      -----
c             if(qname(18:18).eq.' ')write(qname(18:18),'(a1)')'0'
cc          if(select.ge.0.and.select.le.9) write(qname(18:18),'(a1)')'0'
c             write(qname(22:22),'(i1)')nnq !  beam-exchge

         write(3,*)' qname pour grayq before open 52  ',qname
         
           call openold22(qname,sundec,52)
c                   ---------
       
                   write(grayname(2:23),'(a)')qname
                   write(grayname(1:2),'(a)')'sq'
                   write(grayname(24:26),'(a)')'.ps'
                   print *,' grayname ',grayname
                  
c                  call system('rm grayq.ps')
                  call par1('     vps',1,kvps)
         if(kvps.eq.0)then
            call pgbegin(0,'grayq.ps/ps',igrq,jgrq)
         else
             call pgbegin(0,'grayq.ps/vps',igrq,jgrq)
c                                    -------
         endif
       
c             nf2=nfaa+(nxm-1)*ipos*nquv*rafburst
           dname=dnanfp(nf2,1)
           write(hfin(1:2),'(a)')blancs
           write(hfin(1:8),'(a)')dname(9:16)
           write(3,*)' msdp1.f 2801 call gray1'           
 
           call gray1(0,52,qname,igrq,jgrq,hfin,nxm,milsec,
c               ----
     1                grtab,iitab,jje,igrayq,lmh)
           close(52)

           call pgend
           call par1('igrayqps',1,igrayqps)
           call system('rm '//grayname)
           call system('mv grayq.ps '//grayname)
           if(igrayqps.eq.1)call system('gv '//grayname//' &')

        enddo
        enddo
        enddo

      endif

c-------------------------------------------------  igrayp

      if(igrayp.ne.0)then

        nr1=raf1
        nr2=raf2
        if(select.eq.-1)nr2=nr1

        nb1=burst1
        nb2=burst2
        if(select.eq.-1)nb2=nb1

      do nr=nr1,nr2
      do nnq=nq1,nq2
        nq=nqord(nnq,1)
        write(3,*)' grayp: nr,nnq ',nr,nnq

      do nb=nb1,nb2

c             nfaa=nf1+nb-1+burst*(nq-1+nquv*(nr-1))
c             ----
               pname=rnanfp(nf1,1)
               write(pname(1:1),'(a)')'p'

          write(pname(18:19),'(i2)')select  !  select
          if(select.ge.0.and.select.le.9) write(pname(18:18),'(a1)')'0'
          write(pname(22:22),'(i1)')nnq  !  beam-exchge

               call openold22(pname,sundec,54)
c                   ---------
c----
                  write(grayname(2:23),'(a)')pname
                  write(grayname(1:2),'(a)')'sp'
                  write(grayname(24:26),'(a)')'.ps'
        print *,' grayname ',grayname

        call system('rm grayp.ps')
             call pgbegin(0,'grayp.ps/ps',igrp,jgrp)
c                 -------    

c             nf2=nfaa+(nxm-1)*ipos*nquv*rafburst
           rname=rnanfp(nf2,1)
           write(hfin(1:2),'(a)')blancs
           write(hfin(1:8),'(a)')rname(9:16)
           call gray1(1,54,pname,igrp,jgrp,hfin,nxm,milsec,
c               ----
     1                grtab,iitab,jje,igrayp,lmh)
           close(54)

           call pgend
           call system('rm '//grayname)
           call system('mv grayp.ps '//grayname)
      enddo
      enddo
      enddo
      endif
c--------------------------------------------------------  istokes
      if(lbda.eq.6103.or.lbda.eq.6102.or.lbda.eq.6302)then

      if(istokes.ne.0)then
        nr1=raf1
        nr2=raf2
        if(select.eq.-1)nr2=nr1

        nb1=burst1
        nb2=burst2
        if(select.eq.-1)nb2=nb1

c      write(3,*)' entree unnostok: stokord ',stokord
c      print *,' stokord ',stokord
c      call unnostok(rnanfp,nf1,nf2,nr1,nr2,nb1,nb2,nxm,
c          --------            
c     1     milsec,stok,kstok,istok,lbdstok,stokname)


      endif
c--------------------------------------------------------  iunno
      if(iunno.ge.1)then
c      call unnofitffca(stokname,amem,imem,nfit0,nfit1,nfit2)
c          -----------

c      call unnofitffc(stokname,iunno)
c          ----------          1  accelere
c                              2  tous points

      endif
c      if(iunno.ge.2)then
c      call unnofitffca2(stokname,iunno)
c          -----------            2=accel
c      endif
c                   write(3,*)'nscan, nscanmax ',nscan,nscanmax

      endif
800   continue
c******************************  fin boucle scans
c***************************************************
850   write(3,*)'850 nobl= ',nobl
      continue   !   nob 
900   continue

c      close(unit=3)
c     close(unit=91)
      close(unit=8)
      close(unit=9)
c                   modif 20/11/99
      close(unit=7)
c      close(unit=2)  !  correl.lis

      print*,'FIN DE PROGRAMME'
      return
      end
c                               fin ms.f
c*****************************************************

c*****************************************************************
c------------------------------------
      subroutine readf(iu,key,val,idim)
      dimension val(idim)
      return
      end
c------------------------------------
      subroutine openold22(name,sundec,iu)
      character*22 name
      integer sundec
        write(3,*)name
      open(unit=iu,status='old',form='unformatted',file=name)
        write(3,*)'ouvert'
      return
      end
c------------------------------------
      subroutine openold22sf(name,sundec,iu)
      character*22 name
      integer sundec
        write(3,*)name
      open(unit=iu,status='old',form='formatted',file=name)
c        write(3,*)'ouvert'
      return
      end
c------------------------------------
      subroutine opennew22(name,sundec,iu)
      character*22 name
      integer sundec
        write(3,*)name
      open(unit=iu,status='new',form='unformatted',file=name)
c        write(3,*)'ouvert'
      return
      end
c------------------------------------
      subroutine opennew22sf(name,sundec,iu)
c                                           sf = avec format
      character*22 name
      integer sundec
        write(3,*)name
      open(unit=iu,status='new',form='formatted',file=name)
c        write(3,*)'ouvert'
      return
      end
c------------------------------------
      subroutine openold41(name,sundec,iu,repert)
      character*41 name
      integer sundec
      character*80 repert
      character*150 File

      I=1
       DO WHILE (repert(i:i).NE.' ')
                i=i+1
        END DO
        File=repert(1:i-1)//name

        write(3,*)File
      if(sundec.eq.1) then
        open(unit=iu,status='old',form='unformatted',recl=720,
     2access='direct',file=File)
      else
        open(unit=iu,status='old',form='unformatted',recl=2880,
     2access='direct',file=File)           
       endif
c        write(3,*)'ouvert'
c      print *,'fichier ouvert dans open'
      return
      end
c------------------------------------

        subroutine readi(iu,key,ivalkey,idim)
        dimension ivalkey(idim)
        character*8 key
        character buf*2880,m*70
        integer num,nbvir,valparouv,valparfer,valvir(10),max
        character*10 valkeycar(8)
c        integer*4 valkey(8)
        do num=1,10
         read(iu,rec=num) buf(1:2880)
         i=1
         do nb=1,36
          if(buf(i+10:i+17).eq.key(1:8)) then
            m(1:70)=buf(i+10:i+89)
            do ii=1,70
              if(m(ii:ii).eq.'(') then
                valparouv=ii 
                go to 10
              endif
            enddo
10          do ii=1,70
              if(m(ii:ii).eq.')') then
                valparfer=ii 
                go to 20
              endif
            enddo                
20          nbvir=0
            do ii=1,70
              if(m(ii:ii).eq.',') then
                nbvir=nbvir+1
                valvir(nbvir)=ii
              endif
            enddo
            do l=1,nbvir
            enddo
            do k=1,nbvir+1
               if(k.eq.1) then
                  ii=valparouv
               else
                  ii=valvir(k-1)
               endif
               if(nbvir.eq.0) then
                  jj=valparfer
               else
                  max=nbvir+1
                  if(k.eq.max) valvir(k)=valparfer
                  jj=valvir(k)
               endif
                valkeycar(k)(1:10)=m(ii+1:jj-1)
             enddo
             go to 1000
           else
            i=i+80
           endif
          enddo
         enddo                    
1000         do k=1,nbvir+1
c        decode(10,999,valkeycar(k)) ivalkey(k)
         read(valkeycar(k),'(i10)')ivalkey(k)
999      format(i10)
         enddo     
         return
         end
c----------------------------------------------
c         subroutine readifits(iu,key,ivalkey,idim)
c         dimension ivalkey(idim)
c         character*8 key
c         character buf*2880,m*80
c         do n=1,10
c         read(iu,rec=n) buf(1:2880)
c         i=1
c         do nb=1,36
c         if(buf(i:i+3).eq.'END ') go to 1000
c         i=i+80
c         enddo
c         enddo
c1000     continue
c         do jj=1,n
c         read(iu,rec=jj) buf(1:2880)
c         i=1
c         do nb=1,36
c         if(buf(i:i+7).eq.key)then
c         m(1:80)=buf(i:i+79)
c         call remplitw(m,ivalkey)
c         go to 2000
c         endif
c         i=i+80
c         enddo
c         enddo
c2000     return
c        end          
          
c-------------------------------------------
       Subroutine remplitw(keyword,ival)
       character keyword*80,fin*15
       character*80 slash
       integer*4 j,i      
       slash='/'
       do i=11,80
       if(keyword(i:i).EQ.slash) then
       j=i
       go to 10
       endif
       enddo
10     write(fin(1:15),'(A15)') keyword(j-15:j-1)
c      decode(15,100,fin) ival
       read(fin,'(i15)')ival
100   format(i15)
       return
       end
c-------------------------
       Subroutine remplitrw(keyword,ival)
       character keyword*80,fin*10
       character*80 slash
c       real*4 val      
       slash='/'
       do i=11,80
       if(keyword(i:i).EQ.slash) then
       j=i
       go to 10
       endif
       enddo
10       write(fin(1:10),'(A10)') keyword(j-10:j-1)
c       decode(10,200,fin) ival
       read(fin,'(f10.3)')ival
200   format(F10.3)
       return 
      end 
c--------------------------------------
      subroutine comptehead(iu,nb)
      integer nb
      character buf*2880
      do nb=1,10
      read(iu,rec=nb) buf(1:2880)
      i=1
      do n=1,36 
      if(buf(i:i+3).eq.'END ') go to 1000
      i=i+80
      enddo
      enddo
1000  return
      end
c----------------------------------                 
      subroutine moyenne(iu,inbhead,iswap,is,js,xtab,ijcam)
c                -------
      integer*4 is,js,nclim,t,n,uint   !,num,long
      integer*4 xtab(ijcam,ijcam)
      integer*2 sort(1440),kswap1(1),kswap2(1)

c      write(3,*)'call moyenne   is,js ',is,js

      call par1('    uint',1,uint)
       nclim=1

c lecture du fichier, additions et stockage dans xtab(ijcam,ijcam)
        n=inbhead
        i=0
        j=1
        k=1
100      n=n+1

        read(iu,rec=n,iostat=ios)  (sort(t),t=1,1440)
c        print *,'apres read n =',n
        if(ios.lt.0) go to 1000
        t=1
200     i=i+1
        if(i.le.is) go to 500
         if(j.lt.js) then
           j=j+1
           i=1
         else
            if(k.lt.nclim) then
               k=k+1
               j=1
               i=1
            else
               go to 1000
            endif
          endif

500    kswap1(1)=sort(t)

        if(iswap.ne.0)then
        call swap(kswap1,1,kswap2)
c            ----
c          if(t.eq.10)print*,' iswap, kswap1,kswap2',iswap,kswap1,kswap2
        kswap1(1)=kswap2(1)
        endif

        if(uint.eq.1)call sint(kswap1(1))
c                         ----

            xtab(i,j)=xtab(i,j)+ kswap1(1)
       t=t+1

       if (t.eq.1441) go to 100
       go to 200
1000     continue

          return
          end
c----------------------------------
      subroutine sint(k)
c                ----
      integer*2 k

      if(k.ge.0)then
        kpiv=k
      else
        kpiv1=k
        kpiv=kpiv1+65536
      endif

        k=kpiv/2
        return
      end
c----------------------------------                 
       subroutine swap(in,ncar,out)   
c                 ----
c       in = entree
c       out= sortie
c       ncar=nombre de integer*2 a swaper
c
      integer*2 in(1),out(1),auxi2
      logical*1 low,auxl1(2)
      equivalence (auxi2,auxl1(1))
c
      do i=1,ncar
      auxi2=in(i)
      low=auxl1(1)
      auxl1(1)=auxl1(2)
      auxl1(2)=low
      out(i)=auxi2
      enddo
      return
      end
c----------------------------------
      subroutine scan1(nob1,nob2,intert,ntmax,ftime,nfm,nsc,nfdeb)
c                ----
      dimension nfdeb(2000)
      integer*4 ftime(4000)
      write(3,*)'scan1: nob1,nob2,intert,ntmax', nob1,nob2,intert,ntmax

      ntm=ntmax
c      if(ntmax.eq.0.or.ntmax.gt.150)ntm=150
       if(ntmax.eq.0)ntm=10000
      inter=10000
      if(intert.ne.0)inter=intert

c      if(intert.eq.0)then
c        nsc=0
c      do nf=1,nfm,ntmax
c      nsc=nsc+1
c      nfdeb(nsc)=nf
c      enddo
c        write(3,*)' nombre de scans=',nsc
c        return
c      endif

      nfdeb(1)=max0(1,nob1)
      nsc=0  !1
      nf1=1  !2
      nf2=nfm
        if(nob1*nob2.ne.0)then
        nf1=nfdeb(1)   !+1
        nf2=min0(nob2,nfm)
        endif
      write(3,*)' ms1,scan1: nf1,nf2 ',nf1,nf2 

      call par1(' nobstep',1,nobstep)
c                            -------
        do nf=nf1,nf2,nobstep
c        interv=ftime(nf)-ftime(nf-1)
c          write(3,*)' ms1/scan1: nf,ftime(nf) ',nf,ftime(nf)
c        kpiv=nfdeb(nsc)+ntm-1
c          if(interv.gt.inter.or.nf.gt.kpiv)then
          nsc=nsc+1
          nfdeb(nsc)=nf
          write(3,*)'nsc,nfdeb, debut de scan ',
     1               nsc,nfdeb(nsc),ftime(nf)
c          endif
        enddo
      return

      end
c------------------------------------------------------------------------
      subroutine grid(fl,iitab,jjtab,nnfl,im,jm,nm,idebg,itgrig,x,y)
c                ----                      in      out          economie
      integer*2 fl(iitab,jjtab,nnfl)
      dimension x(2000),y(2000)
      character tit1*22,tit2*50
      integer dob,caldeb

      write(3,*)' im,jm,nm, iitab,jjtab,nnfl ',im,jm,nm,iitab,jjtab,nnfl

      jm2=jm/2

      call pgbegin(0,'grid.ps/ps',1,2)

      call par1('     dob',1,dob)
      call par1('    lbda',1,lbda)
      call par1('      li',1,li)
      call par1('  milsec',1,milsec)
        secmil=float(milsec)/1000.
      call par1('   mgrim',1,mgrim)
      call par1('    ideb',1,ideb)
      call par1('    igri',1,igri)
      call par1('   itgri',1,itgri)
      call par1('   itana',1,itana)
      call par1('  caldeb',1,caldeb)

      idebg=ideb

        if(ideb.eq.0)then
        idebg=(li-(mgrim-1)*itgri-igri-itana-1)/2
c                                               
        endif        
      itgrig=itgri
      write(3,*)' dans grid mgrim,itgri,igri,itana,idebg=',
     1                      mgrim,itgri,igri,itana,idebg

      do10 iter=1,2
c-------        1 = dessin seulement avec ideb de ms.par ou idebg standard
      if(iter.eq.2.and.caldeb.ne.0)then
c               2 seulement si calibration deb
        stepd=500.
        stept=500.
             nmax1=float(idebg)/stepd
             nmax2=0.25*float(itgri)/stepd
             ndm2=min0(nmax1,nmax2)
             ndm2=min0(ndm2,20)
          write(3,*) ' idebg,nmax1,nmax2,ndm2 ',idebg,nmax1,nmax2,ndm2
        ntm2=2
        if(caldeb.eq.1)ntm2=0
c                              caldeb=0 pas de calib
c                                     1        calib de ideb
c                                     2                 ideb et itgri
      call mindiff(fl,iitab,jjtab,nnfl,im,jm2,ndm2,ntm2,
c            -------                          in                  
     1             stepd,stept,idebg,itgrig,ndd,ntt)
c                  in                       out
        idebg=idebg+(ndd-ndm2-1)*stepd
        itgrig=itgrig+(ntt-ntm2-1)*stept

      write(3,*)' output mindiff  idebg=',idebg,' itgrig=',itgrig
      endif
c--------     fin iter=2
      call pgadvance
      call pgvport(0.1,0.9,0.1,0.9)
      tit1='         lbda=      '
      write(tit1(1:8),'(i8)')dob
      write(tit1(15:22),'(i6)')lbda
      tit2=' ideb=       igri=       itgri=       itana=      '
      write(tit2(7:12),'(i6)')idebg
      write(tit2(19:24),'(i6)')igri
      write(tit2(32:37),'(i6)')itgrig
      write(tit2(45:50),'(i6)')itana
      call pglabel('arcsec',' ',tit1//tit2)

      xl=float(im-1)*secmil
      yl=2.
c        print *,' secmil ',secmil
      call pgwindow(0.,xl,0.,yl)
      call pgsls(1)
      call pgbox('bcints',10.,10,'bcints',0.5,5)

        do i=1,im
        x(i)=(i-1)*secmil      
        y(i)=0.0001*fl(i,6,1)
        enddo
c           print *,' x ',(x(i),i=1,im,10)
c           print *,' y ',(y(i),i=1,im,10)
      call pgsls(1)
      call pgline(im,x,y)
c          ------
        do i=1,im
        y(i)=10.*y(i)
        enddo
      call pgline(im,x,y)
c          ------

        do i=1,im      
        y(i)=0.0001*fl(i,jm-5,1)
        enddo
      call pgsls(2)
      call pgline(im,x,y)
c          ------
        do i=1,im
        y(i)=10.*y(i)
        enddo
      call pgline(im,x,y)
c          ------
      call pgsls(1)


      call par1('  milsec',1,milsec)
         secmil=milsec/1000.
      call par1('   mgrim',1,mgrim)
      call par1('    ideb',1,ideb)
      call par1('    igri',1,igri)
      call par1('   itgri',1,itgri)
      call par1('   itana',1,itana)
      
      do m=1,mgrim
      x(1)=(idebg+(m-1)*itgrig)*0.001
      x(2)=x(1)
      x(3)=x(1)+igri*0.001
      x(4)=x(3)
      y(1)=0.
      y(2)=1.8
      y(3)=y(2)
      y(4)=0.
      call pgsls(1)
      call pgline(4,x,y)
c          ------
      do i=1,4
      x(i)=x(i)+itana*0.001
      enddo
      call pgsls(2)
      call pgline(4,x,y)
c          ------
      enddo
        if(caldeb.eq.0)goto12
10    continue
12    continue

      call pgend

      return
      end
c----------------------------------------------
      subroutine mindiff
c                -------
     1  (fl,iitab,jjtab,nnfl,im,jm2,ndm2,ntm2,stepd,stept,
c                                   in                    
     2   idebg,itgrig,ndd,ntt)
c                     out
c cherche les meilleures valeurs ndd et ntt
c pour minimiser les valeurs aux points-milieux
c entre les fenetres utiles

      integer*2 fl(iitab,jjtab,nnfl)
      double precision ssom
      dimension sd2(41,18)                ! 18 ?
c              (20*2+1) (5*2+1)
      ndm=2*ndm2+1
      ntm=2*ntm2+1

      call par1('  milsec',1,milsec)
         sec=1./float(milsec)
      call par1('   mgrim',1,mgrim)
c      call par1('    ideb',1,ideb)
      call par1('    igri',1,igri)
c      call par1('   itgri',1,itgri)
      call par1('   itana',1,itana)
      
         smin=1000000000.
      do 20 nt=1,ntm
      itgrix=itgrig+stept*(nt-ntm2-1)+0.01

        do 10 nd=1,ndm
        idebx=idebg+stepd*(nd-ndm2-1)+0.01

        ssom=0.
          do m=1,mgrim
          ia=(idebx+(m-1)*itgrix)*sec+1.5
             if(ia.lt.1)goto10
          ib=ia+igri*sec+0.5
          ic=ia+itana*sec+0.5
          id=ic+igri*sec+0.5
              if(id.gt.im)goto10
c              write(3,*)' m,ia,ib,ic,id ',m,ia,ib,ic,id
c            if(id.gt.im)then
c            print *,' id.gt.im dans mindiff'
c            print *,' verifier parametres grille'
c            stop
c            endif

c            do i=ia+1,ib
c            piv=fl(i,jm2,1)-fl(i-1,jm2,1)
c            ssom=ssom+piv*piv
c            enddo

c            do i=ic+1,id
c            piv=fl(i,jm2,1)-fl(i-1,jm2,1)
c            ssom=ssom+piv*piv
c            enddo

          idip1=0.5*float(ib+ic+1)
          idip2=0.5*(float(id+ia+1)+itgrix*sec)
          ssom=ssom+fl(idip1,jm2,1)+fl(idip2,jm2,1)

          enddo
               som=ssom
            smin=amin1(smin,som)
            if(smin.eq.som)then
               ndd=nd
               ntt=nt
            endif
            sd2(nd,nt)=som

        write(3,*)' itgri,ideb,sd2 ',itgrix,idebx,sd2(nd,nt)
        print *,  ' itgri,ideb,sd2 ',itgrix,idebx,sd2(nd,nt)

 10     continue  

        write(3,*)' '
        print *,' '
20      continue  

c      piv=sd2(1,1)
c      do nt=1,ntm
c      do nd=1,ndm
c        piv2=sd2(nd,nt)
c        piv=amin1(piv,piv2)
c        if(piv.eq.piv2)then
c            ndd=nd
c            ntt=nt
c        endif 
c      enddo
c      enddo


      return
      end
c--------------------------------------
      subroutine ssana(dxana,dyana,sana,kana)
c                -----
c  Met dans dxana(6) et dyana(6)
c  les valeurs correspondant au minimum de sana (paraboloide)

      double precision sana(6)
      dimension dxana(6),dyana(6)
      
      stepx=dxana(3)
      stepy=dyana(5)

      call minpara(sana(2),sana(1),sana(3),stepx,dxana(6))
      call minpara(sana(4),sana(1),sana(5),stepy,dyana(6))
c          -------
        if(dxana(6)-stepx.gt.0.)dxana(6)=stepx
        if(dxana(6)+stepx.lt.0.)dxana(6)=-stepx
        if(dyana(6)-stepy.gt.0.)dyana(6)=stepy
        if(dyana(6)+stepy.lt.0.)dyana(6)=-stepy

      if(kana.eq.6)then
      write(7,*)' sana ',sana
c      write(7,*)' dxana=',dxana(6),' dyana=',dyana(6)
      print *,   ' dxana=',dxana(6),' dyana=',dyana(6)
      endif

      return
      end
c--------------------------------------
      subroutine minpara(s1,s2,s3,step,p)
c                -------
      double precision s1,s2,s3

      x=0.

      d=s1+s3-2.*s2

      if(d.ne.0.)then
      p=step*(s1-s3)*0.5/d
        if(abs(x).le.2.*step)return
      endif

        p=0.
        print *,   ' No solution for improved cospatiality'  
        write(7,*)' No solution for improved cospatiality'      
      return      
      end
c----------------------------------------
      subroutine fail(nfail,jjtab,milsec)
c                ----
      dimension nfail(200),ly(200)
      call par1('     jy1',1,jy1)
      call par1('     jy2',1,jy2)
      pix=float(milsec)
      j1=1+float(jy1)/pix+0.5
      j2=1+float(jy2)/pix+0.5

      do j=j1,j2
      ly(j)=jy1+milsec*(j-j1)
      enddo

1     format(5('     jy  nfail'))
2     format(5(i7,i7))

      write(7,1)
      write(7,2)(ly(j),nfail(j),j=j1,j2,10)

      return
      end
c-----------------------------------------
      subroutine dark(nf1,nf2,ijcam,is,js,tab,xtab,dcn,sundec,
c                ----
     1                repert,iswap,name,tete,ipermu,nw)
c                                  out
      integer*4 xtab(ijcam,ijcam)
      integer*2 tab(ijcam,ijcam)
      integer tete(512),sundec
      character*22 name
      character*41 dcn(5000,2)
      character*80 repert

      print *,' call dark nf1,nf2, is,js,ipermu ',nf1,nf2,is,js,ipermu

       do i=1,ijcam
          do j=1,ijcam
           xtab(i,j)=0
           enddo
        enddo

          piv=float(nf2-nf1+1)
           
          do 45 n=nf1,nf2
             call openold41(dcn(n,nw),sundec,21,repert)
c                 ---------               --
c Calcul du nombre de blocs header : inbhead
             call comptehead(21,inbhead)

c Calcul de la moyenne 
          if(n.eq.nf1) print *,'call moyenne  is,js ',is,js

             if(ipermu.eq.0)then
                call moyenne(21,inbhead,iswap,is,js,xtab,ijcam)  
c                    -------
             else
                call moyenne(21,inbhead,iswap,js,is,xtab,ijcam)         
c                    -------
             endif
45    continue

      if(ipermu.eq.0)then 
         do i=1,is
            do j=1,js
               kpiv=xtab(i,j)
               tab(i,j)=0.5+float(kpiv)/piv
            enddo
         enddo
      else
         do i=1,is
            ip=is+1-i
            do j=1,js
               kpiv=xtab(j,i)
               tab(ip,j)=0.5+float(kpiv)/piv
            enddo
         enddo
      endif

c   ouverture du fichier oname ou xname               23   
        call system('rm '//name)

          print*,'  name ',name
        call opennew22(name,sundec,23)
c            ---------             --        

        tete(1)=3
        tete(2)=is
        tete(3)=js
        tete(4)=1
c
        write(23)tete
        do j=1,js
        write(23)(tab(i,j),i=1,is)  !!!! 30 -> 23
          if(j.eq.js/2)print*,' x: tab(1,is,10 , js/2)',
     1                           (tab(i,j),i=1,is,10)
       enddo

              is2=is/2
       write(3,*)' x: tab(is2,j),j=1,js,10 ',(tab(is2,j),j=1,js,10)
c
        close(unit=21)
        close(unit=23)

      return
      end
c--------------------------------------------------
      subroutine plot_c(cname,sundec,tete,x,y,c,iitab,jjtab,nntab)
c                ------              economie...
      dimension x(2000),y(2000)
      integer tete(512),sundec,ic(3),jc(2)
      integer*2 c(iitab,jjtab,nntab)     !   =tabeco
      character*22 cname
      character*28 titre

      iuc=23
      call openold22(cname,sundec,iuc)
c          ---------
      read(iuc)(tete(i),i=1,512)
      im=tete(2)
      jm=tete(3)
      nm=tete(4)

      ic(1)=11
      ic(2)=im/2
      ic(3)=im-10

      jc(1)=11
      jc(2)=jm-10

        cmax=0.
      do n=1,nm
      do j=1,jm
      read(iuc)(c(i,j,n),i=1,im)
        do i=1,im
        piv=c(i,j,n)
        cmax=amax1(piv,cmax)
        enddo
      enddo
      enddo
      write(3,*)' plot vw : cname,im,jm,nm,cmax ',cname,im,jm,nm,cmax
       cm=cmax*1.1

        call pgsch(1.5)
      do n=1,nm
c                     plot(i)
      call pgadvance
        call pgslw(1)
      write(titre(1:22),'(a)')cname
      write(titre(23:26),'(a4)')'  n='
      write(titre(27:28),'(i2)')n
      call pgvport(0.1,0.9,0.1,0.8)
      call pglabel(' i ',' ',titre)      

      call pgwindow(1.,20.,0.,cm)
      call pgbox('bcnts',5.,5,'bcnts',1000.,2)
        do jcn=1,2
        j=jc(jcn)
      call pgslw(jcn)
        do i=1,21
        x(i)=i
        y(i)=c(i,j,n)
        enddo
      call pgline(21,x,y)
c          ------
        do ip=1,20
        i=im-21+ip
        x(ip)=ip
        y(ip)=c(i,j,n)
        enddo
      call pgline(21,x,y)
c          ------
        enddo
c                      plot(j)
      call pgadvance
        call pgslw(1)
      write(titre(1:22),'(a)')cname
      write(titre(23:26),'(a4)')'  n='
      write(titre(27:28),'(i2)')n
      call pgvport(0.1,0.9,0.1,0.8)
      call pglabel(' j ',' ',titre)      

      call pgwindow(1.,float(jm),0.,cm)
      call pgbox('bcnts',5.,5,'bcnts',1000.,2)
        do icn=1,3
        i=ic(icn)
      call pgslw(icn)
        do j=1,jm
        x(j)=j
        y(j)=c(i,j,n)
        enddo
      call pgline(jm,x,y)
c          ------
        enddo
        call pgslw(1)

      enddo
      close(unit=iuc)
        call pgsch(1.)
      return
      end
c-------------------------------------
      subroutine dmd1(idr,select,nxm,ipos,raf,nq1,nq2,nquv,burst,
c                ---- 0/1
     1                dnanfp,nf1,nxfirst,nqexm,nqord,
     2                ttb,kz,sundec,dreject,maxttb,iitab)
c                     eco

      integer raf,burst,kz(512),select,! raf1,raf2,burst1,burst2
     1        nqord(9,2),sundec,dreject(60)
      integer*4 ttb(maxttb)              ! > 1024*170*60...
      integer*2 lec(2000)
      character*22 dnanfp(10000,2),dname,ddname

      if(select.ge.0)return

c      raf1=1
c      raf2=1
c      burst1=1
c      burst2=1

c moyennes
      if(select.eq.-1)then
c        ------------
        write(3,*)' dmd:nf1,burst,nquv,raf,ipos,nxm ',
     1                  nf1,burst,nquv,raf,ipos,nxm
      do nx=1,nxm
      do ny=1,ipos
         nnr=1
      do nnq=nq1,nq2
      do nqex=1,nqexm
         nq=nqord(nnq,nqex)
         nnb=1

      if(nxfirst.eq.0)then
      nf=nf1+nnb-1+burst*(nq-1+nquv*(nnr-1+raf*(ny-1+ipos*(nx-1))))
      else
      nf=nf1+nnb-1+burst*(nq-1+nquv*(nnr-1+raf*(nx-1+nxm*(ny-1))))
      endif
c  premier ancien
      dname=dnanfp(nf,1)
        write(3,*)' dmd1: nx,ny,nq,nf1,nf ',nx,ny,nq,nf1,nf

      call openold22(dname,sundec,50)
c          ---------
      read(50)kz
      im=kz(2)
      jm=kz(3)
      lm=kz(4)
      rewind(50)
      close(unit=50)

      ijlm=im*jm*lm
      ijrbm=im*jm*raf*burst

      if(ijlm.gt.maxttb.or.ijrbm.gt.maxttb)then
        print *,' too small dimension in dmd1:  increase pixel size',
     1          ' to go on'
        stop
      endif

c  nouveau
      ddname=dname
      write(ddname(18:19),'(a2)')'-1'
        if(idr.eq.1)write(ddname(1:1),'(a1)')'r'
        write(3,*)' dmd1: ddname= ',ddname

      call system('rm '//ddname)
      call opennew22(ddname,sundec,51)
c          ---------

      write(51)kz

         nrbm=raf*burst
      do nrb=1,nrbm
      do j=1,jm
        jl=im*(j-1+jm*(nrb-1))
      do i=1,im
        ijl=i+jl
        ttb(ijl)=0
      enddo
      enddo
      enddo
c------------------------------------------  boucle ll
        nrbm=raf*burst
5     format('  ms1/dmd1: nx,ny,nq, number of bad points:',3i4,i12)
        nbad=0
        ntestbad=0

      do ll=1,lm
        print *,' dmd1: nx,ny,nnq,nqex,ll ',nx,ny,nnq,nqex,ll
      do nr=1,raf    !  boucle raf
      do nb=1,burst  !  boucle burst

        if(raf.eq.1)then
          nrb=nb
        else
          nrb=nr
        endif

      if(nxfirst.eq.0)then
      nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
      else
      nf=nf1+nb-1+burst*(nq-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
      endif

      dname=dnanfp(nf,1)
      call openold22(dname,sundec,50)
c          ---------
      read(50)kz
      do10 l=1,ll      !  on lit jusqu'a ll
      do j=1,jm
        read(50)(lec(i),i=1,im)
        if(l.eq.ll)then
          jl=im*(j-1+jm*(nrb-1))
c                        ---

          do i=1,im
          ijl=i+jl
          ttb(ijl)=lec(i)
      if(i.eq.10.and.j.eq.10.and.nrb.eq.raf*burst)
     1 write(3,*)' dmd: nrb,i,j,ijl,ll,lec,ttb ',
     2                  nrb,i,j,ijl,ll,lec(i),ttb(ijl)
          enddo
      
        endif        
      enddo
10    continue
        close(unit=50)

      enddo  !  burst
      enddo  !  raf
c---
      if(dreject(ll).ne.0)then

15    format(' ms1/dmd1  calcul sreject pour nx,ny,nq,ll ',4i4)
      print 15,nx,ny,nq,ll
      call sreject(ttb,im,jm,nrbm,ll,dreject,nbad,ntestbad,lec,maxttb,
c          -------
     1             iitab)
      goto20
      endif
c---
        den=float(nrbm)

      do j=1,jm
          do i=1,im
          ij=i+im*(j-1)
        
               s=0.
             do nrb=1,nrbm
             ijl=ij+im*jm*(nrb-1)
             s=s+ttb(ijl)
             enddo
               piv=s/den
               if(piv.ge.0)then
               lec(i)=s/den+0.5
               else
               lec(i)=s/den-0.5
               endif
          enddo                               ! i
        write(51)(lec(i),i=1,im)
c        if(ll*j.eq.1)write(3,*)' dmd: lec: ',(lec(i),i=1,4)
      enddo                                   ! j
20    continue

      enddo
c----                                         fin boucle ll
        if(ntestbad.eq.1)then
            print   5,nx,ny,nq,nbad
            write(7,5)nx,ny,nq,nbad
        endif
        close(unit=51)

      enddo    !  nqex
      enddo    !  nnq
      enddo    !  ipos
      enddo    !  nxm

      endif
      return
      end
c-------------------------------------------------
      subroutine echan_modul(iqp,select,nqord,nqexm,
c                -----------
     1        nq1,nq2,raf1,raf2,burst1,burst2,
c             in  in   
     2        nxm,ipos,raf,nquv,burst,
     3        dnanfp,nf1,nxfirst,etab1,etab2,kz,sundec,iitab,jjtab)
c                                eco

c  Beam-exchange for modulator (Pic du Midi)

      integer raf1,raf2,burst1,burst2,raf,burst,nqord(9,2),
     1        kz(512),kz2(512),dlb(32),sundec,select
      integer*2 etab1(iitab,jjtab),etab2(iitab,jjtab)
      character*22 dnanfp(10000,2),dname1,dname2,ddname

      print *,' Echange de faisceaux avec modulateur' 

      if(iqp.eq.0)then
      call par1('  ratiod',1,itaux)
      else
      call par1('  ratior',1,itaux)
      endif

      call par1('  invers',1,invers)
      isigne=1-2*invers
c      isigne=1

      do nx=1,nxm
      do ny=1,ipos
      do nr=raf1,raf2
      do nb=burst1,burst2

c boucle des polars
      do nnq=nq1,nq2

c nom 1ere polar pour nom fichier
      nnq1=nqord(nnq,1)    ! ordre observations

      if(nxfirst.eq.0)then
      nf=nf1+nb-1+burst*(nnq1-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
      else
      nf=nf1+nb-1+burst*(nnq1-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
      endif
       write(3,*)
     1' echan_modul:iqp,nx,ny,nr,nb,nf1,burst,nnq1,nquv,raf,ipos,nxm'
       write(3,*)
     1              iqp,nx,ny,nr,nb,nf1,burst,nnq1,nquv,raf,ipos,nxm

c  premier ancien
        dname1=dnanfp(nf,1)
        
      if(select.lt.0)write(dname1(18:19),'(i2)')select  !  select

      ddname=dname1
      write(ddname(22:22),'(i1)')nnq
                                 
        write(3,*)' echan_modul: nx,ny,nr,nb,nnq1,nf1,nf ',
     1                           nx,ny,nr,nb,nnq1,nf1,nf

c nom 2eme polar
      nnq2=nqord(nnq,2)   !  ordre observations

      if(nxfirst.eq.0)then
      nf=nf1+nb-1+burst*(nnq2-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
      else
      nf=nf1+nb-1+burst*(nnq2-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
      endif
c  second ancien
      dname2=dnanfp(nf,1)
      if(select.lt.0)write(dname2(18:19),'(i2)')select  !  select

      write(3,*)' echan_modul: dname1,dname2,ddname '
      write(3,*)               dname1,dname2,ddname

      call openold22(dname1,sundec,50)
c          ---------
      call openold22(dname2,sundec,51)
c          ---------

      call system('rm '//ddname)
      call opennew22(ddname,sundec,52)
c          ---------
c------------------------  header ddname
      read(51)kz
      read(50)kz

c forme des headers d:
c30    format(/' Header fichier d')
c      write(iud)nd1,ims,jms,lpiv,npo,
c     1          ndd2,day,time,iline,lbda,ncm,ltrj,(dlbb(l),l=1,lpiv),
c                                           ----
c     2          center,lsum,lmp,lbd1,lbdstep,mgrim,
c     3          (kz(i),i=izz1,512) !!!!!!!!!!!!!!!

      im=kz(2)
      jm=kz(3)
      lm=kz(4)

      do l=1,12
      kz2(l)=kz(l)
      enddo

      llm=2*lm
      kz2(4)=llm    !   on double le nombre de dlambdas

        do l=1,lm
        dlb(l)=kz(l+12)
        enddo

        do l=1,lm
        l1=2*l-1
        l2=l1+1
        kz2(l1+12)=dlb(l)
        kz2(l1+13)=dlb(l)
        enddo  

        l1=13+llm
      do lp=l1,512
        l=lp-lm
      kz2(lp)=kz(l)
      enddo

      write(3,*)' header ddname/rrname: ',(kz2(n),n=1,40)
      write(52)kz2
c--------
      do l=1,lm

        do j=1,jm
        read(50)(etab1(i,j),i=1,im)
        read(51)(etab2(i,j),i=1,im)

          do i=1,im
          tt1=etab1(i,j)
          tt2=etab2(i,j)

          etab1(i,j)=0.5*(tt1+tt2)
            if(itaux.eq.0)then
            kt2=0.5*(tt1-tt2)*isigne
            else
              if(tt1+tt2.gt.(2.*itaux))then
              kt2=isigne*10000.*(tt1-tt2)/(tt1+tt2)
              else
              kt2=0
              endif
            endif
          etab2(i,j)=kt2

          enddo

        enddo

        do j=1,jm
        write(52)(etab1(i,j),i=1,im)
        enddo

        do j=1,jm
        write(52)(etab2(i,j),i=1,im)
        enddo

      enddo
      close(50)
      close(51)
      close(52)

      enddo
      enddo
      enddo
      enddo
      enddo

      return
      end
c---------------------------------------------------------------
      subroutine echan_grille(iqp,select,nqord,
c                ------------
     1        nq1,nq2,raf1,raf2,burst1,burst2,
c             in  in  
     2        nxm,ipos,raf,nquv,burst,
     3        dnanfp,nf1,nxfirst,etab1,etab2,kz,sundec,iitab,jjtab)
c                                eco

c  Beam-exchange for grid observations (Themis)

      integer raf1,raf2,burst1,burst2,raf,burst,nqord(9,2),
     1        kz(512),sundec,select
      integer*2 etab1(iitab,jjtab),etab2(iitab,jjtab)
      character*22 dnanfp(10000,2),dname1,dname2,ddname

      print *,' Composite des echanges de faisceaux avec grille'

        nr1=raf1
        nr2=raf2
        if(select.eq.-1)nr2=nr1

        nb1=burst1
        nb2=burst2
        if(select.eq.-1)nb2=nb1

      do nx=1,nxm
      do ny=1,ipos
      do nr=nr1,nr2
      do nb=nb1,nb2
c boucle des polars
      do nnq=nq1,nq2

c nom 1ere polar pour nom fichier
      nnq1=nqord(nnq,1)  !  ordre observationa

      if(nxfirst.eq.0)then
      nf=nf1+nb-1+burst*(nnq1-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
      else
      nf=nf1+nb-1+burst*(nnq1-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
      endif
c  premier ancien
      dname1=dnanfp(nf,1)
      if(select.lt.0)write(dname1(18:19),'(i2)')select  !  select
        nns=1
c      write(dname1(21:21),'(i1)')nns

      ddname=dname1
      write(ddname(22:22),'(i1)')nnq
        write(3,*)' echan_grille: nx,ny,nr,nb,nnq,nnq1,nf1,nf ',
     1                            nx,ny,nr,nb,nnq,nnq1,nf1,nf

c nom 2eme polar
      nnq2=nqord(nnq,2)

      if(nxfirst.eq.0)then
      nf=nf1+nb-1+burst*(nnq2-1+nquv*(nr-1+raf*(ny-1+ipos*(nx-1))))
      else
      nf=nf1+nb-1+burst*(nnq2-1+nquv*(nr-1+raf*(nx-1+nxm*(ny-1))))
      endif
c  second ancien
      dname2=dnanfp(nf,1)
      if(select.lt.0)write(dname2(18:19),'(i2)')select  !  select
c      write(dname2(21:21),'(i1)')nns

      write(3,*)' echan_grille: dname1,dname2,ddname '
      write(3,*)                dname1,dname2,ddname

      call openold22(dname1,sundec,50)
c          ---------
      call openold22(dname2,sundec,51)
c          ---------

      call system('rm '//ddname)
      call opennew22(ddname,sundec,52)
c          ---------
      read(50)kz
      im=kz(2)
      jm=kz(3)
      lm=kz(4)
      read(51)kz

      write(52)kz

      do l=1,lm

        do j=1,jm
        read(50)(etab1(i,j),i=1,im)
        read(51)(etab2(i,j),i=1,im)
          do i=1,im
          kpiv1=etab1(i,j)
          kpiv2=etab2(i,j)
          etab1(i,j)=0.5*(kpiv1+kpiv2)
c          etab2(i,j)=0.5*(kpiv1-kpiv2)
          enddo
        enddo

        do j=1,jm
        write(52)(etab1(i,j),i=1,im)
        enddo

c        do j=1,jm
c        write(52)(etab2(i,j),i=1,im)
c        enddo

      enddo

      close(50)
      close(51)
      close(52)

      enddo
      enddo
      enddo
      enddo
      enddo

      return
      end
c--------------------------------------------------------------
      subroutine sreject(ttb,im,jm,nrbm,ll,dreject,nbad,ntestbad,lec,
c                -------
     1                   maxttb,iitab)
c     Pour chaque i,j le programme examine tous les bursts
c     Chaque pixel dont l'ecart a la moyenne est > dreject*sigma/1000
c     est supprime dans le calcul de moyenne.

      integer*4 ttb(maxttb),tb(100),dreject(60)
      integer*2 lec(2000)
      dimension p(100)

        xsig=float(dreject(ll))/1000.
c                  -------
c      xsig=1.

c      print *,' sreject: im,jm,nrbm,ll,dreject(ll) ',
c     1                   im,jm,nrbm,ll,dreject(ll)

      do j=1,jm
      do i=1,im
         ij=i+im*(j-1)
c------
        do nrb=1,nrbm
           ijl=ij+im*jm*(nrb-1)
           tb(nrb)=ttb(ijl)
        enddo
c             if(i*j.eq.1)print *,' tb= ',(tb(nrb),nrb=1,nrbm)

          do nrb=1,nrbm
          p(nrb)=1.
          enddo
                     iterm=3
      do20 iter=1,iterm
c moyenne et rms
          smoy=0.
          poids=0.
        do nrb=1,nrbm
          smoy=smoy+tb(nrb)*p(nrb)
          poids=poids+p(nrb)
        enddo
          if(poids.eq.0.)goto25
          smoy=smoy/poids

          sig=0.
        do nrb=1,nrbm
          sig=sig+p(nrb)*(tb(nrb)-smoy)**2
        enddo
          sig=sig/poids
          sig=sqrt(sig)
           
c ecarts
            s=0.
            poids=0.
            test=sig*xsig

        do10 nrb=1,nrbm
           if(p(nrb).ne.0.)then
                kpiv=tb(nrb)
                piv=kpiv-smoy
             if(abs(piv).le.test)then

                if(iter.eq.iterm)then
                  s=s+tb(nrb)
                  poids=poids+1.
                endif

             else
                nbad=nbad+1
                ntestbad=1
                p(nrb)=0.
             endif
           endif
10      continue

c moyenne
20    continue           !  boucle iter

25      if(poids.eq.0.)then
           print *,' no acceptable exposure in a full burst:',
     1             ' increase dreject (or rreject)'
           stop
        endif

        s=s/poids
             
               if(s.ge.0)then
               lec(i)=s+0.5
               else
               lec(i)=s-0.5
               endif
c----
      enddo                          !  boucle i
        write(51)(lec(i),i=1,im)    
      enddo                          !  boucle j
      return
      end
c---------------------------------------
      subroutine inter3(a,b,c,x,afluc,bfluc,cfluc)
c                ------
c      a=fluc(2)     !  -1
c      b=fluc(1)     !   0
c      c=fluc(3)     !  +1
      
      d=a+c-2.*b
      x=0.
        if(d.eq.0.)then
        afluc=0.
        bfluc=0.5*(c-a)
        cfluc=b
c          write(7,*)' impossible interpolation fluctuations fl'

        else
          x=0.5*(a-c)/d
          afluc=0.5*(a+c-2.*b)
          bfluc=0.5*(c-a)
          cfluc=b
        endif

      return
      end
c-----------------------------------------
      subroutine interflu(iterfluc,fluc,jt100,deljt,delja,deljb)
c                -------- in                  out
      dimension fluc(10,3),xx(10),
     1          afluc(10),bfluc(10),cfluc(10)

      delu=0.15
      delmax=0.30

      goto(1,2,3,4,5,6,7,8,9,10),iterfluc
c--
1     deljt=-delu*jt100
      return

2     deljt=-deljt
      return

3     piva=fluc(2,2)  !   -delu, 2eme bande
      pivb=fluc(1,2)  !       0, 2eme bande
      pivc=fluc(3,2)  !    delu, 2eme bande

      call inter3(piva,pivb,pivc,xpiv,
c          ------ in             out
     1          afluc(iterfluc),bfluc(iterfluc),cfluc(iterfluc))
c               out
       if(xpiv.gt.abs(delmax))xpiv=0.
      xx(iterfluc)=xpiv
      deljt=xpiv*deljt
        write(7,*)' xx= ',xpiv 
        write(7,*)' new deljt=',deljt

c             deljt a ete calcule
      return
c--
4     delja=-delu*jt100
        write(7,*)' jt100,delja ',jt100,delja
      return

5     delja=-delja
      return

6     piva=fluc(5,1)  !   -delu, 1eme bande
      pivb=fluc(4,1)  !       0, 1eme bande
      pivc=fluc(6,1)  !    delu, 1eme bande

      call inter3(piva,pivb,pivc,xpiv,
c          ------ in             out
     1          afluc(iterfluc),bfluc(iterfluc),cfluc(iterfluc))
       if(xpiv.gt.abs(delmax))xpiv=0.
      xx(iterfluc)=xpiv
      delja=xpiv*delja
        write(7,*)' xx= ',xpiv 
        write(7,*)' new delja=',delja
      return
c--
7     deljb=-delu*jt100
        write(7,*)' jt100,deljb ',jt100,deljb
      return

8     deljb=-deljb
      return

9     piva=fluc(8,3)  !   -delu, 3eme bande
      pivb=fluc(7,3)  !       0, 3eme bande
      pivc=fluc(9,3)  !    delu, 3eme bande

      call inter3(piva,pivb,pivc,xpiv,
c          ------ in             out
     1          afluc(iterfluc),bfluc(iterfluc),cfluc(iterfluc))
       if(xpiv.gt.abs(delmax))xpiv=0.
      xx(iterfluc)=xpiv
      deljb=xpiv*deljb
        write(7,*)' xx= ',xpiv 
        write(7,*)' new deljb=',deljb
      return
c--
10    call desflu(1,2,3,10,2,fluc,xx,delu,delmax,
c          ------    
     1            afluc,bfluc,cfluc)
      call desflu(4,5,6,10,1,fluc,xx,delu,delmax,
c          ------    
     1            afluc,bfluc,cfluc)
      call desflu(7,8,9,10,3,fluc,xx,delu,delmax,
c          ------    
     1            afluc,bfluc,cfluc)
      return
      end
c-----------------------------------------------------     
      subroutine desflu(it1,it2,it3,it4,ibd,fluc,xx,delu,delmax,
c                ------
     1                  afluc,bfluc,cfluc)
      dimension fluc(10,3),xx(10),afluc(10),bfluc(10),cfluc(10),
     1          x(21),y(21)
      character*28 titre

      call pgadvance
      call pgsch(2.)
      write(titre(1:28),'(a)')
     1' H   interpol. fluct. fl    '
      call pgvport(0.1,0.9,0.1,0.7)

         ymax=0.30
      call pgwindow(-delmax,delmax,0.,ymax)
      call pgbox('bcints',0.1,10,'bcints',0.1,10)
      call pglabel('var. rel. (unit=jt1000)',' ',titre)

        x(1)=-delu
         y(1)=fluc(it2,ibd)
        x(2)=0.
         y(2)=fluc(it1,ibd)
        x(3)=delu
         y(3)=fluc(it3,ibd)
      call pgpoint(3,x,y,24)

      aa=afluc(it3)
      bb=bfluc(it3)
      cc=cfluc(it3)
      write(7,*)' parab fluc ',aa,bb,cc

      do i=1,21
      x(i)=(delmax/10.)*float(i-11)
      xpiv=x(i)/delu
      y(i)=cc+xpiv*(bb+aa*xpiv)
      enddo
      call pgline(21,x,y)

      call pgsls(2)
        x(1)=xx(it3)*delu
        x(2)=x(1)
        y(1)=0.
        y(2)=ymax
      call pgline(2,x,y)
      call pgsls(1)

        y(1)=fluc(it4,ibd)
      call pgpoint(1,x,y,12)

      return
      end
c-------------------------------------------------------------
      subroutine unnostok(rnanfp,nf1,nf2,nr1,nr2,nb1,nb2,nxm,milsec,
c                --------            
     1          stok,kstok,istok,lbdstok,stokname)
c                        6   600      29
c ce subroutine prepare les noms de fichiers pour sunnodat

      integer*2 stok(kstok,istok,lbdstok)
      integer*4 stokord(3),select,sundec
      character*22 rnanfp(10000,2),pnameq(3),rname,pname
      character*23 stokname

      call par1(' unnoord',1,kunnoord)
      stokord(1)=kunnoord/100
      stokord(2)=kunnoord/10 -10*(kunnoord/100)
      stokord(3)=kunnoord    -10*(kunnoord/10)
      write(3,*)' stokord ',stokord

      call par1('  select',1,select)
      call par1('  sundec',1,sundec)
      do nr=nr1,nr2
      do nb=nb1,nb2

c------------ nom des 3 fichiers p*  Q,U,V
c                                    8,9,7
      do nnq=1,3
        nq=stokord(nnq)
        write(3,*)' unno: nr,nnq, nq ',nr,nnq,nq

c             nfaa=nf1+nb-1+burst*(nq-1+nquv*(nr-1))
c             ----
               pname=rnanfp(nf1,1)
               write(pname(1:1),'(a)')'p'

          write(pname(18:19),'(i2)')select  !  select
          if(select.ge.0.and.select.le.9) write(pname(18:18),'(a1)')'0'
          write(pname(22:22),'(i1)')nq  !  beam-exchge
c                                  7,8,9
          pnameq(nnq)=pname
          rname=rnanfp(nf2,1)
      enddo
c------

c  nom de stokname (=stokes.dat)
                  write(stokname(2:23),'(a)')pname
                  write(stokname(23:23),'(a)')'0'
                  write(stokname(1:2),'(a)')'st'
c                  write(stokname(24:27),'(a)')'.dat'
        print *,' stokname ',stokname
        write(3,*)' stokname:',stokname

      call sunnodat(pnameq,stokname,stokord,
c          --------
     1              stok,kstok,istok,lbdstok)
c                            6   600      29)

        enddo  ! nb
        enddo  ! nr
      return
      end
c----------------------------------------------------------------
      subroutine sunnodat(name,stokname,stokord,
c                --------
     1                    st,kstok,istok,lbdstok)
c                                6   600      29 
c ce programme tient compte du facteur 10 (cstok) introduit dans Q,U,V
c par ms2
c il moyenne les I des 6 fichiers +Q,-Q,+U,-U,+V,-V
c lus dans l'ordre stokord (d'apres unnoord)

      double precision buf(4)
      integer*4 header(512),cstok,stokord(3)
      dimension prof(50,4),signe(3),xplot(50),yplot(50) ! kst(25)
      integer*2 st(kstok,istok,lbdstok),lecart(1000,1000),rec(2000)
      character*35 tit
c                      k     i                    i    j 
c                      6   400      29
c     k: iQ, iU, iV, Q, U, V                  ecart lbda centrage
c     prog unnofit: integer
c                   st(4,101, 1001,400)
c                     st lbda   X   Y
c                               j   i
      character*22 name(3) ! ,rname
      character*23 stokname

      call system('rm unnodat.lis')
      open(unit=5,file='unnodat.lis',status='new')

      call pgbegin(0,'st.ps/ps',4,4)
      do i=1,50
      xplot(i)=i
      enddo
      write(tit(1:23),'(a)')stokname
      write(tit(24:25),'(a)')'  '
      call pgsch(2.)

 1    format(a)
 2    format(8i8)
 3    format(8f8.1)
      do j=1,3
      write(5,1)name(j)
      enddo

      call par1('   cstok',1,cstok)    ! coef des Q,U,V dans P
      call par1(' newmils',1,newmils)  ! pixel sortie en millisec
      call par1('  lbdmax',1,lbdmax)   ! dans P (input)
      call par1('  lbddat',1,lbddat)   ! dans Stokes (output)
      call par1('  milsec',1,milsec)   ! pixel d'entree
      call par1('  centre',1,kcentre)  ! pour centrer raie en sortie
      call par1(' unnosym',1,ksym)     ! symetrisation profil I
      call par1(' milande',1,milande)  ! 1000*glande
      call par1(' itermax',1,itermax)  ! max iterations unnofit
      call par1('  chimax',1,kchimax)  ! max valeur de chi
        chimax=kchimax
      call par1('    ist1',1,ist1)     ! premier indice i dans tableau p
      call par1('    ist2',1,ist2)     ! dernier indice j dans tableau p
      call par1('    jst1',1,jst1)     ! premier indice i dans tableau p
      call par1('    jst2',1,jst2)     ! dernier indice j dans tableau p
      call par1('   ipdes',1,ipdes)    ! indice ip dessin si 1 plot
      call par1('   jpdes',1,jpdes)    ! indice jp dessin si 1 plot

      call par1('     wu1',1,kpiv)
        w1=float(kpiv)/1000.
      call par1('     wu2',1,kpiv)
        w2=float(kpiv)/1000.
      call par1('     wu3',1,kpiv)
        w3=float(kpiv)/1000.
      call par1('     wu4',1,kpiv)
        w4=float(kpiv)/1000.
      call par1('   accel',1,ipasaccel)
      call par1('   qumax',1,kpiv)
        qumax=float(kpiv)/100.
      call par1('   vvmax',1,kpiv)
        vvmax=float(kpiv)/100.
      call par1(' nplotst',1,nplotst)

      if(lbddat.gt.lbdmax)then
      print *,' lbddat.gt.lbdmax'
      write(5,*)' lbddat.gt.lbdmax'
      stop
      endif
     
      ismax=istok
      jsmax=jstok

      call system('rm '//stokname)
      open(unit=2,file=stokname,form='formatted',status='new')

        pixsize=float(newmils)*0.001
        id=float(newmils)/float(milsec)+0.5
        jd=id
      write(5,*)' id=jd=',id

                lbd0=(lbdmax+1)/2       ! lbda central pour I

                 lbdI=lbd0               ! liste
                 lbdQ=lbd0
                 lbdU=lbd0
                 lbdV=lbd0-4
      write(5,*)' lbd  I,Q,U,V ',lbdI,lbdQ,lbdU,lbdV

c Lecture headers
      do k=1,3
      ku=10+k
      open(unit=ku,status='old',form='unformatted',file=name(k))
c                                                      -------
      read(ku)header

        if(k.eq.1)then     !  header premier fichier
        im=header(2)
        jm=header(3)
        else
        ipiv=header(2)
        jpiv=header(3)
           if(ipiv.ne.im.or.jpiv.ne.jm)then
           print *,'Dimensions differentes entre tableaux'
           stop
           endif

8     format('      im      jm  naxis3')
      write(5,8)
      write(5,2)im,jm,header(4)

        ipm=im/id
        jpm=jm/jd    
      if(ist1.ne.0)then
      ipm=(ist2-ist1+1)/id
      jpm=(jst2-jst1+1)/jd
      else
      ist1=1
      ist2=im
      jst1=1
      jst2=jm
      endif
                   ipm2=ipm/2
                   jpm2=jpm/2
                   ijpas=ipm/10+1
10    format('      id      jd')
      write(5,10)
      write(5,2)id,jd
11    format('    ipm     jpm   lbdmax  lbddat')
      write(5,11)
      write(5,2)ipm,jpm,lbdmax,lbddat
        if(ipm.gt.ismax)then
        print *,' newmils too small: ipm,id,ismax ',
     1            ipm,id,ismax
        stop
        endif
      endif    !  k=1
      enddo    !  k      les 3 headers sont lus

c--------------------
c     ecriture tete de st*
        write(3,*)' Ecriture ',stokname
        write(5,*)' Ecriture ',stokname

      call par1('    lbda',1,lbda)
      call par1(' mdlbdst',1,kpiv)

        xlbda=lbda
        dlbdst=float(kpiv)/1000.   !   dlambda stokes mA
        glande=0.001*float(milande)   
c     
      write(2,*)stokname
      write(5,*)stokname

      write(2,*)xlbda,glande,dlbdst,itermax,chimax,w1,w2,w3,w4,
     1          ipasaccel
      write(5,*)xlbda,glande,dlbdst,itermax,chimax,w1,w2,w3,w4
     1          ipasaccel
      write(2,*)ipm,jpm,lbddat,1,1,0,0
      write(5,*)ipm,jpm,lbddat,1,1,0,0
        piv=float(milsec)*float(id)*0.001
      write(2,*)piv
      write(5,*)piv

                  lbd1=(lbdmax-lbddat)/2+1
c                    3      25     21
c--------------
c      fichiers P  intermediaires               st       stokname  prof
c
c        im        ipm (de ist1 a ist2/id)     3*2        I,Q,U,V    4    
c        jm          3 (k)                     ipm         lbdmax  lbdmax(>lbddat)
c      lbdmax      jpm (de jst1 a jst2/jd)    lbdmax       ipm
c      2 (iq)     -------------------         -------      jpm
c      ------      lbdmax                    chaque jp
c        3 (k)       2 (IQ) 

c ecriture de fichiers intermediaires pour stokes,lbda 
c                                            iq   lbdmax
c                                             2    29
      do iq=1,2
      do l=1,lbdmax   ! lecture complete de P
        liqu=20+l+lbdmax*(iq-1)
          kpiv=2*lbdmax+20
        print*,' stokes: fichier intermediaire ',liqu,' (',kpiv,')'
        open(unit=liqu,form='unformatted')
        rewind(liqu)

        do j=1,jm
        jp=0
        jj1=j-jst1
        if(j.ge.jst1.and.j.le.jst2.and.jj1-jd*(jj1/jd).eq.0)
     1  jp=jj1/jd+1

        do k=1,3
          ku=10+k          
          read(ku)(rec(i),i=1,im)
          if(jp.ne.0)write(liqu)(rec(i),i=ist1,ist2,id)
c                    ----------
c        if(iq.eq.2.and.j.eq.jst1)write(3,*)
c     1  ' iq,l,j,k,rec(ist1),ku,liqu ',iq,l,j,k,rec(ist1),ku,liqu    
        enddo
        enddo
          rewind(unit=liqu)
          
      enddo
      enddo
        do k=1,3
        ku=10+k
        close(unit=ku)
        enddo
c                      les fichiers liq ont pour dimension ipm*3*jpm
c--------------------------------------------
c                                                   Boucle jp
          signe(1)=1.
          signe(2)=1.
          signe(3)=1.
        call par1('  signeq',1,kpiv)
             if(kpiv.ne.0)signe(1)=kpiv
        call par1('  signeu',1,kpiv)
             if(kpiv.ne.0)signe(2)=kpiv
        call par1('  signev',1,kpiv)
             if(kpiv.ne.0)signe(3)=kpiv

      do jp=1,jpm
c---   chargemant de st(k,i,lbd)
         iq=1            !  profils I

      do lp=1,lbdmax     
        liqu=20+lp+lbdmax*(iq-1)
c       ----
      do k=1,3
      read(liqu)(rec(ip),ip=1,ipm)

          do ip=1,ipm
          st(k,ip,lp)=rec(ip)
c            -
          enddo
      enddo
      enddo

         iq=2             !  profils Q,U,V
      do lp=1,lbdmax
        liqu=20+lp+lbdmax*(iq-1)

      do k=1,3
      read(liqu)(rec(ip),ip=1,ipm)
     
          do ip=1,ipm
c                  signe THEMIS (+ permutation Q et U dans donnees)
c          if(k.ne.3)rec(ip)=-rec(ip)
            piv=rec(ip)
          st(k+3,ip,lp)=piv*signe(k)
c            ---
          enddo
      enddo
      enddo


      if(jp.eq.jpm2)then
      do k=1,3
      if(k.eq.1)write(5,*)' Profils I/cstok,Q au point ipm/2,jpm/2'
      if(k.eq.2)write(5,*)' Profils I/cstok,U au point ipm/2,jpm/2'
      if(k.eq.3)write(5,*)' Profils I/cstok,V au point ipm/2,jpm/2'
      write(5,2)(st(k,ipm2,lbd),lbd=1,lbdmax)
      write(5,2)(st(k+3,ipm2,lbd),lbd=1,lbdmax)
      enddo
      endif

c--------------------
                  lbddeb=lbd1
      do ip=1,ipm                                   ! boucle ip /jp
            iprint=0
            if(ip.eq.ipm2.and.jp.eq.jpm2)iprint=1
            lecart(i,j)=0

      do l=1,lbdmax
           kpiv=0
        do k=1,3
        kpiv=kpiv+st(k,ip,l)
        enddo
           piv=kpiv
           piv=piv*float(cstok)/3.
              prof(l,1)=piv
              prof(l,2)=st(4,ip,l)
              prof(l,3)=st(5,ip,l)
              prof(l,4)=st(6,ip,l)
      enddo

      kcentre=0
c      ksym=0
c                      centrage
      if(kcentre.ge.1)then
          pmin=100000.
        do l=1,lbdmax
          piv=prof(l,1)
        pmin=amin1(pmin,prof(l,1))
        if(piv.eq.pmin)lmin=l
        enddo
           lbddeb=lmin-lbddat/2      !  changement de lbd1 en lbddeb
c          ------
           lbdfin=lbddeb+lbddat-1
              if(lbddeb.lt.1.or.lbdfin.gt.lbdmax)lbddeb=lbd1
              lecart(i,j)=lbddeb-lbd1
      endif
c                     egalisation des continus
      if(ksym.eq.-1)then
        if(iprint.eq.1)write(3,*)' egal ',(prof(l,1),l=1,lbdmax)
      ecart=prof(lbdmax,1)-prof(1,1)
      ec2=ecart/2.
      eps=ecart/float(lbdmax-1)
      ec=0
        do l=1,lbdmax
        prof(l,1)=prof(l,1)+ec2+ec
        ec=ec-eps
        enddo
        if(iprint.eq.1)write(3,*)' egal ',(prof(l,1),l=1,lbdmax)
      endif
c                       symetrisation  I
      if(ksym.ge.1)then
          lc=lbdmax/2+1
          if(kcentre.eq.1)lc=lmin
          lm2=lbddat/2
        do lp=1,lm2
        piva=prof(lc+lp,1)
        pivb=prof(lc-lp,1)
           piv=0.5*(piva+pivb)
        prof(lc+lp,1)=piv
        prof(lc-lp,1)=piv
        enddo
      endif
c                       symetrisation   Q,U
      if(ksym.ge.2)then
          lc=lbdmax/2+1
          if(kcentre.eq.1)lc=lmin
          lm2=lbddat/2
        do lp=1,lm2
          do k=2,3
          piva=prof(lc+lp,k)
          pivb=prof(lc-lp,k)
             piv=0.5*(piva+pivb)
          prof(lc+lp,k)=piv
          prof(lc-lp,k)=piv
          enddo
        enddo
      endif
c                       antisymetrie     V
      if(ksym.ge.3)then
          lc=lbdmax/2+1
          if(kcentre.eq.1)lc=lmin
          lm2=lbddat/2
        prof(lc,4)=0.
        do lp=1,lm2
          piva=prof(lc+lp,4)
          pivb=prof(lc-lp,4)
             piv=0.5*(piva-pivb)
          prof(lc+lp,4)=piv
          prof(lc-lp,4)=-piv
        enddo
      endif
c------------------------------------------
c                       ecriture sur stokname
        do l=1,lbddat
          ll=l+lbddeb-1
          do k=1,4
          buf(k)=prof(ll,k)
          enddo
          write(2,*)buf
c                   ---
        enddo
c-------------------------------------------------------------
c                       ecriture unnodat.lis + dessin st.ps
               ipm4=ipm/nplotst
               jpm4=jpm/nplotst
          itest1=ip-ipm4*(ip/ipm4)
          itest2=jp-jpm4*(jp/jpm4)

      kdes=0
      if(ipdes*jpdes.eq.0)then
        if(itest1.eq.0.and.itest2.eq.0)kdes=1
      else
        if(ip.ge.ipdes.and.ip.le.ipdes+3.and.jp.eq.jpdes)kdes=1
      endif

      if(kdes.eq.1)then
c          dessin profils I,Q,U,V
        do k=1,4

      call pgadvance
      call pgvport(0.1,0.9,0.2,0.8)
      write(tit(25:30),'(i5)')ip
      write(tit(31:35),'(i5)')jp
      call pglabel(' ',' ',tit)

      xmin=1.
      xmax=lbddat
        if(k.le.2)then
        ymin=-qumax
        ymax=qumax
        endif
        if(k.eq.4)then
        ymin=-vvmax
        ymax=vvmax
        endif
      if(k.eq.1)then
      ymin=0.
      ymax=-10000
        do lp=1,lbddat
          lprof=lp+lbddeb-1
        piv=prof(lprof,k)
        yplot(lp)=piv
        ymax=amax1(ymax,piv)
        enddo
      endif
      if(k.ne.1)then
        do lp=1,lbddat
          lprof=lp+lbddeb-1
        yplot(lp)=prof(lprof,k)/prof(lprof,1)
        enddo
      endif
      call pgwindow(xmin,xmax,ymin,ymax)
      call pgsls(1)
      call pgsch(2.)
      call pgbox('abcints',10.,10,'abcints',0.,1)

      call pgline(lbddat,xplot,yplot)
      enddo

        do lp=1,lbddat,5
         write(5,*)'ipm,jpm,ip,jp,lp, I,Q,U,V  ',
     1         ipm,jpm,ip,jp,lp,(prof(lp,k),k=1,4)
        enddo
         write(5,*)'  '
        endif
c---
            lbdfin=lbddat+lbddeb-1
        if(ip*jp.eq.1)then
        write(5,*)
     1   ' Profils  I,Q,U,V en ip=1, jp=1'
          do k=1,4
          write(5,3)(prof(l,k),l=lbddeb,lbdfin)
          write(5,*)' '
          enddo
        endif

        if(iprint.eq.1)then
        write(5,*)
     1   ' Profils  I,Q,U,V en ipm/2,jpm/2'
          do k=1,4
          write(5,3)(prof(l,k),l=lbddeb,lbdfin)
          write(5,*)' '
          enddo
        endif

      enddo    !  fin boucle ip
c---------------------------------------
c      print*,' fin jp=',jp
      enddo    !  fin boucle jp

      do l=1,lbdmax
      do iq=1,2
        liqu=20+l+2*(iq-1)
        close(unit=liqu)
      enddo
      enddo

c 110  format('carte des ecarts de centrage (pixels)')
c 111  format(10i8)
c      write(5,110)
c        do i=1,imax,ijpas
c        write(5,111)(lecart(i,j),j=1,jmax,ijpas)
c        enddo

      close(2)
      close(5)
      call pgend
      return
      end
c------------------------------------------------------------
      subroutine coupe_y(tb,iitab,jjtab,lltab,im,jm,nm,jt100)
c                -------
      integer*2 tb(iitab,jjtab,lltab)
c                              90
      dimension xdes(200),ydes(200)
        k=10          
        ic=(im+1)/2

      call pgbegin(0,'coupe_y.ps/ps',1,1)

        call pgsch(1.5)
        call pgslw(1)

      call pgvport(0.1,0.9,0.1,0.8)
c      call pglabel('  ',' ',' ')     
          jt=float(jt100)/100.+0.5 
        xa=1.
        xb=jm+(nm-1)*jt
        ya=0.
           jp=jm-k
        yb=1.2*tb(ic,jp,1)
        
      call pgwindow(xa,xb,ya,yb)
           xjm=jm
      call pgslw(2)
          jkm=jm-2*k
        ls=2
      do n=1,nm
        ls=3-ls
        call pgsls(ls)
       do j=1,jkm
           jp=j+k
         ydes(j)=tb(ic,jp,n)
         xdes(j)=jm-jp+1+(n-1)*jt
       enddo
       call pgline(jkm,xdes,ydes)
      enddo

        piv=(xb-xa)/float(jt)
      xxa=-piv/2.
      xxb=-xxa
      call pgwindow(xxa,xxb,ya,yb)
      call pgsls(1)
      call pgbox('bcnts',1.,5,'bcnts',2000.,2)

      call pgend
      return
      end
c======================================================
      subroutine par1(key,nline,val)
c                ----
c                       in  in  out
      integer lec,val,kpiv
      character*8 etiq,key  !,kname
1     format(a8,i8)!      (10a8)
2     format(a)
c2     format(10i8)
c                          ntitpar=4
      nfile=96
      rewind(nfile)
c      print *,'par1: ',key,nline      
c  dir, repert
c      do i=1,ntitpar  !8
c       print *,'i  ',i
c      read(96,2)kname
c      print *,'kname  ',kname
c      enddo
c boucle
      do i=1,1000
         read(nfile,1)etiq,lec   !  attention espaces !
c      write(3,*)'etiq,lec   ',etiq,lec
        if(etiq.eq.'end     ')then
          val=0
          goto10
        endif     
c        if(etiq.eq.'     dir')read(nfile,2)kname
c            write(3,*)'kname  ',kname      
c        if(etiq.eq.'  filter')read(nfile,2)kname
c            write(3,*)'kname  ',kname      
        if(etiq.eq.key)then
            if(nline.gt.1)then
              np=nline-1
              do n=1,np
                read(nfile,1)kpiv,lec
              enddo  
            endif             
          val=lec
          write(3,*) key,nline,val
c          print *, key,nline,val          
          return
        endif
      enddo

10    continue
c        print *,key,' not found'
      write(3,*) key,' not found -> = 0'
      val=0
        return
      end
c========================================================c fnomw2_long.f
c =============
c -----------------------------------------------------------------------
c cette routine retourne les noms complets des fichiers trouves dans
c un intervalle horaire, entre les fenetres nw1 et nw2 (comprises).
c fname contient les noms pour la fenetre nw2
c ftime contient les temps en secondes

c -----------------------------------------------------------------------
      Subroutine fnomw2(lettre,fprint,nw1,nw2,date,ideb,ifin,
c                ------   in     in in  in     in   in   in 
     1                  nfm,fname,ftime,istop,repert)
c                       out,out,  out,  out   in
      implicit none      
c INPUT:
c Fprint = STRING contenant le modele de filtre de fichiers
c NW1, NW2 = INTEGER*4
c      indices de debut et de fin de balayage de la table des cameras
c DATE = INTEGER*4 date recherchee
c ideb = HHMMSSCS (heure de debut; exemple 10340710 pour 10h34mn07s10cs) (I*4)
c ifin = HHMMSSCS (heure de fin;   exemple 11045620 pour 11h04mn56s20cs) (I*4)
c ISTOP = INTEGER*4 non utilise
c REPERT= STRING repertoire de lecture

c OUTPUT:
c nfm = nombre de fichiers trouves (INTEGER*4)
c fname = tableau contenant le nom des nf fichiers trouves (Tableau de STRING0
c ftime = tableau contenant les heures des nf fichiers trouves (Tableau I*4)
c  
      character*1 lettre
      character*41 fprint(8)
      integer*4 nw1,nw2
      integer*4 date
      integer*4 ideb,ifin
      character*41 fname(5000,2)
      integer*4 ftime(10000)
      integer*4 nfm
      integer*4 istop
      character*80 repert

      character*8 hdeb,hfin,h_der_fic
      character*6 d
      integer*4 num,nw, nf ! k    
      integer*4 ier ! i,j,ier,jjj
      integer*4 ideb2,ifin2,ifinfic,icycle,ifinmax
      logical*4 present
      character*150 file
      character*150 line
      character*3 cycle, sequence
      integer*4 Irep
      character*41 fic1

      Ifinmax=0                      !Heure maximale
      nf=0
c
c Effacer les fichiers *.lis : ml.lis et tableau.lis
c
      inquire(file='ml.lis',exist=present)
      if(present) call system('rm ml.lis')
      inquire(file='tableau.lis',exist=present)
      if(present) call system('rm tableau.lis')     

c
10    format(a9)
11    format(1x,a6)
99    format(I8.8)
98    format(i6.6)
c      encode(6,98,d)date
      write(d,'(i6)')date
c      encode(8,99,hdeb)ideb
      write(hdeb,'(i8)')ideb
        print *,' ideb,hdeb ',ideb,hdeb
             CALL HSTR_HCS( Hdeb, Ideb2) !Heure en cent de secondes
c      encode(8,99,hfin)ifin
      write(hfin,'(i8)')ifin
             CALL HSTR_HCS( Hfin, Ifin2) !Heure en cent de secondes
       if(ifin2.eq.0) ifin2=ideb2

      write(3,*)' ideb,hdeb,ideb2 ',ideb,hdeb,ideb2
      write(3,*)' ifin,hfin,ifin2 ',ifin,hfin,ifin2
      print *,' ideb,hdeb,ideb2 ',ideb,hdeb,ideb2
      print *,' ifin,hfin,ifin2 ',ifin,hfin,ifin2

      irep=1
       DO WHILE (repert(irep:irep).NE.' ')
                irep=irep+1
        END DO 
         print *,'repert',repert(1:irep)
         irep=irep-1
c Boucle PRINCIPALE sur les fenetres

         do nw=nw1,nw2
c           fic1=fprint(nw)
c           fic1(2:7)=d
c           fic1(25:25)=lettre

c           CALL FIND_SEQ_CYCL(Repert, Fic1, hdeb, Sequence, Cycle)
c 
c 2.Faire un ls sur la sequence et le cycle et sauver les noms de fichiers
c   dans un tableau
c
c     BOUCLE SUR LES CYCLES
15           fic1=fprint(nw)
      print *,'fprint(1)',fprint(1)

c             fic1(9:11)=sequence
c             fic1(13:15)=cycle
            if (ideb.eq.0) fic1(25:25)=lettre
             print *,'Cycle :',cycle
             print *,'Sequence :',sequence
             print *,'Nw :',nw
c             File=repert(1:irep)//fic1
		File='"'//fic1//'"' 
             print *,'FILE: ',file

             call system('ls b*'//lettre//'*fts >>tableau.lis')

c        call system
c     1  ('find . -name '//file//'|xargs ls |cut -b 3-80 >>tableau.lis')

c          call system('more tableau.lis')

c       call system
c     1 ('find '//repert(1:irep)//' -name '//file//' >> tableau.lis ')        
      write(3,*)' file '
      write(3,*)file

            Num=0
            open(unit=11,status='old',file='tableau.lis')

c               Traitement d'un cycle
 4          read(11,'(a)',iostat=ier,end=200) line

c      write(3,*)' irep,line ',irep,line

            if(line(1:5).eq.'     ')stop 'erreur pas autre cycle'
            if(ier.ne.0) goto 3
c            h_der_fic=line(irep+30:irep+37)
            h_der_fic=line(30:37)

c      write(3,*)' H_der_fic ',H_der_fic
             CALL HSTR_HCS( H_der_fic, Ifinfic)

c      write(3,*)' h_der_fic,ideb2,ifin2,ifinfic ',
c     1            h_der_fic,ideb2,ifin2,ifinfic

             IF (ifinfic.ge.ideb2) then !SELECTION DES HEURES
                IF (ifinfic.le.ifin2)then
                   nf=nf+1
                   if(nf.gt.10000)
     X                    STOP 'Too much files. Max allowed: 10000'
c                   fname(nf,nw)=line(irep+1:irep+42)
                    fname(nf,nw)=line(1:42)

1     format('fnomw: fname= ',a41)
c      write(3,1)fname(1,nw)
                   ftime(nf)=ifinfic
                endif
             endif
                  IF (Ifinfic.Gt.Ifinmax) Ifinmax=Ifinfic
             go to 4 
c                Fin d'un cycle
 200   CONTINUE               !En fin de liste
        call system('rm tableau.lis')
c
c Si l heure du dernier fichier est inferieure a l heure de fin incrementer
c le numero de cycle, sauf si aucun fichier n a ete trouve dans le cycle
c
               IF (Num.EQ.0) GOTO 3  !Pas de fichier dans le cycle

              if(ifinmax.lt.ifin2)then
c                 decode(3,100,cycle)icycle
                 read(cycle,'(i3)')icycle
                 icycle=icycle+1
c                 encode(3,100,cycle)icycle
                 write(cycle,'(i3)')icycle
100   format(i3.3)
                  num=num+1
                  go to 15
c                                FIN DE BOUCLE SUR LES CYCLES 
               endif  
3          close(unit=11)  

      enddo                     !FIN DE BOUCLE SUR LES FENETRES
      nfm=nf                    !nombre de fichiers

      do nw=nw1,nw2
      write(3,*)' fnomw2: nfm ',nfm
      write(3,*)' fnomw2: nw,fname(nw,1) ',nw,fname(1,nw)   
      write(3,*)' fnomw2: nw,fname(nw,nfm) ',nw,fname(nfm,nw) 
      enddo

      call ordref(nfm,fname,ftime,nw1,nw2)
c          ------

        return
        end

c--------------------------------------------
      subroutine ordref(nfm,fname,ftime,nw1,nw2)
c                ------
c   remet en ordre fname et ftime
      integer*4    ftime(10000),gtime
      character*41 fname(5000,2),gname

      do nw=nw1,nw2

      do5 nessai=1,10000
        npermu=0
      do nf=1,nfm-1
        k1=ftime(nf)
        k2=ftime(nf+1)
      if(k2.lt.k1)then
          npermu=npermu+1
        gtime=ftime(nf)
        gname=fname(nf,nw)
        ftime(nf)=ftime(nf+1)
        fname(nf,nw)=fname(nf+1,nw)
        ftime(nf+1)=gtime
        fname(nf+1,nw)=gname
      endif
      enddo

      if(npermu.eq.0)goto10 
5     continue
 
10    continue

c      print *,' nessai ',nessai 
      write(3,*)' ms1/fnomw2/ordref'
30    format(' nw, nf, fname: ',2i6,2x,41a)
      do nf=1,nfm
      print   30,nw,nf,fname(nf,nw)
      write(3,30)nw,nf,fname(nf,nw)
      enddo

      enddo  !  fin boucle nw
      return
      end
c-------------------------------------------
      Subroutine HSTR_HCS (Hstr, Hcs)  

C     Routine de transformation de l'heure (chaine de caracteres) en 
C     heure en centieme de secondes
C
C ENTREE:
C     HSTR     Heure sous forme de chaine ex: '10325486' pour
C                     10h 32mn 54s 86 centieme
C RETOUR
C     HCS    Integer*4 heure en centieme de secomde

      CHARACTER *(*) Hstr
      INTEGER*4 HCS              !Heure en centieme
      INTEGER*4 IH,IM,IS,ICS

c      print *,Hstr
      read(Hstr(1:2),'(i2)',iostat=ier) ih
             if(ier.ne.0) stop 'Error reading start time 1'

      read(Hstr(3:4),'(i2)',iostat=ier) im
             if(ier.ne.0) stop 'Error reading start time 3'

      read(Hstr(5:6),'(i2)',iostat=ier) is
            if(ier.ne.0) stop 'Error reading start time 5'

      read(Hstr(7:8),'(i2)',iostat=ier) ics
            if(ier.ne.0) stop 'Error reading start time 7'

      HCS=ih*360000+im*6000+is*100+ics

      RETURN
      END

c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Subroutine HCS_HSTR (Hcs, Hstr)  !  non utilise ?

      CHARACTER *(*) Hstr
      INTEGER*4 HCS              !Heure en centieme
      INTEGER*4 IH,IM,IS,ICS
      INTEGER*4 Hint
      CHARACTER*2 Ch,Cm,Cs,Cc

      ICS = MOD (Hcs, 100)
            Hint = Hcs /100
      IS = MOD (Hint, 60)
            Hint = Hint /60
      IM = MOD (Hint, 60)
      IH =  Hint / 60

c      ENCODE(2,99,ch)Ih
c      ENCODE(2,99,cm)Im
c      ENCODE(2,99,cs)Is
c      ENCODE(2,99,cc)Ics
      write(ch,'(i2)')Ih
      write(cm,'(i2)')Im
      write(cs,'(i2)')Is
      write(cc,'(i2)')Ics

      Hstr= Ch//Cm//Cs//Cc

 99   FORMAT(I2.2)

      RETURN

      END
c---------------------------------------
      subroutine readpar
      character*8 nom
      open(unit=96,file='msdp.par',status='old')
      rewind(96)

      do n=1,1000         ! 1000
 1       format(a8,i8)
         read(96,1)nom,nombre
      write(3,*)n,nom,nombre
      print *,n,nom,nombre
        if(nom.eq.'end     ')goto 2
      enddo
   
 2    close(unit=96)
      return
      end


